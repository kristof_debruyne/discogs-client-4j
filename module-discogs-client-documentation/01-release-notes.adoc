:imagesdir: images
:sectanchors:

== Emperial Database Release Notes +

Author: Kristof Debruyne +

== Discogs
Discogs website: https://www.discogs.com +

== Discogs API (provider)
Website: https://www.discogs.com/developers +
Discogs API version: v2.0 +

== Discogs client (consumer)

Discogs client version: v1.2 +

== 1 Project structure

Discogs client uses Maven as build tool. Therefore, several modules can be found in the project. +

*1.1 Module Parent (root)*

Module name: discogs-client-parent +
Description: module is the parent (root) project descriptor. +

*1.2 Module BOM*

Module name: discogs-client-bom +
Description: module is used for managing dependencies and versions. +
Website: https://en.wikipedia.org/wiki/Bill_of_materials

[source,xml]
----
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>com.discogs</groupId>
            <artifactId>discogs-client-bom</artifactId>
            <version>${project.version}</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement>
----
(Snippet from root pom.xml)

*1.3 Module Core API*

Module name: discogs-client-core-api +
Description: module contains the Discogs core API (models, enums, etc...). +

*1.4 Module Core Implementation*

Module name: discogs-client-core-impl +
Description: module is default implementation of module 'discogs-client-core-api'. +

*1.5 Module Documentation*

Module name: discogs-client-documentation +
Description: module contains documentation, release notes and cookbook (so what you are reading now)

*1.6 Module OAuth API*

Module name: discogs-client-oauth +
Description: module contains OAuth API so you can authenticate on Discogs +
OAuth version: 1.0a +

*1.7 Module Spring Boot*

Module name: discogs-client-springboot +
Description: module contains support for Spring Boot and uses the default implementation. +

== 2 Dependency overview

[#img-dependencies]
.A module dependencies diagram
image::dependencies.png[]

== 3 How to use ?

You can use the following approaches: +
- use the client with Java vanilla +
- use the client with Spring framework +
- use the client with Spring Boot framework +

You can find more in the cookbook on how to use it properly.