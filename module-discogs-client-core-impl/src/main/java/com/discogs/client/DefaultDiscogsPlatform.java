package com.discogs.client;

import com.discogs.client.api.DiscogsClient;
import com.discogs.client.api.DiscogsDatabaseClient;
import com.discogs.client.api.DiscogsImageClient;
import com.discogs.client.api.DiscogsInventoryClient;
import com.discogs.client.api.DiscogsJsonpClient;
import com.discogs.client.api.DiscogsMarketplaceClient;
import com.discogs.client.api.DiscogsPlatform;
import com.discogs.client.api.DiscogsRootClient;
import com.discogs.client.api.DiscogsService;
import com.discogs.client.api.DiscogsUserClient;
import com.discogs.client.api.DiscogsWebClient;
import com.discogs.client.api.config.DiscogsContext;
import com.discogs.client.api.config.DiscogsLocator;
import com.discogs.client.api.exception.DiscogsRuntimeException;
import com.discogs.client.auth.oauth.config.OAuthContext;
import com.discogs.client.security.DiscogsOAuthV1Service;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import static com.discogs.client.exception.CustomErrorCodes.CONFIG_ERROR;

/**
 * Implementation of class {@link DiscogsPlatform}.
 *
 * @author Sikke303
 * @since 1.0
 */
@ParametersAreNonnullByDefault
@RequiredArgsConstructor
@Log4j2
public class DefaultDiscogsPlatform implements DiscogsPlatform {

    @Getter
    @NonNull
    private final DiscogsContext<OAuthContext> discogsContext;

    @NonNull
    private final DiscogsLocator discogsLocator;

    //*** CLIENTS ***//

    @Nonnull
    @Override
    public DiscogsDatabaseClient getDatabaseClient() { return getClient(DiscogsDatabaseClient.class); }

    @Nonnull
    @Override
    public DiscogsImageClient getImageClient() { return getClient(DiscogsImageClient.class); }

    @Nonnull
    @Override
    public DiscogsInventoryClient getInventoryClient() { return getClient(DiscogsInventoryClient.class); }

    @Nonnull
    @Override
    public DiscogsJsonpClient getJsonpClient() { return getClient(DiscogsJsonpClient.class); }

    @Nonnull
    @Override
    public DiscogsMarketplaceClient getMarketplaceClient() { return getClient(DiscogsMarketplaceClient.class); }

    @Nonnull
    @Override
    public DiscogsRootClient getRootClient() { return getClient(DiscogsRootClient.class); }

    @Nonnull
    @Override
    public DiscogsUserClient getUserClient() { return getClient(DiscogsUserClient.class); }

    @Nonnull
    @Override
    public DiscogsWebClient getWebClient() { return getClient(DiscogsWebClient.class); }

    @Nonnull
    @Override
    public <T extends DiscogsClient> T getClient(@NonNull final Class<T> clientType) {
        return discogsLocator.lookupClient(clientType)
                .orElseThrow(() -> new DiscogsRuntimeException("No Discogs client configured of type: " + clientType.getName(), CONFIG_ERROR));
    }

    //*** SERVICES ***//

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public DiscogsOAuthV1Service getAuthenticationService() { return getService(DiscogsOAuthV1Service.class); }

    @Nonnull
    @Override
    public <T extends DiscogsService> T getService(@NonNull final Class<T> serviceType) {
        return discogsLocator.lookupService(serviceType)
                .orElseThrow(() -> new DiscogsRuntimeException("No Discogs service configured of type: " + serviceType.getName(), CONFIG_ERROR));
    }
}
