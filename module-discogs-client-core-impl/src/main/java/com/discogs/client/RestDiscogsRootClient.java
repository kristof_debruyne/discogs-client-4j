package com.discogs.client;

import com.discogs.client.api.DiscogsRootClient;
import com.discogs.client.api.annotations.DiscogsTested;
import com.discogs.client.api.config.UserAgentGenerator;
import com.discogs.client.api.exception.DiscogsRuntimeException;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.root.ApiInformationRequest;
import com.discogs.client.api.response.GenericResponse;
import com.discogs.client.api.response.Response;
import com.discogs.client.auth.oauth.service.OAuthService;
import com.discogs.client.core.root.JsonApiInformation;
import com.discogs.client.exception.CustomErrorCodes;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URI;

import static java.util.Objects.nonNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Implementation of class {@link DiscogsRootClient}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractRestDiscogsClient
 */
@ParametersAreNonnullByDefault
@Log4j2
public class RestDiscogsRootClient extends AbstractRestDiscogsClient implements DiscogsRootClient {

    public RestDiscogsRootClient(URI baseUri, OAuthService oauthService, RestTemplate restTemplate, UserAgentGenerator userAgentGenerator) {
        super(baseUri, oauthService, restTemplate, userAgentGenerator);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonApiInformation> getApiInformation() {
        ApiInformationRequest request = ApiInformationRequest.builder()
                .contentType(APPLICATION_JSON_VALUE)
                .mediaType(AcceptMediaType.DEFAULT)
                .build();
        return getApiInformation(request);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonApiInformation> getApiInformation(@NonNull final ApiInformationRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonApiInformation.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<String> getApiVersion() {
        Response<JsonApiInformation> response = getApiInformation();
        if(nonNull(response.getBody())) {
            return new GenericResponse<>(response.getStatusCode(), response.getHeaders(), response.getBody().getVersion());
        }
        throw new DiscogsRuntimeException("Failed to retrieve Discogs API version !", CustomErrorCodes.SERVICE_UNAVAILABLE);
    }

    @Nonnull
    @Override
    public String createArtistApiUrl(@Nonnegative long artistId) {
        return createApiUrl(artistId, "/artists/");
    }

    @Nonnull
    @Override
    public String createLabelApiUrl(@Nonnegative long labelId) {
        return createApiUrl(labelId, "/labels/");
    }

    @Nonnull
    @Override
    public String createMasterApiUrl(@Nonnegative long masterId) {
        return createApiUrl(masterId, "/masters/");
    }

    @Nonnull
    @Override
    public String createReleaseApiUrl(@Nonnegative long releaseId) {
        return createApiUrl(releaseId, "/releases/");
    }

    @Nonnull
    private String createApiUrl(long id, String path) {
        return getBaseUri().toASCIIString().concat(path).concat(String.valueOf(id));
    }
}
