package com.discogs.client.throttle;

import com.discogs.client.api.response.RateLimits;
import com.discogs.client.api.throttle.ThrottleHandler;
import com.discogs.client.api.throttle.ThrottleMode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.DisposableBean;

import javax.annotation.Nonnegative;
import javax.annotation.ParametersAreNonnullByDefault;
import java.math.BigDecimal;
import java.util.concurrent.ScheduledExecutorService;

import static java.lang.System.currentTimeMillis;
import static java.math.RoundingMode.CEILING;
import static java.math.RoundingMode.FLOOR;
import static java.math.RoundingMode.HALF_EVEN;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Default Discogs throttler.
 *
 * @author Sikke303
 * @since 1.0
 * @see ThrottleHandler
 */
@ParametersAreNonnullByDefault
@RequiredArgsConstructor
@Log4j2
public class DefaultThrottleHandler implements ThrottleHandler<ExtendedThrottleContext>, DisposableBean {

    private static final BigDecimal THOUSAND = new BigDecimal("1000");

    @NonNull
    private final ScheduledExecutorService executorService;

    @NonNull
    private final ThrottleMode throttleMode;

    @Getter
    private final long coolDownTimePeriod;

    @Override
    public void destroy() {
        executorService.shutdown();
    }

    @Override
    public void calculateAndThrottle(@NonNull final ExtendedThrottleContext context) {
        Validate.isTrue(context.getRequestTimeWindowInMillis() > 0, "Request time window must be positive.");
        Validate.isTrue(context.getAuthenticatedRequestsPerTimeWindow() > 0, "Authenticated requests per time window must be positive.");
        Validate.isTrue(context.getUnauthenticatedRequestsPerTimeWindow() > 0, "Unauthenticated requests per time window must be positive.");

        if(context.hasRateLimits()) {
            throttle(calculateThrottleTimeFromRateLimits(context));
        } else {
            throttle(calculateThrottleTimeFromDefaults(context));
        }
    }

    @Override
    public void throttle(@Nonnegative final long throttleTimeInMillis) {
        if (throttleTimeInMillis > SKIP_THROTTLING) {
            try {
                Runnable runnable = () -> {
                    log.trace("Applying throttling for current process. Suspending execution for {} ms ...", throttleTimeInMillis);
                    long currentTimeInMillis = currentTimeMillis();
                    long expiredAtInMillis = (currentTimeInMillis + throttleTimeInMillis);
                    while (currentTimeInMillis < expiredAtInMillis) {
                        currentTimeInMillis = currentTimeMillis();
                    }
                };
                executorService.schedule(runnable, 0, MILLISECONDS).get(); //WAIT
                log.trace("Throttling execution for current process has ended. Resuming execution ...");
            } catch (Exception ex) {
                log.error(ExceptionUtils.getStackTrace(ex), ex);
            }
        } else {
            log.trace("Skipping throttling for current request. Proceeding ...");
        }
    }

    @Nonnegative
    long calculateThrottleTimeFromDefaults(@NonNull final ExtendedThrottleContext context) {
        if(context.isAuthorizedRequest()) {
            return calculate(context.getRequestTimeWindowInMillis(), context.getAuthenticatedRequestsPerTimeWindow());
        }
        return calculate(context.getRequestTimeWindowInMillis(), context.getUnauthenticatedRequestsPerTimeWindow());
    }

    @Nonnegative
    long calculateThrottleTimeFromRateLimits(@NonNull final ExtendedThrottleContext context) {
        Validate.notNull(context.getRateLimits(), "Rate limits are required.");

        RateLimits rateLimits = context.getRateLimits();
        if(rateLimits.getRateLimitRemaining() == SKIP_THROTTLING) {
            return getCoolDownTimePeriod();
        } else if(rateLimits.getRateLimitUsed() > SKIP_THROTTLING) {
            return calculate(context.getRequestTimeWindowInMillis(), rateLimits.getRateLimit());
        }
        return SKIP_THROTTLING;
    }

    @Nonnegative
    private long calculate(final long requestWindowTimeInMillis, final long rateLimit) {
        BigDecimal throttleTime = new BigDecimal(requestWindowTimeInMillis).divide(BigDecimal.valueOf(rateLimit), HALF_EVEN);
        switch (throttleMode) {
            case NORMAL_CEILING: return throttleTime.divide(THOUSAND, CEILING).multiply(THOUSAND).longValue();
            case NORMAL_FLOOR: return throttleTime.divide(THOUSAND, FLOOR).multiply(THOUSAND).longValue();
            case TRAMPOLINE:
                throw new UnsupportedOperationException("Trampoline throttling currently not supported !");
        }
        return throttleTime.longValue();
    }
}
