package com.discogs.client.throttle;

import com.discogs.client.api.response.RateLimits;
import com.discogs.client.api.throttle.ThrottleContext;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

import java.util.UUID;

import static java.util.Objects.nonNull;

/**
 * Extended Discogs throttle context.
 *
 * 2 purposes:
 * - default value based (when no rate limits are available)
 * - rate limit based
 *
 * @author Sikke303
 * @since 1.0
 * @see RateLimits
 */
@Getter
@Builder
@ToString
public class ExtendedThrottleContext implements ThrottleContext {

    @NonNull
    private final UUID uuid;
    private final boolean authorizedRequest;
    private final RateLimits rateLimits;

    @Override
    public boolean hasRateLimits() {
        return nonNull(rateLimits);
    }
}
