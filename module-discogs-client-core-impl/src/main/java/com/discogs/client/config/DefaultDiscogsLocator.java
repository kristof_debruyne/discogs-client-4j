package com.discogs.client.config;

import com.discogs.client.api.DiscogsClient;
import com.discogs.client.api.DiscogsService;
import com.discogs.client.api.config.DiscogsLocator;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.context.ApplicationContext;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Optional;

/**
 * Default Discogs client/service locator.
 *
 * @author Sikke303
 * @since 1.0
 * @see DiscogsLocator
 */
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
@Log4j2
public class DefaultDiscogsLocator implements DiscogsLocator {

    @NonNull
    private final ApplicationContext applicationContext;

    @Nonnull
    @Override
    public <T extends DiscogsClient> Optional<T> lookupClient(@NonNull final Class<T> clazz) {
        log.debug("Locating Discogs client: {}", clazz.getName());
        return lookup(clazz);
    }

    @Nonnull
    @Override
    public <T extends DiscogsService> Optional<T> lookupService(@NonNull final Class<T> clazz) {
        log.debug("Locating Discogs service: {}", clazz.getName());
        return lookup(clazz);
    }

    @Nonnull
    private <T> Optional<T> lookup(final Class<T> clazz) {
        try {
            return Optional.of(applicationContext.getBean(clazz));
        } catch (Exception ex) {
            log.error(ExceptionUtils.getStackTrace(ex), ex);
        }
        return Optional.empty();
    }
}
