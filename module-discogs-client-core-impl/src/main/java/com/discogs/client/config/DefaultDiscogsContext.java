package com.discogs.client.config;

import com.discogs.client.api.config.DiscogsContext;
import com.discogs.client.auth.oauth.config.OAuthContext;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;

/**
 * Default Discogs context. <br><br>
 *
 * @author Sikke303
 * @since 1.0
 * @see DiscogsContext
 */
@RequiredArgsConstructor
public class DefaultDiscogsContext implements DiscogsContext<OAuthContext> {

    @NonNull
    @Getter
    private final OAuthContext authenticationContext;

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public String getApplicationName() {
        return authenticationContext.getClientConfiguration().getApplicationName();
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public String getApplicationKey() {
        return authenticationContext.getClientConfiguration().getApplicationKey();
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public String getApplicationVersion() {
        return authenticationContext.getClientConfiguration().getApplicationVersion();
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public String getApiRootUrl() {
        return authenticationContext.getServerConfiguration().getApiRootUrl();
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public String getApiVersion() {
        return authenticationContext.getServerConfiguration().getApiVersion();
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public String getImageRootUrl() {
        return authenticationContext.getServerConfiguration().getImageRootUrl();
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public String getWebRootUrl() {
        return authenticationContext.getServerConfiguration().getWebRootUrl();
    }
}