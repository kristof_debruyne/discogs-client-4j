package com.discogs.client.config;


import com.discogs.client.api.config.DiscogsContext;
import com.discogs.client.api.config.UserAgentGenerator;
import com.discogs.client.auth.oauth.config.OAuthContext;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.Validate;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Default user agent generator.
 *
 * @author Sikke303
 * @since 1.0
 * @see UserAgentGenerator
 */
@ParametersAreNonnullByDefault
@RequiredArgsConstructor
public class DefaultUserAgentGenerator implements UserAgentGenerator {

    private static final String USER_AGENT_MASK = "Discogs/%s Client=%s/%s (ID=%s)";

    @NonNull
    private final DiscogsContext<OAuthContext> discogsContext;

    @Nonnull
    @Override
    public String generate() {
        Validate.notNull(discogsContext.getApiVersion(), "API Version is required.");
        Validate.notNull(discogsContext.getApplicationName(), "Application name is required.");
        Validate.notNull(discogsContext.getApplicationVersion(), "Application version is required.");
        Validate.notNull(discogsContext.getApplicationKey(), "Application key is required.");

        return String.format(USER_AGENT_MASK, discogsContext.getApiVersion(), discogsContext.getApplicationName(), discogsContext.getApplicationVersion(), discogsContext.getApplicationKey());
    }
}