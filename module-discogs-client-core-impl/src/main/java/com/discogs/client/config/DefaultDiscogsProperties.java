package com.discogs.client.config;

import com.discogs.client.api.config.DiscogsProperties;
import com.discogs.client.api.throttle.ThrottleMode;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Default Discogs client properties.
 *
 * @author Sikke303
 * @since 1.0
 */
@Getter
@Builder
@ToString(exclude = { "consumerSecret", "personalToken" })
public class DefaultDiscogsProperties implements DiscogsProperties {

    //*** CLIENT PROPERTIES ***//
    @NonNull
    private final String applicationName;

    @NonNull
    private final String applicationKey;

    @NonNull
    private final String applicationVersion;

    private final String callbackUrl;

    private final String consumerKey;

    private final String consumerSecret;

    private final String personalToken;

    //*** SERVER PROPERTIES ***//
    @NonNull
    private final String apiVersion;

    @NonNull
    private final String apiRootUrl;

    @NonNull
    private final String imageRootUrl;

    @NonNull
    private final String webRootUrl;

    @NonNull
    private final String accessTokenEndpoint;

    @NonNull
    private final String authorizeEndpoint;

    @NonNull
    private final String requestTokenEndpoint;

    @NonNull
    private final String revokeTokenEndpoint;

    @Nonnegative
    private final int connectionTimeout;

    @Nonnegative
    private final int readTimeout;

    private final boolean throttlingRequestsEnabled;

    @Nonnegative
    private final long throttleCoolDownTimePeriod;

    private final ThrottleMode throttleMode;

    /**
     * Checks if callback url is configured or not.
     *
     * @return boolean callback url is configured (true) or not (false)
     */
    @Nonnull
    public boolean hasCallbackUrl() {
        return isNotBlank(getCallbackUrl());
    }
}