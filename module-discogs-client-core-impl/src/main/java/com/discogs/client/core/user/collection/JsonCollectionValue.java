package com.discogs.client.core.user.collection;

import com.discogs.client.api.core.user.collection.CollectionValue;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonCollectionValue extends AbstractJson implements CollectionValue {

    @JsonProperty(value = "minimum", required = true) String minimumValue;
    @JsonProperty(value = "median", required = true) String medianValue;
    @JsonProperty(value = "maximum", required = true) String maximumValue;
}