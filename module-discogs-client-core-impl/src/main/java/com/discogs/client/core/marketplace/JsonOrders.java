package com.discogs.client.core.marketplace;

import com.discogs.client.api.core.marketplace.Orders;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.paging.JsonPagination;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonOrders extends AbstractJson implements Orders {

    @JsonProperty(value = "orders", required = true) private JsonOrder[] orders;
    @JsonProperty(value = "pagination", required = true) private JsonPagination pagination;
}