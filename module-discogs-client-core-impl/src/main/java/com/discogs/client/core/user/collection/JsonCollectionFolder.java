package com.discogs.client.core.user.collection;

import com.discogs.client.api.core.user.collection.CollectionFolder;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonCollectionFolder extends AbstractJson implements CollectionFolder {

    @JsonProperty(value = "id", required = true) private long id;
    @JsonProperty(value = "count", required = true) private long count;
    @JsonProperty(value = "name", required = true) String name;
    @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
}
