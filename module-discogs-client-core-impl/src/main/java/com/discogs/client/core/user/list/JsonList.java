package com.discogs.client.core.user.list;

import com.discogs.client.api.core.user.list.List;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.stats.JsonStatistics;
import com.discogs.client.core.user.JsonUser;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonList extends AbstractJson implements List {

    @JsonProperty(value = "id", required = true) private long id;
    @JsonProperty(value = "public", required = true) private boolean publicList;
    @JsonProperty(value = "date_added") private String dateAdded;
    @JsonProperty(value = "date_changed") private String dateChanged;
    @JsonProperty(value = "description") private String description;
    @JsonProperty(value = "image_url") private String imageUrl;
    @JsonProperty(value = "items", required = true) private JsonListItem[] items;
    @JsonProperty(value = "name", required = true) private String name;
    @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
    @JsonProperty(value = "uri", required = true) private String uri;
    @JsonProperty(value = "user", required = true) private JsonUser user;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonListItem extends AbstractJson implements ListItem {

        @JsonProperty(value = "id", required = true) private long id;
        @JsonProperty(value = "comment") private String comment;
        @JsonProperty(value = "display_title", required = true) private String displayTitle;
        @JsonProperty(value = "image_url") private String imageUrl;
        @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
        @JsonProperty(value = "stats") private JsonStatistics statistics;
        @JsonProperty(value = "type", required = true) private String type;
        @JsonProperty(value = "uri", required = true) private String uri;
    }
}