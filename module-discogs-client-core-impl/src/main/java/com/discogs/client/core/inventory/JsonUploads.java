package com.discogs.client.core.inventory;

import com.discogs.client.api.core.inventory.Uploads;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.paging.JsonPagination;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonUploads extends AbstractJson implements Uploads {

    @JsonProperty(value = "items", required = true) private JsonUpload[] items;
    @JsonProperty(value = "pagination", required = true) private JsonPagination pagination;
}