package com.discogs.client.core.user.wantlist;

import com.discogs.client.api.core.user.wantlist.WantListItem;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.JsonBasicInformation;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonWantListItem extends AbstractJson implements WantListItem {

    @JsonProperty(value = "id", required = true) private long id;
    @JsonProperty(value = "rating", required = true) private int rating;
    @JsonProperty(value = "notes") private String notes;
    @JsonProperty(value = "basic_information", required = true) JsonBasicInformation basicInformation;
    @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
}