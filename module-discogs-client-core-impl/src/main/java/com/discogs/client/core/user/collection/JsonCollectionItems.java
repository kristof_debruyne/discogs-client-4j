package com.discogs.client.core.user.collection;

import com.discogs.client.api.core.user.collection.CollectionList;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.paging.JsonPagination;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonCollectionItems extends AbstractJson implements CollectionList {

    @JsonProperty(value = "releases", required = true) private JsonCollectionItem[] items;
    @JsonProperty(value = "pagination", required = true) private JsonPagination pagination;

}
