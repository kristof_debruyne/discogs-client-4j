package com.discogs.client.core.common;

import com.discogs.client.api.core.common.Company;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonCompany extends AbstractJson implements Company {

    @JsonProperty(value = "id", required = true) long id;
    @JsonProperty(value = "catno") String catalogNumber;
    @JsonProperty(value = "name", required = true) String name;
    @JsonProperty(value = "entity_type") String entityType;
    @JsonProperty(value = "entity_type_name") String entityTypeName;
    @JsonProperty(value = "resource_url") String resourceUrl;
}