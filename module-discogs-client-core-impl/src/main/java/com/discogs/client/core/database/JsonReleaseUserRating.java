package com.discogs.client.core.database;

import com.discogs.client.api.core.database.ReleaseUserRating;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonReleaseUserRating extends AbstractJson implements ReleaseUserRating {

    @JsonProperty(value = "release_id", required = true) long releaseId;
    @JsonProperty(value = "rating") int rating;
    @JsonProperty(value = "username", required = true) String userName;
}