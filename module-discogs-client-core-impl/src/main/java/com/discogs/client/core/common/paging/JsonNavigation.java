package com.discogs.client.core.common.paging;

import com.discogs.client.api.core.common.paging.Navigation;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonNavigation extends AbstractJson implements Navigation {

    @JsonProperty(value = "first") private String first;
    @JsonProperty(value = "previous") private String previous;
    @JsonProperty(value = "next") private String next;
    @JsonProperty(value = "last") private String last;
}