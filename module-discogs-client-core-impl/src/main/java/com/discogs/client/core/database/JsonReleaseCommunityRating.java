package com.discogs.client.core.database;

import com.discogs.client.api.core.database.ReleaseCommunityRating;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.rating.JsonRating;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonReleaseCommunityRating extends AbstractJson implements ReleaseCommunityRating {

    @JsonProperty(value = "release_id", required = true) long releaseId;
    @JsonProperty(value = "rating", required = true)
    JsonRating rating;
}