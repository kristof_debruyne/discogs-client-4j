package com.discogs.client.core.inventory;

import com.discogs.client.api.core.inventory.Exports;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.paging.JsonPagination;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonExports extends AbstractJson implements Exports {

    @JsonProperty(value = "items", required = true) private JsonExport[] items;
    @JsonProperty(value = "pagination", required = true) private JsonPagination pagination;
}