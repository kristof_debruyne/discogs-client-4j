package com.discogs.client.core.user.contribution;

import com.discogs.client.api.core.user.contribution.Contributions;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.JsonCommunity;
import com.discogs.client.core.common.JsonCompany;
import com.discogs.client.core.common.JsonFormat;
import com.discogs.client.core.common.JsonVideo;
import com.discogs.client.core.common.paging.JsonPagination;
import com.discogs.client.core.database.JsonArtist;
import com.discogs.client.core.database.JsonLabel;
import com.discogs.client.core.image.JsonImage;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonContributions extends AbstractJson implements Contributions {

    @JsonProperty(value = "contributions", required = true) private JsonContribution[] results;
    @JsonProperty(value = "pagination", required = true) private JsonPagination pagination;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonContribution extends AbstractJson implements Contribution {

        @JsonProperty(value = "id", required = true) private long id;
        @JsonProperty(value = "artists") private JsonArtist[] artists;
        @JsonProperty(value = "community") private JsonCommunity community;
        @JsonProperty(value = "companies") private JsonCompany[] companies;
        @JsonProperty(value = "country") private String country;
        @JsonProperty(value = "data_quality") private String dataQuality;
        @JsonProperty(value = "date_added") private String dateAdded;
        @JsonProperty(value = "date_changed") private String dateChanged;
        @JsonProperty(value = "estimated_weight") private int estimatedWeight;
        @JsonProperty(value = "format_quantity") private int formatQuantity;
        @JsonProperty(value = "formats") private JsonFormat[] formats;
        @JsonProperty(value = "genres") private String[] genres;
        @JsonProperty(value = "images") private JsonImage[] images;
        @JsonProperty(value = "labels") private JsonLabel[] labels;
        @JsonProperty(value = "master_id") private Long masterId;
        @JsonProperty(value = "master_url") private String masterUrl;
        @JsonProperty(value = "notes") private String notes;
        @JsonProperty(value = "released") private String released;
        @JsonProperty(value = "released_formatted") private String releasedFormatted;
        @JsonProperty(value = "resource_url") private String resourceUrl;
        @JsonProperty(value = "series") private String[] series;
        @JsonProperty(value = "status") private String status;
        @JsonProperty(value = "styles") private String[] styles;
        @JsonProperty(value = "thumb") private String thumbnail;
        @JsonProperty(value = "title") private String title;
        @JsonProperty(value = "uri") private String uri;
        @JsonProperty(value = "videos") private JsonVideo[] videos;
        @JsonProperty(value = "year") private Integer year;
    }
}