package com.discogs.client.core;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Abstract JSON.
 *
 * @author Sikke303
 * @since 1.0
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractJson {

}