package com.discogs.client.core.database;

import com.discogs.client.api.core.database.SearchResult;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.JsonCommunity;
import com.discogs.client.core.common.JsonFormat;
import com.discogs.client.core.common.paging.JsonPagination;
import com.discogs.client.core.common.stats.JsonContainStatistics;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonSearchResult extends AbstractJson implements SearchResult {

    @JsonProperty(value = "results", required = true) private JsonSearchResultItem[] items;
    @JsonProperty(value = "pagination", required = true) private JsonPagination pagination;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonSearchResultItem extends AbstractJson implements SearchResultItem {

        @JsonProperty(value = "id", required = true) private long id;
        @JsonProperty(value = "catno", required = true) private String catalogNumber;
        @JsonProperty(value = "barcode") private String[] barcodes;
        @JsonProperty(value = "community") private JsonCommunity community;
        @JsonProperty(value = "country") private String country;
        @JsonProperty(value = "cover_image") private String coverImage;
        @JsonProperty(value = "format_quantity") private int formatQuantity;
        @JsonProperty(value = "format") private String[] format;
        @JsonProperty(value = "formats") private JsonFormat[] formats;
        @JsonProperty(value = "genre") private String[] genres;
        @JsonProperty(value = "label") private String[] labels;
        @JsonProperty(value = "master_id") private long masterId;
        @JsonProperty(value = "master_url") private String masterUrl;
        @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
        @JsonProperty(value = "style") private String[] styles;
        @JsonProperty(value = "thumb") private String thumbnail;
        @JsonProperty(value = "title") private String title;
        @JsonProperty(value = "type") private String type;
        @JsonProperty(value = "uri") private String uri;
        @JsonProperty(value = "user_data") private JsonContainStatistics userData;
        @JsonProperty(value = "year") private String year;
    }
}