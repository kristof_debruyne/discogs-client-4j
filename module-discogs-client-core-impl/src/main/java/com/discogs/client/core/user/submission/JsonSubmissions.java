package com.discogs.client.core.user.submission;

import com.discogs.client.api.core.user.submission.Submissions;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.paging.JsonPagination;
import com.discogs.client.core.database.JsonArtist;
import com.discogs.client.core.database.JsonLabel;
import com.discogs.client.core.database.JsonRelease;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonSubmissions extends AbstractJson implements Submissions {

    @JsonProperty(value = "submissions", required = true) private JsonSubmission result;
    @JsonProperty(value = "pagination", required = true) private JsonPagination pagination;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonSubmission extends AbstractJson implements Submission {

        @JsonProperty(value = "artists", required = true) private JsonArtist[] artists;
        @JsonProperty(value = "labels", required = true) private JsonLabel[] labels;
        @JsonProperty(value = "releases", required = true) private JsonRelease[] releases;
    }
}