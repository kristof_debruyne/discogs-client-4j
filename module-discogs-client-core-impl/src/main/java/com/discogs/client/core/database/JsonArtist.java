package com.discogs.client.core.database;

import com.discogs.client.api.core.database.Artist;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.image.JsonImage;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonArtist extends AbstractJson implements Artist {

    @JsonProperty(value = "id", required = true) private long id;
    @JsonProperty(value = "anv") private String anv;
    @JsonProperty(value = "data_quality") private String dataQuality;
    @JsonProperty(value = "images") private JsonImage[] images;
    @JsonProperty(value = "join") private String join;
    @JsonProperty(value = "members") private JsonMember[] members;
    @JsonProperty(value = "name", required = true) private String name;
    @JsonProperty(value = "namevariations") private String[] nameVariations;
    @JsonProperty(value = "profile") private String profile;
    @JsonProperty(value = "releases_url") private String releasesUrl;
    @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
    @JsonProperty(value = "role") private String role;
    @JsonProperty(value = "tracks") private String tracks;
    @JsonProperty(value = "uri") private String uri;
    @JsonProperty(value = "urls") private String[] urls;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonMember extends AbstractJson implements Member {

        @JsonProperty(value = "id") private long id;
        @JsonProperty(value = "active") private boolean active;
        @JsonProperty(value = "name", required = true) private String name;
        @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
    }
}