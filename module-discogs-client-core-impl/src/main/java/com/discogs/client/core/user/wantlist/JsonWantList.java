package com.discogs.client.core.user.wantlist;

import com.discogs.client.api.core.user.wantlist.WantList;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.paging.JsonPagination;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonWantList extends AbstractJson implements WantList {

    @JsonProperty(value = "wants", required = true) private JsonWantListItem[] items;
    @JsonProperty(value = "pagination", required = true) private JsonPagination pagination;
}