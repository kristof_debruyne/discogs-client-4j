package com.discogs.client.core.database;

import com.discogs.client.api.core.database.Track;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonTrack extends AbstractJson implements Track {

    @JsonProperty(value = "duration") private String duration;
    @JsonProperty(value = "extraartists") private JsonArtist[] extraArtists;
    @JsonProperty(value = "position") private String position;
    @JsonProperty(value = "title") private String title;
    @JsonProperty(value = "type_") private String type;
}