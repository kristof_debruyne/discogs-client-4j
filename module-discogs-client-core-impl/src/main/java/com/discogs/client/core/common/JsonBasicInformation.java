package com.discogs.client.core.common;

import com.discogs.client.api.core.common.BasicInformation;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.database.JsonArtist;
import com.discogs.client.core.database.JsonLabel;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonBasicInformation extends AbstractJson implements BasicInformation {

    @JsonProperty(value = "id", required = true) private long id;
    @JsonProperty(value = "artists") private JsonArtist[] artists;
    @JsonProperty(value = "cover_image") private String coverImage;
    @JsonProperty(value = "formats") private JsonFormat[] formats;
    @JsonProperty(value = "labels") private JsonLabel[] labels;
    @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
    @JsonProperty(value = "thumb") private String thumbnail;
    @JsonProperty(value = "title") private String title;
    @JsonProperty(value = "year") private int year;
}