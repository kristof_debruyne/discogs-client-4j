package com.discogs.client.core.user.identity;

import com.discogs.client.api.core.user.identity.Identity;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonIdentity extends AbstractJson implements Identity {

    @JsonProperty(value = "id", required = true) private long id;
    @JsonProperty(value = "consumer_name") private String consumerName;
    @JsonProperty(value = "username", required = true) private String userName;
    @JsonProperty(value = "resource_url") private String resourceUrl;
}