package com.discogs.client.core.database;

import com.discogs.client.api.core.database.ArtistReleases;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.paging.JsonPagination;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonArtistReleases extends AbstractJson implements ArtistReleases {

    @JsonProperty(value = "releases", required = true) private JsonArtistRelease[] releases;
    @JsonProperty(value = "pagination", required = true) private JsonPagination pagination;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonArtistRelease extends AbstractJson implements ArtistRelease {

        @JsonProperty(value = "id", required = true) private long id;
        @JsonProperty(value = "artist") private String artistName;
        @JsonProperty(value = "format") private String format;
        @JsonProperty(value = "label") private String labelName;
        @JsonProperty(value = "main_release") private long mainReleaseId;
        @JsonProperty(value = "resource_url") private String resourceUrl;
        @JsonProperty(value = "role") private String role;
        @JsonProperty(value = "status") private String status;
        @JsonProperty(value = "title") private String title;
        @JsonProperty(value = "thumb") private String thumbnail;
        @JsonProperty(value = "type") private String type;
        @JsonProperty(value = "year") private int year;
    }
}