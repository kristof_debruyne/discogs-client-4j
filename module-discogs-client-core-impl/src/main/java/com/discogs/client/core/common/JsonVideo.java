package com.discogs.client.core.common;

import com.discogs.client.api.core.common.Video;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonVideo extends AbstractJson implements Video {

    @JsonProperty(value = "embed") private boolean embedded;
    @JsonProperty(value = "duration") private int duration;
    @JsonProperty(value = "description") private String description;
    @JsonProperty(value = "title") private String title;
    @JsonProperty(value = "uri") private String uri;
}