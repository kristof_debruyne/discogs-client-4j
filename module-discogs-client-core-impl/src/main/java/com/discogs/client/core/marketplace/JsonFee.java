package com.discogs.client.core.marketplace;

import com.discogs.client.api.core.common.Currency;
import com.discogs.client.api.core.marketplace.Fee;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonFee extends AbstractJson implements Fee {

    @JsonProperty(value = "currency", required = true) private Currency currency;
    @JsonProperty(value = "value") private double value;
}