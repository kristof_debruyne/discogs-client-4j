package com.discogs.client.core.inventory;

import com.discogs.client.api.core.inventory.Upload;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonUpload extends AbstractJson implements Upload {

    @JsonProperty(value = "id", required = true) private long id;
    @JsonProperty(value = "created_ts") private String dateCreated;
    @JsonProperty(value = "finished_ts") private String dateFinished;
    @JsonProperty(value = "filename") private String filename;
    @JsonProperty(value = "results") private String results;
    @JsonProperty(value = "status") private String status;
    @JsonProperty(value = "type") private String type;
}