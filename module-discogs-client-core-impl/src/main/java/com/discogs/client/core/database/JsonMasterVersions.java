package com.discogs.client.core.database;

import com.discogs.client.api.core.database.MasterVersions;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.paging.JsonPagination;
import com.discogs.client.core.common.stats.JsonStatistics;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonMasterVersions extends AbstractJson implements MasterVersions {

    @JsonProperty(value = "versions", required = true) private JsonMasterVersion[] versions;
    @JsonProperty(value = "pagination", required = true) private JsonPagination pagination;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonMasterVersion extends AbstractJson implements MasterVersion {

        @JsonProperty(value = "id") private long id;
        @JsonProperty(value = "catno") private String catalogNumber;
        @JsonProperty(value = "country") private String country;
        @JsonProperty(value = "format") private String format;
        @JsonProperty(value = "label") private String labelName;
        @JsonProperty(value = "major_formats") private String[] majorFormats;
        @JsonProperty(value = "released") private String released;
        @JsonProperty(value = "resource_url") private String resourceUrl;
        @JsonProperty(value = "stats") private JsonStatistics statistics;
        @JsonProperty(value = "status") private String status;
        @JsonProperty(value = "thumb") private String thumbnail;
        @JsonProperty(value = "title") private String title;
    }
}