package com.discogs.client.core.user.identity;

import com.discogs.client.api.core.common.Currency;
import com.discogs.client.api.core.user.identity.Profile;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonProfile extends AbstractJson implements Profile {

    @JsonProperty(value = "id", required = true) private long id;
    @JsonProperty(value = "avatar_url") private String avatarUrl;
    @JsonProperty(value = "banner_url") private String bannerUrl;
    @JsonProperty(value = "buyer_num_ratings") private int buyerNumberRating;
    @JsonProperty(value = "buyer_rating") private double buyerRating;
    @JsonProperty(value = "buyer_rating_stars") private int buyerRatingStars;
    @JsonProperty(value = "collection_fields_url") private String collectionFieldsUrl;
    @JsonProperty(value = "collection_folders_url") private String collectionFolderUrl;
    @JsonProperty(value = "curr_abbr") private Currency currency;
    @JsonProperty(value = "email") private String email;
    @JsonProperty(value = "inventory_url") private String inventoryUrl;
    @JsonProperty(value = "home_page") private String homePage;
    @JsonProperty(value = "location") private String location;
    @JsonProperty(value = "name") private String name;
    @JsonProperty(value = "num_collection") private int numberCollection;
    @JsonProperty(value = "num_for_sale") private int numberForSale;
    @JsonProperty(value = "num_lists") private int numberLists;
    @JsonProperty(value = "num_pending") private int numberPending;
    @JsonProperty(value = "num_wantlist") private int numberWantList;
    @JsonProperty(value = "profile") private String profile;
    @JsonProperty(value = "rank") private int rank;
    @JsonProperty(value = "rating_avg") private double ratingAverage;
    @JsonProperty(value = "registered") private String registered;
    @JsonProperty(value = "releases_contributed") private int releasesContributed;
    @JsonProperty(value = "releases_rated") private int releasesRated;
    @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
    @JsonProperty(value = "seller_num_ratings") private int sellerNumberRating;
    @JsonProperty(value = "seller_rating") private double sellerRating;
    @JsonProperty(value = "seller_rating_stars") private int sellerRatingStars;
    @JsonProperty(value = "username", required = true) private String userName;
    @JsonProperty(value = "uri") private String uri;
    @JsonProperty(value = "wantlist_url") private String wantListUrl;
}