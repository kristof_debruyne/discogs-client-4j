package com.discogs.client.core.database;

import com.discogs.client.api.core.database.Label;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.image.JsonImage;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonLabel extends AbstractJson implements Label {

    @JsonProperty(value = "id", required = true) private long id;
    @JsonProperty(value = "contact_info") private String contactInfo;
    @JsonProperty(value = "catno",  required = true) private String catalogNumber;
    @JsonProperty(value = "data_quality") private String dataQuality;
    @JsonProperty(value = "images") private JsonImage[] images;
    @JsonProperty(value = "name", required = true) private String name;
    @JsonProperty(value = "profile") private String profile;
    @JsonProperty(value = "releases_url") private String releasesUrl;
    @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
    @JsonProperty(value = "sublabels", required = true) private JsonSubLabel[] subLabels;
    @JsonProperty(value = "uri") private String uri;
    @JsonProperty(value = "urls") private String[] urls;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonSubLabel extends AbstractJson implements SubLabel {

        @JsonProperty(value = "id", required = true) private long id;
        @JsonProperty(value = "name", required = true) private String name;
        @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
    }
}