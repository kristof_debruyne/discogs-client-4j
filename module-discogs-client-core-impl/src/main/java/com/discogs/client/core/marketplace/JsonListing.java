package com.discogs.client.core.marketplace;

import com.discogs.client.api.core.marketplace.Listing;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonListing extends AbstractJson implements Listing {

    @JsonProperty(value = "id", required = true) private long id;
    @JsonProperty(value = "allow_offers") private boolean allowOffers;
    @JsonProperty(value = "audio") private boolean audio;
    @JsonProperty(value = "comments") private String comment;
    @JsonProperty(value = "condition") private String condition;
    @JsonProperty(value = "posted") private String datePosted;
    @JsonProperty(value = "external_id") private String externalId;
    @JsonProperty(value = "format_quantity", defaultValue = "auto") private String formatQuantity;
    @JsonProperty(value = "location") private String location;
    @JsonProperty(value = "original_price") private JsonPriceOriginal originalPrice;
    @JsonProperty(value = "original_shipping_price") private JsonPriceOriginal originalShippingPrice;
    @JsonProperty(value = "price") private JsonPrice price;
    @JsonProperty(value = "release") private JsonListingRelease release;
    @JsonProperty(value = "resource_url") private String resourceUrl;
    @JsonProperty(value = "seller") private JsonSeller seller;
    @JsonProperty(value = "shipping_price") private JsonPrice shippingPrice;
    @JsonProperty(value = "ships_from") private String shipsFrom;
    @JsonProperty(value = "sleeve_condition") private String sleeveCondition;
    @JsonProperty(value = "status") private String status;
    @JsonProperty(value = "uri") private String uri;
    @JsonProperty(value = "weight", defaultValue = "auto") private String weight;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonListingRelease extends AbstractJson implements ListingRelease {

        @JsonProperty(value = "id", required = true) private long id;
        @JsonProperty(value = "catalog_number") private String catalogNumber;
        @JsonProperty(value = "description") private String description;
        @JsonProperty(value = "thumbnail") private String thumbnail;
        @JsonProperty(value = "resource_url") private String resourceUrl;
        @JsonProperty(value = "year") private int year;
    }
}