package com.discogs.client.core.marketplace;

import com.discogs.client.api.core.marketplace.ReleaseStatistics;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonReleaseStatistics extends AbstractJson implements ReleaseStatistics {

    @JsonProperty(value = "blocked_from_sale") private boolean blockedFromSale;
    @JsonProperty(value = "num_for_sale") private int numberForSale;
    @JsonProperty(value = "lowest_price") private JsonPrice lowestPrice;
}