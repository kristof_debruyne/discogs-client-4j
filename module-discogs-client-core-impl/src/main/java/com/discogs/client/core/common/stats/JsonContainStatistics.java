package com.discogs.client.core.common.stats;

import com.discogs.client.api.core.common.stats.ContainStatistics;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonContainStatistics extends AbstractJson implements ContainStatistics {

    @JsonProperty(value = "in_collection") private boolean inCollection;
    @JsonProperty(value = "in_wantlist") private boolean inWantList;
}