package com.discogs.client.core.root;

import com.discogs.client.api.core.ApiInformation;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonApiInformation extends AbstractJson implements ApiInformation {

    @JsonProperty(value = "hello", required = true) private String description;
    @JsonProperty(value = "documentation_url", required = true) private String documentationUrl;
    @JsonProperty(value = "statistics", required = true) private JsonApiStatistics statistics;
    @JsonProperty(value = "api_version", required = true) private String version;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonApiStatistics extends AbstractJson implements ApiStatistics {

        @JsonProperty(value = "artists", required = true) private long artistCount;
        @JsonProperty(value = "labels", required = true) private long labelCount;
        @JsonProperty(value = "releases", required = true) private long releaseCount;
    }
}