package com.discogs.client.core.user.collection;

import com.discogs.client.api.core.user.collection.CollectionInstance;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonCollectionInstance extends AbstractJson implements CollectionInstance {

    @JsonProperty(value = "instance_id", required = true) private long id;
    @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
}