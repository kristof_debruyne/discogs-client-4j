package com.discogs.client.core.user.collection;

import com.discogs.client.api.core.user.collection.CollectionItem;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.JsonBasicInformation;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonCollectionItem extends AbstractJson implements CollectionItem {

    @JsonProperty(value = "id", required = true)
    private long id;
    @JsonProperty(value = "date_added")
    private String dateAdded;
    @JsonProperty(value = "folder_id", required = true)
    private long folderId;
    @JsonProperty(value = "instance_id", required = true)
    private long instanceId;
    @JsonProperty(value = "rating")
    private int rating;
    @JsonProperty(value = "basic_information", required = true)
    private JsonBasicInformation basicInformation;
    @JsonProperty(value = "notes")
    private JsonNote[] notes;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonNote extends AbstractJson implements Note {

        @JsonProperty(value = "field_id")
        private int fieldId;
        @JsonProperty(value = "value")
        private String value;
    }
}
