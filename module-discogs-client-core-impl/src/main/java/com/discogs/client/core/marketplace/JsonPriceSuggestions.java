package com.discogs.client.core.marketplace;

import com.discogs.client.api.core.marketplace.PriceSuggestions;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonPriceSuggestions extends AbstractJson implements PriceSuggestions {

    private final Map<String, JsonFee> suggestions = new HashMap<>();

    @JsonAnyGetter
    public Map<String, JsonFee> getSuggestions() {
        return suggestions;
    }

    @JsonAnySetter
    public void add(String key, JsonFee fee) {
        suggestions.put(key, fee);
    }
}