package com.discogs.client.core.common;

import com.discogs.client.api.core.common.Community;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.rating.JsonRating;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonCommunity extends AbstractJson implements Community {

    @JsonProperty(value = "contributors") private JsonContributor[] contributors;
    @JsonProperty(value = "data_quality") private String dataQuality;
    @JsonProperty(value = "have") private Integer haveCount;
    @JsonProperty(value = "rating") private JsonRating rating;
    @JsonProperty(value = "status") private String status;
    @JsonProperty(value = "submitter") private JsonSubmitter submitter;
    @JsonProperty(value = "want") private Integer wantCount;
}