package com.discogs.client.core.user.list;

import com.discogs.client.api.core.user.list.UserLists;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.paging.JsonPagination;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonUserLists extends AbstractJson implements UserLists {

    @JsonProperty(value = "lists", required = true) private JsonUserLists.JsonUserList[] lists;
    @JsonProperty(value = "pagination", required = true) private JsonPagination pagination;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonUserList extends AbstractJson implements UserList {

        @JsonProperty(value = "id", required = true) private long id;
        @JsonProperty(value = "public", required = true) private boolean publicList;
        @JsonProperty(value = "date_added") private String dateAdded;
        @JsonProperty(value = "date_changed") private String dateChanged;
        @JsonProperty(value = "description") private String description;
        @JsonProperty(value = "resource_url") private String resourceUrl;
        @JsonProperty(value = "name") private String name;
        @JsonProperty(value = "uri") private String uri;
    }
}