package com.discogs.client.core.common;

import com.discogs.client.api.core.common.Contributor;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonContributor extends AbstractJson implements Contributor {

    @JsonProperty(value = "username", required = true) private String userName;
    @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
}