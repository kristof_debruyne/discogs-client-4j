package com.discogs.client.core.user;

import com.discogs.client.api.core.user.User;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonUser extends AbstractJson implements User {

    @JsonProperty(value = "id", required = true) private long id;
    @JsonProperty(value = "avatar_url") private String avatarUrl;
    @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
    @JsonProperty(value = "username", required = true) private String username;
}