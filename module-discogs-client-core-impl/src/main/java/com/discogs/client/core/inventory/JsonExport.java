package com.discogs.client.core.inventory;

import com.discogs.client.api.core.inventory.Export;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonExport extends AbstractJson implements Export {

    @JsonProperty(value = "id", required = true) private long id;
    @JsonProperty(value = "created_ts") private String dateCreated;
    @JsonProperty(value = "finished_ts") private String dateFinished;
    @JsonProperty(value = "download_url") private String downloadUrl;
    @JsonProperty(value = "filename") private String filename;
    @JsonProperty(value = "url") private String url;
    @JsonProperty(value = "status") private String status;
}