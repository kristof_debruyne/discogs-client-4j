package com.discogs.client.core.image;

import com.discogs.client.api.core.image.Image;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonImage extends AbstractJson implements Image {

    @JsonProperty(value = "height", required = true) private int height;
    @JsonProperty(value = "width", required = true) private int width;
    @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
    @JsonProperty(value = "type", required = true) private String type;
    @JsonProperty(value = "uri", required = true) private String uri;
    @JsonProperty(value = "uri150", required = true) private String uri150;
}