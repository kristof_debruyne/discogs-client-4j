package com.discogs.client.core.common;

import com.discogs.client.api.core.common.Format;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonFormat extends AbstractJson implements Format {

    @JsonProperty(value = "descriptions") private String[] descriptions;
    @JsonProperty(value = "name")  private String name;
    @JsonProperty(value = "qty")  private String quantity;
    @JsonProperty(value = "text")  private String text;
}