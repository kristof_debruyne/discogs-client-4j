package com.discogs.client.core.user.collection;

import com.discogs.client.api.core.user.collection.CollectionFolders;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonCollectionFolders extends AbstractJson implements CollectionFolders {

    @JsonProperty(value = "folders", required = true) private JsonCollectionFolder[] folders;
}