package com.discogs.client.core.marketplace;

import com.discogs.client.api.core.common.Currency;
import com.discogs.client.api.core.marketplace.PriceOriginal;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonPriceOriginal extends AbstractJson implements PriceOriginal {

    @JsonProperty(value = "curr_id") private int currencyId;
    @JsonProperty(value = "curr_abbr") private Currency currency;
    @JsonProperty(value = "formatted") private String formattedValue;
    @JsonProperty(value = "value") private double value;
}