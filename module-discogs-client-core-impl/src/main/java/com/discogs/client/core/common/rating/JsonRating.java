package com.discogs.client.core.common.rating;

import com.discogs.client.api.core.common.rating.Rating;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonRating extends AbstractJson implements Rating {

    @JsonProperty(value = "average") private double average;
    @JsonProperty(value = "count") private int count;
}