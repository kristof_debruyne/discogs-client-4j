package com.discogs.client.core.user.collection;

import com.discogs.client.api.core.user.collection.CollectionFields;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonCollectionFields extends AbstractJson implements CollectionFields {

    @JsonProperty(value = "fields", required = true) private JsonCollectionField[] fields;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonCollectionField extends AbstractJson implements CollectionFields.CollectionField {

        @JsonProperty(value = "id", required = true) private long id;
        @JsonProperty(value = "public", required = true) private boolean publicField;
        @JsonProperty(value = "lines") private int lines;
        @JsonProperty(value = "position", required = true) private int position;
        @JsonProperty(value = "name", required = true) private String name;
        @JsonProperty(value = "options") private String[] options;
        @JsonProperty(value = "type", required = true) private String type;
    }
}