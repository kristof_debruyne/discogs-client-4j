package com.discogs.client.core.common.stats;

import com.discogs.client.api.core.common.stats.CountStatistics;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonCountStatistics extends AbstractJson implements CountStatistics {

    @JsonProperty(value = "in_collection") private int inCollectionCount;
    @JsonProperty(value = "in_wantlist") private int inWantListCount;
}