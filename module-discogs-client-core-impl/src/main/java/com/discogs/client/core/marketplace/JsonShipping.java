package com.discogs.client.core.marketplace;

import com.discogs.client.api.core.common.Currency;
import com.discogs.client.api.core.marketplace.Shipping;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonShipping extends AbstractJson implements Shipping {

    @JsonProperty(value = "currency") private Currency currency;
    @JsonProperty(value = "method") private String method;
    @JsonProperty(value = "value") private double value;
}