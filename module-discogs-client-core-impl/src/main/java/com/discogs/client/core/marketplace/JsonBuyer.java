package com.discogs.client.core.marketplace;

import com.discogs.client.api.core.marketplace.Buyer;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonBuyer extends AbstractJson implements Buyer {

    @JsonProperty(value = "id", required = true) private long id;
    @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
    @JsonProperty(value = "username", required = true) private String username;
}