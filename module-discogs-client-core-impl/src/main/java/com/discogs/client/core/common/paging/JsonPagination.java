package com.discogs.client.core.common.paging;

import com.discogs.client.api.core.common.paging.Pagination;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonPagination extends AbstractJson implements Pagination {

    @JsonProperty(value = "items", required = true) private int itemCount;
    @JsonProperty(value = "page", required = true) private int page;
    @JsonProperty(value = "pages", required = true) private int pageCount;
    @JsonProperty(value = "per_page", required = true) private int perPage;
    @JsonProperty(value = "urls", required = true) private JsonNavigation navigation;
}