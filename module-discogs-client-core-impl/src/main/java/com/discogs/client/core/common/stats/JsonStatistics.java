package com.discogs.client.core.common.stats;

import com.discogs.client.api.core.common.stats.Statistics;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonStatistics extends AbstractJson implements Statistics {

    @JsonProperty(value = "community") private JsonCountStatistics communityStatistics;
    @JsonProperty(value = "user") private JsonCountStatistics userStatistics;
}