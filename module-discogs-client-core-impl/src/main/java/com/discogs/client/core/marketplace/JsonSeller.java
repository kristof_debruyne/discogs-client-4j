package com.discogs.client.core.marketplace;

import com.discogs.client.api.core.marketplace.Seller;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonSeller extends AbstractJson implements Seller {

    @JsonProperty(value = "id", required = true) private long id;
    @JsonProperty(value = "avatar_url") private String avatarUrl;
    @JsonProperty(value = "payment") private String paymentMethod;
    @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
    @JsonProperty(value = "stats") private JsonSellerStatistics statistics;
    @JsonProperty(value = "shipping") private String shippingExtraInformation;
    @JsonProperty(value = "status") private String status;
    @JsonProperty(value = "url") private String url;
    @JsonProperty(value = "username", required = true) private String username;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonSellerStatistics extends AbstractJson implements Seller.SellerStatistics {

        @JsonProperty(value = "stars") private double ratingStars;
        @JsonProperty(value = "rating") private String ratingValue;
        @JsonProperty(value = "total") private int total;
    }
}