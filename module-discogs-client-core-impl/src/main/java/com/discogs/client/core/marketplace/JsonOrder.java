package com.discogs.client.core.marketplace;

import com.discogs.client.api.core.marketplace.Order;
import com.discogs.client.core.AbstractJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonOrder extends AbstractJson implements Order {

    @JsonProperty(value = "id") private String id;
    @JsonProperty(value = "archived") private boolean archived;
    @JsonProperty(value = "additional_instructions") private String additionalInstructions;
    @JsonProperty(value = "buyer") private JsonBuyer buyer;
    @JsonProperty(value = "created") private String dateCreated;
    @JsonProperty(value = "last_activity") private String dateLastActivity;
    @JsonProperty(value = "fee") private JsonFee fee;
    @JsonProperty(value = "items") private JsonOrderItem[] items;
    @JsonProperty(value = "messages_url") private String messagesUrl;
    @JsonProperty(value = "next_status") private String[] nextStatuses;
    @JsonProperty(value = "resource_url") private String resourceUrl;
    @JsonProperty(value = "seller") private JsonSeller seller;
    @JsonProperty(value = "shipping") private JsonShipping shipping;
    @JsonProperty(value = "shipping_address") private String shippingAddress;
    @JsonProperty(value = "status") private String status;
    @JsonProperty(value = "total") private JsonPrice totalPrice;
    @JsonProperty(value = "uri") private String uri;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonOrderItem extends AbstractJson implements Order.OrderItem {

        @JsonProperty(value = "id") private long id;
        @JsonProperty(value = "price") private JsonPrice price;
        @JsonProperty(value = "release") private JsonOrderRelease release;
        @JsonProperty(value = "media_condition") private String condition;
        @JsonProperty(value = "sleeve_condition") private String sleeveCondition;
    }

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonOrderRelease extends AbstractJson implements Order.OrderRelease {

        @JsonProperty(value = "id", required = true) private long id;
        @JsonProperty(value = "description") private String description;
    }
}