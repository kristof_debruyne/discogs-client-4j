package com.discogs.client.core.database;

import com.discogs.client.api.core.database.LabelReleases;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.paging.JsonPagination;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonLabelReleases extends AbstractJson implements LabelReleases {

    @JsonProperty(value = "releases", required = true) private JsonLabelReleases.JsonLabelRelease[] releases;
    @JsonProperty(value = "pagination", required = true) private JsonPagination pagination;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonLabelRelease extends AbstractJson implements LabelRelease {

        @JsonProperty(value = "id", required = true) private long id;
        @JsonProperty(value = "artist") private String artistName;
        @JsonProperty(value = "catno") private String catalogNumber;
        @JsonProperty(value = "format") private String format;
        @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
        @JsonProperty(value = "status") private String status;
        @JsonProperty(value = "title") private String title;
        @JsonProperty(value = "thumb") private String thumbnail;
        @JsonProperty(value = "year") private int year;
    }
}