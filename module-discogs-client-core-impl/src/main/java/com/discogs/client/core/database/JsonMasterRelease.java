package com.discogs.client.core.database;

import com.discogs.client.api.core.database.MasterRelease;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.JsonVideo;
import com.discogs.client.core.image.JsonImage;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonMasterRelease extends AbstractJson implements MasterRelease {

    @JsonProperty(value = "id", required = true) long id;
    @JsonProperty(value = "artists") JsonArtist[] artists;
    @JsonProperty(value = "data_quality") String dataQuality;
    @JsonProperty(value = "genres") String[] genres;
    @JsonProperty(value = "images") JsonImage[] images;
    @JsonProperty(value = "lowest_price") Double lowestPrice;
    @JsonProperty(value = "main_release") long mainReleaseId;
    @JsonProperty(value = "main_release_url") String mainReleaseUrl;
    @JsonProperty(value = "num_for_sale") int numberForSale;
    @JsonProperty(value = "resource_url") String resourceUrl;
    @JsonProperty(value = "styles") String[] styles;
    @JsonProperty(value = "title") String title;
    @JsonProperty(value = "tracklist") JsonTrack[] trackList;
    @JsonProperty(value = "uri") String uri;
    @JsonProperty(value = "versions_url") String versionsUrl;
    @JsonProperty(value = "videos") JsonVideo[] videos;
    @JsonProperty(value = "year") int year;
}