package com.discogs.client.core.database;

import com.discogs.client.api.core.database.Release;
import com.discogs.client.core.AbstractJson;
import com.discogs.client.core.common.JsonCommunity;
import com.discogs.client.core.common.JsonCompany;
import com.discogs.client.core.common.JsonFormat;
import com.discogs.client.core.common.JsonVideo;
import com.discogs.client.core.image.JsonImage;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class JsonRelease extends AbstractJson implements Release {

    @JsonProperty(value = "id", required = true) private long id;
    @JsonProperty(value = "artists") private JsonArtist[] artists;
    @JsonProperty(value = "community") private JsonCommunity community;
    @JsonProperty(value = "companies") private JsonCompany[] companies;
    @JsonProperty(value = "country") private String country;
    @JsonProperty(value = "data_quality") private String dataQuality;
    @JsonProperty(value = "date_added") private String dateAdded;
    @JsonProperty(value = "date_changed") private String dateChanged;
    @JsonProperty(value = "estimated_weight") private int estimatedWeight;
    @JsonProperty(value = "extraartists") private JsonArtist[] extraArtists;
    @JsonProperty(value = "format_quantity") private int formatQuantity;
    @JsonProperty(value = "formats") private JsonFormat[] formats;
    @JsonProperty(value = "genres") private String[] genres;
    @JsonProperty(value = "identifiers") private JsonIdentifier[] identifiers;
    @JsonProperty(value = "images") private JsonImage[] images;
    @JsonProperty(value = "labels") private JsonLabel[] labels;
    @JsonProperty(value = "lowest_price") private Double lowestPrice;
    @JsonProperty(value = "master_id") private Long masterId;
    @JsonProperty(value = "master_url") private String masterUrl;
    @JsonProperty(value = "notes") private String notes;
    @JsonProperty(value = "num_for_sale") private int numberForSale;
    @JsonProperty(value = "released") private String released;
    @JsonProperty(value = "released_formatted") private String releasedFormatted;
    @JsonProperty(value = "resource_url", required = true) private String resourceUrl;
    @JsonProperty(value = "series") private String[] series;
    @JsonProperty(value = "status") private String status;
    @JsonProperty(value = "styles") private String[] styles;
    @JsonProperty(value = "thumb") private String thumbnail;
    @JsonProperty(value = "title") private String title;
    @JsonProperty(value = "tracklist") private JsonTrack[] trackList;
    @JsonProperty(value = "uri") private String uri;
    @JsonProperty(value = "videos") private JsonVideo[] videos;
    @JsonProperty(value = "year") private int year;

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @ToString
    public static class JsonIdentifier extends AbstractJson implements Release.Identifier {

        @JsonProperty(value = "type", required = true) String type;
        @JsonProperty(value = "value", required = true) String value;
    }
}