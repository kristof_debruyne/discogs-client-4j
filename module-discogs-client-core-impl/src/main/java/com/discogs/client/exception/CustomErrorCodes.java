package com.discogs.client.exception;

import com.discogs.client.api.exception.DiscogsErrorCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Supported Discogs error codes.
 *
 * @author Sikke303
 * @since 1.0
 */
@RequiredArgsConstructor
public enum CustomErrorCodes implements DiscogsErrorCode {

    UNKNOWN_ERROR("0000"),
    BUSINESS_ERROR("1000"),
    SYSTEM_ERROR("2000"),
    CONFIG_ERROR("3000"),
    FEATURE_NOT_SUPPORTED("4000"),
    SECURITY_ERROR("8000"),
    SERVICE_UNAVAILABLE("9999");

    @Getter
    private final String code;
}