package com.discogs.client;

import com.discogs.client.api.DiscogsMarketplaceClient;
import com.discogs.client.api.annotations.DiscogsTested;
import com.discogs.client.api.annotations.NotImplemented;
import com.discogs.client.api.config.UserAgentGenerator;
import com.discogs.client.api.core.marketplace.OrderMessages;
import com.discogs.client.api.exception.DiscogsRuntimeException;
import com.discogs.client.api.request.marketplace.FeeCurrencyRequest;
import com.discogs.client.api.request.marketplace.FeeRequest;
import com.discogs.client.api.request.marketplace.ListingCreateRequest;
import com.discogs.client.api.request.marketplace.ListingDeleteRequest;
import com.discogs.client.api.request.marketplace.ListingRequest;
import com.discogs.client.api.request.marketplace.ListingUpdateRequest;
import com.discogs.client.api.request.marketplace.ListingsRequest;
import com.discogs.client.api.request.marketplace.OrderMessageCreateRequest;
import com.discogs.client.api.request.marketplace.OrderMessagesRequest;
import com.discogs.client.api.request.marketplace.OrderRequest;
import com.discogs.client.api.request.marketplace.OrdersRequest;
import com.discogs.client.api.request.marketplace.PriceSuggestionsRequest;
import com.discogs.client.api.request.marketplace.ReleaseStatisticsRequest;
import com.discogs.client.api.response.Response;
import com.discogs.client.auth.oauth.service.OAuthService;
import com.discogs.client.core.marketplace.JsonFee;
import com.discogs.client.core.marketplace.JsonListing;
import com.discogs.client.core.marketplace.JsonListings;
import com.discogs.client.core.marketplace.JsonOrder;
import com.discogs.client.core.marketplace.JsonOrders;
import com.discogs.client.core.marketplace.JsonPriceSuggestions;
import com.discogs.client.core.marketplace.JsonReleaseStatistics;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URI;

import static com.discogs.client.exception.CustomErrorCodes.FEATURE_NOT_SUPPORTED;

/**
 * Implementation of class {@link DiscogsMarketplaceClient}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractRestDiscogsClient
 */
@ParametersAreNonnullByDefault
@Log4j2
public class RestDiscogsMarketplaceClient extends AbstractRestDiscogsClient implements DiscogsMarketplaceClient {

    public RestDiscogsMarketplaceClient(URI baseUri, OAuthService oauthService, RestTemplate restTemplate, UserAgentGenerator userAgentGenerator) {
        super(baseUri, oauthService, restTemplate, userAgentGenerator);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonFee> get(@NonNull final FeeRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonFee.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonFee> get(@NonNull final FeeCurrencyRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonFee.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonPriceSuggestions> get(@NonNull final PriceSuggestionsRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonPriceSuggestions.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonListings> getAll(@NonNull final ListingsRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonListings.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonListing> get(@NonNull final ListingRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonListing.class);
    }

    @Nonnull
    @Override
    public Response<Void> create(@NonNull final ListingCreateRequest request) {
        return post(createUri(request), new HttpEntity<>(createHeaders(request)), Void.class);
    }

    @Nonnull
    @Override
    public Response<Void> modify(@NonNull final ListingUpdateRequest request) {
        return post(createUri(request), new HttpEntity<>(createHeaders(request)), Void.class);
    }

    @Nonnull
    @Override
    public Response<Void> delete(@NonNull final ListingDeleteRequest request) {
        return delete(createUri(request), new HttpEntity<>(createHeaders(request)));
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonOrders> getAll(@NonNull final OrdersRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonOrders.class);
    }

    @Nonnull
    @Override
    public Response<JsonOrder> get(@NonNull final OrderRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonOrder.class);
    }

    @NotImplemented
    @Nonnull
    @Override
    public Response<? extends OrderMessages> get(@NonNull final OrderMessagesRequest request) {
        throw new DiscogsRuntimeException("Operation not implemented !", FEATURE_NOT_SUPPORTED);
    }

    @NotImplemented
    @Nonnull
    @Override
    public Response<? extends OrderMessages.OrderMessage> create(@NonNull final OrderMessageCreateRequest request) {
        throw new DiscogsRuntimeException("Operation not implemented !", FEATURE_NOT_SUPPORTED);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonReleaseStatistics> get(@NonNull final ReleaseStatisticsRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonReleaseStatistics.class);
    }
}
