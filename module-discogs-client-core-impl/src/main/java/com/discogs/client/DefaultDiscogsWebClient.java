package com.discogs.client;

import com.discogs.client.api.DiscogsWebClient;
import com.discogs.client.api.core.database.SearchType;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URI;

@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class DefaultDiscogsWebClient implements DiscogsWebClient {

    @Getter
    @NonNull
    private final URI baseUri;

    @Nonnull
    @Override
    public String getHomepage() {
        return getBaseUri().toASCIIString();
    }

    @Nonnull
    @Override
    public String createSearchUrl(@NonNull final String input) {
        return createSearchUrl(input, SearchType.ALL);
    }

    @Override
    public String createSearchUrl(@NonNull final String input, @NonNull final SearchType searchType) {
        return createSearchUrl(input, searchType, 1, 50);
    }

    @Override
    public String createSearchUrl(@NonNull final String input, @NonNull SearchType searchType, final int page, final int pageSize) {
        return getHomepage().concat("/search?q=").concat(input).concat("&type=")
                .concat(searchType.getValue()).concat("&page=").concat(String.valueOf(page)).concat("&page_size=").concat(String.valueOf(pageSize));
    }

    @Nonnull
    @Override
    public String createArtistWebUrl(@Nonnegative long artistId) {
        return createWebUrl(artistId, "/artist/");
    }

    @Nonnull
    @Override
    public String createLabelWebUrl(@Nonnegative long labelId) {
        return createWebUrl(labelId, "/label/");
    }

    @Nonnull
    @Override
    public String createMasterWebUrl(@Nonnegative long masterId) {
        return createWebUrl(masterId, "/master/");
    }

    @Nonnull
    @Override
    public String createReleaseWebUrl(@Nonnegative long releaseId) {
        return createWebUrl(releaseId, "/release/");
    }

    @Nonnull
    private String createWebUrl(long id, String path) {
        return getHomepage().concat(path).concat(String.valueOf(id));
    }
}
