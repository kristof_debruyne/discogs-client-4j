package com.discogs.client;

import com.discogs.client.api.DiscogsInventoryClient;
import com.discogs.client.api.annotations.DiscogsForum;
import com.discogs.client.api.annotations.DiscogsTested;
import com.discogs.client.api.config.UserAgentGenerator;
import com.discogs.client.api.request.inventory.ExportCreateRequest;
import com.discogs.client.api.request.inventory.ExportDownloadRequest;
import com.discogs.client.api.request.inventory.ExportRequest;
import com.discogs.client.api.request.inventory.ExportsRequest;
import com.discogs.client.api.request.inventory.UploadAddRequest;
import com.discogs.client.api.request.inventory.UploadChangeRequest;
import com.discogs.client.api.request.inventory.UploadDeleteRequest;
import com.discogs.client.api.request.inventory.UploadRequest;
import com.discogs.client.api.request.inventory.UploadsRequest;
import com.discogs.client.api.response.Response;
import com.discogs.client.auth.oauth.service.OAuthService;
import com.discogs.client.core.inventory.JsonExport;
import com.discogs.client.core.inventory.JsonExports;
import com.discogs.client.core.inventory.JsonUpload;
import com.discogs.client.core.inventory.JsonUploads;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URI;

/**
 * Implementation of class {@link com.discogs.client.api.DiscogsInventoryClient}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractRestDiscogsClient
 */
@ParametersAreNonnullByDefault
@Log4j2
public class RestDiscogsInventoryClient extends AbstractRestDiscogsClient implements DiscogsInventoryClient {

    public RestDiscogsInventoryClient(URI baseUri, OAuthService oauthService, RestTemplate restTemplate, UserAgentGenerator userAgentGenerator) {
        super(baseUri, oauthService, restTemplate, userAgentGenerator);
    }

    //*** EXPORT ***//

    @DiscogsTested
    @Nonnull
    @Override
    public Response<Void> create(@NonNull final ExportCreateRequest request) {
        return post(createUri(request), new HttpEntity<>(createHeaders(request)), Void.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<byte[]> download(@NonNull final ExportDownloadRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), byte[].class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonExports> getAll(@NonNull final ExportsRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonExports.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonExport> get(@NonNull final ExportRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonExport.class);
    }

    //*** IMPORT ***//

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonUploads> getAll(@NonNull final UploadsRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonUploads.class);
    }

    @Nonnull
    @Override
    public Response<JsonUpload> get(@NonNull final UploadRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonUpload.class);
    }

    @DiscogsForum(url = "https://www.discogs.com/forum/thread/819598")
    @Nonnull
    @Override
    public Response<Void> upload(@NonNull final UploadAddRequest request) {
        return post(createUri(request), new HttpEntity<>(createMultiPartBody(request), createHeaders(request)), Void.class);
    }

    @DiscogsForum(url = "https://www.discogs.com/forum/thread/819598")
    @Nonnull
    @Override
    public Response<Void> upload(@NonNull final UploadChangeRequest request) {
        return post(createUri(request), new HttpEntity<>(createMultiPartBody(request), createHeaders(request)), Void.class);
    }

    @DiscogsForum(url = "https://www.discogs.com/forum/thread/819598")
    @Nonnull
    @Override
    public Response<Void> upload(@NonNull final UploadDeleteRequest request) {
        return delete(createUri(request), new HttpEntity<>(createMultiPartBody(request), createHeaders(request)));
    }
}
