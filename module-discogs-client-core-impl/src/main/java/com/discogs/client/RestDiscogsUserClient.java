package com.discogs.client;

import com.discogs.client.api.DiscogsUserClient;
import com.discogs.client.api.annotations.DiscogsTested;
import com.discogs.client.api.annotations.NotImplemented;
import com.discogs.client.api.config.UserAgentGenerator;
import com.discogs.client.api.exception.DiscogsRuntimeException;
import com.discogs.client.api.request.user.collection.CollectionFieldUpdateRequest;
import com.discogs.client.api.request.user.collection.CollectionFieldsRequest;
import com.discogs.client.api.request.user.collection.CollectionFolderCreateRequest;
import com.discogs.client.api.request.user.collection.CollectionFolderDeleteRequest;
import com.discogs.client.api.request.user.collection.CollectionFolderRequest;
import com.discogs.client.api.request.user.collection.CollectionFolderUpdateRequest;
import com.discogs.client.api.request.user.collection.CollectionFoldersRequest;
import com.discogs.client.api.request.user.collection.CollectionItemsByFolderRequest;
import com.discogs.client.api.request.user.collection.CollectionItemsByReleaseRequest;
import com.discogs.client.api.request.user.collection.CollectionReleaseAddRequest;
import com.discogs.client.api.request.user.collection.CollectionReleaseDeleteRequest;
import com.discogs.client.api.request.user.collection.CollectionValueRequest;
import com.discogs.client.api.request.user.contribution.ContributionsRequest;
import com.discogs.client.api.request.user.identity.IdentityRequest;
import com.discogs.client.api.request.user.identity.ProfileRequest;
import com.discogs.client.api.request.user.identity.ProfileUpdateRequest;
import com.discogs.client.api.request.user.list.ListRequest;
import com.discogs.client.api.request.user.list.UserListsRequest;
import com.discogs.client.api.request.user.submission.SubmissionsRequest;
import com.discogs.client.api.request.user.wantlist.WantListItemAddRequest;
import com.discogs.client.api.request.user.wantlist.WantListItemDeleteRequest;
import com.discogs.client.api.request.user.wantlist.WantListItemUpdateRequest;
import com.discogs.client.api.request.user.wantlist.WantListRequest;
import com.discogs.client.api.response.Response;
import com.discogs.client.auth.oauth.service.OAuthService;
import com.discogs.client.core.user.collection.JsonCollectionFields;
import com.discogs.client.core.user.collection.JsonCollectionFolder;
import com.discogs.client.core.user.collection.JsonCollectionFolders;
import com.discogs.client.core.user.collection.JsonCollectionInstance;
import com.discogs.client.core.user.collection.JsonCollectionItems;
import com.discogs.client.core.user.collection.JsonCollectionValue;
import com.discogs.client.core.user.contribution.JsonContributions;
import com.discogs.client.core.user.identity.JsonIdentity;
import com.discogs.client.core.user.identity.JsonProfile;
import com.discogs.client.core.user.list.JsonList;
import com.discogs.client.core.user.list.JsonUserLists;
import com.discogs.client.core.user.submission.JsonSubmissions;
import com.discogs.client.core.user.wantlist.JsonWantList;
import com.discogs.client.core.user.wantlist.JsonWantListItem;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URI;

import static com.discogs.client.exception.CustomErrorCodes.FEATURE_NOT_SUPPORTED;

/**
 * Implementation of class {@link DiscogsUserClient}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractRestDiscogsClient
 */
@ParametersAreNonnullByDefault
@Log4j2
public class RestDiscogsUserClient extends AbstractRestDiscogsClient implements DiscogsUserClient {

    public RestDiscogsUserClient(URI baseUri, OAuthService oauthService, RestTemplate restTemplate, UserAgentGenerator userAgentGenerator) {
        super(baseUri, oauthService, restTemplate, userAgentGenerator);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonCollectionFolders> getAll(@NonNull final CollectionFoldersRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonCollectionFolders.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonCollectionFolder> get(@NonNull final CollectionFolderRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonCollectionFolder.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonCollectionFolder> create(@NonNull final CollectionFolderCreateRequest request) {
        return post(createUri(request), new HttpEntity<>(request.getBody(), createHeaders(request)), JsonCollectionFolder.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonCollectionFolder> modify(@NonNull final CollectionFolderUpdateRequest request) {
        return post(createUri(request), new HttpEntity<>(request.getBody(), createHeaders(request)), JsonCollectionFolder.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<Void> delete(@NonNull final CollectionFolderDeleteRequest request) {
        return delete(createUri(request), new HttpEntity<>(createHeaders(request)));
    }

    @Nonnull
    @Override
    public Response<JsonCollectionInstance> create(@NonNull final CollectionReleaseAddRequest request) {
        return post(createUri(request), new HttpEntity<>(createHeaders(request)), JsonCollectionInstance.class);
    }

    @Nonnull
    @Override
    public Response<Void> delete(@NonNull final CollectionReleaseDeleteRequest request) {
        return delete(createUri(request), new HttpEntity<>(createHeaders(request)));
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonCollectionFields> getAll(@NonNull final CollectionFieldsRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonCollectionFields.class);
    }

    @NotImplemented
    @Nonnull
    @Override
    public Response<Void> modify(@NonNull final CollectionFieldUpdateRequest request) {
        throw new DiscogsRuntimeException("Operation not implemented !", FEATURE_NOT_SUPPORTED);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonCollectionItems> getAll(@NonNull final CollectionItemsByFolderRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonCollectionItems.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonCollectionItems> getAll(@NonNull final CollectionItemsByReleaseRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonCollectionItems.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonCollectionValue> get(@NonNull final CollectionValueRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonCollectionValue.class);
    }

    //*** IDENTITY ***//

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonIdentity> get(@NonNull final IdentityRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonIdentity.class);
    }

    //*** PROFILE ***//

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonProfile> get(@NonNull final ProfileRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonProfile.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonProfile> modify(@NonNull final ProfileUpdateRequest request) {
        return post(createUri(request), new HttpEntity<>(request.getBody(), createHeaders(request)), JsonProfile.class);
    }

    //*** LIST ***//

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonList> get(@NonNull final ListRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonList.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonUserLists> getAll(@NonNull final UserListsRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonUserLists.class);
    }

    //*** CONTRIBUTIONS ***//

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonContributions> getAll(@NonNull final ContributionsRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonContributions.class);
    }

    //*** SUBMISSIONS ***//

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonSubmissions> getAll(@NonNull final SubmissionsRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonSubmissions.class);
    }

    //*** WANT LIST ***//

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonWantList> get(@NonNull final WantListRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonWantList.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonWantListItem> create(@NonNull final WantListItemAddRequest request) {
        return put(createUri(request), new HttpEntity<>(request.getBody(), createHeaders(request)), JsonWantListItem.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonWantListItem> modify(@NonNull final WantListItemUpdateRequest request) {
        return post(createUri(request), new HttpEntity<>(request.getBody(), createHeaders(request)), JsonWantListItem.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<Void> delete(@NonNull final WantListItemDeleteRequest request) {
        return delete(createUri(request), new HttpEntity<>(createHeaders(request)));
    }
}
