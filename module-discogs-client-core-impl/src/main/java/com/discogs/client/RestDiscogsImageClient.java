package com.discogs.client;

import com.discogs.client.api.DiscogsImageClient;
import com.discogs.client.api.annotations.DiscogsTested;
import com.discogs.client.api.config.UserAgentGenerator;
import com.discogs.client.api.request.image.ImageRequest;
import com.discogs.client.api.response.Response;
import com.discogs.client.auth.oauth.service.OAuthService;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URI;

import static org.springframework.web.util.UriComponentsBuilder.fromUriString;

/**
 * Implementation of class {@link com.discogs.client.api.DiscogsImageClient}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractRestDiscogsClient
 */
@ParametersAreNonnullByDefault
@Log4j2
public class RestDiscogsImageClient extends AbstractRestDiscogsClient implements DiscogsImageClient {

    public RestDiscogsImageClient(URI baseUri, OAuthService oauthService, RestTemplate restTemplate, UserAgentGenerator userAgentGenerator) {
        super(baseUri, oauthService, restTemplate, userAgentGenerator);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<byte[]> get(@NonNull final ImageRequest request) {
        URI uri = fromUriString(request.getUrl()).build().toUri();
        return get(uri, new HttpEntity<>(createHeaders(request)), byte[].class);
    }
}
