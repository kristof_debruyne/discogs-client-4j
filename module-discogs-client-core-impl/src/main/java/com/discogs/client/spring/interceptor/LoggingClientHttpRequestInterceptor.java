package com.discogs.client.spring.interceptor;

import com.discogs.client.api.exception.DiscogsRuntimeException;
import com.discogs.client.exception.CustomErrorCodes;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.json.JSONObject;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpResponse;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import static com.discogs.client.spring.interceptor.UuidClientHttpRequestInterceptor.X_DISCOGS_CLIENT_UUID_HEADER;
import static org.apache.commons.lang3.StringUtils.startsWithIgnoreCase;

/**
 * Logging client request interceptor.
 *
 * - prints request headers
 * - execute request
 * - prints response headers
 * - prints response body (pretty formatted in case of JSON)
 *
 * @author Sikke303
 * @since 1.0
 */
@Order(2)
@ParametersAreNonnullByDefault
@RequiredArgsConstructor
@Log4j2
public class LoggingClientHttpRequestInterceptor extends AbstractClientHttpRequestInterceptor {

    @NonNull
    private final String imageRootUrl;

    @Nonnull
    @Override
    public ClientHttpResponse intercept(@NonNull final HttpRequest request,
                                        @NonNull final byte[] body,
                                        @NonNull final ClientHttpRequestExecution execution) {
        final String uuid = Optional.ofNullable(request.getHeaders().toSingleValueMap().get(X_DISCOGS_CLIENT_UUID_HEADER))
                .orElseThrow(() -> new DiscogsRuntimeException("No request UUID found !", CustomErrorCodes.BUSINESS_ERROR));

        log.trace("Intercepting HTTP {} request: {} (uuid={})", request.getMethod(), request.getURI(), uuid);
        return printRequestHeaders()
                .andThen(execute(body, execution))
                .andThen(printResponseHeaders())
                .andThen(printResponseBody(request.getURI(), request.getMethod()))
                .apply(request);
    }

    @Nonnull
    private Function<HttpRequest, HttpRequest> printRequestHeaders() {
        return (HttpRequest request) -> {
            log.trace("Printing HTTP request headers:");
            request.getHeaders().toSingleValueMap().entrySet().stream()
                    .sorted(Map.Entry.comparingByKey())
                    .forEach(entry -> log.trace("---> {} - {}", entry.getKey(), entry.getValue()));
            return request;
        };
    }

    @Nonnull
    private Function<ClientHttpResponse, ClientHttpResponse> printResponseHeaders() {
        return (ClientHttpResponse response) -> {
            log.trace("Printing HTTP response headers:");
            response.getHeaders().toSingleValueMap().entrySet().stream()
                    .sorted(Map.Entry.comparingByKey())
                    .forEach(entry -> log.trace("<--- {} - {}", entry.getKey(), entry.getValue()));
            return response;
        };
    }

    @Nonnull
    private Function<ClientHttpResponse, ClientHttpResponse> printResponseBody(final URI uri, @Nullable final HttpMethod method) {
        return (ClientHttpResponse response) -> {
            try {
                if(startsWithIgnoreCase(uri.toString(), imageRootUrl)) {
                    log.trace("Captured HTTP {} response for image URL: {}", method, uri);
                } else {
                    log.trace("Captured HTTP {} response: {}", method, prettyFormatResponse(response));
                }
            } catch (IOException ex) {
                log.error(ExceptionUtils.getStackTrace(ex), ex);
                throw new RuntimeException(ex);
            }
            return response;
        };
    }

    @Nonnull
    private String prettyFormatResponse(final ClientHttpResponse response) throws IOException {
        String responseAsString = IOUtils.toString(response.getBody(), StandardCharsets.UTF_8);
        if(Objects.equals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON) && response.getStatusCode() != HttpStatus.NO_CONTENT) {
            return new JSONObject(responseAsString).toString(4);
        }
        return responseAsString;
    }
}
