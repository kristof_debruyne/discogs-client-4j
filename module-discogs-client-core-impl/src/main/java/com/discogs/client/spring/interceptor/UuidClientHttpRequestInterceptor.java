package com.discogs.client.spring.interceptor;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpResponse;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.UUID;

/**
 * UUID client request interceptor.
 *
 * Generate a unique UUID for each request.
 *
 * @author Sikke303
 * @since 1.0
 */
@Order(1)
@ParametersAreNonnullByDefault
@RequiredArgsConstructor
@Log4j2
public class UuidClientHttpRequestInterceptor extends AbstractClientHttpRequestInterceptor {

    static final String X_DISCOGS_CLIENT_UUID_HEADER = "X-Discogs-Client-UUID";

    @Nonnull
    @Override
    public ClientHttpResponse intercept(@NonNull final HttpRequest request,
                                        @NonNull final byte[] body,
                                        @NonNull final ClientHttpRequestExecution execution) {
        if(!request.getHeaders().containsKey(X_DISCOGS_CLIENT_UUID_HEADER)) {
            final UUID uuid = UUID.randomUUID();
            log.trace("Generating UUID [ {} ] for HTTP {} request: {}", uuid, request.getMethod(), request.getURI());
            request.getHeaders().add(X_DISCOGS_CLIENT_UUID_HEADER, uuid.toString());
        }
        return execute(body, execution).apply(request);
    }
}
