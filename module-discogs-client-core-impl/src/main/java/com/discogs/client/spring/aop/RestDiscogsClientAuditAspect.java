package com.discogs.client.spring.aop;

import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.annotation.Nullable;
import java.util.UUID;

/**
 * Audit aspect.
 *
 * @author Sikke303
 * @since v1.0
 */
@Aspect
@Component
@Log4j2
public class RestDiscogsClientAuditAspect {

    @Pointcut(value = "execution(public * com.discogs.client.RestDiscogs*Client.*(..))")
    public void anyRestDiscogsClientPublicMethod() { }

    /**
     * Around advice. <br>
     *
     * @param joinPoint join point
     * @return Object real object
     * @throws Throwable if any error occurs
     */
    @Around(value = "anyRestDiscogsClientPublicMethod()")
    public Object advice(final ProceedingJoinPoint joinPoint) throws Throwable {
        log("\n>>>>>>>>>>>>>>>>>>>>>>>>> EXECUTING HANDLER (REQUEST) -> {}", joinPoint.getSignature().getName());
        StopWatch timer = new StopWatch(UUID.randomUUID().toString());
        try {
            timer.start();
            return joinPoint.proceed(); //INVOKE
        } catch (Exception ex) {
            log.error(ExceptionUtils.getStackTrace(ex), ex);
            throw ex;
        } finally {
            timer.stop();
            log("\n<<<<<<<<<<<<<<<<<<<<<<<<< EXECUTED HANDLER (RESPONSE) <- {} ({} ms)", joinPoint.getSignature().getName(), timer.getTotalTimeMillis());
        }
    }

    /**
     * Logs message on trace level.
     *
     * @param message message (required)
     * @param args args (optional)
     */
    private void log(@NonNull final String message, @Nullable final Object ...args) {
        if(log.isTraceEnabled()) {
            log.trace(message, args);
        }
    }
}
