package com.discogs.client.spring;

import com.discogs.client.spring.config.DiscogsClientConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Spring Boot support.
 *
 * Just add this annotation to your application. <br><br>
 *
 * Be sure you pass the following arguments always (-D): <br>
 * - DISCOGS_CLIENT_APPLICATION_NAME: name of your application (required) <br>
 * - DISCOGS_CLIENT_APPLICATION_KEY: key of your application (required) <br>
 * - DISCOGS_CLIENT_APPLICATION_VERSION: version of your application (required) <br><br>
 *
 * If you intend to use the full OAuth flow, add the following extra arguments: <br>
 * - DISCOGS_CLIENT_CALLBACK_URL: callback url where Discogs will redirect to after authorization <br>
 * - DISCOGS_CLIENT_CONSUMER_KEY: consumer key of your application <br>
 * - DISCOGS_CLIENT_CONSUMER_SECRET: consumer secret of your application <br><br>
 *
 * If you intend to use your personal token, add the following extra arguments: <br>
 * - DISCOGS_CLIENT_PERSONAL_TOKEN: personal token of your account <br><br>
 *
 * Depending on your scenario, pass the accordingly arguments. <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see DiscogsClientConfiguration
 */
@Documented
@Target(value = { ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Import(DiscogsClientConfiguration.class)
public @interface EnableDiscogsClient {

}