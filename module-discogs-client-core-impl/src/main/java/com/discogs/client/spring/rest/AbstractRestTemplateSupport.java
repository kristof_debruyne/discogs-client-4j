package com.discogs.client.spring.rest;

import com.discogs.client.api.response.ErrorResponse;
import com.discogs.client.api.response.GenericResponse;
import com.discogs.client.api.response.RateLimit;
import com.discogs.client.api.response.RateLimits;
import com.discogs.client.api.response.Response;
import com.discogs.client.api.response.TrackedResponse;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URI;
import java.util.Map;

import static com.discogs.client.api.response.RateLimit.RATE_LIMIT;
import static com.discogs.client.api.response.RateLimit.RATE_LIMIT_REMAINING;
import static com.discogs.client.api.response.RateLimit.RATE_LIMIT_USED;
import static java.lang.Integer.parseInt;

/**
 * Rest template support.
 *
 * @author Sikke303
 * @since 1.0
 */
@ParametersAreNonnullByDefault
@Log4j2
public abstract class AbstractRestTemplateSupport {

    @Getter
    private final RestTemplate restTemplate;
    private final boolean lenientErrorHandling;

    /**
     * Constructor
     * @param restTemplate REST template (required)
     */
    protected AbstractRestTemplateSupport(final RestTemplate restTemplate) {
        this(restTemplate, false);
    }

    /**
     * Constructor
     * @param restTemplate REST template (required)
     * @param lenientErrorHandling lenient error handling (true) or not (false)
     */
    protected AbstractRestTemplateSupport(@NonNull final RestTemplate restTemplate, boolean lenientErrorHandling) {
        this.restTemplate = restTemplate;
        this.lenientErrorHandling = lenientErrorHandling;
    }

    /**
     * Executes HTTP get request.
     *
     * @param uri uri (required)
     * @param entity entity (required)
     * @param responseType response type (required)
     * @param <T> body type
     * @return response entity (never null)
     */
    @Nonnull
    protected final <T> Response<T> get(@NonNull final URI uri, @NonNull final HttpEntity<?> entity, @NonNull final Class<T> responseType) {
        return exchange(uri, HttpMethod.GET, entity, responseType);
    }

    /**
     * Executes HTTP put request.
     *
     * @param uri uri (required)
     * @param entity entity (required)
     * @param responseType response type (required)
     * @param <T> body type
     * @return response entity (never null)
     */
    @Nonnull
    protected final <T> Response<T> put(@NonNull final URI uri, @NonNull final HttpEntity<?> entity, @NonNull final Class<T> responseType) {
        return exchange(uri, HttpMethod.PUT, entity, responseType);
    }

    /**
     * Executes HTTP post request.
     *
     * @param uri uri (required)
     * @param entity entity (required)
     * @param responseType response type (required)
     * @param <T> body type
     * @return response entity (never null)
     */
    @Nonnull
    protected final <T> Response<T> post(@NonNull final URI uri, @NonNull final HttpEntity<?> entity, @NonNull final Class<T> responseType) {
        return exchange(uri, HttpMethod.POST, entity, responseType);
    }

    /**
     * Executes HTTP delete request.
     *
     * @param uri uri (required)
     * @param entity entity (required)
     * @return response entity (never null)
     */
    @Nonnull
    protected final Response<Void> delete(@NonNull final URI uri, @NonNull final HttpEntity<?> entity) {
        return exchange(uri, HttpMethod.DELETE, entity, Void.class);
    }

    /**
     * Exchanges request and returns response.
     *
     * @param uri uri (required)
     * @param method method (required)
     * @param entity entity (required)
     * @param responseType response type (required)
     * @param <T> body type
     * @return response entity (never null)
     */
    @Nonnull
    protected final <T> Response<T> exchange(@NonNull final URI uri,
                                             @NonNull final HttpMethod method,
                                             @NonNull final HttpEntity<?> entity,
                                             @NonNull final Class<T> responseType) {
        try {
            //EXCHANGE
            ResponseEntity<T> exchange = restTemplate.exchange(uri, method, entity, responseType);

            //RATE LIMIT
            Map<String, String> headers = exchange.getHeaders().toSingleValueMap();
            if(RateLimit.containsRateLimitHeaders(headers)) {
                RateLimits.RateLimitsBuilder builder = RateLimits.builder()
                        .rateLimit(parseInt((headers.get(RATE_LIMIT.getHeaderName()))))
                        .rateLimitRemaining(parseInt((headers.get(RATE_LIMIT_REMAINING.getHeaderName()))))
                        .rateLimitUsed(parseInt((headers.get(RATE_LIMIT_USED.getHeaderName()))));
                return new TrackedResponse<>(exchange.getStatusCodeValue(), headers, exchange.getBody(), builder.build()); //TRACKED RESPONSE
            } else {
                return new GenericResponse<>(exchange.getStatusCodeValue(), headers, exchange.getBody()); //GENERIC RESPONSE
            }
        } catch (Exception ex) {
            log.error(ExceptionUtils.getStackTrace(ex), ex);
            if(lenientErrorHandling) {
                return new ErrorResponse<>(ex); //ERROR RESPONSE
            }
            throw new RuntimeException(ex);
        }
    }
}
