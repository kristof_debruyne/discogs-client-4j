package com.discogs.client.spring.interceptor;

import com.discogs.client.api.exception.DiscogsRuntimeException;
import com.discogs.client.api.response.RateLimit;
import com.discogs.client.api.response.RateLimits;
import com.discogs.client.api.throttle.ThrottleHandler;
import com.discogs.client.exception.CustomErrorCodes;
import com.discogs.client.throttle.ExtendedThrottleContext;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpResponse;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URI;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

import static com.discogs.client.api.response.RateLimit.RATE_LIMIT;
import static com.discogs.client.api.response.RateLimit.RATE_LIMIT_REMAINING;
import static com.discogs.client.api.response.RateLimit.RATE_LIMIT_USED;
import static com.discogs.client.spring.interceptor.UuidClientHttpRequestInterceptor.X_DISCOGS_CLIENT_UUID_HEADER;
import static java.lang.Integer.parseInt;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

/**
 * Throttle client request interceptor.
 *
 * - pre-throttle: short throttle
 * - execute request
 * - post-throttle: long throttle (rate limit)
 *
 * @author Sikke303
 * @see ThrottleHandler
 * @since 1.0
 */
@Order(3)
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
@Log4j2
public class ThrottleClientHttpRequestInterceptor extends AbstractClientHttpRequestInterceptor {

    @NonNull
    private final ThrottleHandler<ExtendedThrottleContext> throttler;

    @Nonnull
    @Override
    public ClientHttpResponse intercept(@NonNull final HttpRequest request,
                                        @NonNull final byte[] body,
                                        @NonNull final ClientHttpRequestExecution execution) {
        final String uuid = Optional.ofNullable(request.getHeaders().toSingleValueMap().get(X_DISCOGS_CLIENT_UUID_HEADER))
                .orElseThrow(() -> new DiscogsRuntimeException("No request UUID found !", CustomErrorCodes.BUSINESS_ERROR));

        if(requiresThrottling(request.getURI())) {
            log.trace("Throttling HTTP {} request: {} (uuid={})", request.getMethod(), request.getURI(), uuid);
            return preThrottle()
                    .andThen(execute(body, execution))
                    .andThen(postThrottle(request.getHeaders().toSingleValueMap().get(X_DISCOGS_CLIENT_UUID_HEADER)))
                    .apply(request);
        }
        return execute(body, execution).apply(request);
    }

    /**
     * Checks if request requires throttling or not.
     *
     * @param uri request uri (required)
     * @return boolean requires throttling (true) or not (false)
     */
    private boolean requiresThrottling(final URI uri) {
        return !StringUtils.containsIgnoreCase(uri.toString(), "/oauth/");
    }

    @Nonnull
    private Function<HttpRequest, HttpRequest> preThrottle() {
        return (HttpRequest request) -> {
            if(!request.getHeaders().toSingleValueMap().containsKey(AUTHORIZATION)) {
                throttler.throttle(200); //THROTTLE
            }
            return request;
        };
    }

    @Nonnull
    private Function<ClientHttpResponse, ClientHttpResponse> postThrottle(@NonNull String requestUuid) {
        return (ClientHttpResponse response) -> {
            Map<String, String> headers = response.getHeaders().toSingleValueMap();
            if (RateLimit.containsRateLimitHeaders(headers)) {
                log.trace("Rate limit headers detected ! Calculating throttling time for request: {}", requestUuid);
                ExtendedThrottleContext context = ExtendedThrottleContext.builder()
                        .uuid(UUID.fromString(requestUuid))
                        .authorizedRequest(headers.containsKey(AUTHORIZATION))
                        .rateLimits(RateLimits.builder()
                                .rateLimit(parseInt((headers.get(RATE_LIMIT.getHeaderName()))))
                                .rateLimitRemaining(parseInt((headers.get(RATE_LIMIT_REMAINING.getHeaderName()))))
                                .rateLimitUsed(parseInt((headers.get(RATE_LIMIT_USED.getHeaderName()))))
                                .build()
                        )
                        .build();

                throttler.calculateAndThrottle(context); //THROTTLE
            }
            return response;
        };
    }
}
