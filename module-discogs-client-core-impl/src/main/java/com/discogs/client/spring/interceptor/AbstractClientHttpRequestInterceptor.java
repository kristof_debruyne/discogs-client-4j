package com.discogs.client.spring.interceptor;

import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.io.IOException;
import java.util.function.Function;

/**
 * Abstract client request interceptor.
 *
 * @author Sikke303
 * @since 1.0
 * @see ClientHttpRequestInterceptor
 */
@ParametersAreNonnullByDefault
@Log4j2
abstract class AbstractClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {

    @Nonnull
    protected final Function<HttpRequest, ClientHttpResponse> execute(@NonNull final byte[] body,
                                                                      @NonNull final ClientHttpRequestExecution execution) {
        return (HttpRequest request) -> {
            try {
                return execution.execute(request, body); //EXECUTE
            } catch (IOException ex) {
                log.error(ExceptionUtils.getStackTrace(ex), ex);
                throw new RuntimeException(ex);
            }
        };
    }
}
