package com.discogs.client.spring.config;

import com.discogs.client.api.config.DiscogsProperties;
import com.discogs.client.api.exception.DiscogsException;
import com.discogs.client.api.throttle.ThrottleMode;
import com.discogs.client.config.DefaultDiscogsProperties;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Discogs client properties configuration. <br><br>
 *
 * Following properties files will be loaded: <br>
 * - discogs-client.properties: contains all client information (dynamic properties) <br>
 * - discogs-server.properties: contains all server information (static properties) <br><br>
 *
 * The client properties should be all passed as argument (-D)
 *
 * @author Sikke303
 * @since 1.0
 */
@Configuration
@PropertySource(value = { "classpath:discogs-client.properties", "classpath:discogs-server.properties" })
@Log4j2
class DiscogsClientPropertiesConfiguration {

    //*** CLIENT PROPERTIES ***//
    @Value("${discogs.client.application.name}")
    private String applicationName;

    @Value("${discogs.client.application.key}")
    private String applicationKey;

    @Value("${discogs.client.application.version}")
    private String applicationVersion;

    @Value("${discogs.client.callback.url}")
    private String callbackUrl;

    @Value("${discogs.client.consumer.key}")
    private String consumerKey;

    @Value("${discogs.client.consumer.secret}")
    private String consumerSecret;

    @Value("${discogs.client.personal.token}")
    private String personalToken;

    //*** SERVER PROPERTIES ***//
    @Value("${discogs.server.api.version}")
    private String apiVersion;

    @Value("${discogs.server.api.url}")
    private String apiRootUrl;

    @Value("${discogs.server.web.url}")
    private String webRootUrl;

    @Value("${discogs.server.web.image.url}")
    private String webImageRootUrl;

    @Value("${discogs.server.access.token.endpoint}")
    private String accessTokenEndpoint;

    @Value("${discogs.server.authorize.endpoint}")
    private String authorizeEndpoint;

    @Value("${discogs.server.request.token.endpoint}")
    private String requestTokenEndpoint;

    @Value("${discogs.server.revoke.token.endpoint}")
    private String revokeTokenEndpoint;

    @Value("${discogs.server.connection.timeout:10000}")
    private int connectionTimeout;

    @Value("${discogs.server.read.timeout:15000}")
    private int readTimeout;

    @Value("${discogs.server.throttling.enabled:true}")
    private boolean throttlingRequestsEnabled;

    @Value("${discogs.server.throttling.cool-down:5000}")
    private long throttleCoolDownPeriod;

    @Value("${discogs.server.throttling.mode:NORMAL}")
    private String throttleMode;

    @Bean
    public DiscogsProperties discogsProperties() throws DiscogsException {
        final DefaultDiscogsProperties.DefaultDiscogsPropertiesBuilder builder = DefaultDiscogsProperties.builder();

        //CLIENT
        builder.applicationName(applicationName);
        builder.applicationKey(applicationKey);
        builder.applicationVersion(applicationVersion);
        builder.callbackUrl(callbackUrl);
        builder.consumerKey(consumerKey);
        builder.consumerSecret(consumerSecret);
        builder.personalToken(personalToken);

        //SERVER
        builder.apiVersion(apiVersion);
        builder.apiRootUrl(apiRootUrl);
        builder.webRootUrl(webRootUrl);
        builder.imageRootUrl(webImageRootUrl);
        builder.authorizeEndpoint(authorizeEndpoint);
        builder.accessTokenEndpoint(accessTokenEndpoint);
        builder.requestTokenEndpoint(requestTokenEndpoint);
        builder.revokeTokenEndpoint(revokeTokenEndpoint);
        builder.connectionTimeout(connectionTimeout);
        builder.readTimeout(readTimeout);

        builder.throttlingRequestsEnabled(throttlingRequestsEnabled);
        if(throttlingRequestsEnabled) {
            builder.throttleCoolDownTimePeriod(throttleCoolDownPeriod);
            builder.throttleMode(ThrottleMode.valueOf(throttleMode));
        }
        return builder.build();
    }
}
