package com.discogs.client.spring.config;

import com.discogs.client.DefaultDiscogsPlatform;
import com.discogs.client.DefaultDiscogsWebClient;
import com.discogs.client.RestDiscogsDatabaseClient;
import com.discogs.client.RestDiscogsImageClient;
import com.discogs.client.RestDiscogsInventoryClient;
import com.discogs.client.RestDiscogsJsonpClient;
import com.discogs.client.RestDiscogsMarketplaceClient;
import com.discogs.client.RestDiscogsRootClient;
import com.discogs.client.RestDiscogsUserClient;
import com.discogs.client.api.DiscogsDatabaseClient;
import com.discogs.client.api.DiscogsImageClient;
import com.discogs.client.api.DiscogsInventoryClient;
import com.discogs.client.api.DiscogsJsonpClient;
import com.discogs.client.api.DiscogsMarketplaceClient;
import com.discogs.client.api.DiscogsPlatform;
import com.discogs.client.api.DiscogsRootClient;
import com.discogs.client.api.DiscogsUserClient;
import com.discogs.client.api.DiscogsWebClient;
import com.discogs.client.api.config.DiscogsConfigurable;
import com.discogs.client.api.config.DiscogsContext;
import com.discogs.client.api.config.DiscogsLocator;
import com.discogs.client.api.config.DiscogsProperties;
import com.discogs.client.api.config.UserAgentGenerator;
import com.discogs.client.api.throttle.ThrottleHandler;
import com.discogs.client.auth.oauth.config.OAuthClientConfiguration;
import com.discogs.client.auth.oauth.config.OAuthConfigurable;
import com.discogs.client.auth.oauth.config.OAuthContext;
import com.discogs.client.auth.oauth.config.OAuthContextBuilder;
import com.discogs.client.auth.oauth.config.OAuthScope;
import com.discogs.client.auth.oauth.config.OAuthServerConfiguration;
import com.discogs.client.auth.oauth.service.OAuthService;
import com.discogs.client.auth.oauth.service.OAuthV1Service;
import com.discogs.client.config.DefaultDiscogsContext;
import com.discogs.client.config.DefaultDiscogsLocator;
import com.discogs.client.config.DefaultUserAgentGenerator;
import com.discogs.client.security.DiscogsOAuthV1Service;
import com.discogs.client.spring.interceptor.LoggingClientHttpRequestInterceptor;
import com.discogs.client.spring.interceptor.ThrottleClientHttpRequestInterceptor;
import com.discogs.client.spring.interceptor.UuidClientHttpRequestInterceptor;
import com.discogs.client.throttle.DefaultThrottleHandler;
import com.discogs.client.throttle.ExtendedThrottleContext;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static com.discogs.client.auth.oauth.config.OAuthScope.CALLBACK_URL;
import static com.discogs.client.auth.oauth.config.OAuthScope.OUT_OF_BAND;
import static java.util.concurrent.Executors.newSingleThreadScheduledExecutor;

/**
 * Discogs client configuration.
 *
 * @author Sikke303
 * @since 1.0
 * @see DiscogsConfigurable
 * @see OAuthConfigurable
 */
@Configuration
@ComponentScan("com.discogs.client.spring.aop")
@EnableAspectJAutoProxy(proxyTargetClass = true)
@Import(DiscogsClientPropertiesConfiguration.class)
@Log4j2
public class DiscogsClientConfiguration implements DiscogsConfigurable, OAuthConfigurable {

    @Autowired
    @Getter
    private DiscogsProperties properties;

    @PostConstruct
    public void init() {
        log.info("Enabling Discogs client configuration ...");
    }

    @Nonnull
    @Override
    public OAuthScope getOAuthScope() {
        return properties.hasCallbackUrl() ? CALLBACK_URL : OUT_OF_BAND;
    }

    @Nonnull
    @Override
    public OAuthContext createOAuthContext() {
        return OAuthContextBuilder.builder()
                .withClientConfiguration(createOAuthClientConfiguration()) //CLIENT CONFIG
                .withServerConfiguration(createOAuthServerConfiguration()) //SERVER CONFIG
                .withSignatureService(signatureService())
                .withTokenParser(tokenParser())
                .withVerifierParser(verifierParser())
                .build();
    }

    @Nonnull
    @Override
    public OAuthClientConfiguration createOAuthClientConfiguration() {
        return OAuthClientConfiguration.builder()
                .applicationName(properties.getApplicationName())
                .applicationKey(properties.getApplicationKey())
                .applicationVersion(properties.getApplicationVersion())
                .callbackUrl(properties.getCallbackUrl())
                .consumerKey(properties.getConsumerKey())
                .consumerSecret(properties.getConsumerSecret())
                .personalToken(properties.getPersonalToken())
                .scope(getOAuthScope())
                .build();
    }

    @Nonnull
    @Override
    public OAuthServerConfiguration createOAuthServerConfiguration() {
        return OAuthServerConfiguration.builder()
                .apiVersion(properties.getApiVersion())
                .apiRootUrl(properties.getApiRootUrl())
                .webRootUrl(properties.getWebRootUrl())
                .imageRootUrl(properties.getImageRootUrl())
                .accessTokenEndpoint(properties.getAccessTokenEndpoint())
                .authorizeEndpoint(properties.getAuthorizeEndpoint())
                .requestTokenEndpoint(properties.getRequestTokenEndpoint())
                .revokeTokenEndpoint(properties.getRevokeTokenEndpoint())
                .build();
    }

    //*** DISCOGS ***//

    @Bean
    @Override
    public DiscogsContext<OAuthContext> initializeDiscogsContext() {
        log.trace("Initializing Discogs context ...");
        return new DefaultDiscogsContext(createOAuthContext());
    }

    @Bean
    public UserAgentGenerator discogsUserAgentGenerator(@Autowired DiscogsContext<OAuthContext> context) {
        log.trace("Creating Discogs user agent generator ...");
        return new DefaultUserAgentGenerator(context);
    }

    @Bean
    public DiscogsLocator discogsLocator(@Autowired ApplicationContext applicationContext) {
        log.trace("Creating Discogs locator ...");
        return new DefaultDiscogsLocator(applicationContext);
    }

    @Bean
    public DiscogsPlatform discogsPlatform(@Autowired DiscogsContext<OAuthContext> context, @Autowired DiscogsLocator locator) {
        log.trace("Creating Discogs platform ...");
        return new DefaultDiscogsPlatform(context, locator);
    }

    //*** SERVICES ***//

    @Bean
    public OAuthV1Service discogsOAuthV1Service(@Autowired DiscogsContext<OAuthContext> context,
                                                @Autowired @Qualifier("discogs") RestTemplate template,
                                                @Autowired UserAgentGenerator userAgentGenerator) {
        log.trace("Creating Discogs OAuth v1 service ...");
        return new DiscogsOAuthV1Service(context.getAuthenticationContext(), template, userAgentGenerator);
    }

    //*** CLIENTS ***//

    @Bean
    public DiscogsDatabaseClient discogsDatabaseClient(@Autowired DiscogsContext<OAuthContext> context,
                                                       @Autowired OAuthService service,
                                                       @Autowired @Qualifier("discogs") RestTemplate template,
                                                       @Autowired UserAgentGenerator userAgentGenerator) {
        log.trace("Creating database REST client ...");
        return new RestDiscogsDatabaseClient(URI.create(context.getApiRootUrl()), service, template, userAgentGenerator);
    }

    @Bean
    public DiscogsImageClient discogsImageClient(@Autowired DiscogsContext<OAuthContext> context,
                                                 @Autowired OAuthService service,
                                                 @Autowired @Qualifier("discogs") RestTemplate template,
                                                 @Autowired UserAgentGenerator userAgentGenerator) {
        log.trace("Creating image REST client ...");
        return new RestDiscogsImageClient(URI.create(context.getImageRootUrl()), service, template, userAgentGenerator);
    }

    @Bean
    public DiscogsInventoryClient discogsInventoryClient(@Autowired DiscogsContext<OAuthContext> context,
                                                         @Autowired OAuthService service,
                                                         @Autowired @Qualifier("discogs") RestTemplate template,
                                                         @Autowired UserAgentGenerator userAgentGenerator) {
        log.trace("Creating inventory REST client ...");
        return new RestDiscogsInventoryClient(URI.create(context.getApiRootUrl()), service, template, userAgentGenerator);
    }

    @Bean
    public DiscogsJsonpClient discogsJsonpClient(@Autowired DiscogsContext<OAuthContext> context,
                                                 @Autowired OAuthService service,
                                                 @Autowired @Qualifier("discogs") RestTemplate template,
                                                 @Autowired UserAgentGenerator userAgentGenerator) {
        log.trace("Creating jsonp REST client ...");
        return new RestDiscogsJsonpClient(URI.create(context.getApiRootUrl()), service, template, userAgentGenerator);
    }

    @Bean
    public DiscogsMarketplaceClient discogsMarketplaceClient(@Autowired DiscogsContext<OAuthContext> context,
                                                             @Autowired OAuthService service,
                                                             @Autowired @Qualifier("discogs") RestTemplate template,
                                                             @Autowired UserAgentGenerator userAgentGenerator) {
        log.trace("Creating marketplace REST client ...");
        return new RestDiscogsMarketplaceClient(URI.create(context.getApiRootUrl()), service, template, userAgentGenerator);
    }

    @Bean
    public DiscogsRootClient discogsRootClient(@Autowired DiscogsContext<OAuthContext> context,
                                               @Autowired OAuthService service,
                                               @Autowired @Qualifier("discogs") RestTemplate template,
                                               @Autowired UserAgentGenerator userAgentGenerator) {
        log.trace("Creating root REST client ...");
        return new RestDiscogsRootClient(URI.create(context.getApiRootUrl()), service, template, userAgentGenerator);
    }

    @Bean
    public DiscogsUserClient discogsUserClient(@Autowired DiscogsContext<OAuthContext> context,
                                               @Autowired OAuthService service,
                                               @Autowired @Qualifier("discogs") RestTemplate template,
                                               @Autowired UserAgentGenerator userAgentGenerator) {
        log.trace("Creating user REST client ...");
        return new RestDiscogsUserClient(URI.create(context.getApiRootUrl()), service, template, userAgentGenerator);
    }

    @Bean
    public DiscogsWebClient discogsWebClient(@Autowired DiscogsContext<OAuthContext> context) {
        log.trace("Creating web client ...");
        return new DefaultDiscogsWebClient(URI.create(context.getWebRootUrl()));
    }

    //*** REST TEMPLATES ***//

    @Qualifier("discogs")
    @Bean
    public RestTemplate discogsRestTemplate() {
        log.trace("Creating Discogs REST template instance ...");
        final RestTemplate template = new RestTemplate(createClientHttpRequestFactory());
        //INTERCEPTORS
        final List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors.add(new UuidClientHttpRequestInterceptor());
        interceptors.add(new LoggingClientHttpRequestInterceptor(properties.getImageRootUrl()));
        if(properties.isThrottlingRequestsEnabled()) {
            log.trace("Enabling throttling with mode: {}", properties.getThrottleMode());
            interceptors.add(new ThrottleClientHttpRequestInterceptor(discogsThrottler()));
        }
        template.setInterceptors(interceptors);
        return template;
    }

    @Nonnull
    private ClientHttpRequestFactory createClientHttpRequestFactory() {
        final HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setConnectionRequestTimeout(properties.getConnectionTimeout());
        factory.setConnectTimeout(properties.getConnectionTimeout());
        return new BufferingClientHttpRequestFactory(factory);
    }

    @Nonnull
    private ThrottleHandler<ExtendedThrottleContext> discogsThrottler() {
        return new DefaultThrottleHandler(newSingleThreadScheduledExecutor(), properties.getThrottleMode(), properties.getThrottleCoolDownTimePeriod());
    }
}
