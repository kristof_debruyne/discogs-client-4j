package com.discogs.client;

import com.discogs.client.api.DiscogsJsonpClient;
import com.discogs.client.api.config.UserAgentGenerator;
import com.discogs.client.api.request.PayloadRequest;
import com.discogs.client.api.request.Request;
import com.discogs.client.api.request.image.ImageRequest;
import com.discogs.client.api.response.Response;
import com.discogs.client.auth.oauth.service.OAuthService;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.Validate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URI;
import java.util.Optional;

/**
 * Implementation of class {@link com.discogs.client.api.DiscogsJsonpClient}.
 *
 * @author Sikke303
 * @since 1.2
 * @see AbstractRestDiscogsClient
 */
@ParametersAreNonnullByDefault
@Log4j2
public class RestDiscogsJsonpClient extends AbstractRestDiscogsClient implements DiscogsJsonpClient {

    private static final String CONTENT_TYPE_JAVASCRIPT = "text/javascript";

    public RestDiscogsJsonpClient(URI baseUri, OAuthService oauthService, RestTemplate restTemplate, UserAgentGenerator userAgentGenerator) {
        super(baseUri, oauthService, restTemplate, userAgentGenerator);
    }

    @Nonnull
    @Override
    public <T extends Request> Response<String> execute(@NonNull final String method, @NonNull final T request) {
        validateIfRequestIsJsonpCompliant(request);

        HttpMethod httpMethod = HttpMethod.valueOf(method.toUpperCase());

        URI uri = createUri(request);
        if(log.isTraceEnabled()) {
            log.trace("Executing JSONP {} request: {}", method, uri.toString());
        }
        return Optional.of(request)
                .filter(PayloadRequest.class::isInstance)
                .map(PayloadRequest.class::cast)
                .map(payloadRequest -> exchange(uri, httpMethod, new HttpEntity<>(payloadRequest.getBody(), createHeaders(request)), String.class))
                .orElse(exchange(uri, httpMethod, new HttpEntity<>(createHeaders(request)), String.class));
    }

    /**
     * Validate request for compliancy.
     *
     * Invalid if:
     * - no callback registered
     * - content type is not 'text/javascript'
     * - image request
     *
     * @param request request (required)
     * @param <T> request type
     */
    private <T extends Request> void validateIfRequestIsJsonpCompliant(T request) {
        Validate.isTrue(request.isJsonpRequest(), "Request should be of type JSONP.");
        Validate.isTrue(request.getContentType().equals(CONTENT_TYPE_JAVASCRIPT), "Content type should be 'text/javascript' for JSONP.");

        if(request instanceof ImageRequest) {
            throw new UnsupportedOperationException("Requesting images is not supported in this client ! Use DiscogsImageClient for this ...");
        }
    }
}
