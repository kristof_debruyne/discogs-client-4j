package com.discogs.client;

import com.discogs.client.api.DiscogsDatabaseClient;
import com.discogs.client.api.annotations.DiscogsTested;
import com.discogs.client.api.config.UserAgentGenerator;
import com.discogs.client.api.request.database.ArtistReleasesRequest;
import com.discogs.client.api.request.database.ArtistRequest;
import com.discogs.client.api.request.database.LabelReleasesRequest;
import com.discogs.client.api.request.database.LabelRequest;
import com.discogs.client.api.request.database.MasterReleaseRequest;
import com.discogs.client.api.request.database.MasterVersionsRequest;
import com.discogs.client.api.request.database.ReleaseCommunityRatingRequest;
import com.discogs.client.api.request.database.ReleaseRequest;
import com.discogs.client.api.request.database.ReleaseUserRatingDeleteRequest;
import com.discogs.client.api.request.database.ReleaseUserRatingRequest;
import com.discogs.client.api.request.database.ReleaseUserRatingUpdateRequest;
import com.discogs.client.api.request.database.SearchRequest;
import com.discogs.client.api.response.Response;
import com.discogs.client.auth.oauth.service.OAuthService;
import com.discogs.client.core.database.JsonArtist;
import com.discogs.client.core.database.JsonArtistReleases;
import com.discogs.client.core.database.JsonLabel;
import com.discogs.client.core.database.JsonLabelReleases;
import com.discogs.client.core.database.JsonMasterRelease;
import com.discogs.client.core.database.JsonMasterVersions;
import com.discogs.client.core.database.JsonRelease;
import com.discogs.client.core.database.JsonReleaseCommunityRating;
import com.discogs.client.core.database.JsonReleaseUserRating;
import com.discogs.client.core.database.JsonSearchResult;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URI;

/**
 * Implementation of class {@link DiscogsDatabaseClient}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractRestDiscogsClient
 */
@ParametersAreNonnullByDefault
@Log4j2
public class RestDiscogsDatabaseClient extends AbstractRestDiscogsClient implements DiscogsDatabaseClient {

    public RestDiscogsDatabaseClient(URI baseUri, OAuthService oauthService, RestTemplate restTemplate, UserAgentGenerator userAgentGenerator) {
        super(baseUri, oauthService, restTemplate, userAgentGenerator);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonArtist> get(@NonNull final ArtistRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonArtist.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonArtistReleases> getAll(@NonNull final ArtistReleasesRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonArtistReleases.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonLabel> get(@NonNull final LabelRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonLabel.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonLabelReleases> getAll(@NonNull final LabelReleasesRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonLabelReleases.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonMasterRelease> get(@NonNull final MasterReleaseRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonMasterRelease.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonMasterVersions> getAll(@NonNull final MasterVersionsRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonMasterVersions.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonRelease> get(@NonNull final ReleaseRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonRelease.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonReleaseCommunityRating> get(@NonNull final ReleaseCommunityRatingRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonReleaseCommunityRating.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonReleaseUserRating> get(@NonNull final ReleaseUserRatingRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonReleaseUserRating.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonReleaseUserRating> modify(@NonNull final ReleaseUserRatingUpdateRequest request) {
        return put(createUri(request), new HttpEntity<>(request.getBody(), createHeaders(request)), JsonReleaseUserRating.class);
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<Void> delete(@NonNull final ReleaseUserRatingDeleteRequest request) {
        return delete(createUri(request), new HttpEntity<>(createHeaders(request)));
    }

    @DiscogsTested
    @Nonnull
    @Override
    public Response<JsonSearchResult> search(@NonNull final SearchRequest request) {
        return get(createUri(request), new HttpEntity<>(createHeaders(request)), JsonSearchResult.class);
    }
}
