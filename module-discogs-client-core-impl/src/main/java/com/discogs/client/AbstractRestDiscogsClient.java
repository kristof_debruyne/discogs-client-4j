package com.discogs.client;

import com.discogs.client.api.DiscogsClient;
import com.discogs.client.api.config.UserAgentGenerator;
import com.discogs.client.api.exception.DiscogsRuntimeException;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.MultipartRequest;
import com.discogs.client.api.request.Request;
import com.discogs.client.auth.api.Authentication;
import com.discogs.client.auth.oauth.exception.OAuthException;
import com.discogs.client.auth.oauth.service.OAuthService;
import com.discogs.client.auth.oauth.token.OAuthGeneratedToken;
import com.discogs.client.auth.oauth.token.OAuthToken;
import com.discogs.client.spring.rest.AbstractRestTemplateSupport;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URI;
import java.util.Map;
import java.util.Optional;

import static com.discogs.client.exception.CustomErrorCodes.SECURITY_ERROR;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpHeaders.CONTENT_LENGTH;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpHeaders.USER_AGENT;
import static org.springframework.web.util.UriComponentsBuilder.fromUri;

/**
 * Abstract Discogs client.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractRestTemplateSupport
 */
@ParametersAreNonnullByDefault
@Log4j2
abstract class AbstractRestDiscogsClient extends AbstractRestTemplateSupport implements DiscogsClient {

    private static final String CONTENT_DISPOSITION_ATTRIBUTE_FILE = "file";
    private static final String CONTENT_DISPOSITION_TYPE = "form-data";

    @Getter
    private final URI baseUri;
    private final OAuthService oauthService;
    private final UserAgentGenerator userAgentGenerator;

    /**
     * Constructor
     *
     * @param baseUri base URI (required)
     * @param oauthService OAuth service (required)
     * @param template REST template (required)
     * @param userAgentGenerator user agent generator (required)
     */
    AbstractRestDiscogsClient(@NonNull URI baseUri,
                              @NonNull OAuthService oauthService,
                              @NonNull RestTemplate template,
                              @NonNull UserAgentGenerator userAgentGenerator) {
        super(template);
        this.baseUri = requireNonNull(baseUri);
        this.oauthService = requireNonNull(oauthService);
        this.userAgentGenerator = userAgentGenerator;
    }

    /**
     * Creates URI for given request.
     *
     * @param request request (required)
     * @param <T> request type
     * @return uri (never null)
     */
    @Nonnull
    protected final <T extends Request> URI createUri(@NonNull final T request) {
        UriComponentsBuilder builder = fromUri(getBaseUri()).path(request.getPath());
        Map<String, Object> queryParams = request.getQueryParams();
        if(!queryParams.isEmpty()) {
            queryParams.forEach(builder::queryParam);
        }
        Object[] pathVariables = request.getPathVariables();
        if(isNotEmpty(pathVariables)) {
            return builder.buildAndExpand(pathVariables).toUri();
        }
        return builder.build().toUri();
    }

    /**
     * Creates headers. <br><br>
     *
     * See {@link AbstractRestDiscogsClient#createHeaders(Authentication, AcceptMediaType, MediaType)} <br>
     * for more information which headers are passed.
     *
     * @param request request (required)
     * @param <T> request type
     * @return http headers (never null)
     */
    @Nonnull
    protected final <T extends Request> HttpHeaders createHeaders(@NonNull final T request) {
        if(request instanceof MultipartRequest) {
            return createHeaders(request.getAuthentication(), request.getMediaType(), MediaType.MULTIPART_FORM_DATA);
        }
        return createHeaders(request.getAuthentication(), request.getMediaType(), MediaType.valueOf(request.getContentType()));
    }

    /**
     * Creates headers.
     *
     * - Accept: media type (optional)
     * - Authorization: authentication string (required for authorized resources)
     * - Content-Type: content type (required)
     * - User-Agent: unique user agent (required)
     *
     * @param authentication token (required)
     * @param acceptMediaType accept media type (required)
     * @param mediaType media type (required)
     * @return http headers (never null)
     */
    @Nonnull
    private HttpHeaders createHeaders(@NonNull final Authentication authentication,
                                      @NonNull final AcceptMediaType acceptMediaType,
                                      @NonNull final MediaType mediaType) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType); //CONTENT TYPE

        //ACCEPT HEADER
        if(nonNull(acceptMediaType.getValue())) {
            headers.add(ACCEPT, acceptMediaType.getValue());
        }

        //AUTHORIZATION HEADER
        generateAuthorizationHeaderValue(authentication).ifPresent(value -> headers.add(AUTHORIZATION, value));

        //USER AGENT HEADER
        headers.add(USER_AGENT, userAgentGenerator.generate());

        log.trace("Collected request headers: {}", headers);
        return headers;
    }

    /**
     * Generates the 'Authorization' header value.
     *
     * Discogs supports:
     * - auto generated token: pass the token in header
     * - full token: sign the request properly
     *
     * @param authentication authentication (required)
     * @return optional authorization value (never null)
     * @throws DiscogsRuntimeException if signing request fails
     */
    private Optional<String> generateAuthorizationHeaderValue(final Authentication authentication) {
        if(authentication instanceof OAuthGeneratedToken) {
            OAuthGeneratedToken generatedToken = (OAuthGeneratedToken) authentication;
            return Optional.of("Discogs token=".concat(generatedToken.getToken()));
        }
        if(authentication instanceof OAuthToken) {
            OAuthToken token = (OAuthToken) authentication;;
            try {
                return Optional.of(oauthService.signRequest(token).asOAuthString());
            } catch (OAuthException ex) {
                log.error(ExceptionUtils.getStackTrace(ex), ex);
                throw new DiscogsRuntimeException("Signing request has failed !", SECURITY_ERROR);
            }
        }
        return Optional.empty();
    }

    /**
     * Creates multipart body.
     *
     * @param request request (required)
     * @return multipart body (never null)
     */
    @Nonnull
    protected final MultiValueMap<String, Object> createMultiPartBody(@NonNull final MultipartRequest request) {
        //HEADERS
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(CONTENT_DISPOSITION, ContentDisposition.builder(CONTENT_DISPOSITION_TYPE)
                .name(CONTENT_DISPOSITION_ATTRIBUTE_FILE)
                .filename(request.getFilename())
                .build()
                .toString()
        );
        headers.add(CONTENT_LENGTH, request.getContentLength());
        headers.add(CONTENT_TYPE, request.getContentType());

        //BODY
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add(CONTENT_DISPOSITION_ATTRIBUTE_FILE, new HttpEntity<>(request.getPayload(), headers));
        return body;
    }
}
