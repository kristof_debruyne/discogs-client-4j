package com.discogs.client.security;

import com.discogs.client.api.DiscogsService;
import com.discogs.client.api.config.UserAgentGenerator;
import com.discogs.client.api.response.Response;
import com.discogs.client.auth.oauth.OAuthVersion;
import com.discogs.client.auth.oauth.config.OAuthContext;
import com.discogs.client.auth.oauth.config.OAuthScope;
import com.discogs.client.auth.oauth.exception.OAuthException;
import com.discogs.client.auth.oauth.exception.OAuthRuntimeException;
import com.discogs.client.auth.oauth.request.OAuthRequest;
import com.discogs.client.auth.oauth.service.OAuthV1Service;
import com.discogs.client.auth.oauth.signature.SignatureRequest;
import com.discogs.client.auth.oauth.token.OAuthAuthentication;
import com.discogs.client.auth.oauth.token.OAuthEmptyToken;
import com.discogs.client.auth.oauth.token.OAuthGeneratedToken;
import com.discogs.client.auth.oauth.token.OAuthToken;
import com.discogs.client.auth.oauth.verifier.OAuthVerifier;
import com.discogs.client.spring.rest.AbstractRestTemplateSupport;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URI;

import static com.discogs.client.auth.oauth.OAuthConstants.OAUTH_PARAM_ACCESS_KEY;
import static com.discogs.client.auth.oauth.OAuthConstants.OAUTH_PARAM_CALLBACK;
import static com.discogs.client.auth.oauth.OAuthConstants.OAUTH_PARAM_CONSUMER_KEY;
import static com.discogs.client.auth.oauth.OAuthConstants.OAUTH_PARAM_NONCE;
import static com.discogs.client.auth.oauth.OAuthConstants.OAUTH_PARAM_SIGNATURE;
import static com.discogs.client.auth.oauth.OAuthConstants.OAUTH_PARAM_SIGNATURE_METHOD;
import static com.discogs.client.auth.oauth.OAuthConstants.OAUTH_PARAM_TIMESTAMP;
import static com.discogs.client.auth.oauth.OAuthConstants.OAUTH_PARAM_TOKEN;
import static com.discogs.client.auth.oauth.OAuthConstants.OAUTH_PARAM_TOKEN_SECRET;
import static com.discogs.client.auth.oauth.OAuthConstants.OAUTH_PARAM_VERIFIER;
import static com.discogs.client.auth.oauth.OAuthConstants.OAUTH_PARAM_VERSION;
import static com.discogs.client.auth.oauth.OAuthConstants.SCOPE;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.USER_AGENT;
import static org.springframework.web.util.UriComponentsBuilder.fromUriString;

/**
 * Discogs OAuth service implementation. <br><br>
 *
 * Discogs currently implements the OAuth v1.0a specification. <br>
 * Therefore, this service implements the {@link OAuthV1Service} interface. <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see OAuthV1Service
 */
@ParametersAreNonnullByDefault
@Log4j2
public final class DiscogsOAuthV1Service extends AbstractRestTemplateSupport implements DiscogsService, OAuthV1Service {

    private final OAuthContext oauthContext;
    private final UserAgentGenerator userAgentGenerator;

    public DiscogsOAuthV1Service(@NonNull OAuthContext oauthContext,
                                 @NonNull RestTemplate restTemplate,
                                 @NonNull UserAgentGenerator userAgentGenerator) {
        super(restTemplate);
        this.oauthContext = oauthContext;
        this.userAgentGenerator = userAgentGenerator;
    }

    @Nonnull
    @Override
    public OAuthVersion getOAuthVersion() {
        return OAuthVersion.V1;
    }

    @Nonnull
    @Override
    public OAuthToken getAccessToken(@NonNull final OAuthToken requestToken, @NonNull final OAuthVerifier verifier) throws OAuthException {
        final URI uri = fromUriString(oauthContext.getServerConfiguration().getAccessTokenEndpoint()).build().toUri();
        log.debug("Obtaining access token from endpoint: {}", uri);

        OAuthRequest request = newOAuthRequest(requestToken.getSecret())
                .addOAuthParameter(OAUTH_PARAM_TOKEN, requestToken.getToken())
                .addOAuthParameter(OAUTH_PARAM_VERIFIER, verifier.getValue());

        return parseBody(post(uri, new HttpEntity<>(createDefaultHeaders(request.asOAuthString())), String.class));
    }

    @Nonnull
    @Override
    public OAuthToken getRequestToken() throws OAuthException {
        final URI uri = fromUriString(oauthContext.getServerConfiguration().getRequestTokenEndpoint()).build().toUri();
        log.debug("Obtaining request token from endpoint: {}", uri);

        OAuthRequest request = newOAuthRequest(null);
        if(OAuthScope.CALLBACK_URL == oauthContext.getClientConfiguration().getScope()) {
            request.addOAuthParameter(OAUTH_PARAM_CALLBACK, oauthContext.getClientConfiguration().getCallbackUrl());
        } else if(OAuthScope.OUT_OF_BAND == oauthContext.getClientConfiguration().getScope()) {
            request.addOAuthParameter(SCOPE, OAuthScope.OUT_OF_BAND.getValue());
        }
        return parseBody(get(uri, new HttpEntity<>(createDefaultHeaders(request.asOAuthString())), String.class));
    }

    @Nonnull
    @Override
    public String getAuthorizedUrl(@NonNull final OAuthToken requestToken) {
        validateAuthentication(requestToken, true);
        log.debug("Creating authorized URL for request token: {}", requestToken.getToken());
        final String url = fromUriString(oauthContext.getServerConfiguration().getAuthorizeEndpoint())
                .queryParam(OAUTH_PARAM_TOKEN, requestToken.getToken())
                .toUriString();
        log.debug("Created authorized URL: {}", url);
        return url;
    }

    @Nonnull
    @Override
    public String getRevokeTokenUrl(@NonNull final OAuthToken accessToken) {
        validateAuthentication(accessToken, false);
        log.debug("Creating revoke token URL for access token: {}", accessToken.getToken());
        final String url = fromUriString(oauthContext.getServerConfiguration().getRevokeTokenEndpoint())
                .queryParam(OAUTH_PARAM_ACCESS_KEY, accessToken.getToken())
                .toUriString();
        log.debug("Created revoke URL: {}", url);
        return url;
    }

    /**
     * Creates signature.
     *
     * @param consumerSecret consumer secret (required)
     * @param tokenSecret token secret (optional)
     * @return signature (never null)
     * @throws OAuthException if creating signature has failed
     */
    @Nonnull
    public String createSignature(@NonNull final String consumerSecret, @Nullable final String tokenSecret) throws OAuthException {
        log.debug("Creating signature.");
        SignatureRequest request = SignatureRequest.builder()
                .consumerSecret(consumerSecret)
                .tokenSecret(tokenSecret)
                .build();
        return oauthContext.getSignatureService().createSignature(request);
    }

    /**
     * Signs request. <br><br>
     *
     * Contains params: <br>
     * - see {@link #newOAuthRequest(String)} <br><br>
     *
     * Additional params: <br>
     * - oauth_token <br>
     * - oauth_token_secret <br>
     */
    @Nonnull
    @Override
    public OAuthRequest signRequest(@NonNull final OAuthToken accessToken) throws OAuthException {
        validateAuthentication(accessToken, true);
        log.trace("Signing request with access token: {}", accessToken.getToken());
        return newOAuthRequest(accessToken.getSecret())
                .addOAuthParameter(OAUTH_PARAM_TOKEN, accessToken.getToken())
                .addOAuthParameter(OAUTH_PARAM_TOKEN_SECRET, accessToken.getSecret());
    }

    /**
     * Generates oauth request with default params pre configured. <br><br>
     *
     * Contains params: <br>
     * - oauth_consumer_key <br>
     * - oauth_nonce <br>
     * - oauth_signature <br>
     * - oauth_signature_method <br>
     * - oauth_timestamp <br>
     * - oauth_version <br>
     *
     * @param tokenSecret token secret (optional)
     * @return request (never null)
     * @throws OAuthException if creating signature has failed
     */
    @Nonnull
    private OAuthRequest newOAuthRequest(@Nullable final String tokenSecret) throws OAuthException {
        return new OAuthRequest()
                .addOAuthParameter(OAUTH_PARAM_CONSUMER_KEY, oauthContext.getClientConfiguration().getConsumerKey())
                .addOAuthParameter(OAUTH_PARAM_NONCE, generateNonce())
                .addOAuthParameter(OAUTH_PARAM_SIGNATURE, createSignature(oauthContext.getClientConfiguration().getConsumerSecret(), tokenSecret))
                .addOAuthParameter(OAUTH_PARAM_SIGNATURE_METHOD, oauthContext.getSignatureService().getSignatureMethod().getName())
                .addOAuthParameter(OAUTH_PARAM_TIMESTAMP, generateTimestamp())
                .addOAuthParameter(OAUTH_PARAM_VERSION, getOAuthVersion().getValue());
    }

    /**
     * Validates given token.
     *
     * @param OAuthAuthentication token (required)
     * @param personalTokenNotAllowed personal token not allowed (true) or not (false)
     * @throws OAuthRuntimeException if token is not valid
     */
    private void validateAuthentication(final OAuthAuthentication OAuthAuthentication, final boolean personalTokenNotAllowed) {
        if(OAuthAuthentication instanceof OAuthEmptyToken) {
            throw new OAuthRuntimeException("Token is invalid ! Reason: empty token");
        } else if(personalTokenNotAllowed && OAuthAuthentication instanceof OAuthGeneratedToken) {
            throw new OAuthRuntimeException("Token is invalid ! Reason: personal token");
        }
    }

    /**
     * Creates default HTTP headers.
     *
     * Included:
     * - Authorization: OAuth XXX (were XXX is OAuth conform string)
     * - Content-Type: application/x-www-form-urlencoded
     * - User-Agent: DiscogsClient/1.0 Discogs/v2 (Name=X; UUID=Y) where X is application name and Y is unique identifier
     *
     * @param authorizationHeaderValue authorization header value (required)
     * @return headers (never null)
     */
    @Nonnull
    private HttpHeaders createDefaultHeaders(@NonNull final String authorizationHeaderValue) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.add(AUTHORIZATION, authorizationHeaderValue);
        headers.add(USER_AGENT, userAgentGenerator.generate());
        return headers;
    }

    /**
     * Parses body into a token (access/request).
     *
     * @param response response (required)
     * @return token (never null)
     * @throws OAuthException if response body can't be parsed into a token
     */
    @Nonnull
    private OAuthToken parseBody(final Response<String> response) throws OAuthException {
        return oauthContext.getTokenParser().parse(response.getBody());
    }
}
