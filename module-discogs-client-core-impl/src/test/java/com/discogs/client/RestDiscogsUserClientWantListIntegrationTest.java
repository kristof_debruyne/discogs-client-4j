package com.discogs.client;

import com.discogs.client.api.DiscogsUserClient;
import com.discogs.client.api.core.common.rating.RatingValue;
import com.discogs.client.api.core.user.wantlist.WantList;
import com.discogs.client.api.core.user.wantlist.WantListItem;
import com.discogs.client.api.request.user.wantlist.WantListItemAddRequest;
import com.discogs.client.api.request.user.wantlist.WantListItemDeleteRequest;
import com.discogs.client.api.request.user.wantlist.WantListItemUpdateRequest;
import com.discogs.client.api.request.user.wantlist.WantListRequest;
import com.discogs.client.auth.oauth.token.OAuthGeneratedToken;
import com.discogs.client.test.AbstractIntegrationTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import static com.discogs.client.api.common.DiscogsConstants.CONTENT_TYPE_APPLICATION_JSON;
import static com.discogs.client.api.request.AcceptMediaType.DISCOGS_AND_JSON;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration test for class {@link RestDiscogsUserClient}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractIntegrationTest
 */
@Log4j2
public class RestDiscogsUserClientWantListIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private DiscogsUserClient client;

    @Test
    public void getWantList() {
        WantListRequest request = WantListRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .username(getUsername())
                .pagination(defaultPaginationBuilder())
                .build();

        WantList json = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getItems()).isNotNull();
        assertThat(json.getPagination()).isNotNull();
    }

    @Test
    public void manageWantListItem() {
        try {
            //ADD
            WantListItemAddRequest addRequest = WantListItemAddRequest.builder()
                    .releaseId(2861)
                    .contentType(CONTENT_TYPE_APPLICATION_JSON)
                    .mediaType(DISCOGS_AND_JSON)
                    .authentication(new OAuthGeneratedToken(getPersonalToken()))
                    .username(getUsername())
                    .rating(RatingValue.MEDIUM)
                    .build();

            WantListItem item = assertResponseAndReturnBody(client.create(addRequest), HttpStatus.CREATED);
            assertThat(item).isNotNull();
            assertThat(item.getNotes()).isEqualTo("");
            assertThat(item.getRating()).isEqualTo(RatingValue.MEDIUM.getValue());

            //UPDATE
            WantListItemUpdateRequest updateRequest = WantListItemUpdateRequest.builder()
                    .releaseId(item.getId())
                    .contentType(CONTENT_TYPE_APPLICATION_JSON)
                    .mediaType(DISCOGS_AND_JSON)
                    .authentication(new OAuthGeneratedToken(getPersonalToken()))
                    .username(getUsername())
                    .notes("Test Notes")
                    .rating(RatingValue.HIGHEST)
                    .build();

            item = assertResponseAndReturnBody(client.modify(updateRequest), HttpStatus.OK);
            assertThat(item).isNotNull();
            assertThat(item.getNotes()).isEqualTo("Test Notes");
            assertThat(item.getRating()).isEqualTo(5);
        } finally {
            //DELETE
            WantListItemDeleteRequest deleteRequest = WantListItemDeleteRequest.builder()
                    .releaseId(2861)
                    .contentType(CONTENT_TYPE_APPLICATION_JSON)
                    .mediaType(DISCOGS_AND_JSON)
                    .authentication(new OAuthGeneratedToken(getPersonalToken()))
                    .username(getUsername())
                    .build();

            assertResponseAndReturnBody(client.delete(deleteRequest), HttpStatus.NO_CONTENT);
        }
    }
}
