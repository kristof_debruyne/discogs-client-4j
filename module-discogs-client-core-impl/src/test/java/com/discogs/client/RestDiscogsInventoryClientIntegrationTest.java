package com.discogs.client;

import com.discogs.client.api.DiscogsInventoryClient;
import com.discogs.client.api.core.inventory.Export;
import com.discogs.client.api.core.inventory.Exports;
import com.discogs.client.api.core.inventory.Upload;
import com.discogs.client.api.core.inventory.Uploads;
import com.discogs.client.api.request.inventory.ExportCreateRequest;
import com.discogs.client.api.request.inventory.ExportDownloadRequest;
import com.discogs.client.api.request.inventory.ExportRequest;
import com.discogs.client.api.request.inventory.ExportsRequest;
import com.discogs.client.api.request.inventory.UploadAddRequest;
import com.discogs.client.api.request.inventory.UploadChangeRequest;
import com.discogs.client.api.request.inventory.UploadDeleteRequest;
import com.discogs.client.api.request.inventory.UploadRequest;
import com.discogs.client.api.request.inventory.UploadsRequest;
import com.discogs.client.auth.oauth.token.OAuthGeneratedToken;
import com.discogs.client.test.AbstractIntegrationTest;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.io.InputStream;

import static com.discogs.client.api.common.DiscogsConstants.CONTENT_TYPE_APPLICATION_JSON;
import static com.discogs.client.api.common.DiscogsConstants.CONTENT_TYPE_TEXT_CSV;
import static org.apache.commons.compress.utils.IOUtils.toByteArray;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration test for class {@link RestDiscogsInventoryClient}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractIntegrationTest
 */
@Log4j2
public class RestDiscogsInventoryClientIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private DiscogsInventoryClient client;

    //*** EXPORT ***//

    @Test
    public void export() {
        //GET EXPORTS
        Exports exports = retrieveExports();
        while(ArrayUtils.isEmpty(exports.getItems())) {
            //EXPORT
            ExportCreateRequest request = ExportCreateRequest.builder()
                    .contentType(CONTENT_TYPE_APPLICATION_JSON)
                    .authentication(new OAuthGeneratedToken(getPersonalToken()))
                    .build();
            assertResponseAndReturnBody(client.create(request), HttpStatus.CREATED); //EXECUTE
            exports = retrieveExports();
        }

        //GET EXPORT
        ExportRequest exportRequest = ExportRequest.builder()
                .exportId(exports.getItems()[0].getId())
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .build();
        Export export = assertResponseAndReturnBody(client.get(exportRequest), HttpStatus.OK); //EXECUTE
        assertThat(export).isNotNull();

        //DOWNLOAD EXPORT
        ExportDownloadRequest downloadRequest = ExportDownloadRequest.builder()
                .exportId(exports.getItems()[0].getId())
                .contentType(CONTENT_TYPE_TEXT_CSV)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .build();
        byte[] data = assertResponseAndReturnBody(client.download(downloadRequest), HttpStatus.OK);
        assertThat(data).isNotNull();
    }

    private Exports retrieveExports() {
        ExportsRequest request = ExportsRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .build();

        Exports exports = assertResponseAndReturnBody(client.getAll(request), HttpStatus.OK); //EXECUTE
        assertThat(exports).isNotNull();
        return exports;
    }

    //*** UPLOAD ***//

    @Test
    public void upload() throws Exception {
        //GET UPLOADS
        Uploads uploads = retrieveUploads();
        assertThat(uploads).isNotNull();

        //ADD INVENTORY
        UploadAddRequest addRequest = UploadAddRequest.builder()
                .contentType(CONTENT_TYPE_TEXT_CSV)
                .filename("add.csv")
                .payload(createPayload("csv/add.csv"))
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .build();
        assertResponseAndReturnBody(client.upload(addRequest), HttpStatus.OK);

        //CHANGE INVENTORY
        UploadChangeRequest changeRequest = UploadChangeRequest.builder()
                .contentType(CONTENT_TYPE_TEXT_CSV)
                .filename("change.csv")
                .payload(createPayload("csv/change.csv"))
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .build();
        assertResponseAndReturnBody(client.upload(changeRequest), HttpStatus.OK);

        //DELETE INVENTORY
        UploadDeleteRequest deleteRequest = UploadDeleteRequest.builder()
                .contentType(CONTENT_TYPE_TEXT_CSV)
                .filename("delete.csv")
                .payload(createPayload("csv/delete.csv"))
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .build();
        assertResponseAndReturnBody(client.upload(deleteRequest), HttpStatus.OK);

        //GET UPLOADS
        uploads = retrieveUploads();
        assertThat(uploads).isNotNull();
        assertThat(uploads.getItems()).isNotEmpty();

        //GET UPLOAD
        UploadRequest uploadRequest = UploadRequest.builder()
                .uploadId(uploads.getItems()[0].getId())
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .build();
        Upload upload = assertResponseAndReturnBody(client.get(uploadRequest), HttpStatus.OK); //EXECUTE
        assertThat(upload).isNotNull();
    }

    private Uploads retrieveUploads() {
        UploadsRequest request = UploadsRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .pagination(defaultPaginationBuilder())
                .build();

        Uploads uploads = assertResponseAndReturnBody(client.getAll(request), HttpStatus.OK); //EXECUTE
        assertThat(uploads).isNotNull();
        return uploads;
    }

    private byte[] createPayload(String path) throws IOException {
        InputStream inputStream = RestDiscogsInventoryClientIntegrationTest.class.getClassLoader().getResourceAsStream(path);
        assertThat(inputStream).isNotNull();
        return toByteArray(inputStream);
    }
}
