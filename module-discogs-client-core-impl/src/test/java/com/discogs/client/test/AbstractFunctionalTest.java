package com.discogs.client.test;

import com.discogs.client.spring.config.DiscogsClientConfiguration;
import com.discogs.client.webdriver.ChromeConfiguration;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Abstract functional test (profile functional-test).
 *
 * @author Sikke303
 * @since 1.0
 */
@ActiveProfiles("functional-test")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ChromeConfiguration.class, DiscogsClientConfiguration.class })
public abstract class AbstractFunctionalTest {

}
