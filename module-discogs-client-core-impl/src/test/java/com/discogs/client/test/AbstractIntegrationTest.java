package com.discogs.client.test;

import com.discogs.client.api.request.common.PaginationRequest;
import com.discogs.client.api.response.Response;
import com.discogs.client.spring.config.DiscogsClientConfiguration;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.EnumSet;

import static com.discogs.client.api.common.DiscogsConstants.FIRST_PAGE_NUMBER;
import static com.discogs.client.api.common.DiscogsConstants.MINIMUM_PER_PAGE;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Abstract integration test (profile integration-test). <br><br>
 *
 * Required arguments (-D): <br>
 * - DISCOGS_CLIENT_APPLICATION_NAME: application name <br>
 * - DISCOGS_CLIENT_APPLICATION_KEY: application key <br>
 * - DISCOGS_CLIENT_APPLICATION_VERSION: application version <br>
 * - DISCOGS_CLIENT_PERSONAL_TOKEN: personal token <br>
 * - DISCOGS_CLIENT_USERNAME: username <br><br>
 *
 * The integration tests work with the personal token <br>
 * so we don't always have to go through the complete OAuth flow. <br>
 *
 * @author Sikke303
 * @since 1.0
 */
@ActiveProfiles("integration-test")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DiscogsClientConfiguration.class)
@Getter
@Log4j2
public abstract class AbstractIntegrationTest {

    protected static final String JSONP_NOT_SUPPORTED = "JSONP is currently not supported !";
    protected static final String LIST_NOT_FOUND = "NotFound: 404 NOT FOUND: [{\"message\": \"List not found\"}]";
    protected static final String FORBIDDEN = "Forbidden: 403 FORBIDDEN: [{\"message\": \"You are not allowed to view this resource. Please authenticate as the owner to view this content.\"}]";
    protected static final String UNAUTHORIZED = "Unauthorized: 401 UNAUTHORIZED: [{\"message\": \"You must authenticate to access this resource.\"}]";

    @Value("#{systemProperties['DISCOGS_CLIENT_PERSONAL_TOKEN']}")
    private String personalToken;

    @Value("#{systemProperties['DISCOGS_CLIENT_USERNAME']}")
    private String username;

    /**
     * Asserts response and returns body (on HTTP status 200 or 201 only).
     *
     * @param response response (required)
     * @return response body or null
     */
    @Nullable
    protected <T> T assertResponseAndReturnBody(@NonNull final Response<T> response, HttpStatus status) {
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(status.value());
        if(EnumSet.of(HttpStatus.OK, HttpStatus.CREATED).contains(status)) {
            return response.getBody();
        }
        return null;
    }

    /**
     * Creates default pagination request.
     *
     * - Page: 1
     * - Size: 10
     *
     * @return pagination (never null)
     */
    @Nonnull
    protected final PaginationRequest defaultPaginationBuilder() {
        return PaginationRequest.builder().page(FIRST_PAGE_NUMBER).perPage(MINIMUM_PER_PAGE).build();
    }
}
