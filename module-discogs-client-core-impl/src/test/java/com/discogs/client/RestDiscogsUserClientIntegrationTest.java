package com.discogs.client;

import com.discogs.client.api.DiscogsUserClient;
import com.discogs.client.api.core.common.Currency;
import com.discogs.client.api.core.common.sorting.SortOrder;
import com.discogs.client.api.core.user.contribution.Contributions;
import com.discogs.client.api.core.user.identity.Identity;
import com.discogs.client.api.core.user.identity.Profile;
import com.discogs.client.api.core.user.submission.Submissions;
import com.discogs.client.api.request.user.contribution.ContributionsRequest;
import com.discogs.client.api.request.user.identity.IdentityRequest;
import com.discogs.client.api.request.user.identity.ProfileRequest;
import com.discogs.client.api.request.user.identity.ProfileUpdateRequest;
import com.discogs.client.api.request.user.submission.SubmissionsRequest;
import com.discogs.client.auth.oauth.token.OAuthGeneratedToken;
import com.discogs.client.test.AbstractIntegrationTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientException;

import static com.discogs.client.api.common.DiscogsConstants.CONTENT_TYPE_APPLICATION_JSON;
import static com.discogs.client.api.request.AcceptMediaType.DISCOGS_AND_JSON;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;

/**
 * Integration test for class {@link RestDiscogsUserClient}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractIntegrationTest
 */
@Log4j2
public class RestDiscogsUserClientIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private DiscogsUserClient client;

    //*** IDENTITY ***//

    @Test
    public void getIdentity() {
        IdentityRequest request = IdentityRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .build();

        Identity json = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getId()).isGreaterThan(0);
        assertThat(json.getUserName()).isNotNull();
    }

    @Test
    public void getIdentity_Unauthorized() {
        assertThrows(UNAUTHORIZED, RestClientException.class, () -> {
            IdentityRequest request = IdentityRequest.builder()
                    .contentType(CONTENT_TYPE_APPLICATION_JSON)
                    .mediaType(DISCOGS_AND_JSON)
                    .build();
            client.get(request); //EXECUTE
        });
    }

    //*** PROFILE ***//

    @Test
    public void getProfile() {
        Profile json = retrieveAndParseProfile();
        assertThat(json.getId()).isGreaterThan(0);
        assertThat(json.getUserName()).isNotNull();
        assertThat(json.getUserName()).isEqualTo(getUsername());
    }

    @Test
    public void updateProfile() {
        Profile json = retrieveAndParseProfile();
        ProfileUpdateRequest updateRequest = ProfileUpdateRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .username(getUsername())
                .name(json.getName())
                .homePage(json.getHomePage())
                .location(json.getLocation())
                .profile(json.getProfile())
                .currency(Currency.EUR)
                .build();

        json = assertResponseAndReturnBody(client.modify(updateRequest), HttpStatus.OK);//EXECUTE
        assertThat(json).isNotNull();
    }

    private Profile retrieveAndParseProfile() {
        ProfileRequest request = ProfileRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .username(getUsername())
                .build();
        return assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
    }

    //*** CONTRIBUTIONS ***//

    @Test
    public void getUserContributions() {
        ContributionsRequest request = ContributionsRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .username(getUsername())
                .pagination(defaultPaginationBuilder())
                .sort(ContributionsRequest.SortMode.CATALOG_NUMBER)
                .sortOrder(SortOrder.ASC)
                .build();

        Contributions json = assertResponseAndReturnBody(client.getAll(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getResults()).isNotNull();
        assertThat(json.getPagination()).isNotNull();
    }

    //*** SUBMISSIONS ***//

    @Test
    public void getUserSubmissions() {
        SubmissionsRequest request = SubmissionsRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .username(getUsername())
                .pagination(defaultPaginationBuilder())
                .build();

        Submissions json = assertResponseAndReturnBody(client.getAll(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getResult()).isNotNull();
        assertThat(json.getPagination()).isNotNull();
    }
}
