package com.discogs.client.security;

import com.discogs.client.api.DiscogsUserClient;
import com.discogs.client.api.config.DiscogsContext;
import com.discogs.client.api.core.user.identity.Identity;
import com.discogs.client.api.request.user.identity.IdentityRequest;
import com.discogs.client.api.response.Response;
import com.discogs.client.auth.oauth.config.OAuthContext;
import com.discogs.client.auth.oauth.exception.OAuthException;
import com.discogs.client.auth.oauth.service.OAuthV1Service;
import com.discogs.client.auth.oauth.token.OAuthEmptyToken;
import com.discogs.client.auth.oauth.token.OAuthGeneratedToken;
import com.discogs.client.auth.oauth.token.OAuthToken;
import com.discogs.client.auth.oauth.verifier.OAuthVerifier;
import com.discogs.client.test.AbstractFunctionalTest;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.function.Supplier;

import static com.discogs.client.api.common.DiscogsConstants.CONTENT_TYPE_APPLICATION_JSON;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Functional test for class {@link DiscogsOAuthV1Service}. <br><br>
 *
 * Required arguments (-D): <br>
 * - DISCOGS_CLIENT_APPLICATION_NAME: application name <br>
 * - DISCOGS_CLIENT_APPLICATION_KEY: application key <br>
 * - DISCOGS_CLIENT_APPLICATION_VERSION: application version <br>
 * - DISCOGS_CLIENT_CALLBACK_URL: OAuth callback URL <br>
 * - DISCOGS_CLIENT_CONSUMER_KEY: OAuth consumer key <br>
 * - DISCOGS_CLIENT_CONSUMER_SECRET: OAuth consumer secret <br>
 * - DISCOGS_CLIENT_USERNAME: username <br>
 * - DISCOGS_CLIENT_PASSWORD: password <br>
 *
 * @author Sikke303
 * @since 1.0
 */
@Log4j2
public class DiscogsOAuthV1ServiceFunctionalTest extends AbstractFunctionalTest {

    @Autowired
    private DiscogsContext<OAuthContext> context;

    @Autowired
    private OAuthV1Service service;

    @Autowired
    private DiscogsUserClient client;

    @Autowired
    private WebDriver driver;

    @Value("#{systemProperties['DISCOGS_CLIENT_APPLICATION_NAME']}")
    private String applicationName;

    @Value("#{systemProperties['DISCOGS_CLIENT_APPLICATION_KEY']}")
    private String applicationKey;

    @Value("#{systemProperties['DISCOGS_CLIENT_APPLICATION_VERSION']}")
    private String applicationVersion;

    @Value("#{systemProperties['DISCOGS_CLIENT_USERNAME']}")
    private String username;

    @Value("#{systemProperties['DISCOGS_CLIENT_PASSWORD']}")
    private String password;

    private WebDriverWait driverWait;

    @Before
    public void before() {
        driverWait = new WebDriverWait(driver, 10L, 1000L);
    }

    @Test(timeout = 30000L)
    public void authenticate() throws OAuthException {
        //REQUEST TOKEN
        OAuthToken requestToken = service.getRequestToken();
        verifyToken(requestToken);
        log.info("Request token obtained: {}", requestToken);

        //AUTHORIZE
        OAuthVerifier verifier = authorize(() -> service.getAuthorizedUrl(requestToken), username, password);
        log.info("Verifier obtained: {}", verifier.getValue());

        //ACCESS TOKEN
        OAuthToken accessToken = service.getAccessToken(requestToken, verifier);
        verifyToken(accessToken);
        log.info("Access token obtained: {}", accessToken);

        //RESOURCE
        try {
            IdentityRequest request = IdentityRequest.builder()
                    .authentication(accessToken)
                    .contentType(CONTENT_TYPE_APPLICATION_JSON)
                    .build();
            Response<? extends Identity> response = client.get(request);
            assertThat(response).isNotNull();
            assertThat(response.getStatusCode()).isEqualTo(200);
            assertThat(response.getBody()).isNotNull();
        } finally {
            revoke(() -> service.getRevokeTokenUrl(accessToken));
        }
    }

    /**
     * Authorizing on Discogs website.
     *
     * @param supplier authorized url supplier (required)
     * @param username username (required)
     * @param password password (required)
     * @return verifier
     */
    private OAuthVerifier authorize(@NonNull Supplier<String> supplier, @NonNull String username, @NonNull String password) throws OAuthException {
        driver.get(supplier.get());

        log.info("Entering credentials for user: {}", username);
        acceptCookies();
        driver.findElement(By.id("username")).sendKeys(username);
        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.name("submit")).click();

        log.info("Authorize application: {} {} - Key = {}", applicationName, applicationVersion, applicationKey);
        acceptCookies();
        driver.findElement(By.cssSelector("form#oauth_form_block button[type=\"submit\"]")).click();

        return context.getAuthenticationContext().getVerifierParser().parse(driver.getCurrentUrl());
    }

    /**
     * Revoking on Discogs website.
     *
     * @param supplier supplier (required)
     */
    private void revoke(@NonNull Supplier<String> supplier) {
        driver.get(supplier.get());
        driver.findElement(By.cssSelector("form button[type=\"submit\"]")).click();
    }

    /**
     * Accept cookies.
     */
    private void acceptCookies() {
        driverWait.until(ExpectedConditions.elementToBeClickable(By.id("onetrust-accept-btn-handler"))).click();
    }

    /**
     * Verifies given token.
     *
     * @param token token (required)
     */
    private void verifyToken(@NonNull final OAuthToken token) {
        assertThat(token).isNotNull();
        assertThat(token).isNotInstanceOf(OAuthEmptyToken.class);
        assertThat(token).isNotInstanceOf(OAuthGeneratedToken.class);
    }
}
