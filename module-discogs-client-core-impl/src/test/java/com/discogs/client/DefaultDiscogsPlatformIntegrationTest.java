package com.discogs.client;

import com.discogs.client.api.DiscogsClient;
import com.discogs.client.api.DiscogsService;
import com.discogs.client.api.exception.DiscogsRuntimeException;
import com.discogs.client.test.AbstractIntegrationTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration test for class {@link DefaultDiscogsPlatform}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractIntegrationTest
 */
@Log4j2
public class DefaultDiscogsPlatformIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private DefaultDiscogsPlatform facade;

    @Test
    public void getContext() {
        assertThat(facade.getDiscogsContext()).isNotNull();
    }

    @Test
    public void getClients_success() {
        assertThat(facade.getDatabaseClient()).isNotNull();
        assertThat(facade.getInventoryClient()).isNotNull();
        assertThat(facade.getImageClient()).isNotNull();
        assertThat(facade.getJsonpClient()).isNotNull();
        assertThat(facade.getMarketplaceClient()).isNotNull();
        assertThat(facade.getRootClient()).isNotNull();
        assertThat(facade.getUserClient()).isNotNull();
    }

    @Test(expected = DiscogsRuntimeException.class)
    public void getClient_error() {
        facade.getClient(DummyDiscogsClient.class);
    }

    @Test
    public void getServices_success() {
        assertThat(facade.getAuthenticationService()).isNotNull();
    }

    @Test(expected = DiscogsRuntimeException.class)
    public void getServices_error() {
        facade.getService(DummyDiscogsService.class);
    }

    //*** DUMMY CLIENT/SERVICE ***//

    static class DummyDiscogsClient implements DiscogsClient {

        @Nonnull
        @Override
        public URI getBaseUri() {
            return URI.create("http://api.doscogs.com");
        }
    }

    static class DummyDiscogsService implements DiscogsService { }
}
