package com.discogs.client.webdriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ActiveProfiles;

import javax.annotation.PostConstruct;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Spring Chrome configuration.
 *
 * Chrome options: https://peter.sh/experiments/chromium-command-line-switches
 *
 * @author Sikke303
 * @since 1.0
 */
@ActiveProfiles("chrome")
@Configuration
@PropertySource("classpath:chrome.properties")
@Log4j2
public class ChromeConfiguration {

    private static final String CHROME_PREFS = "chrome.prefs";

    @Autowired
    private Path temporaryDirectoryForDownloads;

    @Value("${chrome.version}")
    private String version;

    @Value("#{'${chrome.options}'.split(';')}")
    private List<String> options;

    @PostConstruct
    private void init() {
        log.info("Loading Chrome test configuration ...");
    }

    @Bean
    public Path getTemporaryDirectoryForDownloads() throws Exception {
        final Path path = Files.createTempDirectory("functional-test");
        log.info("Created temporary directory to store downloaded files {}", path);
        return path;
    }

    @Bean(destroyMethod = "quit")
    public WebDriver driver() throws Exception {
        WebDriverManager.chromedriver().version(version).setup(); //MAGIC

        //OPTIONS
        final ChromeOptions chromeOptions = new ChromeOptions();
        options.forEach(option -> {
            log.info("Registering Chrome option -> {}", option);
            chromeOptions.addArguments(option);
        });

        final Map<String, Object> preferences = new Hashtable<>();
        preferences.put("download.default_directory", temporaryDirectoryForDownloads.toRealPath());
        preferences.put("download.prompt_for_download", true);
        chromeOptions.setCapability(CHROME_PREFS, preferences);

        //DRIVER
        final WebDriver driver = new ChromeDriver(chromeOptions);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        log.info("Chrome Driver is configured !");
        return driver;
    }
}
