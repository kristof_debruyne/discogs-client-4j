package com.discogs.client;

import com.discogs.client.api.DiscogsRootClient;
import com.discogs.client.api.DiscogsUserClient;
import com.discogs.client.api.core.ApiInformation;
import com.discogs.client.api.core.user.identity.Identity;
import com.discogs.client.api.request.user.identity.IdentityRequest;
import com.discogs.client.api.response.RateLimit;
import com.discogs.client.api.response.Response;
import com.discogs.client.auth.oauth.token.OAuthGeneratedToken;
import com.discogs.client.test.AbstractIntegrationTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.function.IntFunction;
import java.util.stream.IntStream;

import static com.discogs.client.api.common.DiscogsConstants.CONTENT_TYPE_APPLICATION_JSON;
import static com.discogs.client.api.request.AcceptMediaType.DISCOGS_AND_JSON;
import static org.assertj.core.api.Assertions.assertThat;

@Log4j2
public class RestDiscogsThrottleIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private DiscogsRootClient rootClient;

    @Autowired
    private DiscogsUserClient userClient;

    /**
     * Authenticated throttle integration test.
     *
     * Limit: 60 requests per minute
     * Requests: 70 requests
     */
    @Test(timeout = 60000)
    public void givenAuthenticatedRequestsPerMinute_whenRequesting_thenThrottleRequests() {
        IdentityRequest request = IdentityRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .build();

        IntStream.range(1, 71)
                .peek(index -> log.info("Executing authenticated request: {}", index))
                .mapToObj((IntFunction<Response<? extends Identity>>) index -> userClient.get(request))
                .forEach((Response<? extends Identity> response) -> {
                    assertThat(response.getStatusCode()).isEqualTo(200);
                    assertThat(response.getHeaders()).containsEntry(RateLimit.RATE_LIMIT.getHeaderName(), "60");
                });
    }

    /**
     * Unauthenticated throttle integration test.
     *
     * Limit: 25 requests per minute
     * Requests: 30 requests
     */
    @Test(timeout = 60000)
    public void givenUnauthenticatedRequestsPerMinute_whenRequesting_thenThrottleRequests() {
        IntStream.range(1, 31)
                .peek(index -> log.info("Executing unauthenticated request: {}", index))
                .mapToObj((IntFunction<Response<? extends ApiInformation>>) index -> rootClient.getApiInformation())
                .forEach((Response<? extends ApiInformation> response) -> {
                    assertThat(response.getStatusCode()).isEqualTo(200);
                    assertThat(response.getHeaders()).containsEntry(RateLimit.RATE_LIMIT.getHeaderName(), "25");
                });
    }
}
