package com.discogs.client.core.database;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonLabelReleasesTest extends AbstractJsonTest {

    @Test
    public void parseLabelReleases() throws Exception {
        JsonLabelReleases json = readJson("database/label-releases", JsonLabelReleases.class);
        log.info("Parsed label releases to JSON: {}", json);
        assertNotNull(json);
        assertNotNull(json.getPagination());
        assertThat(json.getReleases()).hasSize(3);
    }

    @Test
    public void parseLabelRelease() throws Exception {
        JsonLabelReleases.JsonLabelRelease json = readJson("database/label-release", JsonLabelReleases.JsonLabelRelease.class);
        log.info("Parsed label release to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(2801);
        assertThat(json.getArtistName()).isEqualTo("Andrea Parker");
        assertThat(json.getCatalogNumber()).isEqualTo("!K7071CD");
        assertThat(json.getFormat()).isEqualTo("CD, Mixed");
        assertThat(json.getResourceUrl()).isEqualTo("http://api.discogs.com/releases/2801");
        assertThat(json.getStatus()).isEqualTo("Accepted");
        assertThat(json.getThumbnail()).isEqualTo("https://api-img.discogs.com/cYmCut4Yh99FaLFHyoqkFo-Md1E=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb()/discogs-images/L-1-1111053865.png.jpg");
        assertThat(json.getTitle()).isEqualTo("DJ-Kicks");
        assertThat(json.getYear()).isEqualTo(1998);
    }
}
