package com.discogs.client.core.inventory;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonUploadTest extends AbstractJsonTest {

    @Test
    public void parseUpload() throws Exception {
        JsonUpload json = readJson("inventory/inventory-upload", JsonUpload.class);
        log.info("Parsed inventory upload to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(119615);
        assertThat(json.getDateCreated()).isEqualTo("2017-12-18T09:20:28");
        assertThat(json.getDateFinished()).isEqualTo("2017-12-18T09:20:29");
        assertThat(json.getFilename()).isEqualTo("cburmeister-inventory.csv");
        assertThat(json.getResults()).isEqualTo("CSV file contains 1 records. Processed 1 records.");
        assertThat(json.getStatus()).isEqualTo("success");
        assertThat(json.getType()).isEqualTo("change");
    }
}
