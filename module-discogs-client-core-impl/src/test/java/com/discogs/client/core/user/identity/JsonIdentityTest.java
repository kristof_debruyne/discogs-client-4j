package com.discogs.client.core.user.identity;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonIdentityTest extends AbstractJsonTest {

    @Test
    public void parseIdentity() throws Exception {
        JsonIdentity json = readJson("user/identity/identity", JsonIdentity.class);
        log.info("Parsed identity to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(1337);
        assertThat(json.getConsumerName()).isEqualTo("Discogs Client");
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/users/sikke303");
        assertThat(json.getUserName()).isEqualTo("sikke303");
    }
}
