package com.discogs.client.core.inventory;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonExportsTest extends AbstractJsonTest {

    @Test
    public void parseExports() throws Exception {
        JsonExports json = readJson("inventory/inventory-exports", JsonExports.class);
        log.info("Parsed inventory exports to JSON: {}", json);
        assertNotNull(json);
        assertNotNull(json.getPagination());
        assertThat(json.getItems()).hasSize(1);
    }
}
