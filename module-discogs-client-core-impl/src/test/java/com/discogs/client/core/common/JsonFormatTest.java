package com.discogs.client.core.common;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonFormatTest extends AbstractJsonTest {

    @Test
    public void parseFormat() throws Exception {
        JsonFormat json = readJson("common/format", JsonFormat.class);
        log.info("Parsed format to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getDescriptions()).containsExactly("Compilation", "Limited Edition");
        assertThat(json.getName()).isEqualTo("CD");
        assertThat(json.getQuantity()).isEqualTo("1");
        assertThat(json.getText()).isEqualTo("Digipak");
    }
}
