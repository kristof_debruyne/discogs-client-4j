package com.discogs.client.core.database;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonLabelTest extends AbstractJsonTest {

    @Test
    public void parseLabel() throws Exception {
        JsonLabel json = readJson("database/label", JsonLabel.class);
        log.info("Parsed label to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(1337);
        assertThat(json.getContactInfo()).isEqualTo("Planet E Communications P.O. Box 27218 Detroit, 48227, USA p: 313.874.8729 f: 313.874.8732");
        assertThat(json.getDataQuality()).isEqualTo("Needs Vote");
        assertThat(json.getImages()).hasSize(1);
        assertThat(json.getName()).isEqualTo("Planet E");
        assertThat(json.getProfile()).isEqualTo("Classic Techno label from Detroit, USA");
        assertThat(json.getReleasesUrl()).isEqualTo("https://api.discogs.com/labels/1337/releases");
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/labels/1337");
        assertThat(json.getSubLabels()).hasSize(2);
        assertThat(json.getUri()).isEqualTo("http://www.discogs.com/label/1337-Planet-E");
        assertThat(json.getUrls()).containsExactly("http://www.planet-e.net", "http://planetecommunications.bandcamp.com", "http://twitter.com/planetedetroit");
    }

    @Test
    public void parseSubLabel() throws Exception {
        JsonLabel.JsonSubLabel json = readJson("database/sub-label", JsonLabel.JsonSubLabel.class);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(86537);
        assertThat(json.getName()).isEqualTo("Antidote (4)");
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/labels/86537");
    }
}
