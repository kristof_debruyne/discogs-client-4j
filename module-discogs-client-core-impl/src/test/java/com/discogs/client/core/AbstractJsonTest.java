package com.discogs.client.core;

import com.discogs.client.core.common.JsonCommunityTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import org.junit.Before;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.InputStream;

import static java.lang.String.format;
import static org.junit.Assert.assertNotNull;

public abstract class AbstractJsonTest {

    private static final String JSON_RESOURCE_MASK = "json/%s.json";

    private ObjectMapper objectMapper;

    @Before
    public void before() {
        objectMapper = new ObjectMapper();
    }

    /**
     * Reads JSON from classpath, relative from folder 'json'. Extension doesn't need to be passed.
     *
     * @param path path (required)
     * @param responseType response type (required)
     * @param <T> response type or null
     * @return response type
     * @throws Exception if reading has failed
     */
    @Nullable
    protected final <T> T readJson(@NonNull final String path, @NonNull final Class<T> responseType) throws Exception {
        return objectMapper.readValue(getInputStream(path), responseType);
    }

    @Nonnull
    private InputStream getInputStream(String fileName) {
        InputStream inputStream = JsonCommunityTest.class.getClassLoader().getResourceAsStream(format(JSON_RESOURCE_MASK, fileName));
        assertNotNull(inputStream);
        return inputStream;
    }
}