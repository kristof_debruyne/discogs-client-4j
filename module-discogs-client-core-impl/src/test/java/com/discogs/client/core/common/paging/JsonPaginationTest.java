package com.discogs.client.core.common.paging;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonPaginationTest extends AbstractJsonTest {

    @Test
    public void parsePagination() throws Exception {
        JsonPagination json = readJson("common/pagination", JsonPagination.class);
        log.info("Parsed pagination to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getItemCount()).isEqualTo(100);
        assertThat(json.getPage()).isEqualTo(3);
        assertThat(json.getPageCount()).isEqualTo(5);
        assertThat(json.getPerPage()).isEqualTo(20);
        assertThat(json.getNavigation()).isNotNull();
    }
}
