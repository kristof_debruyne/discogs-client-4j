package com.discogs.client.core.image;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonImageTest extends AbstractJsonTest {

    @Test
    public void parseImage() throws Exception {
        JsonImage json = readJson("image/image", JsonImage.class);
        log.info("Parsed image to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getHeight()).isEqualTo(260);
        assertThat(json.getWidth()).isEqualTo(330);
        assertThat(json.getType()).isEqualTo("primary");
        assertThat(json.getResourceUrl()).isEqualTo("https://api-img.discogs.com/9xJ5T7IBn23DDMpg1USsDJ7IGm4=/330x260/smart/filters:strip_icc():format(jpeg):mode_rgb():quality(96)/discogs-images/A-108713-1110576087.jpg.jpg");
        assertThat(json.getUri()).isEqualTo("https://api-img.discogs.com/9xJ5T7IBn23DDMpg1USsDJ7IGm4=/330x260/smart/filters:strip_icc():format(jpeg):mode_rgb():quality(96)/discogs-images/A-108713-1110576087.jpg.jpg");
        assertThat(json.getUri150()).isEqualTo("https://api-img.discogs.com/--xqi8cBtaBZz3qOjVcvzGvNRmU=/150x150/smart/filters:strip_icc():format(jpeg):mode_rgb()/discogs-images/A-108713-1110576087.jpg.jpg");
    }
}
