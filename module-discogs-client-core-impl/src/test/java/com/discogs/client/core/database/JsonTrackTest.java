package com.discogs.client.core.database;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonTrackTest extends AbstractJsonTest {

    @Test
    public void parseTrack() throws Exception {
        JsonTrack json = readJson("database/track", JsonTrack.class);
        log.info("Parsed track to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getDuration()).isEqualTo("7:13");
        assertThat(json.getPosition()).isEqualTo("2");
        assertThat(json.getExtraArtists()).hasSize(1);
        assertThat(json.getTitle()).isEqualTo("From The Heart");
        assertThat(json.getType()).isEqualTo("track");
    }
}
