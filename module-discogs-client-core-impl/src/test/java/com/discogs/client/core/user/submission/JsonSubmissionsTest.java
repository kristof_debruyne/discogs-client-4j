package com.discogs.client.core.user.submission;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonSubmissionsTest extends AbstractJsonTest {

    @Test
    public void parseSubmissions() throws Exception {
        JsonSubmissions json = readJson("user/submission/submissions", JsonSubmissions.class);
        log.info("Parsed submissions to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getResult()).isNotNull();
        assertThat(json.getPagination()).isNotNull();
    }

    @Test
    public void parseSubmission() throws Exception {
        JsonSubmissions.JsonSubmission json = readJson("user/submission/submission", JsonSubmissions.JsonSubmission.class);
        log.info("Parsed submission to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getArtists()).hasSize(1);
        Assertions.assertThat(json.getLabels()).isNotNull();
        Assertions.assertThat(json.getReleases()).hasSize(1);
    }
}
