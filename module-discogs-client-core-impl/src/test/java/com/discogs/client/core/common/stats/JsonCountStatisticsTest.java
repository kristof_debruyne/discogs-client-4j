package com.discogs.client.core.common.stats;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonCountStatisticsTest extends AbstractJsonTest {

    @Test
    public void parseCountStatistic() throws Exception {
        JsonCountStatistics json = readJson("common/statistic-count", JsonCountStatistics.class);
        log.info("Parsed count statistic to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getInCollectionCount()).isEqualTo(842);
        assertThat(json.getInWantListCount()).isEqualTo(762);
    }
}
