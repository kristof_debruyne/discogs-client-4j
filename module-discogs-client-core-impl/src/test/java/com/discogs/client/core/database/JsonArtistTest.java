package com.discogs.client.core.database;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonArtistTest extends AbstractJsonTest {

    @Test
    public void parseArtist() throws Exception {
        JsonArtist json = readJson("database/artist", JsonArtist.class);
        log.info("Parsed artist to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(108713);
        assertThat(json.getAnv()).isEqualTo("Some anv");
        assertThat(json.getDataQuality()).isEqualTo("Needs Vote");
        assertThat(json.getImages()).hasSize(1);
        assertThat(json.getJoin()).isEqualTo("Chad Kroeger & Mike Kroeger");
        assertThat(json.getMembers()).hasSize(2);
        assertThat(json.getNameVariations()).containsExactly("Nickleback");
        assertThat(json.getProfile()).isEqualTo("Nickelback is a Canadian rock band formed in 1995.");
        assertThat(json.getReleasesUrl()).isEqualTo("https://api.discogs.com/artists/108713/releases");
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/artists/108713");
        assertThat(json.getRole()).isEqualTo("Some role");
        assertThat(json.getTracks()).isEqualTo("Some tracks");
        assertThat(json.getUri()).isEqualTo("http://www.discogs.com/artist/108713-Nickelback");
        assertThat(json.getUrls()).containsExactly("http://www.nickelback.com", "http://en.wikipedia.org/wiki/Nickelback");
    }

    @Test
    public void parseMember() throws Exception {
        JsonArtist.JsonMember json = readJson("database/member", JsonArtist.JsonMember.class);
        log.info("Parsed member to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(270222);
        assertThat(json.getName()).isEqualTo("Chad Kroeger");
        assertThat(json.isActive()).isTrue();
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/artists/270222");
    }
}
