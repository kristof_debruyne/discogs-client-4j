package com.discogs.client.core.database;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonReleaseCommunityRatingTest extends AbstractJsonTest {

    @Test
    public void parseReleaseCommunityRating() throws Exception {
        JsonReleaseCommunityRating json = readJson("database/release-community-rating", JsonReleaseCommunityRating.class);
        log.info("Parsed community rating to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getReleaseId()).isEqualTo(249504);
        Assertions.assertThat(json.getRating()).isNotNull();
        Assertions.assertThat(json.getRating().getAverage()).isEqualTo(4.19);
        Assertions.assertThat(json.getRating().getCount()).isEqualTo(47);
    }
}
