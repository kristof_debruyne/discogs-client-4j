package com.discogs.client.core.marketplace;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonListingsTest extends AbstractJsonTest {

    @Test
    public void parseListings() throws Exception {
        JsonListings json = readJson("marketplace/listings", JsonListings.class);
        log.info("Parsed listings to JSON: {}", json);
        assertNotNull(json);
        assertNotNull(json.getPagination());
        assertThat(json.getListings()).hasSize(1);
    }
}
