package com.discogs.client.core.common;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonCompanyTest extends AbstractJsonTest {

    @Test
    public void parseCompany() throws Exception {
        JsonCompany json = readJson("common/company", JsonCompany.class);
        log.info("Parsed company to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(82835);
        assertThat(json.getCatalogNumber()).isEqualTo("My Company");
        assertThat(json.getEntityType()).isEqualTo("13");
        assertThat(json.getEntityTypeName()).isEqualTo("Phonographic Copyright");
        assertThat(json.getName()).isEqualTo("BMG Records (UK) Ltd.");
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/labels/82835");
    }
}
