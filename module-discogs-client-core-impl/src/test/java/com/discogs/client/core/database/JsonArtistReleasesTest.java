package com.discogs.client.core.database;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonArtistReleasesTest extends AbstractJsonTest {

    @Test
    public void parseArtistReleases() throws Exception {
        JsonArtistReleases json = readJson("database/artist-releases", JsonArtistReleases.class);
        log.info("Parsed artist releases to JSON: {}", json);
        assertNotNull(json);
        assertNotNull(json.getPagination());
        assertThat(json.getReleases()).hasSize(3);
    }

    @Test
    public void parseArtistRelease() throws Exception {
        JsonArtistReleases.JsonArtistRelease json = readJson("database/artist-release", JsonArtistReleases.JsonArtistRelease.class);
        log.info("Parsed artist release to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(4299404);
        assertThat(json.getArtistName()).isEqualTo("Nickelback");
        assertThat(json.getFormat()).isEqualTo("CD, EP");
        assertThat(json.getLabelName()).isEqualTo("Not On Label (Nickelback Self-released)");
        assertThat(json.getMainReleaseId()).isEqualTo(3128432);
        assertThat(json.getResourceUrl()).isEqualTo("http://api.discogs.com/releases/4299404");
        assertThat(json.getRole()).isEqualTo("Main");
        assertThat(json.getStatus()).isEqualTo("Accepted");
        assertThat(json.getThumbnail()).isEqualTo("https://api-img.discogs.com/eFRGD78N7UhtvRjhdLZYXs2QJXk=/fit-in/90x90/filters:strip_icc():format(jpeg):mode_rgb()/discogs-images/R-4299404-1361106117-3632.jpeg.jpg");
        assertThat(json.getTitle()).isEqualTo("Hesher");
        assertThat(json.getType()).isEqualTo("release");
        assertThat(json.getYear()).isEqualTo(1996);
    }
}
