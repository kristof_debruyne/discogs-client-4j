package com.discogs.client.core.user.wantlist;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonWantListTest extends AbstractJsonTest {

    @Test
    public void parseWantList() throws Exception {
        JsonWantList json = readJson("user/wantlist/wantlist", JsonWantList.class);
        log.info("Parsed want list to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getItems()).isNotNull();
        assertThat(json.getPagination()).isNotNull();
    }
}
