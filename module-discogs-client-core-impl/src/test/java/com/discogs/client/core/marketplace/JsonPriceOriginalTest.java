package com.discogs.client.core.marketplace;

import com.discogs.client.api.core.common.Currency;
import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonPriceOriginalTest extends AbstractJsonTest {

    @Test
    public void parseOriginalPrice() throws Exception {
        JsonPriceOriginal json = readJson("marketplace/price-original", JsonPriceOriginal.class);
        log.info("Parsed original price to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getCurrencyId()).isEqualTo(1);
        assertThat(json.getCurrency()).isEqualTo(Currency.USD);
        assertThat(json.getFormattedValue()).isEqualTo("$2.50");
        assertThat(json.getValue()).isEqualTo(2.50);
    }
}
