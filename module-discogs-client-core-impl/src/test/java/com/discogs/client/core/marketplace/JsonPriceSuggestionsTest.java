package com.discogs.client.core.marketplace;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonPriceSuggestionsTest extends AbstractJsonTest {

    @Test
    public void parsePriceSuggestions() throws Exception {
        JsonPriceSuggestions json = readJson("marketplace/price-suggestions", JsonPriceSuggestions.class);
        log.info("Parsed price suggestions to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getSuggestions()).hasSize(8);
        json.getSuggestions()
                .forEach((condition, fee) -> {
                    assertThat(condition).isNotNull();
                    assertThat(fee).isNotNull();
                    assertThat(fee.getCurrency()).isNotNull();
                    assertThat(fee.getValue()).isGreaterThan(0);
                });
    }
}
