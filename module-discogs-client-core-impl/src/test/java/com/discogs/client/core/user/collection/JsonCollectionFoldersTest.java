package com.discogs.client.core.user.collection;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonCollectionFoldersTest extends AbstractJsonTest {

    @Test
    public void parseCollectionFolders() throws Exception{
        JsonCollectionFolders json = readJson("user/collection/collection-folders", JsonCollectionFolders.class);
        log.info("Parsed collection folders to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getFolders()).hasSize(2);
    }
}
