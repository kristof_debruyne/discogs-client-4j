package com.discogs.client.core.user.collection;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonCollectionListTest extends AbstractJsonTest {

    @Test
    public void parseCollectionItems() throws Exception {
        JsonCollectionItems json = readJson("user/collection/collection-items", JsonCollectionItems.class);
        log.info("Parsed collection items to JSON: {}", json);
        assertNotNull(json);
        assertNotNull(json.getItems());
        assertNotNull(json.getPagination());
    }

    @Test
    public void parseCollectionItem() throws Exception {
        JsonCollectionItem json = readJson("user/collection/collection-item", JsonCollectionItem.class);
        log.info("Parsed collection item to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(2464521);
        assertThat(json.getFolderId()).isEqualTo(1L);
        assertThat(json.getInstanceId()).isEqualTo(123456789);
        assertThat(json.getRating()).isEqualTo(5);
        assertThat(json.getBasicInformation()).isNotNull();
        assertThat(json.getNotes()).hasSize(1);
    }

    @Test
    public void parseNote() throws Exception {
        JsonCollectionItem.JsonNote json = readJson("user/collection/note", JsonCollectionItem.JsonNote.class);
        log.info("Parsed note to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getFieldId()).isEqualTo(3);
        assertThat(json.getValue()).isEqualTo("bleep bloop blorp.");
    }
}
