package com.discogs.client.core.common.paging;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonNavigationTest extends AbstractJsonTest {

    @Test
    public void parseNavigation() throws Exception {
        JsonNavigation json = readJson("common/navigation", JsonNavigation.class);
        log.info("Parsed navigation to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getFirst()).isEqualTo("https://api.discogs.com/database/search?per_page=20&artist=nirvana&release_title=nevermind&page=1");
        assertThat(json.getPrevious()).isEqualTo("https://api.discogs.com/database/search?per_page=20&artist=nirvana&release_title=nevermind&page=2");
        assertThat(json.getNext()).isEqualTo("https://api.discogs.com/database/search?per_page=20&artist=nirvana&release_title=nevermind&page=4");
        assertThat(json.getLast()).isEqualTo("https://api.discogs.com/database/search?per_page=20&artist=nirvana&release_title=nevermind&page=5");
    }
}
