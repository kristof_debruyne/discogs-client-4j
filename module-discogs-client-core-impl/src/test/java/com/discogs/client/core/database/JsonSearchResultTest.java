package com.discogs.client.core.database;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonSearchResultTest extends AbstractJsonTest {

    @Test
    public void parseSearchResult() throws Exception {
        JsonSearchResult json = readJson("database/search-result", JsonSearchResult.class);
        log.info("Parsed search to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getPagination()).isNotNull();
        assertThat(json.getItems()).hasSize(2);
    }

    @Test
    public void parseSearchResultItem() throws Exception {
        JsonSearchResult.JsonSearchResultItem json = readJson("database/search-result-item", JsonSearchResult.JsonSearchResultItem.class);
        log.info("Parsed item to JSON: {}", json);
        assertNotNull(json);
        assertThat(json).isNotNull();
        assertThat(json.getId()).isEqualTo(132409);
        assertThat(json.getBarcodes()).containsExactly("673885030228", "SU1011464A SHOWCASE", "IFPI LG06", "IFPI KZO2");
        assertThat(json.getCatalogNumber()).isEqualTo("MERCK 015");
        assertThat(json.getCountry()).isEqualTo("US");
        assertThat(json.getCoverImage()).isEqualTo("https://img.discogs.com/7yJzrM_JZ_PiHE4TPqQp6kTQC3c=/fit-in/600x600/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-132409-1163023346.jpeg.jpg");
        assertThat(json.getFormatQuantity()).isEqualTo(1);
        assertThat(json.getFormat()).containsExactly("CD", "Compilation", "Limited Edition");
        assertThat(json.getFormats()).hasSize(1);
        assertThat(json.getGenres()).containsExactly("Electronic");
        assertThat(json.getLabels()).containsExactly("Merck");
        assertThat(json.getMasterId()).isEqualTo(700894);
        assertThat(json.getMasterUrl()).isEqualTo("https://api.discogs.com/masters/700894");
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/releases/132409");
        assertThat(json.getStyles()).containsExactly("IDM", "Electro", "Ambient");
        assertThat(json.getThumbnail()).isEqualTo("https://img.discogs.com/h9paaLUFOjMNSWGI06tWH7um41U=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/R-132409-1163023346.jpeg.jpg");
        assertThat(json.getTitle()).isEqualTo("Lackluster - Showcase");
        assertThat(json.getType()).isEqualTo("release");
        assertThat(json.getUri()).isEqualTo("/Lackluster-Showcase/release/132409");
        assertThat(json.getUserData()).isNotNull();
        assertThat(json.getUserData().isInCollection()).isTrue();
        assertThat(json.getUserData().isInWantList()).isTrue();
        assertThat(json.getYear()).isEqualTo("2003");
    }
}
