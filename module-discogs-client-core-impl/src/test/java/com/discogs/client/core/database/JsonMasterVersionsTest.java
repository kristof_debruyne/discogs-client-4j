package com.discogs.client.core.database;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonMasterVersionsTest extends AbstractJsonTest {

    @Test
    public void parseMasterVersions() throws Exception {
        JsonMasterVersions json = readJson("database/master-versions", JsonMasterVersions.class);
        log.info("Parsed master versions to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getPagination()).isNotNull();
        assertThat(json.getVersions()).hasSize(3);
    }

    @Test
    public void parseMasterVersion() throws Exception {
        JsonMasterVersions.JsonMasterVersion json = readJson("database/master-version", JsonMasterVersions.JsonMasterVersion.class);
        log.info("Parsed master version to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(34228);
        assertThat(json.getCatalogNumber()).isEqualTo("RSGB 101T");
        assertThat(json.getCountry()).isEqualTo("UK");
        assertThat(json.getFormat()).isEqualTo("12\", 33 RPM");
        assertThat(json.getLabelName()).isEqualTo("R & S Records");
        assertThat(json.getMajorFormats()).containsExactly("Vinyl");
        assertThat(json.getReleased()).isEqualTo("1993");
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/releases/34228");
        assertThat(json.getThumbnail()).isEqualTo("https://img.discogs.com/KAm38-Op5VlkvuJOaTGZieKwRVg=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/R-34228-1215760312.jpeg.jpg");
        assertThat(json.getStatistics()).isNotNull();
        assertThat(json.getTitle()).isEqualTo("Plastic Dreams (Mixes)");
    }
}
