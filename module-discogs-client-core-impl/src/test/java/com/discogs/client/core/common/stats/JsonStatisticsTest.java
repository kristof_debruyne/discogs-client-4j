package com.discogs.client.core.common.stats;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonStatisticsTest extends AbstractJsonTest {

    @Test
    public void parseStatistics() throws Exception {
        JsonStatistics json = readJson("common/statistics", JsonStatistics.class);
        log.info("Parsed statistics to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getCommunityStatistics()).isNotNull();
        assertThat(json.getUserStatistics()).isNotNull();
    }
}
