package com.discogs.client.core.user.collection;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonCollectionFieldsTest extends AbstractJsonTest {

    @Test
    public void parseCollectionCustomFields() throws Exception{
        JsonCollectionFields json = readJson("user/collection/collection-custom-fields", JsonCollectionFields.class);
        log.info("Parsed collection custom fields to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getFields()).hasSize(2);
    }

    @Test
    public void parseCollectionCustomFieldSelect() throws Exception{
        JsonCollectionFields.JsonCollectionField json =
                readJson("user/collection/collection-custom-fields-select", JsonCollectionFields.JsonCollectionField.class);
        log.info("Parsed collection custom field (type select) to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(1);
        assertThat(json.isPublicField()).isTrue();
        assertThat(json.getPosition()).isEqualTo(1);
        assertThat(json.getLines()).isEqualTo(0);
        assertThat(json.getName()).isEqualTo("Sleeve");
        assertThat(json.getType()).isEqualTo("dropdown");
        assertThat(json.getOptions()).containsExactly("Mint", "Good", "Fair", "Poor");
    }

    @Test
    public void parseCollectionCustomFieldText() throws Exception{
        JsonCollectionFields.JsonCollectionField json =
                readJson("user/collection/collection-custom-fields-text", JsonCollectionFields.JsonCollectionField.class);
        log.info("Parsed collection custom field (type text) to JSON: {}", json);
        assertNotNull(json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(2);
        assertThat(json.isPublicField()).isTrue();
        assertThat(json.getPosition()).isEqualTo(2);
        assertThat(json.getLines()).isEqualTo(3);
        assertThat(json.getName()).isEqualTo("Notes");
        assertThat(json.getType()).isEqualTo("textarea");
        assertThat(json.getOptions()).isNull();
    }
}
