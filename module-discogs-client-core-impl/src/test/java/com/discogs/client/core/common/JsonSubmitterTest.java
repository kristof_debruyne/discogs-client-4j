package com.discogs.client.core.common;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonSubmitterTest extends AbstractJsonTest {

    @Test
    public void parseSubmitter() throws Exception {
        JsonSubmitter json = readJson("common/submitter", JsonSubmitter.class);
        log.info("Parsed submitter to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/users/shooezgirl");
        assertThat(json.getUserName()).isEqualTo("shooezgirl");
    }
}
