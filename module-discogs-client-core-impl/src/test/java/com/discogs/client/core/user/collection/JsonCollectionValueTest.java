package com.discogs.client.core.user.collection;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonCollectionValueTest extends AbstractJsonTest {

    @Test
    public void parseCollectionValue() throws Exception {
        JsonCollectionValue json = readJson("user/collection/collection-value", JsonCollectionValue.class);
        log.info("Parsed collection value to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getMinimumValue()).isEqualTo("$75.00");
        assertThat(json.getMedianValue()).isEqualTo("$150.00");
        assertThat(json.getMaximumValue()).isEqualTo("$300.00");
    }
}
