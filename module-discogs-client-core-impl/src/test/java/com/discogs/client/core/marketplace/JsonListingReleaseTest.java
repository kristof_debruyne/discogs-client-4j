package com.discogs.client.core.marketplace;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonListingReleaseTest extends AbstractJsonTest {

    @Test
    public void parseListingRelease() throws Exception {
        JsonListing.JsonListingRelease json = readJson("marketplace/listing-release", JsonListing.JsonListingRelease.class);
        log.info("Parsed listing release to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(5610049);
        assertThat(json.getCatalogNumber()).isEqualTo("541125-1, 1-541125 (K1)");
        assertThat(json.getDescription()).isEqualTo("LCD Soundsystem - The Long Goodbye: LCD Soundsystem Live At Madison Square Garden (5xLP + Box)");
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/releases/5610049");
        assertThat(json.getThumbnail()).isEqualTo("https://api-img.discogs.com/UsvcarhmrXb0km4QH_dRP8gEf3E=/fit-in/600x600/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/R-5610049-1399500556-9283.jpeg.jpg");
        assertThat(json.getYear()).isEqualTo(2014);
    }
}
