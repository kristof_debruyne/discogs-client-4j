package com.discogs.client.core.marketplace;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonSellerTest extends AbstractJsonTest {

    @Test
    public void parseSeller() throws Exception {
        JsonSeller json = readJson("marketplace/seller", JsonSeller.class);
        log.info("Parsed seller to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(1369620);
        assertThat(json.getAvatarUrl()).isEqualTo("https://secure.gravatar.com/avatar/8aa676fcfa2be14266d0ccad88da2cc4?s=500&r=pg&d=mm");
        assertThat(json.getPaymentMethod()).isEqualTo("PayPal");
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/users/Booms528");
        assertThat(json.getShippingExtraInformation()).isEqualTo("Buyer responsible for shipping. Price depends on distance but is usually $5-10.");
        assertThat(json.getStatistics()).isNotNull();
        assertThat(json.getStatistics().getRatingStars()).isEqualTo(5.0);
        assertThat(json.getStatistics().getRatingValue()).isEqualTo("100");
        assertThat(json.getStatistics().getTotal()).isEqualTo(15);
        assertThat(json.getStatus()).isEqualTo("For Sale");
        assertThat(json.getUrl()).isEqualTo("https://api.discogs.com/users/Booms528");
        assertThat(json.getUsername()).isEqualTo("Booms528");
    }
}
