package com.discogs.client.core.user.wantlist;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonWantListItemTest extends AbstractJsonTest {

    @Test
    public void parseWantListItem() throws Exception {
        JsonWantListItem json = readJson("user/wantlist/wantlist-item", JsonWantListItem.class);
        log.info("Parsed want list item to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(1867708);
        assertThat(json.getBasicInformation()).isNotNull();
        assertThat(json.getNotes()).isEqualTo("Test Notes");
        assertThat(json.getRating()).isEqualTo(4);
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/users/example/wants/1867708");
    }
}
