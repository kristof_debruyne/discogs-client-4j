package com.discogs.client.core.marketplace;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonBuyerTest extends AbstractJsonTest {

    @Test
    public void parseBuyer() throws Exception {
        JsonBuyer json = readJson("marketplace/buyer", JsonBuyer.class);
        log.info("Parsed buyer to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(1369620);
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/users/Booms528");
        assertThat(json.getUsername()).isEqualTo("Booms528");
    }
}
