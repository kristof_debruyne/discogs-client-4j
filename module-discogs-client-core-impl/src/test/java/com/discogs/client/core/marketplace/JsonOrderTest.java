package com.discogs.client.core.marketplace;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonOrderTest extends AbstractJsonTest {

    @Test
    public void parseOrder() throws Exception {
        JsonOrder json = readJson("marketplace/order", JsonOrder.class);
        log.info("Parsed listing to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.isArchived()).isTrue();
        assertThat(json.getId()).isEqualTo("1-1");
        assertThat(json.getAdditionalInstructions()).isEqualTo("please use sturdy packaging.");
        assertThat(json.getBuyer()).isNotNull();
        assertThat(json.getDateCreated()).isEqualTo("2011-10-21T09:25:17-07:00");
        assertThat(json.getDateLastActivity()).isEqualTo("2011-10-21T09:25:17-07:00");
        assertThat(json.getFee()).isNotNull();
        assertThat(json.getItems()).hasSize(1);
        assertThat(json.getNextStatuses()).isNotEmpty();
        assertThat(json.getMessagesUrl()).isEqualTo("https://api.discogs.com/marketplace/orders/1-1/messages");
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/marketplace/orders/1-1");
        assertThat(json.getSeller()).isNotNull();
        assertThat(json.getShipping()).isNotNull();
        assertThat(json.getShippingAddress()).isEqualTo("Asdf Exampleton\n234 NE Asdf St.\nAsdf Town, Oregon, 14423\nUnited States\n\nPhone: 555-555-2733\nPaypal address: asdf@example.com");
        assertThat(json.getStatus()).isEqualTo("New Order");
        assertThat(json.getTotalPrice()).isNotNull();
        assertThat(json.getUri()).isEqualTo("http://www.discogs.com/sell/order/1-1");
    }
}
