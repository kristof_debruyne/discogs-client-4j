package com.discogs.client.core.root;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonApiInformationTest extends AbstractJsonTest {

    @Test
    public void parseApi() throws Exception {
        JsonApiInformation json = readJson("api", JsonApiInformation.class);
        log.info("Parsed api information to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getDescription()).isEqualTo("Welcome to the Discogs API.");
        assertThat(json.getDocumentationUrl()).isEqualTo("http://www.discogs.com/developers/");
        assertThat(json.getStatistics()).isNotNull();
        assertThat(json.getStatistics().getArtistCount()).isEqualTo(6_864_655);
        assertThat(json.getStatistics().getLabelCount()).isEqualTo(1_523_717);
        assertThat(json.getStatistics().getReleaseCount()).isEqualTo(12_493_221);
        assertThat(json.getVersion()).isEqualTo("v2");
    }
}
