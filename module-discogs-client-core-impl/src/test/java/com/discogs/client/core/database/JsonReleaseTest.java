package com.discogs.client.core.database;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonReleaseTest extends AbstractJsonTest {

    @Test
    public void parseRelease() throws Exception {
        JsonRelease json = readJson("database/release", JsonRelease.class);
        log.info("Parsed release to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(249504);
        assertThat(json.getArtists()).hasSize(1);
        assertThat(json.getExtraArtists()).hasSize(2);
        assertThat(json.getCommunity()).isNotNull();
        assertThat(json.getCompanies()).hasSize(2);
        assertThat(json.getDataQuality()).isEqualTo("Correct");
        assertThat(json.getDateAdded()).isEqualTo("2004-04-30T08:10:05-07:00");
        assertThat(json.getDateChanged()).isEqualTo("2012-12-03T02:50:12-07:00");
        assertThat(json.getEstimatedWeight()).isEqualTo(60);
        assertThat(json.getFormats()).hasSize(1);
        assertThat(json.getFormatQuantity()).isEqualTo(1);
        assertThat(json.getGenres()).containsExactly("Electronic", "Pop");
        assertThat(json.getIdentifiers()).hasSize(1);
        assertThat(json.getImages()).hasSize(2);
        assertThat(json.getLabels()).hasSize(1);
        assertThat(json.getMasterId()).isEqualTo(96559);
        assertThat(json.getMasterUrl()).isEqualTo("https://api.discogs.com/masters/96559");
        assertThat(json.getNotes()).isEqualTo("UK Release has a black label with the text \"Manufactured In England\" printed on it.");
        assertThat(json.getNumberForSale()).isEqualTo(58);
        assertThat(json.getReleased()).isEqualTo("1987");
        assertThat(json.getReleasedFormatted()).isEqualTo("1987");
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/releases/249504");
        assertThat(json.getSeries()).hasSize(0);
        assertThat(json.getStyles()).containsExactly("Synth-pop");
        assertThat(json.getThumbnail()).isEqualTo("https://api-img.discogs.com/kAXVhuZuh_uat5NNr50zMjN7lho=/fit-in/300x300/filters:strip_icc():format(jpeg):mode_rgb()/discogs-images/R-249504-1334592212.jpeg.jpg");
        assertThat(json.getTitle()).isEqualTo("Never Gonna Give You Up");
        assertThat(json.getTrackList()).hasSize(2);
        assertThat(json.getUri()).isEqualTo("http://www.discogs.com/Rick-Astley-Never-Gonna-Give-You-Up/release/249504");
        assertThat(json.getVideos()).hasSize(1);
        assertThat(json.getYear()).isEqualTo(1987);
    }

    @Test
    public void parseIdentifier() throws Exception {
        JsonRelease.JsonIdentifier json = readJson("database/identifier", JsonRelease.JsonIdentifier.class);
        log.info("Parsed identifier to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getType()).isEqualTo("Barcode");
        assertThat(json.getValue()).isEqualTo("5012394144777");
    }
}
