package com.discogs.client.core.marketplace;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonOrderReleaseTest extends AbstractJsonTest {

    @Test
    public void parseOrderRelease() throws Exception {
        JsonOrder.JsonOrderRelease json = readJson("marketplace/order-release", JsonOrder.JsonOrderRelease.class);
        log.info("Parsed order release to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(1);
        assertThat(json.getDescription()).isEqualTo("Persuader, The - Stockholm (2x12\")");
    }
}
