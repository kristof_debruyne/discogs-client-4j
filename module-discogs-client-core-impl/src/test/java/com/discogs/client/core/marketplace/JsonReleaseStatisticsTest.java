package com.discogs.client.core.marketplace;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonReleaseStatisticsTest extends AbstractJsonTest {

    @Test
    public void parseReleaseStatistics() throws Exception {
        JsonReleaseStatistics json = readJson("marketplace/release-statistics", JsonReleaseStatistics.class);
        assertNotNull(json);
        assertThat(json.isBlockedFromSale()).isTrue();
        assertThat(json.getNumberForSale()).isEqualTo(26);
        assertThat(json.getLowestPrice()).isNotNull();
    }
}
