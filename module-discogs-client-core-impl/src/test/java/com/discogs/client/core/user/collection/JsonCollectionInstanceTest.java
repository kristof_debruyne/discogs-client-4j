package com.discogs.client.core.user.collection;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonCollectionInstanceTest extends AbstractJsonTest {

    @Test
    public void parseCollectionInstance() throws Exception{
        JsonCollectionInstance json = readJson("user/collection/collection-instance", JsonCollectionInstance.class);
        log.info("Parsed collection instance to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(123456789);
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/users/Sikke303/collection/folders/1/release/1/instance/123456789");
    }

}
