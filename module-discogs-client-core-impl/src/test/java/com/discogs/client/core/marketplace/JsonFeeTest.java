package com.discogs.client.core.marketplace;

import com.discogs.client.api.core.common.Currency;
import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonFeeTest extends AbstractJsonTest {

    @Test
    public void parseFee() throws Exception {
        JsonFee json = readJson("marketplace/fee", JsonFee.class);
        log.info("Parsed fee to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getCurrency()).isEqualTo(Currency.USD);
        assertThat(json.getValue()).isEqualTo(0.42);
    }
}
