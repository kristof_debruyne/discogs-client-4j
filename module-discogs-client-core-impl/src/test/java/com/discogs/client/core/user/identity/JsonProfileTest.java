package com.discogs.client.core.user.identity;

import com.discogs.client.api.core.common.Currency;
import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonProfileTest extends AbstractJsonTest {

    @Test
    public void parseProfile() throws Exception {
        JsonProfile json = readJson("user/identity/profile", JsonProfile.class);
        log.info("Parsed profile to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(1337);
        assertThat(json.getAvatarUrl()).isEqualTo("http://www.gravatar.com/avatar/55502f40dc8b7c769880b10874abc9d0?s=52&r=pg&d=mm");
        assertThat(json.getBannerUrl()).isEqualTo("https://img.discogs.com/dhuJe-pRJmod7hN3cdVi2PugEh4=/1600x400/filters:strip_icc():format(jpeg)/discogs-banners/B-1578108-user-1436314164-9231.jpg.jpg");
        assertThat(json.getBuyerNumberRating()).isEqualTo(10);
        assertThat(json.getBuyerRating()).isEqualTo(75.00);
        assertThat(json.getBuyerRatingStars()).isEqualTo(5);
        assertThat(json.getCollectionFieldsUrl()).isEqualTo("https://api.discogs.com/users/sikke303/collection/fields");
        assertThat(json.getCollectionFolderUrl()).isEqualTo("https://api.discogs.com/users/sikke303/collection/folders");
        assertThat(json.getCurrency()).isEqualTo(Currency.EUR);
        assertThat(json.getEmail()).isEqualTo("kristof.debruyne@hotmail.com");
        assertThat(json.getHomePage()).isEqualTo("https://www.myhomepage.com");
        assertThat(json.getInventoryUrl()).isEqualTo("https://api.discogs.com/users/sikke303/inventory");
        assertThat(json.getLocation()).isEqualTo("Bierbeek");
        assertThat(json.getName()).isEqualTo("Kristof Debruyne");
        assertThat(json.getNumberCollection()).isEqualTo(100);
        assertThat(json.getNumberForSale()).isEqualTo(2);
        assertThat(json.getNumberLists()).isEqualTo(25);
        assertThat(json.getNumberPending()).isEqualTo(10);
        assertThat(json.getNumberWantList()).isEqualTo(200);
        assertThat(json.getProfile()).isEqualTo("Hello, my name is Kristof Debruyne");
        assertThat(json.getRank()).isEqualTo(149);
        assertThat(json.getRatingAverage()).isEqualTo(2.50);
        assertThat(json.getRegistered()).isEqualTo("2012-08-15T21:13:36-07:00");
        assertThat(json.getReleasesContributed()).isEqualTo(5);
        assertThat(json.getReleasesRated()).isEqualTo(116);
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/users/sikke303");
        assertThat(json.getSellerNumberRating()).isEqualTo(88);
        assertThat(json.getSellerRating()).isEqualTo(100.00);
        assertThat(json.getSellerRatingStars()).isEqualTo(69);
        assertThat(json.getUri()).isEqualTo("http://www.discogs.com/user/sikke303");
        assertThat(json.getUserName()).isEqualTo("sikke303");
        assertThat(json.getWantListUrl()).isEqualTo("https://api.discogs.com/users/sikke303/wants");
    }
}
