package com.discogs.client.core.common;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonContributorTest extends AbstractJsonTest {

    @Test
    public void parseContributor() throws Exception {
        JsonContributor json = readJson("common/contributor", JsonContributor.class);
        log.info("Parsed contributor to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/users/shooezgirl");
        assertThat(json.getUserName()).isEqualTo("shooezgirl");
    }
}
