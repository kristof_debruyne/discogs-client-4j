package com.discogs.client.core.marketplace;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonOrdersTest extends AbstractJsonTest {

    @Test
    public void parseOrders() throws Exception {
        JsonOrders json = readJson("marketplace/orders", JsonOrders.class);
        log.info("Parsed orders to JSON: {}", json);
        assertNotNull(json);
        assertNotNull(json.getPagination());
        assertThat(json.getOrders()).hasSize(1);
    }
}
