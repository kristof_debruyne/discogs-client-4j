package com.discogs.client.core.marketplace;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonOrderItemTest extends AbstractJsonTest {

    @Test
    public void parseOrderItem() throws Exception {
        JsonOrder.JsonOrderItem json = readJson("marketplace/order-item", JsonOrder.JsonOrderItem.class);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(41578242);
        assertThat(json.getPrice()).isNotNull();
        assertThat(json.getRelease()).isNotNull();
        assertThat(json.getCondition()).isEqualTo("Mint (M)");
        assertThat(json.getSleeveCondition()).isEqualTo("Fair (F)");
    }
}
