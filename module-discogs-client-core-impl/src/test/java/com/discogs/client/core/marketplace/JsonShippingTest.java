package com.discogs.client.core.marketplace;

import com.discogs.client.api.core.common.Currency;
import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonShippingTest extends AbstractJsonTest {

    @Test
    public void parseShipping() throws Exception {
        JsonShipping json = readJson("marketplace/shipping", JsonShipping.class);
        log.info("Parsed shipping to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getCurrency()).isEqualTo(Currency.USD);
        assertThat(json.getMethod()).isEqualTo("Standard");
        assertThat(json.getValue()).isEqualTo(50.00);
    }
}
