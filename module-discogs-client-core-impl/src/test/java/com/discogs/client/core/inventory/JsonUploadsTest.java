package com.discogs.client.core.inventory;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonUploadsTest extends AbstractJsonTest {

    @Test
    public void parseUploads() throws Exception {
        JsonUploads json = readJson("inventory/inventory-uploads", JsonUploads.class);
        log.info("Parsed inventory uploads to JSON: {}", json);
        assertNotNull(json);
        assertNotNull(json.getPagination());
        assertThat(json.getItems()).hasSize(1);
    }
}
