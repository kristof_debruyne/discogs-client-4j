package com.discogs.client.core.user.list;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonListTest extends AbstractJsonTest {

    @Test
    public void parsePrivateList() throws Exception {
        JsonList json = readJson("user/list/list-private", JsonList.class);
        log.info("Parsed private list to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(132409);
        assertThat(json.isPublicList()).isFalse();
        assertThat(json.getDateAdded()).isEqualTo("2020-05-13T04:23:49-07:00");
        assertThat(json.getDateChanged()).isEqualTo("2020-05-13T04:25:27-07:00");
        assertThat(json.getDescription()).isEqualTo("Test description.");
        assertThat(json.getImageUrl()).isNotNull();
        assertItems(json.getItems(), false);
        assertThat(json.getName()).isEqualTo("Test list (private)");
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/lists/132409");
        assertThat(json.getUri()).isEqualTo("https://www.discogs.com/lists/Test-list-private/132409");
        assertThat(json.getUser()).isNotNull();
    }

    private void assertItems(JsonList.JsonListItem[] list, boolean isPublic) {
        assertThat(list).hasSize(1);
        JsonList.JsonListItem item = list[0];
        assertThat(item.getId()).isEqualTo(132409);
        assertThat(item.getComment()).isEqualTo("Test comment.");
        assertThat(item.getDisplayTitle()).isEqualTo("Lackluster - Showcase");
        assertThat(item.getResourceUrl()).isEqualTo("https://api.discogs.com/releases/132409");
        assertThat(item.getType()).isEqualTo("release");
        assertThat(item.getUri()).isEqualTo("https://www.discogs.com/Lackluster-Showcase/release/132409");
        assertThat(item.getStatistics()).isNotNull();
        assertThat(item.getStatistics().getCommunityStatistics()).isNotNull();
        if(isPublic) {
            assertThat(item.getImageUrl()).isEqualTo("");
            assertThat(item.getStatistics().getUserStatistics()).isNull();
        } else {
            assertThat(item.getImageUrl()).isEqualTo("https://img.discogs.com/UXykgo4AanWZHUx4hhwyXfXSDWg=/fit-in/300x300/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/R-132409-1163023346.jpeg.jpg");
            assertThat(item.getStatistics().getUserStatistics()).isNotNull();
        }
    }

    @Test
    public void parsePublicList() throws Exception {
        JsonList json = readJson("user/list/list-public", JsonList.class);
        log.info("Parsed public list to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(582222);
        assertThat(json.isPublicList()).isTrue();
        assertThat(json.getDateAdded()).isEqualTo("2020-05-13T04:26:02-07:00");
        assertThat(json.getDateChanged()).isEqualTo("2020-05-13T04:26:02-07:00");
        assertThat(json.getDescription()).isEqualTo("Test description.");
        assertThat(json.getImageUrl()).isNotNull();
        assertItems(json.getItems(), true);
        assertThat(json.getName()).isEqualTo("Test list (public)");
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/lists/582222");
        assertThat(json.getUri()).isEqualTo("https://www.discogs.com/lists/Test-list-public/582222");
        assertThat(json.getUser()).isNotNull();
    }
}
