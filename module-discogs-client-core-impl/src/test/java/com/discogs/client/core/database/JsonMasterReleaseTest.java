package com.discogs.client.core.database;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonMasterReleaseTest extends AbstractJsonTest {

    @Test
    public void parseMaster() throws Exception {
        JsonMasterRelease json = readJson("database/master", JsonMasterRelease.class);
        log.info("Parsed master to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(1000);
        assertThat(json.getArtists()).hasSize(1);
        assertThat(json.getDataQuality()).isEqualTo("Correct");
        assertThat(json.getGenres()).containsExactly("Electronic");
        Assertions.assertThat(json.getImages()).hasSize(1);
        assertThat(json.getLowestPrice()).isEqualTo(9.36);
        assertThat(json.getMainReleaseId()).isEqualTo(66785);
        assertThat(json.getMainReleaseUrl()).isEqualTo("https://api.discogs.com/releases/66785");
        assertThat(json.getNumberForSale()).isEqualTo(9);
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/masters/1000");
        assertThat(json.getStyles()).containsExactly("Goa Trance");
        assertThat(json.getTitle()).isEqualTo("Stardiver");
        assertThat(json.getTrackList()).hasSize(3);
        assertThat(json.getUri()).isEqualTo("http://www.discogs.com/Electric-Universe-Stardiver/master/1000");
        assertThat(json.getVersionsUrl()).isEqualTo("https://api.discogs.com/masters/1000/versions");
        Assertions.assertThat(json.getVideos()).hasSize(1);
        assertThat(json.getYear()).isEqualTo(1997);
    }
}
