package com.discogs.client.core.marketplace;

import com.discogs.client.api.core.common.Currency;
import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonPriceTest extends AbstractJsonTest {

    @Test
    public void parsePrice() throws Exception {
        JsonPrice json = readJson("marketplace/price", JsonPrice.class);
        log.info("Parsed price to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getCurrency()).isEqualTo(Currency.USD);
        assertThat(json.getValue()).isEqualTo(2.5);
    }
}
