package com.discogs.client.core.user.contribution;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonContributionsTest extends AbstractJsonTest {

    @Test
    public void parseContributions() throws Exception {
        JsonContributions json = readJson("user/contribution/contributions", JsonContributions.class);
        log.info("Parsed contributions to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getResults()).isNotNull();
        Assertions.assertThat(json.getPagination()).isNotNull();
    }

    @Test
    public void parseContribution() throws Exception {
        JsonContributions.JsonContribution json = readJson("user/contribution/contribution", JsonContributions.JsonContribution.class);
        log.info("Parsed contribution to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(5531933);
        assertThat(json.getArtists()).hasSize(1);
        assertThat(json.getCommunity()).isNotNull();
        assertThat(json.getCompanies()).hasSize(0);
        assertThat(json.getCountry()).isEqualTo("US");
        assertThat(json.getDataQuality()).isEqualTo("Needs Vote");
        assertThat(json.getDateAdded()).isEqualTo("2014-03-25T15:16:13-07:00");
        assertThat(json.getDateChanged()).isEqualTo("2014-05-14T13:36:00-07:00");
        assertThat(json.getEstimatedWeight()).isEqualTo(60);
        assertThat(json.getFormatQuantity()).isEqualTo(1);
        assertThat(json.getFormats()).hasSize(1);
        assertThat(json.getGenres()).containsExactly("Rock", "Pop");
        assertThat(json.getImages()).hasSize(1);
        assertThat(json.getLabels()).hasSize(1);
        assertThat(json.getMasterId()).isEqualTo(223379);
        assertThat(json.getMasterUrl()).isEqualTo("https://api.discogs.com/masters/223379");
        assertThat(json.getNotes()).isEqualTo("Promotion Not For Sale");
        assertThat(json.getReleased()).isEqualTo("1989");
        assertThat(json.getReleasedFormatted()).isEqualTo("1989");
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/releases/5531933");
        assertThat(json.getSeries()).hasSize(0);
        assertThat(json.getStatus()).isEqualTo("Accepted");
        assertThat(json.getStyles()).containsExactly("Ballad");
        assertThat(json.getThumbnail()).isEqualTo("https://api-img.discogs.com/8et0xtf9REFloKoqi6NSJ6AJvFI=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb()/discogs-images/R-5531933-1400099758-6444.jpeg.jpg");
        assertThat(json.getTitle()).isEqualTo("After All");
        assertThat(json.getUri()).isEqualTo("http://www.discogs.com/Cher-And-Peter-Cetera-After-All/release/5531933");
        assertThat(json.getVideos()).hasSize(1);
        assertThat(json.getYear()).isEqualTo(1989);
    }
}
