package com.discogs.client.core.user;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonUserTest extends AbstractJsonTest {

    @Test
    public void parseUser() throws Exception {
        JsonUser json = readJson("user/user", JsonUser.class);
        log.info("Parsed user to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(1055016);
        assertThat(json.getAvatarUrl()).isEqualTo("https://secure.gravatar.com/avatar/d2a923f08608851ab3e29fe5267d46fc?s=500&r=pg&d=mm");
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/users/Sikke303");
        assertThat(json.getUsername()).isEqualTo("Sikke303");
    }
}
