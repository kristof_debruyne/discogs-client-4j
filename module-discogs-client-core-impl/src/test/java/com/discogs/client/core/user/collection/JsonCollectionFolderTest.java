package com.discogs.client.core.user.collection;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonCollectionFolderTest extends AbstractJsonTest {

    @Test
    public void parseCollectionFolder() throws Exception{
        JsonCollectionFolder json = readJson("user/collection/collection-folder", JsonCollectionFolder.class);
        log.info("Parsed collection folder to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(0);
        assertThat(json.getCount()).isEqualTo(23);
        assertThat(json.getName()).isEqualTo("All");
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/users/Sikke303/collection/folders/0");
    }
}
