package com.discogs.client.core.common;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonVideoTest extends AbstractJsonTest {

    @Test
    public void parseVideo() throws Exception {
        JsonVideo json = readJson("common/video", JsonVideo.class);
        log.info("Parsed video to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.isEmbedded()).isTrue();
        assertThat(json.getDescription()).isEqualTo("Rick Astley - Never Gonna Give You Up (Extended Version)");
        assertThat(json.getDuration()).isEqualTo(330);
        assertThat(json.getTitle()).isEqualTo("Never Gonna Give You Up (Extended Version)");
        assertThat(json.getUri()).isEqualTo("https://www.youtube.com/watch?v=te2jJncBVG4");
    }
}
