package com.discogs.client.core.common.stats;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonContainStatisticsTest extends AbstractJsonTest {

    @Test
    public void parseContainStatistic() throws Exception {
        JsonContainStatistics json = readJson("common/statistic-contain", JsonContainStatistics.class);
        log.info("Parsed contain statistic to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.isInCollection()).isTrue();
        assertThat(json.isInWantList()).isTrue();
    }
}
