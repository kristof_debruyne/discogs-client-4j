package com.discogs.client.core.common;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonCommunityTest extends AbstractJsonTest {

    @Test
    public void parseCommunity() throws Exception {
        JsonCommunity json = readJson("common/community", JsonCommunity.class);
        log.info("Parsed community to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getContributors()).hasSize(1);
        assertThat(json.getDataQuality()).isEqualTo("Needs Vote");
        assertThat(json.getHaveCount()).isEqualTo(1);
        assertThat(json.getRating()).isNotNull();
        assertThat(json.getRating().getAverage()).isEqualTo(2.5);
        assertThat(json.getRating().getCount()).isEqualTo(666);
        assertThat(json.getStatus()).isEqualTo("Accepted");
        assertThat(json.getSubmitter()).isNotNull();
        assertThat(json.getWantCount()).isEqualTo(5);
    }
}
