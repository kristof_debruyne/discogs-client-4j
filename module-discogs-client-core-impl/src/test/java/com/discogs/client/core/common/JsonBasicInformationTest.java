package com.discogs.client.core.common;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

@Log4j2
public class JsonBasicInformationTest extends AbstractJsonTest {

    @Test
    public void parseBasicInformation() throws Exception {
        JsonBasicInformation json = readJson("common/basic-information", JsonBasicInformation.class);
        assertThat(json).isNotNull();
        assertThat(json.getId()).isEqualTo(1867708);
        assertThat(json.getArtists()).hasSize(1);
        assertThat(json.getCoverImage()).isEqualTo("https://api-img.discogs.com/PsLAcp_I0-EPPkSBaHx2t7dmXTg=/fit-in/500x500/filters:strip_icc():format(jpeg):mode_rgb()/discogs-images/R-1867708-1248886216.jpeg.jpg");
        Assertions.assertThat(json.getFormats()).hasSize(1);
        assertThat(json.getLabels()).hasSize(1);
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/releases/1867708");
        assertThat(json.getThumbnail()).isEqualTo("https://api-img.discogs.com/PsLAcp_I0-EPPkSBaHx2t7dmXTg=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb()/discogs-images/R-1867708-1248886216.jpeg.jpg");
        assertThat(json.getTitle()).isEqualTo("Year Zero");
        assertThat(json.getYear()).isEqualTo(2007);
    }
}
