package com.discogs.client.core.inventory;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonExportTest extends AbstractJsonTest {

    @Test
    public void parseExport() throws Exception {
        JsonExport json = readJson("inventory/inventory-export", JsonExport.class);
        log.info("Parsed inventory export to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(599632);
        assertThat(json.getDateCreated()).isEqualTo("2018-09-27T12:59:02");
        assertThat(json.getDateFinished()).isEqualTo("2018-09-27T12:59:02");
        assertThat(json.getDownloadUrl()).isEqualTo("https://api.discogs.com/inventory/export/599632/download");
        assertThat(json.getFilename()).isEqualTo("cburmeister-inventory.csv");
        assertThat(json.getUrl()).isEqualTo("https://api.discogs.com/inventory/export/599632");
        assertThat(json.getStatus()).isEqualTo("success");
    }
}
