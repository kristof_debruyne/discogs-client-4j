package com.discogs.client.core.marketplace;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonListingTest extends AbstractJsonTest {

    @Test
    public void parseListing() throws Exception {
        JsonListing json = readJson("marketplace/listing", JsonListing.class);
        log.info("Parsed listing to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getId()).isEqualTo(172723812);
        assertThat(json.isAllowOffers()).isTrue();
        assertThat(json.isAudio()).isFalse();
        assertThat(json.getComment()).isEqualTo("Brand new... Still sealed!");
        assertThat(json.getCondition()).isEqualTo("Mint (M)");
        assertThat(json.getDatePosted()).isEqualTo("2014-07-15T12:55:01-07:00");
        assertThat(json.getExternalId()).isEqualTo("1234567890");
        assertThat(json.getFormatQuantity()).isEqualTo("1");
        assertThat(json.getLocation()).isEqualTo("New York");
        assertThat(json.getOriginalPrice()).isNotNull();
        assertThat(json.getOriginalShippingPrice()).isNotNull();
        assertThat(json.getPrice()).isNotNull();
        assertThat(json.getRelease()).isNotNull();
        assertThat(json.getResourceUrl()).isEqualTo("https://api.discogs.com/marketplace/listings/172723812");
        assertThat(json.getSeller()).isNotNull();
        assertThat(json.getShippingPrice()).isNotNull();
        assertThat(json.getShipsFrom()).isEqualTo("United States");
        assertThat(json.getSleeveCondition()).isEqualTo("Fair (F)");
        assertThat(json.getUri()).isEqualTo("http://www.discogs.com/sell/item/172723812");
        assertThat(json.getWeight()).isEqualTo("500");
    }
}
