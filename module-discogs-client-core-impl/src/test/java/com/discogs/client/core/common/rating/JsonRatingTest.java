package com.discogs.client.core.common.rating;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonRatingTest extends AbstractJsonTest {

    @Test
    public void parseRating() throws Exception {
        JsonRating json = readJson("common/rating", JsonRating.class);
        assertNotNull(json);
        assertThat(json.getAverage()).isEqualTo(2.5);
        assertThat(json.getCount()).isEqualTo(666);
    }
}
