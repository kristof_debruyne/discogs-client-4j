package com.discogs.client.core.database;

import com.discogs.client.core.AbstractJsonTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@Log4j2
public class JsonReleaseUserRatingTest extends AbstractJsonTest {

    @Test
    public void parseReleaseUserRating() throws Exception {
        JsonReleaseUserRating json = readJson("database/release-user-rating", JsonReleaseUserRating.class);
        log.info("Parsed user rating to JSON: {}", json);
        assertNotNull(json);
        assertThat(json.getReleaseId()).isEqualTo(249504);
        assertThat(json.getRating()).isEqualTo(5);
        assertThat(json.getUserName()).isEqualTo("memory");
    }
}
