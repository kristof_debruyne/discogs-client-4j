package com.discogs.client;

import com.discogs.client.api.DiscogsUserClient;
import com.discogs.client.api.core.common.sorting.SortOrder;
import com.discogs.client.api.core.user.collection.CollectionFields;
import com.discogs.client.api.core.user.collection.CollectionFolder;
import com.discogs.client.api.core.user.collection.CollectionFolders;
import com.discogs.client.api.core.user.collection.CollectionList;
import com.discogs.client.api.core.user.collection.CollectionValue;
import com.discogs.client.api.request.user.collection.CollectionFieldsRequest;
import com.discogs.client.api.request.user.collection.CollectionFolderCreateRequest;
import com.discogs.client.api.request.user.collection.CollectionFolderDeleteRequest;
import com.discogs.client.api.request.user.collection.CollectionFolderRequest;
import com.discogs.client.api.request.user.collection.CollectionFolderUpdateRequest;
import com.discogs.client.api.request.user.collection.CollectionFoldersRequest;
import com.discogs.client.api.request.user.collection.CollectionItemsByFolderRequest;
import com.discogs.client.api.request.user.collection.CollectionItemsByReleaseRequest;
import com.discogs.client.api.request.user.collection.CollectionValueRequest;
import com.discogs.client.auth.oauth.token.OAuthGeneratedToken;
import com.discogs.client.test.AbstractIntegrationTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientException;

import static com.discogs.client.api.common.DiscogsConstants.CONTENT_TYPE_APPLICATION_JSON;
import static com.discogs.client.api.request.AcceptMediaType.DISCOGS_AND_JSON;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;

/**
 * Integration test for class {@link RestDiscogsUserClient}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractIntegrationTest
 */
@Log4j2
public class RestDiscogsUserClientCollectionIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private DiscogsUserClient client;

    @Test
    public void getCollectionFolders() {
        CollectionFoldersRequest request = CollectionFoldersRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .username(getUsername())
                .build();

        CollectionFolders json = assertResponseAndReturnBody(client.getAll(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getFolders()).hasSizeGreaterThanOrEqualTo(2);
    }

    @Test
    public void getCollectionFolder0() {
        CollectionFolderRequest request = CollectionFolderRequest.builder()
                .folderId(0)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .username(getUsername())
                .build();

        CollectionFolder json = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
    }

    @Test
    public void getCollectionFolder1() {
        CollectionFolderRequest request = CollectionFolderRequest.builder()
                .folderId(1)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .username(getUsername())
                .build();

        CollectionFolder json = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
    }

    @Test
    public void getCollectionFolder1_NotAuthenticated() {
        assertThrows(FORBIDDEN, RestClientException.class, () -> {
            CollectionFolderRequest request = CollectionFolderRequest.builder()
                    .folderId(1)
                    .contentType(CONTENT_TYPE_APPLICATION_JSON)
                    .mediaType(DISCOGS_AND_JSON)
                    .username(getUsername())
                    .build();
            client.get(request);
        });
    }

    @Test
    public void manageCollectionFolder() {
        //CREATE
        CollectionFolderCreateRequest createRequest = CollectionFolderCreateRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .username(getUsername())
                .name("Test Folder")
                .build();

        CollectionFolder json = assertResponseAndReturnBody(client.create(createRequest), HttpStatus.CREATED); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getName()).isEqualTo("Test Folder");

        //UPDATE
        CollectionFolderUpdateRequest updateRequest = CollectionFolderUpdateRequest.builder()
                .folderId(json.getId())
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .username(getUsername())
                .name("Test Folder (updated)")
                .build();

        json = assertResponseAndReturnBody(client.modify(updateRequest), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getName()).isEqualTo("Test Folder (updated)");

        //DELETE
        CollectionFolderDeleteRequest deleteRequest = CollectionFolderDeleteRequest.builder()
                .folderId(json.getId())
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .username(getUsername())
                .build();
        assertResponseAndReturnBody(client.delete(deleteRequest), HttpStatus.NO_CONTENT); //EXECUTE
    }

    @Test
    public void getCollectionCustomFields() {
        CollectionFieldsRequest request = CollectionFieldsRequest.builder()
                .mediaType(DISCOGS_AND_JSON)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .username(getUsername())
                .build();

        CollectionFields json = assertResponseAndReturnBody(client.getAll(request), HttpStatus.OK);
        assertThat(json).isNotNull();
    }

    @Test
    public void getCollectionItemsByFolder() {
        CollectionItemsByFolderRequest request = CollectionItemsByFolderRequest.builder()
                .folderId(1)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .username(getUsername())
                .pagination(defaultPaginationBuilder())
                .sort(CollectionItemsByFolderRequest.SortMode.YEAR)
                .sortOrder(SortOrder.ASC)
                .build();
        CollectionList json = assertResponseAndReturnBody(client.getAll(request), HttpStatus.OK);
        assertThat(json).isNotNull();
    }

    @Test
    public void getCollectionItemsByRelease() {
        CollectionItemsByReleaseRequest request = CollectionItemsByReleaseRequest.builder()
                .releaseId(1241403)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .username(getUsername())
                .pagination(defaultPaginationBuilder())
                .build();
        CollectionList json = assertResponseAndReturnBody(client.getAll(request), HttpStatus.OK);
        assertThat(json).isNotNull();
    }

    @Test
    public void getCollectionValue() {
        CollectionValueRequest request = CollectionValueRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .username(getUsername())
                .build();

        CollectionValue json = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
    }
}
