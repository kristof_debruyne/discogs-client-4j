package com.discogs.client;

import com.discogs.client.api.DiscogsUserClient;
import com.discogs.client.api.core.user.list.List;
import com.discogs.client.api.core.user.list.UserLists;
import com.discogs.client.api.request.user.list.ListRequest;
import com.discogs.client.api.request.user.list.UserListsRequest;
import com.discogs.client.auth.oauth.token.OAuthGeneratedToken;
import com.discogs.client.test.AbstractIntegrationTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientException;

import static com.discogs.client.api.common.DiscogsConstants.CONTENT_TYPE_APPLICATION_JSON;
import static com.discogs.client.api.request.AcceptMediaType.DISCOGS_AND_JSON;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;

/**
 * Integration test for class {@link RestDiscogsUserClient}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractIntegrationTest
 */
@Log4j2
public class RestDiscogsUserClientListIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private DiscogsUserClient client;

    @Test
    public void getPrivateList() {
        ListRequest request = ListRequest.builder()
                .listId(582221)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .build();

        List json = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
    }

    @Test
    public void getPrivateList_NotFound() {
        assertThrows(LIST_NOT_FOUND, RestClientException.class, () -> {
            ListRequest request = ListRequest.builder()
                    .listId(582221)
                    .contentType(CONTENT_TYPE_APPLICATION_JSON)
                    .mediaType(DISCOGS_AND_JSON)
                    .build();
            client.get(request);
        });
    }

    @Test
    public void getPublicList() {
        ListRequest request = ListRequest.builder()
                .listId(582222)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .build();

        List json = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
    }

    @Test
    public void getUserLists() {
        UserListsRequest request = UserListsRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .username(getUsername())
                .pagination(defaultPaginationBuilder())
                .build();

        UserLists json = assertResponseAndReturnBody(client.getAll(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getLists()).isNotNull();
        assertThat(json.getPagination()).isNotNull();
    }
}
