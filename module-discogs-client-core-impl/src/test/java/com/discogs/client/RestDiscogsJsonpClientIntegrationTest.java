package com.discogs.client;

import com.discogs.client.api.DiscogsJsonpClient;
import com.discogs.client.api.request.root.ApiInformationRequest;
import com.discogs.client.test.AbstractIntegrationTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import static com.discogs.client.api.common.DiscogsConstants.CONTENT_TYPE_TEXT_JAVASCRIPT;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration test for class {@link RestDiscogsJsonpClient}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractIntegrationTest
 */
@Log4j2
public class RestDiscogsJsonpClientIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private DiscogsJsonpClient client;

    @Test
    public void givenJsonpRequest_whenExecuting_thenReturnOk() {
        ApiInformationRequest request = ApiInformationRequest.builder()
                .callback("testCallback")
                .contentType(CONTENT_TYPE_TEXT_JAVASCRIPT)
                .build();

        String response = assertResponseAndReturnBody(client.execute(HttpMethod.GET.name(), request), HttpStatus.OK); //EXECUTE
        assertThat(response).isNotNull();
        assertThat(response).startsWith("testCallback(");
        assertThat(response).endsWith(")");
    }
}
