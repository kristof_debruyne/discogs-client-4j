package com.discogs.client.config;

import com.discogs.client.api.config.DiscogsContext;
import com.discogs.client.auth.oauth.config.OAuthContext;
import com.discogs.client.test.AbstractUnitTest;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class DefaultUserAgentGeneratorTest extends AbstractUnitTest {

    @InjectMocks
    private DefaultUserAgentGenerator handler;

    @Mock
    private DiscogsContext<OAuthContext> discogsContext;

    @Test(expected = NullPointerException.class)
    public void givenInvokeGenerateUserAgent_whenNullApplicationNameIsPassed_thenThrowException() {
        when(discogsContext.getApiVersion()).thenReturn("v2");

        handler.generate();
    }

    @Test(expected = NullPointerException.class)
    public void givenInvokeGenerateUserAgent_whenNullApiVersionIsPassed_thenThrowException() {
        when(discogsContext.getApiVersion()).thenReturn("v2");

        handler.generate();
    }

    @Test(expected = NullPointerException.class)
    public void givenInvokeGenerateUserAgent_whenNullApplicationVersionIsPassed_thenThrowException() {
        when(discogsContext.getApiVersion()).thenReturn("v2");
        when(discogsContext.getApplicationName()).thenReturn("TestClient");

        handler.generate();
    }

    @Test(expected = NullPointerException.class)
    public void givenInvokeGenerateUserAgent_whenNullUuidIsPassed_thenThrowException() {
        when(discogsContext.getApiVersion()).thenReturn("v2");
        when(discogsContext.getApplicationName()).thenReturn("TestClient");
        when(discogsContext.getApplicationVersion()).thenReturn("v1.0");

        handler.generate();
    }

    @Test
    public void givenInvokeGenerateUserAgent_whenNonValuesArePassed_thenReturnUserAgent() {
        when(discogsContext.getApiVersion()).thenReturn("v2");
        when(discogsContext.getApplicationName()).thenReturn("TestClient");
        when(discogsContext.getApplicationVersion()).thenReturn("v1.0");
        when(discogsContext.getApplicationKey()).thenReturn("JMx952");

        assertThat(handler.generate())
                .isEqualTo("Discogs/v2 Client=TestClient/v1.0 (ID=%s)", "JMx952");
    }
}