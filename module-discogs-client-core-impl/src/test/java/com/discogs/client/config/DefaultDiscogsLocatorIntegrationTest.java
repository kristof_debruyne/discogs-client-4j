package com.discogs.client.config;

import com.discogs.client.api.DiscogsClient;
import com.discogs.client.api.DiscogsDatabaseClient;
import com.discogs.client.api.DiscogsImageClient;
import com.discogs.client.api.DiscogsInventoryClient;
import com.discogs.client.api.DiscogsJsonpClient;
import com.discogs.client.api.DiscogsMarketplaceClient;
import com.discogs.client.api.DiscogsRootClient;
import com.discogs.client.api.DiscogsUserClient;
import com.discogs.client.api.config.DiscogsLocator;
import com.discogs.client.security.DiscogsOAuthV1Service;
import com.discogs.client.test.AbstractIntegrationTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration test for class {@link DefaultDiscogsLocator}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractIntegrationTest
 */
@Log4j2
public class DefaultDiscogsLocatorIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private DiscogsLocator locator;

    @Test
    public void getClients() {
        assertThat(locator.lookupClient(DiscogsDatabaseClient.class)).isPresent();
        assertThat(locator.lookupClient(DiscogsImageClient.class)).isPresent();
        assertThat(locator.lookupClient(DiscogsInventoryClient.class)).isPresent();
        assertThat(locator.lookupClient(DiscogsJsonpClient.class)).isPresent();
        assertThat(locator.lookupClient(DiscogsMarketplaceClient.class)).isPresent();
        assertThat(locator.lookupClient(DiscogsRootClient.class)).isPresent();
        assertThat(locator.lookupClient(DiscogsUserClient.class)).isPresent();
        assertThat(locator.lookupClient(DiscogsClient.class)).isNotPresent();
    }

    @Test
    public void getServices() {
        assertThat(locator.lookupService(DiscogsOAuthV1Service.class)).isPresent();
    }
}
