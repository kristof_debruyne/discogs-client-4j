package com.discogs.client.config;

import com.discogs.client.auth.oauth.config.OAuthClientConfiguration;
import com.discogs.client.auth.oauth.config.OAuthContext;
import com.discogs.client.auth.oauth.config.OAuthServerConfiguration;
import com.discogs.client.auth.oauth.signature.SignatureService;
import com.discogs.client.auth.oauth.token.OAuthTokenParser;
import com.discogs.client.auth.oauth.verifier.OAuthVerifierParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

@RunWith(MockitoJUnitRunner.class)
public class DefaultDiscogsContextTest {

    @InjectMocks
    private DefaultDiscogsContext context;

    @Spy
    private final OAuthContext oAuthContext = createOAuthContext();

    @Before
    public void before() {
        context = new DefaultDiscogsContext(oAuthContext);
    }

    @Test
    public void givenInvokeGetApplicationName_thenApplicationNameIsReturned() {
        assertThat(context.getApplicationName()).isEqualTo("DiscogsTestClient");
    }

    @Test
    public void givenInvokeGetApplicationVersion_thenApplicationVersionIsReturned() {
        assertThat(context.getApplicationVersion()).isEqualTo("v1.0");
    }

    @Test
    public void givenInvokeGetApiRootUrl_thenApiRootUrlIsReturned() {
        assertThat(context.getApiRootUrl()).isEqualTo("https://api.discogs.com");
    }

    @Test
    public void givenInvokeGetImageRootUrl_thenWebImageRootUrlIsReturned() {
        assertThat(context.getImageRootUrl()).isEqualTo("https://api-img.discogs.com");
    }

    @Test
    public void givenInvokeGetWebRootUrl_thenWebRootUrlIsReturned() {
        assertThat(context.getWebRootUrl()).isEqualTo("https://www.discogs.com");
    }

    @Test
    public void givenInvokeGetOAuthContext_thenContextIsReturned() {
        assertThat(oAuthContext).isEqualTo(context.getAuthenticationContext());
    }

    private OAuthContext createOAuthContext() {
        OAuthClientConfiguration clientConfiguration = spy(OAuthClientConfiguration.builder()
                .applicationVersion("v1.0")
                .applicationKey("fu7Vp4v")
                .applicationName("DiscogsTestClient")
                .build()
        );
        OAuthServerConfiguration serverConfiguration = spy(OAuthServerConfiguration.builder()
                .apiVersion("v2")
                .apiRootUrl("https://api.discogs.com")
                .imageRootUrl("https://api-img.discogs.com")
                .webRootUrl("https://www.discogs.com")
                .build()
        );
        OAuthTokenParser tokenParser = mock(OAuthTokenParser.class);
        OAuthVerifierParser verifierParser = mock(OAuthVerifierParser.class);
        SignatureService signatureService = mock(SignatureService.class);
        return new OAuthContext(clientConfiguration, serverConfiguration, tokenParser, verifierParser, signatureService);
    }
}