package com.discogs.client.config;

import com.discogs.client.api.throttle.ThrottleMode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class DefaultDiscogsPropertiesTest {

    @Test
    public void givenNoConsumerKeyAndSecretButPersonalToken_whenCreatingProperties_thenReturnProperties() {
        final DefaultDiscogsProperties properties = newBuilder()
                .personalToken("898413af-e4df-4a8b-ab03-ee917920f3b8")
                .build();

        assertThat(properties).isNotNull();
        assertThat(properties.getCallbackUrl()).isNull();
        assertThat(properties.getConsumerKey()).isNull();
        assertThat(properties.getConsumerSecret()).isNull();
        assertThat(properties.getPersonalToken()).isNotNull();
    }

    @Test
    public void givenConsumerKeyAndSecretButNoPersonalToken_whenCreatingProperties_thenReturnProperties() {
        final DefaultDiscogsProperties properties = newBuilder()
                .callbackUrl("http://localhost:8080/callback")
                .consumerKey("consumerKey")
                .consumerSecret("consumerSecret")
                .build();

        assertThat(properties).isNotNull();
        assertThat(properties.getCallbackUrl()).isNotNull();
        assertThat(properties.getConsumerKey()).isNotNull();
        assertThat(properties.getConsumerSecret()).isNotNull();
        assertThat(properties.getPersonalToken()).isNull();
    }

    private DefaultDiscogsProperties.DefaultDiscogsPropertiesBuilder newBuilder() {
        return DefaultDiscogsProperties.builder()
                .applicationKey("cef4b76e-88cc-4f4c-b484-d20ee19b2ecd")
                .applicationName("Test")
                .applicationVersion("1")
                .apiVersion("v2")
                .apiRootUrl("http://localhost:8080/api")
                .imageRootUrl("http://localhost:8080/images")
                .webRootUrl("http://localhost:8080/web")
                .authorizeEndpoint("http://localhost:8080/oauth/authorize")
                .accessTokenEndpoint("http://localhost:8080/oauth/access_token")
                .requestTokenEndpoint("http://localhost:8080/oauth/request_token")
                .revokeTokenEndpoint("http://localhost:8080/oauth/revoke")
                .throttlingRequestsEnabled(true)
                .throttleMode(ThrottleMode.NORMAL)
                .throttleCoolDownTimePeriod(5000);
    }
}