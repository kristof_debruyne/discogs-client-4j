package com.discogs.client;

import com.discogs.client.api.DiscogsDatabaseClient;
import com.discogs.client.api.core.common.Currency;
import com.discogs.client.api.core.common.rating.RatingValue;
import com.discogs.client.api.core.common.sorting.SortOrder;
import com.discogs.client.api.core.database.Artist;
import com.discogs.client.api.core.database.ArtistReleases;
import com.discogs.client.api.core.database.Label;
import com.discogs.client.api.core.database.LabelReleases;
import com.discogs.client.api.core.database.MasterRelease;
import com.discogs.client.api.core.database.MasterVersions;
import com.discogs.client.api.core.database.Release;
import com.discogs.client.api.core.database.ReleaseCommunityRating;
import com.discogs.client.api.core.database.ReleaseUserRating;
import com.discogs.client.api.core.database.SearchResult;
import com.discogs.client.api.core.database.SearchType;
import com.discogs.client.api.request.database.ArtistReleasesRequest;
import com.discogs.client.api.request.database.ArtistRequest;
import com.discogs.client.api.request.database.LabelReleasesRequest;
import com.discogs.client.api.request.database.LabelRequest;
import com.discogs.client.api.request.database.MasterReleaseRequest;
import com.discogs.client.api.request.database.MasterVersionsRequest;
import com.discogs.client.api.request.database.ReleaseCommunityRatingRequest;
import com.discogs.client.api.request.database.ReleaseRequest;
import com.discogs.client.api.request.database.ReleaseUserRatingDeleteRequest;
import com.discogs.client.api.request.database.ReleaseUserRatingRequest;
import com.discogs.client.api.request.database.ReleaseUserRatingUpdateRequest;
import com.discogs.client.api.request.database.SearchRequest;
import com.discogs.client.auth.oauth.token.OAuthGeneratedToken;
import com.discogs.client.test.AbstractIntegrationTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientException;

import static com.discogs.client.api.common.DiscogsConstants.CONTENT_TYPE_APPLICATION_JSON;
import static com.discogs.client.api.request.AcceptMediaType.DISCOGS_AND_JSON;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;

/**
 * Integration test for class {@link RestDiscogsDatabaseClient}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractIntegrationTest
 */
@Log4j2
public class RestDiscogsDatabaseClientIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private DiscogsDatabaseClient client;

    //*** ARTIST ***//

    @Test
    public void getArtist() {
        ArtistRequest request = ArtistRequest.builder()
                .artistId(108713)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .build();

        Artist json = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getId()).isEqualTo(108713);
    }

    @Test
    public void getArtistReleases() {
        ArtistReleasesRequest request = ArtistReleasesRequest.builder()
                .artistId(108713)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .pagination(defaultPaginationBuilder())
                .sort(ArtistReleasesRequest.SortMode.TITLE)
                .sortOrder(SortOrder.ASC)
                .build();

        ArtistReleases json = assertResponseAndReturnBody(client.getAll(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getPagination()).isNotNull();
        assertThat(json.getReleases()).isNotNull();
    }

    //*** LABEL ***//

    @Test
    public void getLabel() {
        LabelRequest request = LabelRequest.builder()
                .labelId(434)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .build();

        Label json = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getId()).isEqualTo(434);
    }

    @Test
    public void getLabelReleases() {
        LabelReleasesRequest request = LabelReleasesRequest.builder()
                .labelId(434)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .pagination(defaultPaginationBuilder())
                .build();

        LabelReleases json = assertResponseAndReturnBody(client.getAll(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getPagination()).isNotNull();
        assertThat(json.getReleases()).isNotNull();
    }

    //*** MASTER ***//

    @Test
    public void getMasterRelease() {
        MasterReleaseRequest request = MasterReleaseRequest.builder()
                .masterId(700894)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .build();

        MasterRelease json = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getId()).isEqualTo(700894);
    }

    @Test
    public void getMasterVersions() {
        MasterVersionsRequest request = MasterVersionsRequest.builder()
                .masterId(700894)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .pagination(defaultPaginationBuilder())
                .labelName("Narita Records")
                .sort(MasterVersionsRequest.SortMode.LABEL)
                .sortOrder(SortOrder.ASC)
                .build();

        MasterVersions json = assertResponseAndReturnBody(client.getAll(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getVersions()).isNotNull();
        assertThat(json.getPagination()).isNotNull();
    }

    //*** RELEASE ***//

    @Test
    public void getRelease() {
        ReleaseRequest request = ReleaseRequest.builder()
                .releaseId(132409)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .currency(Currency.EUR)
                .build();

        Release json  = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getId()).isEqualTo(132409);
    }

    //*** RELEASE RATING COMMUNITY ***//

    @Test
    public void getReleaseCommunityRating() {
        ReleaseCommunityRatingRequest request = ReleaseCommunityRatingRequest.builder()
                .releaseId(132409)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .build();

        ReleaseCommunityRating json = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getReleaseId()).isEqualTo(132409);
        assertThat(json.getRating()).isNotNull();
        assertThat(json.getRating().getAverage()).isGreaterThan(0.0);
        assertThat(json.getRating().getCount()).isGreaterThan(0);
    }

    //*** RELEASE RATING USER ***//

    @Test
    public void getReleaseUserRating() {
        ReleaseUserRatingRequest request = ReleaseUserRatingRequest.builder()
                .releaseId(132409)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .username(getUsername())
                .build();

        ReleaseUserRating json = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getReleaseId()).isEqualTo(132409);
        assertThat(json.getRating()).isGreaterThanOrEqualTo(0);
        assertThat(json.getUserName()).isEqualTo(getUsername());
    }

    @Test
    public void updateReleaseUserRating() {
        ReleaseUserRatingUpdateRequest request = ReleaseUserRatingUpdateRequest.builder()
                .releaseId(132409)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .username(getUsername())
                .rating(RatingValue.HIGHEST)
                .build();

        ReleaseUserRating json = assertResponseAndReturnBody(client.modify(request), HttpStatus.CREATED); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getRating()).isEqualTo(5);
    }

    @Test
    public void updateReleaseUserRating_Forbidden() {
        assertThrows(FORBIDDEN, RestClientException.class, () -> {
            ReleaseUserRatingUpdateRequest request = ReleaseUserRatingUpdateRequest.builder()
                    .releaseId(132409)
                    .contentType(CONTENT_TYPE_APPLICATION_JSON)
                    .mediaType(DISCOGS_AND_JSON)
                    .authentication(new OAuthGeneratedToken(getPersonalToken()))
                    .username("TyIvy")
                    .rating(RatingValue.HIGH)
                    .build();
            client.modify(request);
        });
    }

    @Test
    public void deleteReleaseUserRating() {
        ReleaseUserRatingDeleteRequest request = ReleaseUserRatingDeleteRequest.builder()
                .releaseId(132409)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .username(getUsername())
                .build();
        assertResponseAndReturnBody(client.delete(request), HttpStatus.NO_CONTENT);
    }

    @Test
    public void deleteReleaseUserRating_Forbidden() {
        assertThrows(FORBIDDEN, RestClientException.class, () -> {
            ReleaseUserRatingDeleteRequest request = ReleaseUserRatingDeleteRequest.builder()
                    .releaseId(132409)
                    .contentType(CONTENT_TYPE_APPLICATION_JSON)
                    .mediaType(DISCOGS_AND_JSON)
                    .authentication(new OAuthGeneratedToken(getPersonalToken()))
                    .username("BR1fJm1")
                    .build();
            client.delete(request); //EXECUTE
        });
    }

    //*** SEARCH ***//

    @Test
    public void search() {
        SearchRequest request = SearchRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .mediaType(DISCOGS_AND_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .pagination(defaultPaginationBuilder())
                .query("lackluster - showcase")
                .artist("Lackluster")
                .catalogNumber("MERCK 015")
                .country("US")
                .genre("Electronic")
                .format("CD")
                .label("Merck")
                .style("IDM")
                .title("Showcase")
                .type(SearchType.RELEASE)
                .build();

        SearchResult json = assertResponseAndReturnBody(client.search(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getItems()).isNotNull();
        assertThat(json.getPagination()).isNotNull();
    }
}
