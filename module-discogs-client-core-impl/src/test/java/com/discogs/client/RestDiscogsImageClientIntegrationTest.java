package com.discogs.client;

import com.discogs.client.api.DiscogsImageClient;
import com.discogs.client.api.request.image.ImageRequest;
import com.discogs.client.api.response.Response;
import com.discogs.client.test.AbstractIntegrationTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;

import static com.discogs.client.api.common.DiscogsConstants.CONTENT_TYPE_IMAGE_JPEG;
import static com.discogs.client.api.common.DiscogsConstants.CONTENT_TYPE_IMAGE_PNG;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration test for class {@link RestDiscogsImageClient}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractIntegrationTest
 */
@Log4j2
public class RestDiscogsImageClientIntegrationTest extends AbstractIntegrationTest {

    private static final String IMAGE_URL =
            "https://api-img.discogs.com/z_u8yqxvDcwVnR4tX2HLNLaQO2Y=/fit-in/600x600/filters:strip_icc():format(jpeg):mode_rgb():quality(96)/discogs-images/R-249504-1334592212.jpeg.jpg";

    @Autowired
    private DiscogsImageClient client;

    @Test(expected = IllegalArgumentException.class)
    public void givenInvokeGet_whenIncorrectUrlIsPassed_thenThrowException() {
        ImageRequest request = ImageRequest.builder()
                .contentType(CONTENT_TYPE_IMAGE_PNG)
                .url(IMAGE_URL)
                .build();

        client.get(request);
    }

    @Test
    public void givenInvokeGet_whenCorrectUrlIsPassed_thenReturnImage() throws Exception {
        ImageRequest request = ImageRequest.builder()
                .contentType(CONTENT_TYPE_IMAGE_JPEG)
                .url(IMAGE_URL)
                .build();

        Response<byte[]> response = client.get(request);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getBody()).isNotNull();
        assertThat(ImageIO.read(new ByteArrayInputStream(response.getBody()))).isNotNull();
    }
}
