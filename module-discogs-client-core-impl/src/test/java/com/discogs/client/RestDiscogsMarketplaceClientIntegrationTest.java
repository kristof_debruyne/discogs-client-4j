package com.discogs.client;

import com.discogs.client.api.core.common.Currency;
import com.discogs.client.api.core.common.sorting.SortOrder;
import com.discogs.client.api.core.marketplace.ListingStatus;
import com.discogs.client.api.core.marketplace.OrderStatus;
import com.discogs.client.api.request.marketplace.FeeCurrencyRequest;
import com.discogs.client.api.request.marketplace.FeeRequest;
import com.discogs.client.api.request.marketplace.ListingsRequest;
import com.discogs.client.api.request.marketplace.OrdersRequest;
import com.discogs.client.api.request.marketplace.PriceSuggestionsRequest;
import com.discogs.client.api.request.marketplace.ReleaseStatisticsRequest;
import com.discogs.client.auth.oauth.token.OAuthGeneratedToken;
import com.discogs.client.core.marketplace.JsonFee;
import com.discogs.client.core.marketplace.JsonListings;
import com.discogs.client.core.marketplace.JsonOrders;
import com.discogs.client.core.marketplace.JsonPriceSuggestions;
import com.discogs.client.core.marketplace.JsonReleaseStatistics;
import com.discogs.client.test.AbstractIntegrationTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import static com.discogs.client.api.common.DiscogsConstants.CONTENT_TYPE_APPLICATION_JSON;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration test for class {@link RestDiscogsMarketplaceClient}. <br><br>
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractIntegrationTest
 */
@Log4j2
public class RestDiscogsMarketplaceClientIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private RestDiscogsMarketplaceClient client;

    @Test
    public void getFeeForDefaultCurrency() {
        FeeRequest request = FeeRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .price(10.00)
                .build();

        JsonFee json = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
    }

    @Test
    public void getFeeForCustomCurrency() {
        FeeCurrencyRequest request = FeeCurrencyRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .price(10.00)
                .currency(Currency.EUR)
                .build();

        JsonFee json = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
    }

    @Test
    public void getPriceSuggestions() {
        PriceSuggestionsRequest request = PriceSuggestionsRequest.builder()
                .releaseId(1241403)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .build();

        JsonPriceSuggestions json = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
    }

    @Test
    public void getListings() {
        ListingsRequest request = ListingsRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .pagination(defaultPaginationBuilder())
                .sort(ListingsRequest.SortMode.PRICE)
                .sortOrder(SortOrder.ASC)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .username(getUsername())
                .status(ListingStatus.FOR_SALE)
                .build();

        JsonListings json = assertResponseAndReturnBody(client.getAll(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
    }

    @Test
    public void getOrders() {
        OrdersRequest request = OrdersRequest.builder()
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .pagination(defaultPaginationBuilder())
                .sort(OrdersRequest.SortMode.ID)
                .sortOrder(SortOrder.ASC)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .status(OrderStatus.ALL)
                .build();

        JsonOrders json = assertResponseAndReturnBody(client.getAll(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
    }

    @Test
    public void getReleaseStatistics() {
        ReleaseStatisticsRequest request = ReleaseStatisticsRequest.builder()
                .releaseId(1241403)
                .currency(Currency.EUR)
                .contentType(CONTENT_TYPE_APPLICATION_JSON)
                .authentication(new OAuthGeneratedToken(getPersonalToken()))
                .build();

        JsonReleaseStatistics json = assertResponseAndReturnBody(client.get(request), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
    }
}
