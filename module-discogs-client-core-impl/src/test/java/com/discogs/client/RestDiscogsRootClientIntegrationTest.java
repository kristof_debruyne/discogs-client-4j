package com.discogs.client;

import com.discogs.client.api.DiscogsRootClient;
import com.discogs.client.api.core.ApiInformation;
import com.discogs.client.api.exception.DiscogsRuntimeException;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.root.ApiInformationRequest;
import com.discogs.client.test.AbstractIntegrationTest;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import static com.discogs.client.api.common.DiscogsConstants.CONTENT_TYPE_TEXT_JAVASCRIPT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;

/**
 * Integration test for class {@link RestDiscogsRootClient}.
 *
 * @author Sikke303
 * @since 1.0
 * @see AbstractIntegrationTest
 */
@Log4j2
public class RestDiscogsRootClientIntegrationTest extends AbstractIntegrationTest {

    private static final String DISCOGS_API_VERSION = "v2";

    @Autowired
    private DiscogsRootClient client;

    @Test
    public void getApiInformation() {
        ApiInformation json = assertResponseAndReturnBody(client.getApiInformation(), HttpStatus.OK); //EXECUTE
        assertThat(json).isNotNull();
        assertThat(json.getDescription()).isNotNull();
        assertThat(json.getDocumentationUrl()).isNotNull();
        assertThat(json.getStatistics()).isNotNull();
        assertThat(json.getStatistics().getArtistCount()).isGreaterThan(0);
        assertThat(json.getStatistics().getLabelCount()).isGreaterThan(0);
        assertThat(json.getStatistics().getReleaseCount()).isGreaterThan(0);
        assertThat(json.getVersion()).isEqualTo(DISCOGS_API_VERSION);
    }

    @Test
    public void getApiInformationAsJsonp_throwException() {
        assertThrows(JSONP_NOT_SUPPORTED, DiscogsRuntimeException.class, () -> {
            ApiInformationRequest request = ApiInformationRequest.builder()
                    .callback("someCallbackName")
                    .mediaType(AcceptMediaType.DEFAULT)
                    .contentType(CONTENT_TYPE_TEXT_JAVASCRIPT)
                    .build();
            client.getApiInformation(request);
        });
    }

    @Test
    public void getApiVersion() {
        String version = assertResponseAndReturnBody(client.getApiVersion(), HttpStatus.OK); //EXECUTE
        assertThat(version).isNotNull();
        assertThat(version).isEqualTo(DISCOGS_API_VERSION);
    }
}
