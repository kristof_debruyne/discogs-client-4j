package com.discogs.client.example.spring;

import com.discogs.client.api.DiscogsRootClient;
import com.discogs.client.api.core.ApiInformation;
import com.discogs.client.api.response.Response;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.annotation.Nullable;

/**
 * Spring example application. <br><br>
 *
 * Required arguments: <br>
 * - DISCOGS_CLIENT_APPLICATION_NAME <br>
 * - DISCOGS_CLIENT_APPLICATION_KEY <br>
 * - DISCOGS_CLIENT_APPLICATION_VERSION <br>
 * - DISCOGS_CLIENT_PERSONAL_TOKEN <br><br>
 *
 * We use the personal token just for testing purposes.
 */
@RequiredArgsConstructor
public class SpringExampleApplication extends Thread {

    @Getter
    private final String[] args;

    public static void main(@Nullable String[] args) { new SpringExampleApplication(args).start(); }

    @Override
    public void run() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        DiscogsRootClient client = context.getBean(DiscogsRootClient.class);

        Response<? extends ApiInformation> response = client.getApiInformation();
        System.out.println("Discogs API response -> " + response.getBody());
    }
}