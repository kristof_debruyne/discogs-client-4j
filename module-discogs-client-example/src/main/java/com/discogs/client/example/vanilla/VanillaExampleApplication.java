package com.discogs.client.example.vanilla;

import com.discogs.client.RestDiscogsRootClient;
import com.discogs.client.api.DiscogsRootClient;
import com.discogs.client.api.config.DiscogsConfigurable;
import com.discogs.client.api.config.DiscogsContext;
import com.discogs.client.api.config.DiscogsProperties;
import com.discogs.client.api.config.UserAgentGenerator;
import com.discogs.client.api.core.ApiInformation;
import com.discogs.client.api.response.Response;
import com.discogs.client.api.throttle.ThrottleMode;
import com.discogs.client.auth.oauth.config.OAuthClientConfiguration;
import com.discogs.client.auth.oauth.config.OAuthConfigurable;
import com.discogs.client.auth.oauth.config.OAuthContext;
import com.discogs.client.auth.oauth.config.OAuthContextBuilder;
import com.discogs.client.auth.oauth.config.OAuthScope;
import com.discogs.client.auth.oauth.config.OAuthServerConfiguration;
import com.discogs.client.auth.oauth.service.OAuthV1Service;
import com.discogs.client.config.DefaultDiscogsContext;
import com.discogs.client.config.DefaultDiscogsProperties;
import com.discogs.client.config.DefaultUserAgentGenerator;
import com.discogs.client.security.DiscogsOAuthV1Service;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;
import java.util.UUID;

import static com.discogs.client.auth.oauth.config.OAuthScope.CALLBACK_URL;
import static com.discogs.client.auth.oauth.config.OAuthScope.OUT_OF_BAND;

/**
 * Vanilla example application. <br><br>
 *
 * Required arguments: <br>
 * - DISCOGS_CLIENT_APPLICATION_NAME <br>
 * - DISCOGS_CLIENT_APPLICATION_KEY <br>
 * - DISCOGS_CLIENT_APPLICATION_VERSION <br>
 * - DISCOGS_CLIENT_PERSONAL_TOKEN <br><br>
 *
 * We use the personal token just for testing purposes.
 */
@RequiredArgsConstructor
public class VanillaExampleApplication extends Thread implements DiscogsConfigurable, OAuthConfigurable {

    private DiscogsProperties properties;

    @Getter
    private final String[] args;

    public static void main(@Nullable String[] args) { new VanillaExampleApplication(args).start(); }

    @Override
    public void run() {
        properties = getProperties();

        DiscogsContext<OAuthContext> context = initializeDiscogsContext();
        UserAgentGenerator userAgentGenerator = new DefaultUserAgentGenerator(context);
        RestTemplate template = new RestTemplate();
        OAuthV1Service service = new DiscogsOAuthV1Service(context.getAuthenticationContext(), template, userAgentGenerator);
        DiscogsRootClient client = new RestDiscogsRootClient(URI.create(context.getApiRootUrl()), service, template, userAgentGenerator);

        Response<? extends ApiInformation> response = client.getApiInformation();
        System.out.println("Discogs API response -> " + response.getBody());
    }

    //*** CONFIGURATION ***//

    @Nonnull
    @Override
    public OAuthScope getOAuthScope() {
        return properties.hasCallbackUrl() ? CALLBACK_URL : OUT_OF_BAND;
    }

    @Nonnull
    public DiscogsContext<OAuthContext> initializeDiscogsContext() {
        return new DefaultDiscogsContext(createOAuthContext());
    }

    @Nonnull
    public DiscogsProperties getProperties() {
        DefaultDiscogsProperties.DefaultDiscogsPropertiesBuilder builder = DefaultDiscogsProperties.builder();
        //CLIENT
        builder.applicationKey(UUID.randomUUID().toString());
        builder.applicationName("Vanilla Test Client");
        builder.applicationKey("bNSMks0Y");
        builder.applicationVersion("v1.0");
        builder.callbackUrl("http://www.somesite.com/callback");
        builder.consumerKey("myConsumerKey");
        builder.consumerSecret("myConsumerSecret");
        builder.personalToken("myPersonalToken");
        //SERVER
        builder.apiVersion("v2");
        builder.apiRootUrl("https://api.discogs.com");
        builder.webRootUrl("https://www.discogs.com");
        builder.imageRootUrl("https://img.discogs.com");
        builder.authorizeEndpoint("https://www.discogs.com/oauth/authorize");
        builder.accessTokenEndpoint("https://api.discogs.com/oauth/access_token");
        builder.requestTokenEndpoint("https://api.discogs.com/oauth/request_token");
        builder.revokeTokenEndpoint("https://www.discogs.com/oauth/revoke");
        builder.connectionTimeout(10000);
        builder.readTimeout(10000);
        builder.throttlingRequestsEnabled(true);
        builder.throttleCoolDownTimePeriod(5000);
        builder.throttleMode(ThrottleMode.NORMAL);
        return builder.build();
    }

    @Nonnull
    @Override
    public OAuthContext createOAuthContext() {
        return OAuthContextBuilder.builder()
                .withClientConfiguration(createOAuthClientConfiguration())
                .withServerConfiguration(createOAuthServerConfiguration())
                .withSignatureService(signatureService())
                .withTokenParser(tokenParser())
                .withVerifierParser(verifierParser())
                .build();
    }

    @Nonnull
    @Override
    public OAuthClientConfiguration createOAuthClientConfiguration() {
        return OAuthClientConfiguration.builder()
                .applicationVersion(properties.getApplicationVersion())
                .applicationName(properties.getApplicationName())
                .callbackUrl(properties.getCallbackUrl())
                .consumerKey(properties.getConsumerKey())
                .consumerSecret(properties.getConsumerSecret())
                .personalToken(properties.getPersonalToken())
                .scope(OAuthScope.CALLBACK_URL)
                .build();
    }

    @Nonnull
    @Override
    public OAuthServerConfiguration createOAuthServerConfiguration() {
        return OAuthServerConfiguration.builder()
                .apiVersion(properties.getApiVersion())
                .apiRootUrl(properties.getApiRootUrl())
                .webRootUrl(properties.getWebRootUrl())
                .imageRootUrl(properties.getImageRootUrl())
                .accessTokenEndpoint(properties.getAccessTokenEndpoint())
                .authorizeEndpoint(properties.getAuthorizeEndpoint())
                .requestTokenEndpoint(properties.getRequestTokenEndpoint())
                .revokeTokenEndpoint(properties.getRevokeTokenEndpoint())
                .build();
    }
}