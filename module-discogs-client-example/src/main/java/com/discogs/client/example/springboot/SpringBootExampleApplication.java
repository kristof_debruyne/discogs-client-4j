package com.discogs.client.example.springboot;

import com.discogs.client.api.DiscogsRootClient;
import com.discogs.client.api.core.ApiInformation;
import com.discogs.client.api.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Nullable;

/**
 * Spring Boot example application. <br><br>
 *
 * Required arguments: <br>
 * - DISCOGS_CLIENT_APPLICATION_NAME <br>
 * - DISCOGS_CLIENT_APPLICATION_KEY <br>
 * - DISCOGS_CLIENT_APPLICATION_VERSION <br>
 * - DISCOGS_CLIENT_PERSONAL_TOKEN <br><br>
 *
 * We use the personal token just for testing purposes.
 */
@SpringBootApplication
public class SpringBootExampleApplication implements ApplicationRunner {

    @Autowired
    private DiscogsRootClient client;

    public static void main(@Nullable String[] args) {
        new SpringApplication(SpringBootExampleApplication.class).run(args);
    }

    @Override
    public void run(ApplicationArguments args) {
        Response<? extends ApiInformation> response = client.getApiInformation();
        System.out.println("Discogs API response -> " + response.getBody());

        System.exit(0);
    }
}