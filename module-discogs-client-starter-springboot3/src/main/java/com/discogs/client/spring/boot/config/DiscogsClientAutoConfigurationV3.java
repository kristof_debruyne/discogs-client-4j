package com.discogs.client.spring.boot.config;

import com.discogs.client.spring.EnableDiscogsClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Discogs client Spring Boot auto configuration.
 *
 * @author Sikke303
 * @since 1.0
 */
@AutoConfiguration
@Configuration
@EnableDiscogsClient
@RequiredArgsConstructor
@Log4j2
public class DiscogsClientAutoConfigurationV3 {

    @PostConstruct
    public void init() {
        log.info("Enabling Discogs client auto configuration ...");
    }
}
