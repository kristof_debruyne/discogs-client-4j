package com.discogs.client.auth.oauth.config;

import com.discogs.client.auth.oauth.signature.SignatureService;
import com.discogs.client.auth.oauth.token.OAuthTokenParser;
import com.discogs.client.auth.oauth.verifier.CallbackUrlVerifierParser;
import com.discogs.client.auth.oauth.verifier.OAuthVerifierParser;
import com.discogs.client.auth.oauth.verifier.OutOfBandVerifierParser;
import org.junit.Test;

import static com.discogs.client.auth.oauth.config.OAuthScope.CALLBACK_URL;
import static com.discogs.client.auth.oauth.config.OAuthScope.OUT_OF_BAND;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

public class OAuthContextBuilderTest {

    @Test(expected = IllegalArgumentException.class)
    public void givenMissingClientConfiguration_whenBuilding_thenThrowException() {
        OAuthContextBuilder.builder()
                .withServerConfiguration(mock(OAuthServerConfiguration.class))
                .withSignatureService(mock(SignatureService.class))
                .withTokenParser(mock(OAuthTokenParser.class))
                .withVerifierParser(mock(OAuthVerifierParser.class))
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenMissingServerConfiguration_whenBuilding_thenThrowException() {
        OAuthContextBuilder.builder()
                .withClientConfiguration(mock(OAuthClientConfiguration.class))
                .withSignatureService(mock(SignatureService.class))
                .withTokenParser(mock(OAuthTokenParser.class))
                .withVerifierParser(mock(OAuthVerifierParser.class))
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenMissingSignatureService_whenBuilding_thenThrowException() {
        OAuthContextBuilder.builder()
                .withClientConfiguration(mock(OAuthClientConfiguration.class))
                .withServerConfiguration(mock(OAuthServerConfiguration.class))
                .withTokenParser(mock(OAuthTokenParser.class))
                .withVerifierParser(mock(OAuthVerifierParser.class))
                .build();
    }

    @Test
    public void givenMissingTokenParser_whenBuilding_thenReturnContext() {
        OAuthContext context = OAuthContextBuilder.builder()
                .withClientConfiguration(mock(OAuthClientConfiguration.class))
                .withServerConfiguration(mock(OAuthServerConfiguration.class))
                .withSignatureService(mock(SignatureService.class))
                .withVerifierParser(mock(OAuthVerifierParser.class))
                .build();
        assertThat(context.getTokenParser()).isNotNull();
    }

    @Test
    public void givenMissingVerifierParserAndScopeCallbackUrl_whenBuilding_thenReturnContext() {
        OAuthContext context = OAuthContextBuilder.builder()
                .withClientConfiguration(spy(OAuthClientConfiguration.builder().scope(CALLBACK_URL).build()))
                .withServerConfiguration(mock(OAuthServerConfiguration.class))
                .withSignatureService(mock(SignatureService.class))
                .withTokenParser(mock(OAuthTokenParser.class))
                .build();
        assertThat(context.getVerifierParser()).isNotNull();
        assertThat(context.getVerifierParser()).isInstanceOf(CallbackUrlVerifierParser.class);
    }

    @Test
    public void givenMissingVerifierParserAndScopeOutOfBand_whenBuilding_thenReturnContext() {
        OAuthContext context = OAuthContextBuilder.builder()
                .withClientConfiguration(spy(OAuthClientConfiguration.builder().scope(OUT_OF_BAND).build()))
                .withServerConfiguration(mock(OAuthServerConfiguration.class))
                .withSignatureService(mock(SignatureService.class))
                .withTokenParser(mock(OAuthTokenParser.class))
                .build();
        assertThat(context.getVerifierParser()).isNotNull();
        assertThat(context.getVerifierParser()).isInstanceOf(OutOfBandVerifierParser.class);
    }

    @Test
    public void givenNoMissingBeans_whenBuilding_thenReturnContext() {
        OAuthContext context = OAuthContextBuilder.builder()
                .withClientConfiguration(mock(OAuthClientConfiguration.class))
                .withServerConfiguration(mock(OAuthServerConfiguration.class))
                .withSignatureService(mock(SignatureService.class))
                .withTokenParser(mock(OAuthTokenParser.class))
                .withVerifierParser(mock(OAuthVerifierParser.class))
                .build();
        assertThat(context).isNotNull();
    }
}
