package com.discogs.client.auth.oauth.verifier;

import com.discogs.client.auth.oauth.exception.OAuthException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CallbackUrlVerifierParserTest {

    private final OAuthVerifierParser parser = new CallbackUrlVerifierParser();

    @Test
    public void getVerifier() throws OAuthException {
        OAuthVerifier result = parser.parse("https://www.discogs.com/my?oauth_token=lCcRspZdpegpKnFlfhMTkWlPBTeifvSRlnMMttKN&oauth_verifier=QanBKZhGru");
        assertThat(result).isNotNull();
        assertThat(result.getValue()).isEqualTo("QanBKZhGru");
    }
}