package com.discogs.client.auth.oauth.signature;

import com.discogs.client.auth.oauth.exception.OAuthException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("all")
public class PlainTextSignatureServiceTest {

    private final SignatureService signatureService = new PlainTextSignatureService();

    @Test
    public void getSignatureMethod() {
        assertThat(signatureService.getSignatureMethod()).isEqualTo(SignatureMethod.PLAINTEXT);
    }

    @Test(expected = NullPointerException.class)
    public void createSignature_NullRequest() throws OAuthException {
        signatureService.createSignature(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createSignature_NullConsumerSecret() throws OAuthException {
        SignatureRequest request = SignatureRequest.builder()
                .tokenSecret("aTokenSecret")
                .build();
        signatureService.createSignature(request);
    }

    @Test
    public void createSignature_NullTokenSecret() throws OAuthException {
        SignatureRequest request = SignatureRequest.builder()
                .consumerSecret("aConsumerSecret")
                .build();
        String signature = signatureService.createSignature(request);
        assertThat(signature).isNotBlank();
        assertThat(signature).isEqualTo("aConsumerSecret&");
    }

    @Test
    public void createSignature_ConsumerAndTokenSecret() throws OAuthException {
        SignatureRequest request = SignatureRequest.builder()
                .consumerSecret("aConsumerSecret")
                .tokenSecret("aTokenSecret")
                .build();
        String signature = signatureService.createSignature(request);
        assertThat(signature).isNotBlank();
        assertThat(signature).isEqualTo("aConsumerSecret&aTokenSecret");
    }
}
