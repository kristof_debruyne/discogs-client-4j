package com.discogs.client.auth.oauth.verifier;

import com.discogs.client.auth.oauth.exception.OAuthException;
import org.junit.Test;

public class OutOfBandVerifierParserTest {

    private final OAuthVerifierParser parser = new OutOfBandVerifierParser();

    @Test(expected = UnsupportedOperationException.class)
    public void parse() throws OAuthException {
        parser.parse("anyResponse");
    }
}