package com.discogs.client.auth.oauth.token;

import com.discogs.client.auth.oauth.exception.OAuthException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultTokenParserTest {

    private final OAuthTokenParser parser = new DefaultTokenParser();

    @Test
    public void getToken() throws OAuthException {
        OAuthToken result = parser.parse("oauth_token=lCcRspZdpegpKnFlfhMTkWlPBTeifvSRlnMMttKN&oauth_token_secret=QanBKZhGru");
        assertThat(result).isNotNull();
        assertThat(result.getToken()).isEqualTo("lCcRspZdpegpKnFlfhMTkWlPBTeifvSRlnMMttKN");
        assertThat(result.getSecret()).isEqualTo("QanBKZhGru");
    }
}