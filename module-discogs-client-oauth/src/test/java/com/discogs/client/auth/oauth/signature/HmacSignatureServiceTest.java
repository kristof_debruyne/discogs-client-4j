package com.discogs.client.auth.oauth.signature;

import com.discogs.client.auth.oauth.exception.OAuthException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("all")
public class HmacSignatureServiceTest {

    private final HmacSignatureService signatureService = new HmacSignatureService();

    @Test
    public void getAlgorithm() {
        assertThat(signatureService.getAlgorithm()).isEqualTo("HmacSHA1");
    }

    @Test
    public void getSignatureMethod() {
        assertThat(signatureService.getSignatureMethod()).isEqualTo(SignatureMethod.HMAC);
    }

    @Test(expected = NullPointerException.class)
    public void createSignature_NullRequest() throws OAuthException {
        signatureService.createSignature(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createSignature_NullConsumerSecret() throws OAuthException {
        SignatureRequest request = SignatureRequest.builder()
                .baseString("content")
                .tokenSecret("aTokenSecret")
                .build();
        signatureService.createSignature(request);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createSignature_NullTokenSecret() throws OAuthException {
        SignatureRequest request = SignatureRequest.builder()
                .baseString("content")
                .consumerSecret("aConsumerSecret")
                .build();
        signatureService.createSignature(request);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createSignature_NullContent() throws OAuthException {
        SignatureRequest request = SignatureRequest.builder()
                .consumerSecret("aConsumerSecret")
                .tokenSecret("aTokenSecret")
                .build();
        signatureService.createSignature(request);
    }

    @Test
    public void createSignature_Success() throws OAuthException {
        SignatureRequest request = SignatureRequest.builder()
                .baseString("content")
                .consumerSecret("aConsumerSecret")
                .tokenSecret("aTokenSecret")
                .build();
        String signature = signatureService.createSignature(request);
        assertThat(signature).isNotBlank();
        assertThat(signature).isEqualTo("MTM0WEj1MSLdNveGtbmFSO8PeFQ=");
    }
}
