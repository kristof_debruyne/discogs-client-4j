package com.discogs.client.auth.oauth.signature;

import lombok.Getter;
import lombok.NonNull;

/**
 * Supported signature methods.
 *
 * @author Sikke303
 * @since 1.0
 */
@Getter
public enum SignatureMethod {

    HMAC("HMAC-SHA1"),
    PLAINTEXT("PLAINTEXT"),
    RSA("RSA-SHA1");

    String name;

    /**
     * Constructor
     *
     * @param name name (required)
     */
    SignatureMethod(@NonNull final String name) {
        this.name = name;
    }
}