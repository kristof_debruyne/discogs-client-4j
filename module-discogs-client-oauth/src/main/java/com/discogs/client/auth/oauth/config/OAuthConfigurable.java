package com.discogs.client.auth.oauth.config;

import com.discogs.client.auth.oauth.signature.PlainTextSignatureService;
import com.discogs.client.auth.oauth.signature.SignatureService;
import com.discogs.client.auth.oauth.token.DefaultTokenParser;
import com.discogs.client.auth.oauth.token.OAuthTokenParser;
import com.discogs.client.auth.oauth.verifier.CallbackUrlVerifierParser;
import com.discogs.client.auth.oauth.verifier.OAuthVerifierParser;
import com.discogs.client.auth.oauth.verifier.OutOfBandVerifierParser;

import javax.annotation.Nonnull;

/**
 * OAuth configurable interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface OAuthConfigurable {

    /**
     * Gets OAuth scope.
     *
     * @return scope (never null)
     */
    @Nonnull
    OAuthScope getOAuthScope();

    /**
     * Creates new OAuth context.
     *
     * - client configuration
     * - server configuration
     * - signature service
     * - token parser
     * - verifier parser
     *
     * @return context (never null)
     */
    @Nonnull
    OAuthContext createOAuthContext();

    /**
     * Creates OAuth client configuration.
     *
     * @return client configuration (never null)
     */
    @Nonnull
    OAuthClientConfiguration createOAuthClientConfiguration();

    /**
     * Creates OAuth server configuration.
     *
     * @return server configuration (never null)
     */
    @Nonnull
    OAuthServerConfiguration createOAuthServerConfiguration();

    /**
     * Creates signature service instance.
     *
     * @return signature service (never null)
     */
    @Nonnull
    default SignatureService signatureService() {
        return new PlainTextSignatureService();
    }

    /**
     * Creates token parser instance.
     *
     * @return token parser (never null)
     */
    @Nonnull
    default OAuthTokenParser tokenParser() {
        return new DefaultTokenParser();
    }

    /**
     * Creates verifier parser instance.
     *
     * @return verifier parser (never null)
     */
    @Nonnull
    default OAuthVerifierParser verifierParser() {
        return getOAuthScope() == OAuthScope.CALLBACK_URL ? new CallbackUrlVerifierParser() : new OutOfBandVerifierParser();
    }
}