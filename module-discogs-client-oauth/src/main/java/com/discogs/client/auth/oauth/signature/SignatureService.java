package com.discogs.client.auth.oauth.signature;

import com.discogs.client.auth.oauth.exception.OAuthException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Signature service interface.
 *
 * @author Sikke303
 * @since 1.0
 */
@ParametersAreNonnullByDefault
public interface SignatureService {

    /**
     * Gets signature method.
     *
     * @return signature method (never null)
     */
    @Nonnull
    SignatureMethod getSignatureMethod();

    /**
     * Creates signature by request.
     *
     * @param request signature request (required)
     * @return signature (never null)
     * @throws OAuthException if creating signature has failed
     */
    @Nonnull
    String createSignature(SignatureRequest request) throws OAuthException;
}