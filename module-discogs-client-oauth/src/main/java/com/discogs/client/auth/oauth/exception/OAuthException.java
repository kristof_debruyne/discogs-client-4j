package com.discogs.client.auth.oauth.exception;

import com.discogs.client.auth.api.AuthenticationException;
import lombok.NonNull;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Exception thrown if OAuth fails for any reason.
 *
 * @author Sikke303
 * @since 1.0
 * @see AuthenticationException
 */
@ParametersAreNonnullByDefault
public class OAuthException extends AuthenticationException {

    private static final long serialVersionUID = 821011220700L;

    public OAuthException(@NonNull final String message) {
        super(message);
    }

    public OAuthException(@NonNull final String message, @NonNull final Throwable exception) {
        super(message, exception);
    }
}