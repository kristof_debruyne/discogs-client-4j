package com.discogs.client.auth.oauth.request;

import com.discogs.client.auth.oauth.OAuthConstants;
import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;

/**
 * OAuth request model.
 *
 * @author Sikke303
 * @since 1.0
 */
@Getter
public class OAuthRequest {

    private final Map<String, String> oAuthParameters = new HashMap<>();

    /**
     * Adds OAuth parameter.
     *
     * @param key key (required)
     * @param value value (required)
     * @return request (never null)
     */
    @Nonnull
    public OAuthRequest addOAuthParameter(@NonNull final String key, @NonNull final String value) {
        this.oAuthParameters.put(validateKey(key), value);
        return this;
    }

    /**
     * Validates key.
     *
     * @param key key (required)
     * @return key (never null)
     * @throws IllegalArgumentException if key is not valid
     */
    @Nonnull
    private String validateKey(final String key) {
        if (StringUtils.startsWith(key, OAuthConstants.OAUTH_PARAM_PREFIX) || StringUtils.equals(key, OAuthConstants.SCOPE)) {
            return key;
        }
        throw new IllegalArgumentException(format("OAuth parameters must either be '%s' or start with '%s'", OAuthConstants.SCOPE, OAuthConstants.OAUTH_PARAM_PREFIX));
    }

    /**
     * Gets OAuth parameters as string.
     *
     * @return parameter string or empty string
     */
    public String asOAuthString() {
        if(getOAuthParameters().isEmpty()) {
            return OAuthConstants.EMPTY;
        }
        StringBuilder builder = new StringBuilder(OAuthConstants.OAUTH_PREAMBLE);
        for(Map.Entry<String, String> entry : getOAuthParameters().entrySet()) {
            builder.append(entry.getKey()).append("=").append(entry.getValue()).append(",");
        }
        return StringUtils.removeEnd(builder.toString(), ",");
    }
}