package com.discogs.client.auth.oauth.token;

import com.discogs.client.auth.api.Authentication;

/**
 * OAuth authentication interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see Authentication
 */
public interface OAuthAuthentication extends Authentication {

    String getToken();

    String getSecret();
}