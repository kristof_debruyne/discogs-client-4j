package com.discogs.client.auth.oauth.consumer;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@RequiredArgsConstructor
@Getter
@EqualsAndHashCode
@ToString(exclude = "consumerSecret")
public final class ConsumerDetails implements Serializable {

    private static final long serialVersionUID = 0L;

    private final String consumerKey;
    private final String consumerSecret;
}
