package com.discogs.client.auth.oauth;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * OAuth constants.
 *
 * @author Sikke303
 * @since 1.0
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class OAuthConstants {

    public static final String OAUTH_NAME = "OAuth";
    public static final String OAUTH_PARAM_ACCESS_KEY = "access_key";
    public static final String OAUTH_PARAM_PREFIX = "oauth_";
    public static final String OAUTH_PARAM_CALLBACK = OAUTH_PARAM_PREFIX + "callback";
    public static final String OAUTH_PARAM_CONSUMER_KEY = OAUTH_PARAM_PREFIX + "consumer_key";
    public static final String OAUTH_PARAM_NONCE = OAUTH_PARAM_PREFIX + "nonce";
    public static final String OAUTH_PARAM_SIGNATURE = OAUTH_PARAM_PREFIX + "signature";
    public static final String OAUTH_PARAM_SIGNATURE_METHOD = OAUTH_PARAM_PREFIX + "signature_method";
    public static final String OAUTH_PARAM_TIMESTAMP = OAUTH_PARAM_PREFIX + "timestamp";
    public static final String OAUTH_PARAM_TOKEN = OAUTH_PARAM_PREFIX + "token";
    public static final String OAUTH_PARAM_TOKEN_SECRET = OAUTH_PARAM_PREFIX + "token_secret";
    public static final String OAUTH_PARAM_VERIFIER = OAUTH_PARAM_PREFIX + "verifier";
    public static final String OAUTH_PARAM_VERSION = OAUTH_PARAM_PREFIX + "version";
    public static final String OAUTH_PREAMBLE = "OAuth ";

    public static final String EMPTY = "";
    public static final String SCOPE = "scope";
}