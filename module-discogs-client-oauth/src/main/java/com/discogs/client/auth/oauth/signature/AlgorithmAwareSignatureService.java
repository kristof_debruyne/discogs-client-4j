package com.discogs.client.auth.oauth.signature;

import javax.annotation.Nonnull;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Algorithm aware signature service interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see HmacSignatureService
 */
public interface AlgorithmAwareSignatureService extends SignatureService {

    /**
     * Gets algorithm.
     *
     * @return optional algorithm (never null)
     */
    @Nonnull
    String getAlgorithm();

    /**
     * Gets character set.
     *
     * @return character set (never null)
     */
    @Nonnull
    default Charset getCharacterSet() {
        return StandardCharsets.UTF_8;
    }
}