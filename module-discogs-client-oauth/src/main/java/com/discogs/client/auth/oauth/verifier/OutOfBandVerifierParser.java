package com.discogs.client.auth.oauth.verifier;

import com.discogs.client.auth.oauth.config.OAuthScope;
import lombok.NonNull;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Out of band verifier parser.
 *
 * This parser will be used if the {@link OAuthScope} is set to 'OUT_OF_BAND'.
 *
 * @author Sikke303
 * @since 1.0
 * @see OAuthVerifierParser
 */
@ParametersAreNonnullByDefault
public class OutOfBandVerifierParser implements OAuthVerifierParser {

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public OAuthVerifier parse(@NonNull final String response) {
        throw new UnsupportedOperationException("Parsing verifier in " + OAuthScope.OUT_OF_BAND.name() + " mode is currently not supported !");
    }
}