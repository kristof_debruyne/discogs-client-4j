package com.discogs.client.auth.oauth.token;

import com.discogs.client.auth.oauth.OAuthConstants;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * OAuth empty token.
 *
 * @author Sikke303
 * @since 1.0
 * @see OAuthAuthentication
 */
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class OAuthEmptyToken implements OAuthAuthentication {

    private static final long serialVersionUID = 0L;

    @Override
    public String getToken() {
        return OAuthConstants.EMPTY;
    }

    @Override
    public String getSecret() {
        return OAuthConstants.EMPTY;
    }
}