package com.discogs.client.auth.oauth.signature;

import com.discogs.client.auth.oauth.OAuthConstants;
import com.discogs.client.auth.oauth.exception.OAuthException;
import com.discogs.client.auth.oauth.utils.OAuthUtils;
import lombok.NonNull;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import static com.discogs.client.auth.oauth.internal.Assert.notNull;

/**
 * Signature service for method 'HMAC'.
 *
 * @author Sikke303
 * @since 1.0
 * @see AlgorithmAwareSignatureService
 */
@ParametersAreNonnullByDefault
public class HmacSignatureService implements AlgorithmAwareSignatureService {

    private static final String CARRIAGE_RETURN = "\r";
    private static final String LINE_FEED = "\n";

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public String getAlgorithm() {
        return "HmacSHA1";
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public SignatureMethod getSignatureMethod() {
        return SignatureMethod.HMAC;
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public String createSignature(@NonNull final SignatureRequest request) throws OAuthException {
        notNull(request.getBaseString(), "Content is required.");
        notNull(request.getConsumerSecret(), "Consumer secret is required.");
        notNull(request.getTokenSecret(), "Token secret is required.");

        final String signature = OAuthUtils.encode(request.getConsumerSecret()) + "&" + OAuthUtils.encode(request.getTokenSecret());
        try {
            SecretKeySpec key = new SecretKeySpec((signature).getBytes(getCharacterSet().displayName()), getAlgorithm());

            Mac mac = Mac.getInstance(getAlgorithm());
            mac.init(key);

            byte[] bytes = mac.doFinal(request.getBaseString().getBytes(getCharacterSet().displayName()));
            return new String(Base64.getEncoder().encode(bytes), getCharacterSet())
                    .replace(CARRIAGE_RETURN, OAuthConstants.EMPTY).replace(LINE_FEED, OAuthConstants.EMPTY);
        } catch (InvalidKeyException ex) {
            ex.printStackTrace();
            throw new OAuthException("Invalid secret key !", ex);
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
            throw new OAuthException("Algorithm not supported: " + getAlgorithm(), ex);
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
            throw new OAuthException("Character set not supported: " + getCharacterSet().displayName(), ex);
        }
    }
}