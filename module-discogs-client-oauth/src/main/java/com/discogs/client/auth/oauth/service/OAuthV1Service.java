package com.discogs.client.auth.oauth.service;

import com.discogs.client.auth.oauth.exception.OAuthException;
import com.discogs.client.auth.oauth.token.OAuthToken;
import com.discogs.client.auth.oauth.verifier.OAuthVerifier;
import lombok.NonNull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * OAuth service for OAuth v1.0a version.
 *
 * @author Sikke303
 * @since 1.0
 */
@ParametersAreNonnullByDefault
public interface OAuthV1Service extends OAuthService {

    /**
     * Gets access token.
     *
     * @param requestToken request token (required)
     * @param verifier verifier (required)
     * @return access token (never null)
     * @throws OAuthException if obtaining access token has failed
     */
    @Nonnull
    OAuthToken getAccessToken(OAuthToken requestToken, OAuthVerifier verifier) throws OAuthException;

    /**
     * Gets authorize url.
     *
     * @param requestToken request token (required)
     * @return authorize url (never null)
     */
    @Nonnull
    String getAuthorizedUrl(OAuthToken requestToken);

    /**
     * Gets request token.
     *
     * @return request token (never null)
     * @throws OAuthException if obtaining request token has failed
     */
    @Nonnull
    OAuthToken getRequestToken() throws OAuthException;

    /**
     * Gets revoke token url.
     *
     * @param accessToken access token (required)
     * @return revoke token url (never null)
     */
    @Nonnull
    String getRevokeTokenUrl(OAuthToken accessToken);

    /**
     * Creates signature.
     *
     * @param consumerSecret consumer secret (required)
     * @param tokenSecret token secret (optional)
     * @return signature (never null)
     * @throws OAuthException if creating signature has failed
     */
    @Nonnull
    String createSignature(@NonNull final String consumerSecret, @Nullable final String tokenSecret) throws OAuthException;
}