package com.discogs.client.auth.oauth.internal;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Assert {

    /**
     * Checks if given value is null.
     *
     * @param value value (required)
     * @param message message (required)
     * @throws IllegalArgumentException if value is null
     */
    public static void notNull(final Object value, @NonNull final String message) {
        if(value == null) {
            throw new IllegalArgumentException(message);
        }
    }
}