package com.discogs.client.auth.oauth.config;

public enum OAuthFlow {

    /**
     * Authentication from scratch is when you don't have an access token, yet. You can generate an access token
     * by completing the complete OAuth flow successfully.
     */
    AUTHENTICATION_FROM_SCRATCH,
    /**
     * Auto generated token is when you generated an acces token on the website of 3rd party. You bypass a complete
     * authentication flow with this, but usually it is secure because the 3rd party offers this.
     */
    AUTO_GENERATED_TOKEN
}