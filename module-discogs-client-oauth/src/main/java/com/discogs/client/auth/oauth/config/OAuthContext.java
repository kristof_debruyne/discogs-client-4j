package com.discogs.client.auth.oauth.config;

import com.discogs.client.auth.api.AuthenticationContext;
import com.discogs.client.auth.oauth.signature.SignatureService;
import com.discogs.client.auth.oauth.token.OAuthTokenParser;
import com.discogs.client.auth.oauth.verifier.OAuthVerifierParser;
import lombok.Getter;
import lombok.NonNull;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import static com.discogs.client.auth.oauth.OAuthConstants.OAUTH_NAME;

/**
 * OAuth server configuration.
 *
 * @author Sikke303
 * @since 1.0
 */
@Getter
@ParametersAreNonnullByDefault
public class OAuthContext implements AuthenticationContext {

    private final OAuthClientConfiguration clientConfiguration;
    private final OAuthServerConfiguration serverConfiguration;
    private final OAuthTokenParser tokenParser;
    private final OAuthVerifierParser verifierParser;
    private final SignatureService signatureService;

    @Nonnull
    @Override
    public String getName() {
        return OAUTH_NAME;
    }

    /**
     * Constructor
     *
     * @param clientConfiguration client configuration (required)
     * @param serverConfiguration server configuration (required)
     * @param tokenParser token parser (required)
     * @param verifierParser verifier parser (required)
     * @param signatureService signature server (required)
     */
    public OAuthContext(@NonNull OAuthClientConfiguration clientConfiguration,
                        @NonNull OAuthServerConfiguration serverConfiguration,
                        @NonNull OAuthTokenParser tokenParser,
                        @NonNull OAuthVerifierParser verifierParser,
                        @NonNull SignatureService signatureService) {
        this.clientConfiguration = clientConfiguration;
        this.serverConfiguration = serverConfiguration;
        this.tokenParser = tokenParser;
        this.verifierParser = verifierParser;
        this.signatureService = signatureService;
    }
}