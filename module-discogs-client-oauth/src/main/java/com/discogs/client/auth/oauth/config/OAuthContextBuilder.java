package com.discogs.client.auth.oauth.config;

import com.discogs.client.auth.oauth.internal.Assert;
import com.discogs.client.auth.oauth.signature.SignatureService;
import com.discogs.client.auth.oauth.token.DefaultTokenParser;
import com.discogs.client.auth.oauth.token.OAuthTokenParser;
import com.discogs.client.auth.oauth.verifier.CallbackUrlVerifierParser;
import com.discogs.client.auth.oauth.verifier.OAuthVerifierParser;
import com.discogs.client.auth.oauth.verifier.OutOfBandVerifierParser;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * OAuth context builder.
 *
 * @author Sikke303
 * @since 1.0
 */
@NoArgsConstructor(staticName = "builder")
@ParametersAreNonnullByDefault
public class OAuthContextBuilder {

    private OAuthClientConfiguration clientConfiguration;
    private OAuthServerConfiguration serverConfiguration;
    private OAuthTokenParser tokenParser;
    private OAuthVerifierParser verifierParser;
    private SignatureService signatureService;

    @Nonnull
    public OAuthContextBuilder withClientConfiguration(@NonNull final OAuthClientConfiguration clientConfiguration) {
        this.clientConfiguration = clientConfiguration;
        return this;
    }

    @Nonnull
    public OAuthContextBuilder withServerConfiguration(@NonNull final OAuthServerConfiguration serverConfiguration) {
        this.serverConfiguration = serverConfiguration;
        return this;
    }

    @Nonnull
    public OAuthContextBuilder withTokenParser(@NonNull final OAuthTokenParser tokenParser) {
        this.tokenParser = tokenParser;
        return this;
    }

    @Nonnull
    public OAuthContextBuilder withVerifierParser(@NonNull final OAuthVerifierParser verifierParser) {
        this.verifierParser = verifierParser;
        return this;
    }

    @Nonnull
    public OAuthContextBuilder withSignatureService(@NonNull final SignatureService signatureService) {
        this.signatureService = signatureService;
        return this;
    }

    /**
     * Builds context.
     *
     * @return context (never null)
     * @throws IllegalArgumentException if context is missing required
     */
    @Nonnull
    public OAuthContext build() {
        Assert.notNull(clientConfiguration, "Client configuration is required.");
        Assert.notNull(serverConfiguration, "Server configuration is required.");
        Assert.notNull(signatureService, "Signature service is required.");

        if(tokenParser == null) {
            tokenParser = new DefaultTokenParser();
        }
        if(verifierParser == null) {
            if(clientConfiguration.getScope() == OAuthScope.CALLBACK_URL) {
                verifierParser = new CallbackUrlVerifierParser();
            } else {
                verifierParser = new OutOfBandVerifierParser();
            }
        }
        return new OAuthContext(clientConfiguration, serverConfiguration, tokenParser, verifierParser, signatureService);
    }
}