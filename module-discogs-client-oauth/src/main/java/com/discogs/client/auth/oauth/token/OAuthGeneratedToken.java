package com.discogs.client.auth.oauth.token;

import com.discogs.client.auth.oauth.OAuthConstants;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * OAuth generated token (personal token). <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see OAuthToken
 */
@Getter
@ParametersAreNonnullByDefault
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class OAuthGeneratedToken extends OAuthToken {

    private static final long serialVersionUID = 0L;

    /**
     * Constructor
     *
     * @param token token (required)
     */
    public OAuthGeneratedToken(@NonNull final String token) {
        super(token, OAuthConstants.EMPTY);
    }
}