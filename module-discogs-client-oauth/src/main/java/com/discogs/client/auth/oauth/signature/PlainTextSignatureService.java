package com.discogs.client.auth.oauth.signature;

import com.discogs.client.auth.oauth.exception.OAuthException;
import com.discogs.client.auth.oauth.utils.OAuthUtils;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import static com.discogs.client.auth.oauth.internal.Assert.notNull;

/**
 * Signature service for method 'PLAINTEXT'.
 *
 * @author Sikke303
 * @since 1.0
 * @see SignatureService
 */
@ParametersAreNonnullByDefault
public class PlainTextSignatureService implements SignatureService {

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public String createSignature(@NonNull final SignatureRequest request) throws OAuthException {
        notNull(request.getConsumerSecret(), "Consumer secret is required.");

        StringBuilder builder = new StringBuilder(request.getConsumerSecret()).append("&");
        try {
            if(StringUtils.isNotBlank(request.getTokenSecret())) {
                builder.append(OAuthUtils.encode(request.getTokenSecret()));
            }
            return builder.toString();
        } catch (Exception ex) {
            throw new OAuthException("Failed to create signature.", ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public SignatureMethod getSignatureMethod() {
        return SignatureMethod.PLAINTEXT;
    }
}