package com.discogs.client.auth.oauth.verifier;

import com.discogs.client.auth.oauth.OAuthConstants;
import com.discogs.client.auth.oauth.config.OAuthScope;
import com.discogs.client.auth.oauth.exception.OAuthException;
import com.discogs.client.auth.oauth.utils.OAuthUtils;
import lombok.NonNull;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Callback URL verifier parser.
 *
 * This parser will be used if the {@link OAuthScope} is set to 'CALLBACK_URL'.
 *
 * @author Sikke303
 * @since 1.0
 * @see OAuthVerifierParser
 */
@ParametersAreNonnullByDefault
public class CallbackUrlVerifierParser implements OAuthVerifierParser {

    private static final Pattern VERIFIER_REGEX = Pattern.compile(OAuthConstants.OAUTH_PARAM_VERIFIER + "=([^&]+)");

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public OAuthVerifier parse(@NonNull final String response) throws OAuthException {
        return new OAuthVerifier(extract(response));
    }

    private String extract(final String response) throws OAuthException {
        Matcher matcher = VERIFIER_REGEX.matcher(response);
        if (matcher.find() && matcher.groupCount() >= 1) {
            return OAuthUtils.decode(matcher.group(1));
        }
        throw new OAuthException("Failed to extract verifier ! Reason: response body is incorrect");
    }
}