package com.discogs.client.auth.oauth.config;

import com.discogs.client.auth.oauth.consumer.ConsumerDetails;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

/**
 * OAuth client configuration.
 *
 * @author Sikke303
 * @since 1.0
 */
@Builder
@Getter
@ToString(exclude = "consumerSecret")
public class OAuthClientConfiguration {

    private final String applicationName;
    private final String applicationKey;
    private final String applicationVersion;
    private String consumerKey;
    private String consumerSecret;
    private final String callbackUrl;
    private final String personalToken;
    private final OAuthScope scope;

    /**
     * Update consumer details.
     *
     * @param consumerDetails consumer details (required)
     */
    public void updateConsumerDetails(@NonNull final ConsumerDetails consumerDetails) {
        this.consumerKey = consumerDetails.getConsumerKey();
        this.consumerSecret = consumerDetails.getConsumerSecret();
    }
}