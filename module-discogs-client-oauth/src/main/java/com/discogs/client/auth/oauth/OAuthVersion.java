package com.discogs.client.auth.oauth;

import lombok.Getter;
import lombok.NonNull;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Supported versions.
 *
 * @author Sikke303
 * @since 1.0
 */
@Getter
@ParametersAreNonnullByDefault
public enum OAuthVersion {

    V1("1.0"),
    V2("2.0");

    String value;

    /**
     * Constructor
     *
     * @param value value (required)
     */
    OAuthVersion(@NonNull final String value) {
        this.value = value;
    }
}