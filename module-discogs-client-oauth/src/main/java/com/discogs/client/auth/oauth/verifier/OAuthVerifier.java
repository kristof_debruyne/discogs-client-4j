package com.discogs.client.auth.oauth.verifier;

import lombok.Getter;
import lombok.NonNull;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Wrapper for OAuth verifier.
 *
 * @author Sikke303
 * @since 1.0
 */
@Getter
@ParametersAreNonnullByDefault
public class OAuthVerifier {

    private final String value;

    /**
     * Constructor
     *
     * @param value verifier value (required)
     */
    public OAuthVerifier(@NonNull final String value) {
        this.value = value;
    }
}