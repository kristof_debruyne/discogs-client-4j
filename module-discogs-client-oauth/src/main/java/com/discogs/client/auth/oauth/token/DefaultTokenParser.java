package com.discogs.client.auth.oauth.token;

import com.discogs.client.auth.oauth.OAuthConstants;
import com.discogs.client.auth.oauth.exception.OAuthException;
import com.discogs.client.auth.oauth.utils.OAuthUtils;

import javax.annotation.Nonnull;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Default OAuth token parser.
 *
 * @author Sikke303
 * @since 1.0
 * @see OAuthToken
 */
public class DefaultTokenParser implements OAuthTokenParser {

    private static final Pattern TOKEN_REGEX = Pattern.compile(OAuthConstants.OAUTH_PARAM_TOKEN + "=([^&]+)");
    private static final Pattern SECRET_REGEX = Pattern.compile(OAuthConstants.OAUTH_PARAM_TOKEN_SECRET + "=([^&]*)");

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public OAuthToken parse(final String response) throws OAuthException {
        if(response == null) {
            throw new OAuthException("Failed to extract token ! Reason: response body is empty");
        }
        String token = extract(response, TOKEN_REGEX);
        String secret = extract(response, SECRET_REGEX);
        return new OAuthToken(token, secret);
    }

    private String extract(final String response, final Pattern pattern) throws OAuthException {
        Matcher matcher = pattern.matcher(response);
        if (matcher.find() && matcher.groupCount() >= 1) {
            return OAuthUtils.decode(matcher.group(1));
        }
        throw new OAuthException("Failed to extract token ! Reason: response body is incorrect");
    }
}