package com.discogs.client.auth.oauth.token;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * OAuth token (access / request).
 *
 * @author Sikke303
 * @since 1.0
 * @see OAuthAuthentication
 */
@Getter
@ParametersAreNonnullByDefault
@EqualsAndHashCode
@ToString(exclude = "secret")
public class OAuthToken implements OAuthAuthentication {

    private static final long serialVersionUID = 0L;

    private final String token;
    private final String secret;

    /**
     * Constructor
     *
     * @param token token (required)
     * @param secret secret (required)
     */
    public OAuthToken(@NonNull final String token, @NonNull final String secret) {
        this.token = token;
        this.secret = secret;
    }
}