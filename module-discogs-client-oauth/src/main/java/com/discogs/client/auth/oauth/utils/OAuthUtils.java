package com.discogs.client.auth.oauth.utils;

import com.discogs.client.auth.oauth.exception.OAuthRuntimeException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Utility class for OAuth framework.
 *
 * @author Sikke303
 * @since 1.0
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@ParametersAreNonnullByDefault
public final class OAuthUtils {

    private static final Map<String, String> EXTRA_ENCODING_RULES;

    static {
        final Map<String, String> rules = new HashMap<>();
        rules.put("*", "%2A");
        rules.put("+", "%20");
        rules.put("%7E", "~");
        EXTRA_ENCODING_RULES = Collections.unmodifiableMap(rules);
    }

    /**
     * Encodes plain text string with default character set.
     *
     * @param plainText plain text string (required)
     * @return decoded string (never null)
     */
    public static String encode(@NonNull final String plainText) {
        return encode(plainText, StandardCharsets.UTF_8);
    }

    /**
     * Encodes plain text string with given character set.
     *
     * @param plainText plain text string (required)
     * @param charset character set (required)
     * @return decoded string (never null)
     */
    public static String encode(@NonNull final String plainText, @NonNull final Charset charset) {
        try {
            String encoded = URLEncoder.encode(plainText, charset.displayName());
            for(Map.Entry<String, String> rule : EXTRA_ENCODING_RULES.entrySet()) {
                encoded = applyRule(encoded, rule.getKey(), rule.getValue());
            }
            return encoded;
        } catch (UnsupportedEncodingException uee) {
            throw new OAuthRuntimeException("Character set not supported during encoding: " + charset.displayName(), uee);
        }
    }

    /**
     * Applies extra encoding rule to encoded string.
     *
     * @param encodedString encoded string (required)
     * @param replacer string to replace (required)
     * @param replacement replacement string (required)
     * @return encoded string (never null)
     */
    @Nonnull
    private static String applyRule(@NonNull final String encodedString, @NonNull final String replacer, @NonNull final String replacement) {
        return encodedString.replaceAll(Pattern.quote(replacer), replacement);
    }

    /**
     * Decodes encoded string with default character set.
     *
     * @param encodedString encoded string (required)
     * @return decoded string (never null)
     */
    @Nonnull
    public static String decode(@NonNull final String encodedString) {
        return decode(encodedString, StandardCharsets.UTF_8);
    }

    /**
     * Decodes encoded string with given character set.
     *
     * @param encodedString encoded string (required)
     * @param charset character set (required)
     * @return decoded string (never null)
     */
    @Nonnull
    public static String decode(@NonNull String encodedString, @NonNull final Charset charset) {
        try {
            return URLDecoder.decode(encodedString, charset.displayName());
        } catch(UnsupportedEncodingException ex) {
            throw new OAuthRuntimeException("Character set not supported during decoding: " + charset.displayName(), ex);
        }
    }
}