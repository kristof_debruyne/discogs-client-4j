package com.discogs.client.auth.oauth.service;

import com.discogs.client.auth.api.AuthenticationService;
import com.discogs.client.auth.oauth.OAuthVersion;
import com.discogs.client.auth.oauth.exception.OAuthException;
import com.discogs.client.auth.oauth.request.OAuthRequest;
import com.discogs.client.auth.oauth.token.OAuthToken;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import static java.time.LocalDateTime.now;

/**
 * OAuth service interface.
 *
 * @author Sikke303
 * @since 1.0
 */
@ParametersAreNonnullByDefault
public interface OAuthService extends AuthenticationService {

    /**
     * Generates nonce number.
     *
     * Default implementation will use the current time in ms and adds a random integer.
     *
     * @return nonce (never null)
     */
    @Nonnull
    default String generateNonce() {
        return String.valueOf(System.currentTimeMillis() + new Random().nextInt());
    }

    /**
     * Generates current timestamp.
     *
     * Default implementation will take current date time in format 'ddMMyyyyHHmmssSSS'
     *
     * @return nonce (never null)
     */
    @Nonnull
    default String generateTimestamp() {
        return now().format(DateTimeFormatter.ofPattern("ddMMyyyyHHmmssSSS"));
    }

    /**
     * Gets OAuth version.
     *
     * @return version (never null)
     */
    @Nonnull
    OAuthVersion getOAuthVersion();

    /**
     * Signs request.
     *
     * @param accessToken access token (required)
     * @return signed request (never null)
     * @throws OAuthException if signing request has failed
     */
    @Nonnull
    OAuthRequest signRequest(OAuthToken accessToken) throws OAuthException;
}