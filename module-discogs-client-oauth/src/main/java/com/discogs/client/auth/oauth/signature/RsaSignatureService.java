package com.discogs.client.auth.oauth.signature;

import com.discogs.client.auth.oauth.exception.OAuthException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.security.PrivateKey;
import java.security.Signature;
import java.util.Base64;

/**
 * Signature service for method 'RSA'.
 *
 * @author Sikke303
 * @since 1.0
 * @see SignatureService
 */
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class RsaSignatureService implements AlgorithmAwareSignatureService {

    private final PrivateKey privateKey;

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public String getAlgorithm() {
        return "SHA1withRSA";
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public SignatureMethod getSignatureMethod() {
        return SignatureMethod.RSA;
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public String createSignature(@NonNull final SignatureRequest request) throws OAuthException {
        try {
            Signature signature = Signature.getInstance(getAlgorithm());
            signature.initSign(privateKey);
            byte[] payload = request.getBaseString().getBytes(getCharacterSet());
            signature.update(payload);
            return Base64.getEncoder().encodeToString(payload);
        } catch (Exception ex) {
            throw new OAuthException("Failed to create signature with RSA.", ex);
        }
    }
}
