package com.discogs.client.auth.oauth.verifier;

import com.discogs.client.auth.oauth.config.OAuthScope;
import com.discogs.client.auth.oauth.exception.OAuthException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * OAuth verifier parser.
 *
 * @author Sikke303
 * @since 1.0
 * @see OAuthVerifier
 * @see OAuthScope
 */
@ParametersAreNonnullByDefault
public interface OAuthVerifierParser {

    /**
     * Parses verifier from given response.
     *
     * @param response response (required)
     * @return verifier (never null)
     * @throws OAuthException if response couldn't be parsed successfully to a verifier
     */
    @Nonnull
    OAuthVerifier parse(String response) throws OAuthException;
}