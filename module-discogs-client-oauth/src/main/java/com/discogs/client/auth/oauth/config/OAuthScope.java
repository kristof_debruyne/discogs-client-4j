package com.discogs.client.auth.oauth.config;

import lombok.Getter;

import javax.annotation.Nullable;

/**
 * Supported signature types.
 *
 * @author Sikke303
 * @since 1.0
 */
@Getter
public enum OAuthScope {

    /**
     * Appends the verifier to the callback URL (only after successful authorization)
     */
    CALLBACK_URL(null),
    /**
     * Writes the verifier in the page response (only after successful authorization)
     */
    OUT_OF_BAND("oob");

    String value;

    /**
     * Constructor
     * @param value value (optional)
     */
    OAuthScope(@Nullable final String value) {
        this.value = value;
    }
}