package com.discogs.client.auth.oauth.token;

import com.discogs.client.auth.oauth.exception.OAuthException;

import javax.annotation.Nonnull;

/**
 * OAuth token parser.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface OAuthTokenParser {

    /**
     * Parses tokens from given body.
     *
     * @param body body (required)
     * @return token (never null)
     * @throws OAuthException if response couldn't be parsed successfully to a token
     */
    @Nonnull
    OAuthToken parse(String body) throws OAuthException;
}