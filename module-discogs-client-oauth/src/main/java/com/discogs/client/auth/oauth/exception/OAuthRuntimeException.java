package com.discogs.client.auth.oauth.exception;

import com.discogs.client.auth.api.AuthenticationRuntimeException;
import lombok.NonNull;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Exception thrown if OAuth fails for any reason.
 *
 * @author Sikke303
 * @since 1.0
 * @see AuthenticationRuntimeException
 */
@ParametersAreNonnullByDefault
public class OAuthRuntimeException extends AuthenticationRuntimeException {

    private static final long serialVersionUID = 821011220800L;

    public OAuthRuntimeException(@NonNull final String message) {
        super(message);
    }

    public OAuthRuntimeException(@NonNull final String message, @NonNull final Throwable exception) {
        super(message, exception);
    }
}