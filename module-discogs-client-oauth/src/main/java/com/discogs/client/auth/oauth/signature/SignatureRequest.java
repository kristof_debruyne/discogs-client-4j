package com.discogs.client.auth.oauth.signature;

import lombok.Builder;
import lombok.Getter;

/**
 * Signature request.
 *
 * @author Sikke303
 * @since 1.0
 */
@Builder
@Getter
public class SignatureRequest {

    private final String baseString;
    private final String consumerSecret;
    private final String tokenSecret;
}