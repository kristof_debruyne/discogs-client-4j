package com.discogs.client.auth.oauth.config;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 * OAuth server configuration.
 *
 * @author Sikke303
 * @since 1.0
 */
@Builder
@Getter
@ToString
public class OAuthServerConfiguration {

    private final String apiVersion;
    private final String apiRootUrl;
    private final String webRootUrl;
    private final String imageRootUrl;
    private final String accessTokenEndpoint;
    private final String authorizeEndpoint;
    private final String requestTokenEndpoint;
    private final String revokeTokenEndpoint;
}