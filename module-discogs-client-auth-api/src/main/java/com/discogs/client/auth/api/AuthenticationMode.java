package com.discogs.client.auth.api;

/**
 * Authentication modes.
 *
 * @author Sikke303
 * @since 1.0
 */
public enum AuthenticationMode {

    NOT_APPLICABLE,
    /**
     * You need to be authenticated to fetch the resource. <br>
     * If not, you will receive a 401 Unauthorized.
     */
    REQUIRED,
    /**
     * In some cases you need to be authenticated, for some cases you don't. <br>
     * E.g public want list vs private want list.
     */
    CONDITIONAL,
    /**
     * You don't need to be authenticated to fetch the resource. <br>
     * But if you do, you will see more data than usual.
     */
    OPTIONAL,
    /**
     * You don't need to be authenticated to fetch the resource.
     */
    NOT_REQUIRED
}
