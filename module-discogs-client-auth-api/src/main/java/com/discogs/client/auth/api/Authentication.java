package com.discogs.client.auth.api;

import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Authentication interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Authentication extends Serializable {

    /**
     * Anonymous interface.
     *
     * @author Sikke303
     * @since 1.0
     * @see Authentication
     */
    @NoArgsConstructor
    class Anonymous implements Authentication {

    }
}