package com.discogs.client.auth.api;

import lombok.NonNull;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Runtime exception thrown if authentication fails for any reason.
 *
 * @author Sikke303
 * @since 1.0
 * @see RuntimeException
 */
@ParametersAreNonnullByDefault
public abstract class AuthenticationRuntimeException extends RuntimeException {

    protected AuthenticationRuntimeException(@NonNull final String message) {
        super(message);
    }

    protected AuthenticationRuntimeException(@NonNull final String message, @NonNull final Throwable exception) {
        super(message, exception);
    }
}