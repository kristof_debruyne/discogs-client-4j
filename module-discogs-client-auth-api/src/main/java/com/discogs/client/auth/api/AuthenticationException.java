package com.discogs.client.auth.api;

import lombok.NonNull;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Exception thrown if authentication fails for any reason.
 *
 * @author Sikke303
 * @since 1.0
 * @see Exception
 */
@ParametersAreNonnullByDefault
public abstract class AuthenticationException extends Exception {

    protected AuthenticationException(@NonNull final String message) {
        super(message);
    }

    protected AuthenticationException(@NonNull final String message, @NonNull final Throwable exception) {
        super(message, exception);
    }
}