package com.discogs.client.auth.api;

import javax.annotation.Nonnull;

/**
 * Authentication context interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface AuthenticationContext {

    /**
     * Gets authentication name.
     *
     * @return name (never null)
     */
    @Nonnull
    String getName();
}