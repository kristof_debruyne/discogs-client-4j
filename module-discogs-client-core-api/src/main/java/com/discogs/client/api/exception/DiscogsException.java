package com.discogs.client.api.exception;

import lombok.Getter;
import lombok.NonNull;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Default Discogs exception (business).
 *
 * @author Sikke303
 * @since 1.0
 * @see Exception
 */
@ParametersAreNonnullByDefault
public class DiscogsException extends Exception {

    @Getter
    private final DiscogsErrorCode errorCode;

    public DiscogsException(@NonNull final String message,
                            @NonNull final DiscogsErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public DiscogsException(@NonNull final String message,
                            @NonNull final DiscogsErrorCode errorCode,
                            @NonNull final Throwable exception) {
        super(message, exception);
        this.errorCode = errorCode;
    }
}