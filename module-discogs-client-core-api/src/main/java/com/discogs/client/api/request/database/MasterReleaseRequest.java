package com.discogs.client.api.request.database;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Getter
public class MasterReleaseRequest extends AbstractRequest {

    private final long masterId;

    @Builder
    private MasterReleaseRequest(@Nullable AcceptMediaType mediaType,
                                 @Nonnull String contentType,
                                 @Nullable String callback,
                                 @Nullable Authentication authentication,
                                 long masterId) {
        super(mediaType, contentType, callback, authentication);
        this.masterId = requirePositiveIdentifier(masterId);
    }

    @Nonnull
    @Override
    public String getPath() { return "/masters/{masterId}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getMasterId() }; }
}