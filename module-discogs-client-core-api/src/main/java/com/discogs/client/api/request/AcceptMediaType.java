package com.discogs.client.api.request;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Supported media types for 'Accept' header. <br><br>
 *
 * Resource: https://www.discogs.com/developers#page:home,header:home-versioning-and-media-types
 *
 * @author Sikke303
 * @since 1.0
 */
@RequiredArgsConstructor
public enum AcceptMediaType {

    DISCOGS_AND_JSON("application/vnd.discogs.v2.discogs+json"),
    HTML_AND_JSON("application/vnd.discogs.v2.html+json"),
    PLAINTEXT_AND_JSON("application/vnd.discogs.v2.plaintext+json"),
    DEFAULT(null);

    @Getter
    private final String value;
}