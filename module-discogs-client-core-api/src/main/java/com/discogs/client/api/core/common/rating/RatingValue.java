package com.discogs.client.api.core.common.rating;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Supported ratings.
 *
 * @author Sikke303
 * @since 1.0
 */
@RequiredArgsConstructor
public enum RatingValue {

    LOWEST(1),
    LOW(2),
    MEDIUM(3),
    HIGH(4),
    HIGHEST(5);

    @Getter
    private final int value;
}