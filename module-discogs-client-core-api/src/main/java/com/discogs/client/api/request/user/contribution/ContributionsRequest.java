package com.discogs.client.api.request.user.contribution;

import com.discogs.client.api.core.common.sorting.Sort;
import com.discogs.client.api.core.common.sorting.SortOrder;
import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.common.PaginationRequest;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class ContributionsRequest extends AbstractRequest {

    private final String username;

    @Builder
    private ContributionsRequest(@Nullable AcceptMediaType mediaType,
                                 @Nonnull String contentType,
                                 @Nullable String callback,
                                 @Nullable PaginationRequest pagination,
                                 @Nullable SortMode sort,
                                 @Nullable SortOrder sortOrder,
                                 @Nonnull String username) {
        super(mediaType, contentType, callback, pagination, sort, sortOrder);
        this.username = requireNonNull(username);
    }

    @Nonnull
    @Override
    public String getPath() { return "/users/{username}/contributions"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getUsername() }; }

    /**
     * Supported sort modes.
     *
     * @author Sikke303
     * @since 1.0
     * @see Sort
     * @see SortOrder
     */
    @RequiredArgsConstructor
    public enum SortMode implements Sort {

        ADDED("added"),
        ARTIST("artist"),
        CATALOG_NUMBER("catno"),
        FORMAT("format"),
        LABEL("label"),
        RATING("rating"),
        TITLE("title"),
        YEAR("year");

        @Getter
        private final String value;
    }
}