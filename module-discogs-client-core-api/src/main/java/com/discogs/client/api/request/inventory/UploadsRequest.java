package com.discogs.client.api.request.inventory;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.common.PaginationRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class UploadsRequest extends AbstractRequest {

    @Builder
    private UploadsRequest(@Nullable AcceptMediaType mediaType,
                           @Nonnull String contentType,
                           @Nullable String callback,
                           @Nonnull Authentication authentication,
                           @Nullable PaginationRequest pagination) {
        super(mediaType, contentType, callback, requireNonNull(authentication), pagination,null,null);
    }

    @Nonnull
    @Override
    public String getPath() { return "/inventory/upload"; }
}