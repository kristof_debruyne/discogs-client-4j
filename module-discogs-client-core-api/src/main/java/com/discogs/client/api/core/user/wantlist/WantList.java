package com.discogs.client.api.core.user.wantlist;

import com.discogs.client.api.core.common.paging.PaginationAware;

/**
 * Discogs want list interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see PaginationAware
 */
public interface WantList extends PaginationAware {

    WantListItem[] getItems();
}