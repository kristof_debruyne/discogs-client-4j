package com.discogs.client.api.response;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Map;

/**
 * Response interface.
 *
 * @author Sikke303
 * @since 1.0
 * @param <T> body type
 */
public interface Response<T> {

    /**
     * Gets response status code.
     *
     * @return status code
     */
    int getStatusCode();

    /**
     * Returns response headers.
     *
     * @return headers (never null)
     */
    @Nonnull
    default Map<String, String> getHeaders() {
        return Collections.emptyMap();
    }

    /**
     * Gets response body.
     *
     * @return body or null
     */
    @Nullable
    default T getBody() {
        return null;
    }
}