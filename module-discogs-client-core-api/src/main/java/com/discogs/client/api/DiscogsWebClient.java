package com.discogs.client.api;

import com.discogs.client.api.annotations.DiscogsClientInterface;
import com.discogs.client.api.core.database.SearchType;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
/**
 * Discogs web client interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see DiscogsClient
 */
@DiscogsClientInterface(
        domain = "web",
        description = "Client for Discogs web",
        readOnly = true,
        throttling = false,
        links = {
                "https://www.discogs.com"
        }
)
@ParametersAreNonnullByDefault
public interface DiscogsWebClient extends DiscogsClient {

    @Nonnull
    String getHomepage();

    @Nonnull
    String createSearchUrl(@Nonnull String input);

    @Nonnull
    String createSearchUrl(@Nonnull String input, @Nonnull SearchType searchType);

    @Nonnull
    String createSearchUrl(@Nonnull String input, @Nonnull SearchType searchType, int page, int pageSize);

    @Nonnull
    String createArtistWebUrl(@Nonnegative long artistId);

    @Nonnull
    String createLabelWebUrl(@Nonnegative long labelId);

    @Nonnull
    String createMasterWebUrl(@Nonnegative long masterId);

    @Nonnull
    String createReleaseWebUrl(@Nonnegative long releaseId);
}
