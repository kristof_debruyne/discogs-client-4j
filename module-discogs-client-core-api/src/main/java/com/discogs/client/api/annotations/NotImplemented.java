package com.discogs.client.api.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Not implemented annotation.
 *
 * @author Sikke303
 * @since 1.0
 */
@Inherited
@Documented
@Retention(RetentionPolicy.SOURCE)
@Target(value = ElementType.METHOD)
public @interface NotImplemented {

}