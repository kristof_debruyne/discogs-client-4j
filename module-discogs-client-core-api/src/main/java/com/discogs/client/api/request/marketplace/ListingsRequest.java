package com.discogs.client.api.request.marketplace;

import com.discogs.client.api.core.common.sorting.Sort;
import com.discogs.client.api.core.common.sorting.SortOrder;
import com.discogs.client.api.core.marketplace.ListingStatus;
import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.common.PaginationRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;

@Getter
public class ListingsRequest extends AbstractRequest {

    private final String username;
    private final ListingStatus status;

    @Builder
    private ListingsRequest(@Nullable AcceptMediaType mediaType,
                            @Nonnull String contentType,
                            @Nullable String callback,
                            @Nullable Authentication authentication,
                            @Nullable PaginationRequest pagination,
                            @Nullable SortMode sort,
                            @Nullable SortOrder sortOrder,
                            @Nonnull String username,
                            @Nullable ListingStatus status) {
        super(mediaType, contentType, callback, authentication, pagination, sort, sortOrder);
        this.username = requireNonNull(username);
        this.status = status;
    }

    @Nonnull
    @Override
    public String getPath() { return "/users/{username}/inventory"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getUsername() }; }

    @Nonnull
    @Override
    public Map<String, Object> getQueryParams() {
        final Map<String, Object> params = new HashMap<>(super.getQueryParams());
        if(nonNull(status)) {
            params.put("status", status.getValue());
        }
        return unmodifiableMap(params);
    }

    /**
     * Supported sort modes.
     *
     * @author Sikke303
     * @since 1.0
     * @see Sort
     * @see SortOrder
     */
    @RequiredArgsConstructor
    public enum SortMode implements Sort {

        ARTIST("artist"),
        AUDIO("audio"),
        CATALOG_NUMBER("catno"),
        ITEM("item"),
        LISTED("listed"),
        LOCATION("location"),
        PRICE("price"),
        STATUS("status");

        @Getter
        private final String value;
    }
}