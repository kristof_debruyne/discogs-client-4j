package com.discogs.client.api.core.marketplace;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Supported listing statuses.
 *
 * @author Sikke303
 * @since 1.0
 */
@RequiredArgsConstructor
public enum ListingStatus {

    DRAFT("Draft"),
    EXPIRED("Expired"),
    FOR_SALE("For Sale");

    @Getter
    private final String value;
}
