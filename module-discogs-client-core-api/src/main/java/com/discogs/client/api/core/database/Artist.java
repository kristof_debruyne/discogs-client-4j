package com.discogs.client.api.core.database;

import com.discogs.client.api.core.image.Image;

/**
 * Discogs artist interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Artist {

    long getId();

    String getAnv();

    String getDataQuality();

    Image[] getImages();

    String getJoin();

    Member[] getMembers();

    String getName();

    String[] getNameVariations();

    String getProfile();

    String getReleasesUrl();

    String getResourceUrl();

    String getRole();

    String getTracks();

    String getUri();

    String[] getUrls();

    /**
     * Discogs member interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface Member {

        long getId();

        boolean isActive();

        String getName();

        String getResourceUrl();
    }
}