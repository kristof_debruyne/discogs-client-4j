package com.discogs.client.api.core.common.sorting;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Supported sort orders.
 * 
 * @author Sikke303
 * @since 1.0
 * @see Sort
 */
@RequiredArgsConstructor
public enum SortOrder {

	ASC("asc"),
    DESC("desc");

    @Getter
    private final String value;
}