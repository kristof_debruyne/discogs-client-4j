package com.discogs.client.api.core.marketplace;

import java.util.Map;

/**
 * Discogs price suggestions interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface PriceSuggestions {

    Map<String, ? extends Fee> getSuggestions();
}