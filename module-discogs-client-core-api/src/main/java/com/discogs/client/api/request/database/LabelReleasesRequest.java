package com.discogs.client.api.request.database;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.common.PaginationRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Getter
public class LabelReleasesRequest extends AbstractRequest {

    private final long labelId;

    @Builder
    private LabelReleasesRequest(@Nullable AcceptMediaType mediaType,
                                 @Nonnull String contentType,
                                 @Nullable String callback,
                                 @Nullable Authentication authentication,
                                 @Nullable PaginationRequest pagination,
                                 long labelId) {
        super(mediaType, contentType, callback, authentication, pagination, null, null);
        this.labelId = requirePositiveIdentifier(labelId);
    }

    @Nonnull
    @Override
    public String getPath() { return "/labels/{labelId}/releases"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getLabelId() }; }
}