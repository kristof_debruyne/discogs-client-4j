package com.discogs.client.api;

import com.discogs.client.api.annotations.DiscogsClientInterface;
import com.discogs.client.api.annotations.DiscogsEndpoint;
import com.discogs.client.api.request.Request;
import com.discogs.client.api.response.Response;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Discogs JSONP client interface.
 *
 * @author Sikke303
 * @since 1.2
 * @see DiscogsClient
 */
@DiscogsClientInterface(
        domain = "all",
        description = "Client for retrieving Discogs data in JSONP format.",
        readOnly = false,
        throttling = true,
        links = { "https://www.discogs.com/developers#header-jsonp" }
)
@ParametersAreNonnullByDefault
public interface DiscogsJsonpClient extends DiscogsClient {

    /**
     * Execute JSONP request.
     *
     * If you pass a callback query param, you will receive the response in JSONP format.
     * JSONP format is actually response with content type 'text/javascript'.
     *
     * @param method method (required)
     * @param request request (required)
     * @param <T> request type
     * @return JSONP response (never null)
     */
    @DiscogsEndpoint(
            description = "Execute JSONP request.",
            link = "https://www.discogs.com/developers#header-jsonp",
            cacheable = false
    )
    @Nonnull
    <T extends Request> Response<String> execute(String method, T request);
}