package com.discogs.client.api.core.marketplace;

/**
 * Discogs release statistics interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface ReleaseStatistics {

    boolean isBlockedFromSale();

    Price getLowestPrice();

    int getNumberForSale();
}