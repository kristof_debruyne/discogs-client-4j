package com.discogs.client.api.response;

import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

import static java.util.Collections.emptyMap;
import static java.util.Objects.requireNonNull;

/**
 * Generic response. <br><br>
 *
 * Contains following properties: <br>
 * - status code <br>
 * - response headers <br>
 * - response body <br>
 *
 * @author Sikke303
 * @since 1.0
 * @param <T> body type
 * @see Response
 */
@Getter
public class GenericResponse<T> implements Response<T> {

    private final int statusCode;
    private final Map<String, String> headers;
    private final T body;

    /**
     * Constructor
     */
    public GenericResponse() {
        this(-1, emptyMap(), null);
    }

    /**
     * Constructor
     * @param statusCode status code (required)
     * @param headers response headers (required)
     * @param body body (optional)
     */
    public GenericResponse(final int statusCode, @Nonnull final Map<String, String> headers, @Nullable final T body) {
        this.statusCode = checkStatus(statusCode);
        this.headers = requireNonNull(headers);
        this.body = body;
    }

    /**
     * Checks status code.
     *
     * If status code is greater than zero than status code is returned else 0 is returned.
     *
     * @param statusCode status code
     * @return status code
     */
    private int checkStatus(int statusCode) { return Math.max(statusCode, 0); }
}