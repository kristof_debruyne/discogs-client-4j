package com.discogs.client.api.core.marketplace;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Supported conditions.
 *
 * @author Sikke303
 * @since 1.0
 */
@RequiredArgsConstructor
public enum Condition {

    MINT("Mint (M)"),
    NEAR_MINT("Near Mint (NM or M-)"),
    VERY_GOOD_PLUS("Very Good Plus (VG+)"),
    VERY_GOOD("Very Good (VG)"),
    GOOD_PLUS("Good Plus (G+)"),
    GOOD("Good (G)"),
    FAIR("Fair (F)"),
    POOR("Poor (P)");

    @Getter
    private final String value;
}