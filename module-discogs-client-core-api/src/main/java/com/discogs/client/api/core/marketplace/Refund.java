package com.discogs.client.api.core.marketplace;

/**
 * Discogs refund interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Refund {

    double getAmount();

    OrderInfo getOrder();
}