package com.discogs.client.api.core.user.submission;

import com.discogs.client.api.core.common.paging.PaginationAware;
import com.discogs.client.api.core.database.Artist;
import com.discogs.client.api.core.database.Label;
import com.discogs.client.api.core.database.Release;

/**
 * Discogs submissions interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Submissions extends PaginationAware {

    Submission getResult();

    /**
     * Discogs submission interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface Submission {

        Artist[] getArtists();

        Label[] getLabels();

        Release[] getReleases();
    }
}