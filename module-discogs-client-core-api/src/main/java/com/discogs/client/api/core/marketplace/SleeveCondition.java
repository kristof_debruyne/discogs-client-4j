package com.discogs.client.api.core.marketplace;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Supported sleeve conditions.
 *
 * @author Sikke303
 * @since 1.0
 */
@RequiredArgsConstructor
public enum SleeveCondition {

    MINT("Mint (M)"),
    NEAR_MINT("Near Mint (NM or M-)"),
    VERY_GOOD_PLUS("Very Good Plus (VG+)"),
    VERY_GOOD("Very Good (VG)"),
    GOOD_PLUS("Good Plus (G+)"),
    GOOD("Good (G)"),
    FAIR("Fair (F)"),
    POOR("Poor (P)"),
    GENERIC("Generic"),
    NOT_GRADED("Not Graded"),
    NO_COVER("No Cover");

    @Getter
    private final String value;
}