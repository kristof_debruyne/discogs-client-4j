package com.discogs.client.api.core.common.sorting;

/**
 * Sort interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see SortOrder
 */
public interface Sort {

    String getValue();
}