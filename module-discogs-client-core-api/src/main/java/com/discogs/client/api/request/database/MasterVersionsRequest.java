package com.discogs.client.api.request.database;

import com.discogs.client.api.core.common.sorting.Sort;
import com.discogs.client.api.core.common.sorting.SortOrder;
import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.common.PaginationRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.unmodifiableMap;

@Getter
public class MasterVersionsRequest extends AbstractRequest {

    private final long masterId;
    private final String format;
    private final String labelName;
    private final String released;
    private final String country;

    @Builder
    private MasterVersionsRequest(@Nullable AcceptMediaType mediaType,
                                  @Nonnull String contentType,
                                  @Nullable String callback,
                                  @Nullable PaginationRequest pagination,
                                  @Nullable Authentication authentication,
                                  @Nullable SortMode sort,
                                  @Nullable SortOrder sortOrder,
                                  long masterId,
                                  @Nullable String country,
                                  @Nullable String format,
                                  @Nullable String labelName,
                                  @Nullable String released) {
        super(mediaType, contentType, callback, authentication, pagination, sort, sortOrder);
        this.masterId = requirePositiveIdentifier(masterId);
        this.country = country;
        this.format = format;
        this.labelName = labelName;
        this.released = released;
    }

    @Nonnull
    @Override
    public String getPath() { return "/masters/{masterId}/versions"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getMasterId() }; }

    @Nonnull
    @Override
    public Map<String, Object> getQueryParams() {
        final Map<String, Object> params = new HashMap<>(super.getQueryParams());
        params.computeIfAbsent("format", key -> format);
        params.computeIfAbsent("label", key -> labelName);
        params.computeIfAbsent("released", key -> released);
        params.computeIfAbsent("country", key -> country);
        return unmodifiableMap(params);
    }

    /**
     * Supported sort modes.
     *
     * @author Sikke303
     * @since 1.0
     * @see Sort
     * @see SortOrder
     */
    @RequiredArgsConstructor
    public enum SortMode implements Sort {

        CATALOG_NUMBER("catno"),
        COUNTRY("country"),
        FORMAT("format"),
        LABEL("label"),
        RELEASED("released"),
        TITLE("title");

        @Getter
        private final String value;
    }
}