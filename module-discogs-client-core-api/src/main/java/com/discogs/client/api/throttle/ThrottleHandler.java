package com.discogs.client.api.throttle;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

/**
 * Discogs throttle handler interface.
 *
 * All requests should be throttled (per minute):
 * - authenticated: 60
 * - unauthenticated: 25
 *
 * Link: https://www.discogs.com/developers#page:home,header:home-rate-limiting
 *
 * This can change so be careful !
 *
 * @author Sikke303
 * @since 1.0
 * @see ThrottleContext
 */
public interface ThrottleHandler<T extends ThrottleContext> {

    int SKIP_THROTTLING = 0;

    /**
     * Throttles process for X amount of milli seconds.
     *
     * @param timeToThrottle throttle time in milli seconds (not negative)
     */
    void throttle(@Nonnegative long timeToThrottle);

    /**
     * Calculates time to throttle based on the given context and then throttles process for X amount of milli seconds.
     *
     * Possible throttle times:
     * - 0: skipping throttling
     * - 1+: proceed with throttling
     *
     * Never negative value is possible !
     *
     * @param context throttle context (required)
     */
    void calculateAndThrottle(@Nonnull T context);
}
