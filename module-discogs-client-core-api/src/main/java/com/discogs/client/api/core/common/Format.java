package com.discogs.client.api.core.common;

/**
 * Discogs format interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Format {

    String[] getDescriptions();

    String getName();

    String getQuantity();

    String getText();
}