package com.discogs.client.api.config;


import com.discogs.client.auth.api.AuthenticationContext;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Discogs client configurable interface.
 *
 * @author Sikke303
 * @since 1.0
 */
@ParametersAreNonnullByDefault
public interface DiscogsConfigurable {

    /**
     * Initializes Discogs context.
     *
     * @return context (never null)
     */
    @Nonnull
    DiscogsContext<? extends AuthenticationContext> initializeDiscogsContext();

    /**
     * Gets Discogs properties.
     *
     * @return properties (never null)
     */
    @Nonnull
    DiscogsProperties getProperties();
}