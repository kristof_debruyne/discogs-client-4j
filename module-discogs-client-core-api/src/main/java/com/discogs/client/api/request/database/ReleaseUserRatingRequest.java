package com.discogs.client.api.request.database;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class ReleaseUserRatingRequest extends AbstractRequest {

    private final long releaseId;
    private final String username;

    @Builder
    private ReleaseUserRatingRequest(@Nullable AcceptMediaType mediaType,
                                     @Nonnull String contentType,
                                     @Nullable String callback,
                                     long releaseId,
                                     @Nonnull String username) {
        super(mediaType, contentType, callback);
        this.releaseId = requirePositiveIdentifier(releaseId);
        this.username = requireNonNull(username);
    }

    @Nonnull
    @Override
    public String getPath() { return "/releases/{releaseId}/rating/{username}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getReleaseId(), getUsername() }; }
}