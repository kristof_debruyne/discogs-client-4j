package com.discogs.client.api.request.user.submission;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.common.PaginationRequest;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class SubmissionsRequest extends AbstractRequest {

    private final String username;

    @Builder
    private SubmissionsRequest(@Nullable AcceptMediaType mediaType,
                               @Nonnull String contentType,
                               @Nullable String callback,
                               @Nullable PaginationRequest pagination,
                               @Nonnull String username) {
        super(mediaType, contentType, callback, pagination, null, null);
        this.username = requireNonNull(username);
    }

    @Nonnull
    @Override
    public String getPath() { return "/users/{username}/submissions"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getUsername() }; }
}