package com.discogs.client.api.core.database;

/**
 * Discogs user rating interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface ReleaseUserRating {

    long getReleaseId();

    int getRating();

    String getUserName();
}