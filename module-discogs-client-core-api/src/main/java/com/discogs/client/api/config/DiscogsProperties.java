package com.discogs.client.api.config;

import com.discogs.client.api.throttle.ThrottleMode;
import lombok.NonNull;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Serializable;

/**
 * Discogs properties interface. <br><br>
 *
 * @author Sikke303
 * @since 1.0
 */
public interface DiscogsProperties extends Serializable {

    //*** CLIENT ***//

    /**
     * Gets application key.
     *
     * This will be used in the 'User-Agent' header.
     *
     * @return application key (never null)
     */
    @Nonnull
    String getApplicationKey();

    /**
     * Gets application name.
     *
     * This will be used in the 'User-Agent' header.
     *
     * @return application name (never null)
     */
    @Nonnull
    String getApplicationName();

    /**
     * Gets application version.
     *
     * This will be used in the 'User-Agent' header.
     *
     * @return application version (never null)
     */
    @Nonnull
    String getApplicationVersion();

    /**
     * Gets callback url.
     *
     * @return callback url or null
     */
    @Nullable
    String getCallbackUrl();

    /**
     * Gets consumer key.
     *
     * @return consumer key (never null)
     */
    @NonNull
    String getConsumerKey();

    /**
     * Gets consumer secret.
     *
     * @return consumer secret (never null)
     */
    @NonNull
    String getConsumerSecret();

    /**
     * Gets personal token.
     *
     * Remark: recommended to use this for testing purposes only !
     *
     * @return personal token or null
     */
    @Nullable
    String getPersonalToken();

    /**
     * Checks if callback url is configured or not.
     *
     * @return boolean callback url configured (true) or not (false)
     */
    boolean hasCallbackUrl();

    //*** SERVER ***//

    /**
     * Gets Discogs api version.
     *
     * @return api version (never null)
     */
    @Nonnull
    String getApiVersion();

    /**
     * Gets Discogs root url version.
     *
     * @return api root url (never null)
     */
    @Nonnull
    String getApiRootUrl();

    /**
     * Gets access token url.
     *
     * @return access token url (never null)
     */
    @Nonnull
    String getAccessTokenEndpoint();

    /**
     * Gets authorize url.
     *
     * @return authorize url (never null)
     */
    @Nonnull
    String getAuthorizeEndpoint();

    /**
     * Gets request token url.
     *
     * @return request token url (never null)
     */
    @Nonnull
    String getRequestTokenEndpoint();

    /**
     * Gets revoke token url.
     *
     * @return revoke token url (never null)
     */
    @Nonnull
    String getRevokeTokenEndpoint();

    /**
     * Gets image root url.
     *
     * @return image root url (never null)
     */
    @Nonnull
    String getImageRootUrl();

    /**
     * Gets web root url.
     *
     * @return web root url (never null)
     */
    @Nonnull
    String getWebRootUrl();

    /**
     * Gets connection timeout (0 = infinite).
     *
     * @return connection timeout
     */
    @Nonnegative
    int getConnectionTimeout();

    /**
     * Gets read timeout (0 = infinite).
     *
     * @return read timeout
     */
    @Nonnegative
    int getReadTimeout();

    /**
     * Checks if throttling is enabled or not.
     *
     * @return boolean throttling enabled (true) or not (false)
     */
    boolean isThrottlingRequestsEnabled();

    /**
     * Gets the throttling cool down time period in millis.
     *
     * @return cool down time period in millis (never negative)
     */
    @Nonnegative
    long getThrottleCoolDownTimePeriod();

    /**
     * Gets throttle mode (only applicable if throttling is enabled).
     *
     * @return thottle mode (never null)
     */
    @Nonnull
    ThrottleMode getThrottleMode();
}