package com.discogs.client.api.core.user.collection;

/**
 * Discogs collection value interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface CollectionValue {

    String getMinimumValue();

    String getMedianValue();

    String getMaximumValue();
}