package com.discogs.client.api.core.common.paging;

/**
 * Discogs navigation interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see Pagination
 */
public interface Navigation {

    String getFirst();

    String getPrevious();

    String getNext();

    String getLast();
}