package com.discogs.client.api.request.user.wantlist;

import com.discogs.client.api.core.common.rating.RatingValue;
import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.PayloadRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class WantListItemUpdateRequest extends AbstractRequest implements PayloadRequest<String> {

    private final long releaseId;
    private final String username;
    private final String notes;
    private final RatingValue rating;

    @Builder
    private WantListItemUpdateRequest(@Nullable AcceptMediaType mediaType,
                                      @Nonnull String contentType,
                                      @Nullable String callback,
                                      @Nonnull Authentication authentication,
                                      long releaseId,
                                      @Nonnull String username,
                                      @Nullable String notes,
                                      @Nullable RatingValue rating) {
        super(mediaType, contentType, callback, requireNonNull(authentication));
        this.releaseId = requirePositiveIdentifier(releaseId);
        this.username = requireNonNull(username);
        this.notes = notes;
        this.rating = rating;
    }

    @Nonnull
    @Override
    public String getPath() { return "/users/{username}/wants/{releaseId}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getUsername(), getReleaseId() }; }

    @Nonnull
    @Override
    public String getBody() {
        JSONObject json = new JSONObject();
        json.putOpt("notes", notes);
        if(rating != null) {
            json.put("rating", rating.getValue());
        }
        return json.toString();
    }
}