package com.discogs.client.api;

import com.discogs.client.api.annotations.DiscogsClientInterface;
import com.discogs.client.api.annotations.DiscogsEndpoint;
import com.discogs.client.api.core.inventory.Export;
import com.discogs.client.api.core.inventory.Exports;
import com.discogs.client.api.core.inventory.Upload;
import com.discogs.client.api.core.inventory.Uploads;
import com.discogs.client.api.request.inventory.ExportCreateRequest;
import com.discogs.client.api.request.inventory.ExportDownloadRequest;
import com.discogs.client.api.request.inventory.ExportRequest;
import com.discogs.client.api.request.inventory.ExportsRequest;
import com.discogs.client.api.request.inventory.UploadAddRequest;
import com.discogs.client.api.request.inventory.UploadChangeRequest;
import com.discogs.client.api.request.inventory.UploadDeleteRequest;
import com.discogs.client.api.request.inventory.UploadRequest;
import com.discogs.client.api.request.inventory.UploadsRequest;
import com.discogs.client.api.response.Response;
import com.discogs.client.auth.api.AuthenticationMode;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Discogs inventory client interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see DiscogsClient
 */
@DiscogsClientInterface(
        domain = "inventory",
        description = "Client for managing inventory.",
        readOnly = false,
        throttling = true,
        links = {
            "https://www.discogs.com/developers#page:inventory-export",
            "https://www.discogs.com/developers#page:inventory-upload"
        }
)
@ParametersAreNonnullByDefault
public interface DiscogsInventoryClient extends DiscogsClient {

    /**
     * Create inventory export by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Create inventory export.",
            link = "https://www.discogs.com/developers#page:inventory-export,header:inventory-export-export-your-inventory",
            cacheable = false,
            allowPagination = false,
            allowSorting = false
    )
    @Nonnull
    Response<Void> create(ExportCreateRequest request);

    /**
     * Download export by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Download inventory export by id.",
            link = "https://www.discogs.com/developers#page:inventory-export,header:inventory-export-download-an-export",
            requiredRequestParams = "exportId",
            cacheable = false,
            allowPagination = false,
            allowSorting = false
    )
    @Nonnull
    Response<byte[]> download(ExportDownloadRequest request);

    /**
     * Get exports by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Get recent inventory exports.",
            link = "https://www.discogs.com/developers#page:inventory-export,header:inventory-export-get-recent-exports",
            cacheable = true,
            allowPagination = true,
            allowSorting = false
    )
    @Nonnull
    Response<? extends Exports> getAll(ExportsRequest request);

    /**
     * Get export by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Get inventory export by id.",
            link = "https://www.discogs.com/developers#page:inventory-export,header:inventory-export-get-an-export",
            requiredRequestParams = "exportId",
            cacheable = true
    )
    @Nonnull
    Response<? extends Export> get(ExportRequest request);

    /**
     * Get uploads by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Get recent inventory uploads.",
            link = "https://www.discogs.com/developers#page:inventory-upload,header:inventory-upload-get-recent-uploads",
            cacheable = true,
            allowPagination = true
    )
    @Nonnull
    Response<? extends Uploads> getAll(UploadsRequest request);

    /**
     * Get upload by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Get inventory upload by id.",
            link = "https://www.discogs.com/developers#page:inventory-upload,header:inventory-upload-get-an-upload",
            requiredRequestParams = "uploadId",
            cacheable = true
    )
    @Nonnull
    Response<? extends Upload> get(UploadRequest request);

    /**
     * Add to inventory by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Add items to inventory.",
            link = "https://www.discogs.com/developers#page:inventory-upload,header:inventory-upload-add-inventory",
            cacheable = false
    )
    @Nonnull
    Response<Void> upload(UploadAddRequest request);

    /**
     * Change inventory by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Change items from inventory.",
            link = "https://www.discogs.com/developers#page:inventory-upload,header:inventory-upload-change-inventory",
            cacheable = false
    )
    @Nonnull
    Response<Void> upload(UploadChangeRequest request);

    /**
     * Delete from inventory by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Delete items from inventory.",
            link = "https://www.discogs.com/developers#page:inventory-upload,header:inventory-upload-delete-inventory",
            cacheable = false
    )
    @Nonnull
    Response<Void> upload(UploadDeleteRequest request);
}