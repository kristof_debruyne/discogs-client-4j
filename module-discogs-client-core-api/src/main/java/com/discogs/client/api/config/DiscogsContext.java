package com.discogs.client.api.config;

import com.discogs.client.auth.api.AuthenticationContext;

import javax.annotation.Nonnull;

/**
 * Discogs context interface. <br><br>
 *
 * The context allows you to: <br>
 * - retrieve consumer properties: application name, key and version <br>
 * - retrieve provider properties: api version, root url and image root url <br>
 * - retrieve authentication context: authentication context <br>
 *
 * @author Sikke303
 * @since 1.0
 */
public interface DiscogsContext<T extends AuthenticationContext> {

    /**
     * Gets application name (consumer)
     *
     * @return version (never null)
     */
    @Nonnull
    String getApplicationName();

    /**
     * Gets application key (consumer)
     *
     * @return key (never null)
     */
    @Nonnull
    String getApplicationKey();

    /**
     * Gets application version (consumer)
     *
     * @return version (never null)
     */
    @Nonnull
    String getApplicationVersion();

    /**
     * Gets API version (provider)
     *
     * @return version (never null)
     */
    @Nonnull
    String getApiVersion();

    /**
     * Gets API root URL (provider)
     *
     * @return url (never null)
     */
    @Nonnull
    String getApiRootUrl();

    /**
     * Gets image root URL (provider)
     *
     * @return url (never null)
     */
    @Nonnull
    String getImageRootUrl();

    /**
     * Gets web root URL (provider)
     *
     * @return url (never null)
     */
    @Nonnull
    String getWebRootUrl();

    /**
     * Gets authentication context.
     *
     * @return context (never null)
     */
    @Nonnull
    T getAuthenticationContext();
}