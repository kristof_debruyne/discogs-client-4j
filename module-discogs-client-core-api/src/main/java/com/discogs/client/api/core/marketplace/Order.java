package com.discogs.client.api.core.marketplace;

/**
 * Discogs order interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see OrderInfo
 */
public interface Order extends OrderInfo {

    boolean isArchived();

    String getAdditionalInstructions();

    Buyer getBuyer();

    String getDateCreated();

    String getDateLastActivity();

    Fee getFee();

    OrderItem[] getItems();

    String getMessagesUrl();

    String[] getNextStatuses();

    Price getTotalPrice();

    Seller getSeller();

    Shipping getShipping();

    String getShippingAddress();

    String getStatus();

    String getUri();

    /**
     * Discogs order item interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface OrderItem {

        long getId();

        Price getPrice();

        OrderRelease getRelease();

        String getCondition();

        String getSleeveCondition();
    }

    /**
     * Discogs order release interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface OrderRelease {

        long getId();

        String getDescription();
    }
}