package com.discogs.client.api.core.marketplace;

/**
 * Discogs listing interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Listing {

    long getId();

    boolean isAllowOffers();

    boolean isAudio();

    String getComment();

    String getCondition();

    String getDatePosted();

    String getExternalId();

    String getFormatQuantity(); //value 'auto' allowed

    String getLocation();

    PriceOriginal getOriginalPrice();

    PriceOriginal getOriginalShippingPrice();

    Price getPrice();

    ListingRelease getRelease();

    Seller getSeller();

    String getShipsFrom();

    Price getShippingPrice();

    String getSleeveCondition();

    String getStatus();

    String getUri();

    String getWeight(); //value 'auto' allowed

    /**
     * Discogs listing release interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface ListingRelease {

        long getId();

        String getCatalogNumber();

        String getDescription();

        String getResourceUrl();

        String getThumbnail();

        int getYear();
    }
}