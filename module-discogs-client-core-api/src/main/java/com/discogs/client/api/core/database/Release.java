package com.discogs.client.api.core.database;

import com.discogs.client.api.core.common.Community;
import com.discogs.client.api.core.common.Company;
import com.discogs.client.api.core.common.Format;
import com.discogs.client.api.core.common.Video;
import com.discogs.client.api.core.image.Image;

/**
 * Discogs release interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Release {

    long getId();

    Artist[] getArtists();

    Community getCommunity();

    Company[] getCompanies();

    String getCountry();

    String getDataQuality();

    String getDateAdded();

    String getDateChanged();

    int getEstimatedWeight();

    Artist[] getExtraArtists();

    int getFormatQuantity();

    Format[] getFormats();

    String[] getGenres();

    Identifier[] getIdentifiers();

    Image[] getImages();

    Label[] getLabels();

    Double getLowestPrice();

    Long getMasterId();

    String getMasterUrl();

    String getNotes();

    int getNumberForSale();

    String getReleased();

    String getReleasedFormatted();

    String[] getSeries();

    String getStatus();

    String[] getStyles();

    String getThumbnail();

    String getTitle();

    Track[] getTrackList();

    String getUri();

    Video[] getVideos();

    int getYear();

    /**
     * Discogs identifier interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface Identifier {

        String getType();

        String getValue();
    }
}