package com.discogs.client.api.request.marketplace;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.common.PaginationRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class OrderMessagesRequest extends AbstractRequest {

    private final String orderId;

    @Builder
    private OrderMessagesRequest(@Nullable AcceptMediaType mediaType,
                                 @Nonnull String contentType,
                                 @Nullable String callback,
                                 @Nonnull Authentication authentication,
                                 @Nullable PaginationRequest pagination,
                                 @Nonnull String orderId) {
        super(mediaType, contentType, callback, requireNonNull(authentication), pagination, null, null);
        this.orderId = requireNonNull(orderId);
    }

    @Nonnull
    @Override
    public String getPath() { return "/marketplace/orders/{orderId}/messages"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getOrderId() }; }
}