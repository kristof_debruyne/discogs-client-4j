package com.discogs.client.api;

import com.discogs.client.api.annotations.DiscogsClientInterface;
import com.discogs.client.api.annotations.DiscogsEndpoint;
import com.discogs.client.api.request.image.ImageRequest;
import com.discogs.client.api.response.Response;
import com.discogs.client.auth.api.AuthenticationMode;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Discogs image client interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see DiscogsClient
 */
@DiscogsClientInterface(
        domain = "image",
        description = "Client for retrieving images.",
        readOnly = true,
        throttling = true,
        links = { "https://www.discogs.com/developers#page:images" }
)
@ParametersAreNonnullByDefault
public interface DiscogsImageClient extends DiscogsClient {

    /**
     * Get image by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Get image by URL.",
            link = "https://www.discogs.com/developers#page:images",
            requiredRequestParams = "url",
            cacheable = true
    )
    @Nonnull
    Response<byte[]> get(ImageRequest request);
}