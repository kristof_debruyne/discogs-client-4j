package com.discogs.client.api.core.marketplace;

import com.discogs.client.api.core.common.Currency;

/**
 * Discogs shipping interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Shipping {

    Currency getCurrency();

    String getMethod();

    double getValue();
}