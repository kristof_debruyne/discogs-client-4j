package com.discogs.client.api.request.user.collection;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.common.PaginationRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class CollectionItemsByReleaseRequest extends AbstractRequest {

    private final long releaseId;
    private final String username;

    @Builder
    private CollectionItemsByReleaseRequest(@Nullable AcceptMediaType mediaType,
                                            @Nonnull String contentType,
                                            @Nullable String callback,
                                            @Nullable Authentication authentication,
                                            @Nullable PaginationRequest pagination,
                                            long releaseId,
                                            @Nonnull String username) {
        super(mediaType, contentType, callback, authentication, pagination, null, null);
        this.releaseId = requirePositiveIdentifier(releaseId);
        this.username = requireNonNull(username);
    }

    @Nonnull
    @Override
    public String getPath() { return "/users/{username}/collection/releases/{releaseId}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getUsername(), getReleaseId() }; }
}