package com.discogs.client.api.request.user.identity;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class ProfileRequest extends AbstractRequest {

    private final String username;

    @Builder
    private ProfileRequest(@Nullable AcceptMediaType mediaType,
                           @Nonnull String contentType,
                           @Nullable String callback,
                           @Nullable Authentication authentication,
                           @Nonnull String username) {
        super(mediaType, contentType, callback, authentication);
        this.username = requireNonNull(username);
    }

    @Nonnull
    @Override
    public String getPath() { return "/users/{username}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getUsername() }; }
}