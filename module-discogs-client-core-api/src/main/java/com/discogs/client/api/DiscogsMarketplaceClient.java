package com.discogs.client.api;

import com.discogs.client.api.annotations.DiscogsClientInterface;
import com.discogs.client.api.annotations.DiscogsEndpoint;
import com.discogs.client.api.core.marketplace.Fee;
import com.discogs.client.api.core.marketplace.Listing;
import com.discogs.client.api.core.marketplace.Listings;
import com.discogs.client.api.core.marketplace.Order;
import com.discogs.client.api.core.marketplace.OrderMessages;
import com.discogs.client.api.core.marketplace.Orders;
import com.discogs.client.api.core.marketplace.PriceSuggestions;
import com.discogs.client.api.core.marketplace.ReleaseStatistics;
import com.discogs.client.api.request.marketplace.FeeCurrencyRequest;
import com.discogs.client.api.request.marketplace.FeeRequest;
import com.discogs.client.api.request.marketplace.ListingCreateRequest;
import com.discogs.client.api.request.marketplace.ListingDeleteRequest;
import com.discogs.client.api.request.marketplace.ListingRequest;
import com.discogs.client.api.request.marketplace.ListingUpdateRequest;
import com.discogs.client.api.request.marketplace.ListingsRequest;
import com.discogs.client.api.request.marketplace.OrderMessageCreateRequest;
import com.discogs.client.api.request.marketplace.OrderMessagesRequest;
import com.discogs.client.api.request.marketplace.OrderRequest;
import com.discogs.client.api.request.marketplace.OrdersRequest;
import com.discogs.client.api.request.marketplace.PriceSuggestionsRequest;
import com.discogs.client.api.request.marketplace.ReleaseStatisticsRequest;
import com.discogs.client.api.response.Response;
import com.discogs.client.auth.api.AuthenticationMode;
import lombok.NonNull;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Discogs marketplace client interface. <br><br>
 *
 * @author Sikke303
 * @since 1.0
 * @see DiscogsClient
 */
@DiscogsClientInterface(
        domain = "marketplace",
        description = "Client for interacting with marketplace.",
        readOnly = false,
        throttling = true,
        links = { "https://www.discogs.com/developers#page:marketplace" }
)
@ParametersAreNonnullByDefault
public interface DiscogsMarketplaceClient extends DiscogsClient {

    /**
     * Get fee by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.NOT_REQUIRED,
            description = "Calculate fee for price.",
            link = "https://www.discogs.com/developers#page:marketplace,header:marketplace-fee",
            requiredRequestParams = "price",
            cacheable = true
    )
    @Nonnull
    Response<? extends Fee> get(FeeRequest request);

    /**
     * Get fee with currency by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.NOT_REQUIRED,
            description = "Calculate fee for price and currency.",
            link = "https://www.discogs.com/developers#page:marketplace,header:marketplace-fee-with-currency",
            requiredRequestParams = { "currency", "price" },
            cacheable = true
    )
    @Nonnull
    Response<? extends Fee> get(FeeCurrencyRequest request);

    /**
     * Get price suggestions by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Get price suggestions by id.",
            link = "https://www.discogs.com/developers#page:marketplace,header:marketplace-price-suggestions",
            requiredRequestParams = "releaseId",
            cacheable = true
    )
    @Nonnull
    Response<? extends PriceSuggestions> get(PriceSuggestionsRequest request);

    /**
     * Get listings by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.CONDITIONAL,
            description = "Get listings by user.",
            link = "https://www.discogs.com/developers#page:marketplace,header:marketplace-inventory",
            requiredRequestParams = "username",
            cacheable = true,
            allowPagination = true,
            allowSorting = true
    )
    @Nonnull
    Response<? extends Listings> getAll(ListingsRequest request);

    /**
     * Get listing by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.CONDITIONAL,
            description = "Get listing by id.",
            link = "https://www.discogs.com/developers#page:marketplace,header:marketplace-listing",
            requiredRequestParams = "listingId",
            cacheable = true
    )
    @Nonnull
    Response<? extends Listing> get(ListingRequest request);

    /**
     * Create listing by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Create new listing.",
            link = "https://www.discogs.com/developers#page:marketplace,header:marketplace-new-listing",
            cacheable = false
    )
    @Nonnull
    Response<Void> create(ListingCreateRequest request);

    /**
     * Update listing by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Update listing by id.",
            link = "https://www.discogs.com/developers#page:marketplace,header:marketplace-listing-post",
            requiredRequestParams = { "listingId", "releaseId" },
            cacheable = false
    )
    @Nonnull
    Response<Void> modify(ListingUpdateRequest request);

    /**
     * Delete listing by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Delete listing by id.",
            link = "https://www.discogs.com/developers#page:marketplace,header:marketplace-listing-delete",
            requiredRequestParams = "listingId",
            cacheable = false
    )
    @Nonnull
    Response<Void> delete(ListingDeleteRequest request);

    /**
     * Get orders by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Get orders from logged in user.",
            link = "https://www.discogs.com/developers#page:marketplace,header:marketplace-list-orders",
            cacheable = true,
            allowPagination = true,
            allowSorting = true
    )
    @Nonnull
    Response<? extends Orders> getAll(OrdersRequest request);

    /**
     * Get order by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Get order by id.",
            link = "https://www.discogs.com/developers#page:marketplace,header:marketplace-order",
            requiredRequestParams = "orderId",
            cacheable = true
    )
    @Nonnull
    Response<? extends Order> get(OrderRequest request);

    /**
     * Get order messages by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Get order messages by id.",
            link = "https://www.discogs.com/developers#page:marketplace,header:marketplace-list-order-messages",
            requiredRequestParams = "orderId",
            cacheable = true,
            allowPagination = true
    )
    @Nonnull
    Response<? extends OrderMessages> get(@NonNull OrderMessagesRequest request);

    /**
     * Create order message by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Create new order message.",
            link = "https://www.discogs.com/developers#page:marketplace,header:marketplace-list-order-messages-post",
            requiredRequestParams = "orderId",
            cacheable = false
    )
    @Nonnull
    Response<? extends OrderMessages.OrderMessage> create(@NonNull OrderMessageCreateRequest request);

    /**
     * Get release statistics by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.OPTIONAL,
            description = "Get release statistics by id.",
            link = "https://www.discogs.com/developers#page:marketplace,header:marketplace-release-statistics",
            requiredRequestParams = "releaseId",
            cacheable = true
    )
    @Nonnull
    Response<? extends ReleaseStatistics> get(ReleaseStatisticsRequest request);
}