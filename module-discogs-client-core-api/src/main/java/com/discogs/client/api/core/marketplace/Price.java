package com.discogs.client.api.core.marketplace;

import com.discogs.client.api.core.common.Currency;

/**
 * Discogs price interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Price {

    Currency getCurrency();

    double getValue();
}