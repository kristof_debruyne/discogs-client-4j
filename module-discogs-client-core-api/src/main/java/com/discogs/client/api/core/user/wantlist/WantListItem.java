package com.discogs.client.api.core.user.wantlist;

import com.discogs.client.api.core.common.BasicInformation;

/**
 * Discogs want list item interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface WantListItem {

    long getId();

    int getRating();

    String getNotes();

    BasicInformation getBasicInformation();

    String getResourceUrl();
}