package com.discogs.client.api.core.database;

import com.discogs.client.api.core.common.Community;
import com.discogs.client.api.core.common.Format;
import com.discogs.client.api.core.common.paging.PaginationAware;
import com.discogs.client.api.core.common.stats.ContainStatistics;

/**
 * Discogs search result interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface SearchResult extends PaginationAware {

    SearchResultItem[] getItems();

    /**
     * Discogs search result item interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface SearchResultItem {

        long getId();

        String getCatalogNumber();

        String[] getBarcodes();

        String getCountry();

        String getCoverImage();

        Community getCommunity();

        String[] getFormat();

        Format[] getFormats();

        int getFormatQuantity();

        String[] getGenres();

        String[] getLabels();

        long getMasterId();

        String getMasterUrl();

        String getResourceUrl();

        String[] getStyles();

        String getThumbnail();

        String getTitle();

        String getType();

        String getUri();

        ContainStatistics getUserData();

        String getYear();
    }
}