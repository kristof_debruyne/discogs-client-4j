package com.discogs.client.api.request.user.wantlist;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.common.PaginationRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class WantListRequest extends AbstractRequest {

    private final String username;

    @Builder
    private WantListRequest(@Nullable AcceptMediaType mediaType,
                            @Nonnull String contentType,
                            @Nullable String callback,
                            @Nullable Authentication authentication,
                            @Nullable PaginationRequest pagination,
                            @Nonnull String username) {
        super(mediaType, contentType, callback, authentication, pagination, null, null);
        this.username = requireNonNull(username);
    }

    @Nonnull
    @Override
    public String getPath() { return "/users/{username}/wants"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getUsername() }; }
}