package com.discogs.client.api.core.database;

import com.discogs.client.api.core.common.paging.PaginationAware;
import com.discogs.client.api.core.common.stats.Statistics;

/**
 * Discogs master versions interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface MasterVersions extends PaginationAware {

    MasterVersion[] getVersions();

    /**
     * Discogs master version interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface MasterVersion {

        long getId();

        String getCatalogNumber();

        String getCountry();

        String getFormat();

        String getLabelName();

        String[] getMajorFormats();

        String getReleased();

        String getResourceUrl();

        Statistics getStatistics();

        String getStatus();

        String getThumbnail();

        String getTitle();
    }
}