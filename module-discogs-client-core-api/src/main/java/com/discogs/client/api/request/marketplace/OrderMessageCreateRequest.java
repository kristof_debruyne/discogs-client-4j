package com.discogs.client.api.request.marketplace;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.PayloadRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class OrderMessageCreateRequest extends AbstractRequest implements PayloadRequest<String> {

    private final String orderId;
    private final String message;
    private final String status;

    @Builder
    private OrderMessageCreateRequest(@Nullable AcceptMediaType mediaType,
                                      @Nonnull String contentType,
                                      @Nullable String callback,
                                      @Nonnull Authentication authentication,
                                      @Nonnull String orderId,
                                      @Nullable String message,
                                      @Nullable String status) {
        super(mediaType, contentType, callback, requireNonNull(authentication));
        this.orderId = requireNonNull(orderId);
        this.message = message;
        this.status = status;
    }

    @Nonnull
    @Override
    public String getPath() { return "/marketplace/orders/{orderId}/messages"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getOrderId() }; }

    @Nonnull
    @Override
    public String getBody() {
        return new JSONObject()
                .put("message", message)
                .put("status", status)
                .toString();
    }
}