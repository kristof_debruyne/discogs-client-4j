package com.discogs.client.api.core.user.collection;

import com.discogs.client.api.core.common.paging.PaginationAware;

/**
 * Discogs collection items interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface CollectionList extends PaginationAware {

    CollectionItem[] getItems();
}
