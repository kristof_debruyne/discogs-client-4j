package com.discogs.client.api;

import com.discogs.client.api.annotations.DiscogsClientInterface;
import com.discogs.client.api.annotations.DiscogsEndpoint;
import com.discogs.client.api.core.user.collection.CollectionFields;
import com.discogs.client.api.core.user.collection.CollectionFolder;
import com.discogs.client.api.core.user.collection.CollectionFolders;
import com.discogs.client.api.core.user.collection.CollectionInstance;
import com.discogs.client.api.core.user.collection.CollectionList;
import com.discogs.client.api.core.user.collection.CollectionValue;
import com.discogs.client.api.core.user.contribution.Contributions;
import com.discogs.client.api.core.user.identity.Identity;
import com.discogs.client.api.core.user.identity.Profile;
import com.discogs.client.api.core.user.list.List;
import com.discogs.client.api.core.user.list.UserLists;
import com.discogs.client.api.core.user.submission.Submissions;
import com.discogs.client.api.core.user.wantlist.WantList;
import com.discogs.client.api.core.user.wantlist.WantListItem;
import com.discogs.client.api.request.user.collection.CollectionFieldUpdateRequest;
import com.discogs.client.api.request.user.collection.CollectionFieldsRequest;
import com.discogs.client.api.request.user.collection.CollectionFolderCreateRequest;
import com.discogs.client.api.request.user.collection.CollectionFolderDeleteRequest;
import com.discogs.client.api.request.user.collection.CollectionFolderRequest;
import com.discogs.client.api.request.user.collection.CollectionFolderUpdateRequest;
import com.discogs.client.api.request.user.collection.CollectionFoldersRequest;
import com.discogs.client.api.request.user.collection.CollectionItemsByFolderRequest;
import com.discogs.client.api.request.user.collection.CollectionItemsByReleaseRequest;
import com.discogs.client.api.request.user.collection.CollectionReleaseAddRequest;
import com.discogs.client.api.request.user.collection.CollectionReleaseDeleteRequest;
import com.discogs.client.api.request.user.collection.CollectionValueRequest;
import com.discogs.client.api.request.user.contribution.ContributionsRequest;
import com.discogs.client.api.request.user.identity.IdentityRequest;
import com.discogs.client.api.request.user.identity.ProfileRequest;
import com.discogs.client.api.request.user.identity.ProfileUpdateRequest;
import com.discogs.client.api.request.user.list.ListRequest;
import com.discogs.client.api.request.user.list.UserListsRequest;
import com.discogs.client.api.request.user.submission.SubmissionsRequest;
import com.discogs.client.api.request.user.wantlist.WantListItemAddRequest;
import com.discogs.client.api.request.user.wantlist.WantListItemDeleteRequest;
import com.discogs.client.api.request.user.wantlist.WantListItemUpdateRequest;
import com.discogs.client.api.request.user.wantlist.WantListRequest;
import com.discogs.client.api.response.Response;
import com.discogs.client.auth.api.AuthenticationMode;
import lombok.NonNull;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Discogs user client interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see DiscogsClient
 */
@DiscogsClientInterface(
        domain = "user",
        description = "Client for managing user related stuff.",
        readOnly = false,
        throttling = true,
        links = {
            "https://www.discogs.com/developers#page:user-collection",
            "https://www.discogs.com/developers#page:user-identity",
            "https://www.discogs.com/developers#page:user-lists",
            "https://www.discogs.com/developers#page:user-wantlist"
        }
)
@ParametersAreNonnullByDefault
public interface DiscogsUserClient extends DiscogsClient {

    /**
     * Get collection folders by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.CONDITIONAL,
            description = "Get collection folders by user.",
            link = "https://www.discogs.com/developers#page:user-collection,header:user-collection-collection-get",
            requiredRequestParams = "username",
            cacheable = true
    )
    @Nonnull
    Response<? extends CollectionFolders> getAll(CollectionFoldersRequest request);

    /**
     * Get collection folder by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.CONDITIONAL,
            description = "Get collection folder.",
            link = "https://www.discogs.com/developers#page:user-collection,header:user-collection-collection-folder-get",
            requiredRequestParams = { "folderId", "username" },
            cacheable = true
    )
    @Nonnull
    Response<? extends CollectionFolder> get(CollectionFolderRequest request);

    /**
     * Create collection folder by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Create collection folder.",
            link = "https://www.discogs.com/developers#page:user-collection,header:user-collection-collection-post",
            requiredRequestParams = "username",
            cacheable = false
    )
    @Nonnull
    Response<? extends CollectionFolder> create(CollectionFolderCreateRequest request);

    /**
     * Update collection folder by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Update collection folder.",
            link = "https://www.discogs.com/developers#page:user-collection,header:user-collection-collection-folder-post",
            requiredRequestParams = { "folderId", "username" },
            cacheable = false
    )
    @Nonnull
    Response<? extends CollectionFolder> modify(CollectionFolderUpdateRequest request);

    /**
     * Delete collection folder by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Delete collection folder.",
            link = "https://www.discogs.com/developers#page:user-collection,header:user-collection-collection-folder-delete",
            requiredRequestParams = { "folderId", "username" },
            cacheable = false
    )
    @Nonnull
    Response<Void> delete(CollectionFolderDeleteRequest request);

    /**
     * Add release to collection folder by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Add release to collection folder.",
            link = "https://www.discogs.com/developers#page:user-collection,header:user-collection-add-to-collection-folder",
            requiredRequestParams = { "folderId", "releaseId", "username" },
            cacheable = false
    )
    @Nonnull
    Response<? extends CollectionInstance> create(CollectionReleaseAddRequest request);

    /**
     * Add release to collection folder by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Delete release from collection folder.",
            link = "https://www.discogs.com/developers#page:user-collection,header:user-collection-delete-instance-from-folder",
            requiredRequestParams = { "folderId", "instanceId", "releaseId", "username" },
            cacheable = false
    )
    @Nonnull
    Response<Void> delete(CollectionReleaseDeleteRequest request);

    /**
     * Get collection custom fields by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.CONDITIONAL,
            description = "Get collection custom fields by user.",
            link = "https://www.discogs.com/developers#page:user-collection,header:user-collection-list-custom-fields",
            requiredRequestParams = "username",
            cacheable = true
    )
    @Nonnull
    Response<? extends CollectionFields> getAll(CollectionFieldsRequest request);

    /**
     * Update collection custom fields by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.CONDITIONAL,
            description = "Update collection custom fields by id.",
            link = "https://www.discogs.com/developers#page:user-collection,header:user-collection-edit-fields-instance",
            requiredRequestParams = { "folderId", "fieldId", "instanceId", "releaseId", "username" },
            cacheable = false
    )
    @Nonnull
    Response<Void> modify(@NonNull final CollectionFieldUpdateRequest request);

    /**
     * Create collection items by request, based on folder id.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.CONDITIONAL,
            description = "Get collection items by folder id.",
            link = "https://www.discogs.com/developers#page:user-collection,header:user-collection-collection-items-by-folder",
            requiredRequestParams = { "folderId", "username" },
            cacheable = true,
            allowPagination = true,
            allowSorting = true
    )
    @Nonnull
    Response<? extends CollectionList> getAll(CollectionItemsByFolderRequest request);

    /**
     * Create collection items by request, based on release id.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.CONDITIONAL,
            description = "Get collection items by release id.",
            link = "https://www.discogs.com/developers#page:user-collection,header:user-collection-collection-items-by-release",
            requiredRequestParams = { "releaseId", "username" },
            cacheable = true,
            allowPagination = true
    )
    @Nonnull
    Response<? extends CollectionList> getAll(CollectionItemsByReleaseRequest request);

    /**
     * Get collection value by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Get collection value by user.",
            link = "https://www.discogs.com/developers#page:user-collection,header:user-collection-collection-value",
            requiredRequestParams = "username",
            cacheable = true
    )
    @Nonnull
    Response<? extends CollectionValue> get(CollectionValueRequest request);

    /**
     * Get identity by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Get identity from logged in user.",
            link = "https://www.discogs.com/developers#page:user-identity,header:user-identity-identity",
            cacheable = true
    )
    @Nonnull
    Response<? extends Identity> get(IdentityRequest request);

    /**
     * Get profile by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.OPTIONAL,
            description = "Get profile by user.",
            link = "https://www.discogs.com/developers#page:user-identity,header:user-identity-profile-get",
            requiredRequestParams = "username",
            cacheable = true
    )
    @Nonnull
    Response<? extends Profile> get(ProfileRequest request);

    /**
     * Update profile by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Update profile for user.",
            link = "https://www.discogs.com/developers#page:user-identity,header:user-identity-profile-post",
            requiredRequestParams = "username",
            cacheable = false
    )
    @Nonnull
    Response<? extends Profile> modify(ProfileUpdateRequest request);

    /**
     * Get list by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.CONDITIONAL,
            description = "Get list by id.",
            link = "https://www.discogs.com/developers#page:user-lists,header:user-lists-list",
            requiredRequestParams = "listId",
            cacheable = true
    )
    @Nonnull
    Response<? extends List> get(ListRequest request);

    /**
     * Get user lists by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.OPTIONAL,
            description = "Get lists by user.",
            link = "https://www.discogs.com/developers#page:user-lists,header:user-lists-user-lists",
            requiredRequestParams = "username",
            cacheable = true,
            allowPagination = true
    )
    @Nonnull
    Response<? extends UserLists> getAll(UserListsRequest request);

    /**
     * Get user contributions by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.NOT_REQUIRED,
            description = "Get contributions by user.",
            link = "https://www.discogs.com/developers#page:user-identity,header:user-identity-user-contributions",
            requiredRequestParams = "username",
            cacheable = true,
            allowPagination = true,
            allowSorting = true
    )
    @Nonnull
    Response<? extends Contributions> getAll(ContributionsRequest request);

    /**
     * Get user submissions by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.NOT_REQUIRED,
            description = "Get submissions by user.",
            link = "https://www.discogs.com/developers#page:user-identity,header:user-identity-user-submissions",
            requiredRequestParams = "username",
            cacheable = true,
            allowPagination = true
    )
    @Nonnull
    Response<? extends Submissions> getAll(SubmissionsRequest request);

    /**
     * Get want list by request. <br><br>
     *
     * - Public want list: no authentication required <br>
     * - Private want list: authentication required (owner)<br>
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.CONDITIONAL,
            description = "Get want list by user.",
            link = "https://www.discogs.com/developers#page:user-wantlist,header:user-wantlist-wantlist",
            requiredRequestParams = "username",
            cacheable = true,
            allowPagination = true
    )
    @Nonnull
    Response<? extends WantList> get(WantListRequest request);

    /**
     * Add release to want list by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Add release to want list.",
            link = "https://www.discogs.com/developers#page:user-wantlist,header:user-wantlist-add-to-wantlist",
            requiredRequestParams = { "releaseId", "username" },
            cacheable = false
    )
    @Nonnull
    Response<? extends WantListItem> create(WantListItemAddRequest request);

    /**
     * Update release from want list by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Update release from want list.",
            link = "https://www.discogs.com/developers#page:user-wantlist,header:user-wantlist-add-to-wantlist-post",
            requiredRequestParams = { "releaseId", "username" },
            cacheable = false
    )
    @Nonnull
    Response<? extends WantListItem> modify(WantListItemUpdateRequest request);

    /**
     * Delete release from want list by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Delete release from want list.",
            link = "https://www.discogs.com/developers#page:user-wantlist,header:user-wantlist-add-to-wantlist-delete",
            requiredRequestParams = { "releaseId", "username" },
            cacheable = false
    )
    @Nonnull
    Response<Void> delete(WantListItemDeleteRequest request);
}
