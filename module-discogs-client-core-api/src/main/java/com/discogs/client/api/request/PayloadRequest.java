package com.discogs.client.api.request;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Payload request interface. <br><br>
 *
 * @author Sikke303
 * @since 1.0
 * @param <T> target type
 */
@ParametersAreNonnullByDefault
public interface PayloadRequest<T> extends Request {

    /**
     * Extracts body from request into another type.
     *
     * @return target type (never null)
     */
    @Nonnull
    T getBody();
}