package com.discogs.client.api.request.user.collection;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class CollectionReleaseDeleteRequest extends AbstractRequest {

    private final long folderId;
    private final long releaseId;
    private final long instanceId;
    private final String username;

    @Builder
    private CollectionReleaseDeleteRequest(@Nullable AcceptMediaType mediaType,
                                           @Nonnull String contentType,
                                           @Nullable String callback,
                                           @Nonnull Authentication authentication,
                                           long folderId,
                                           long releaseId,
                                           long instanceId,
                                           @Nonnull String username) {
        super(mediaType, contentType, callback, requireNonNull(authentication));
        this.folderId = requireValidIdentifier(folderId, true);
        this.releaseId = requirePositiveIdentifier(releaseId);
        this.instanceId = requirePositiveIdentifier(instanceId);
        this.username = requireNonNull(username);
    }

    @Nonnull
    @Override
    public String getPath() { return "/users/{username}/collection/folders/{folderId}/releases/{releaseId}/instances/{instanceId}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getUsername(), getFolderId(), getReleaseId(), getInstanceId() }; }
}
