package com.discogs.client.api.request;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.util.Base64;

/**
 * Multipart request interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface MultipartRequest extends Request {

    /**
     * Gets content length.
     *
     * @return content length (never null)
     */
    @Nonnull
    default String getContentLength() {
        return String.valueOf(ArrayUtils.getLength(getPayload()));
    }

    /**
     * Gets content type.
     *
     * @return content type (never null)
     */
    @Nonnull
    String getContentType();

    /**
     * Gets filename.
     *
     * @return filename (never null)
     */
    @Nonnull
    String getFilename();

    /**
     * Gets raw payload (byte array).
     *
     * @return payload (never null)
     */
    @Nonnull
    byte[] getPayload();

    /**
     * Gets encoded payload (string).
     *
     * @return payload (never null)
     */
    @Nonnull
    default String getPayloadAsString() {
        byte[] payload = getPayload();
        if(ArrayUtils.isNotEmpty(payload)) {
            return Base64.getEncoder().encodeToString(payload);
        }
        return StringUtils.EMPTY;
    }
}