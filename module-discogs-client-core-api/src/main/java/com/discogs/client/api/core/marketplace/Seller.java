package com.discogs.client.api.core.marketplace;

/**
 * Discogs seller interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Seller {

    long getId();

    String getAvatarUrl();

    String getUsername();

    String getPaymentMethod();

    String getResourceUrl();

    SellerStatistics getStatistics();

    String getShippingExtraInformation();

    String getStatus();

    String getUrl();

    /**
     * Discogs seller statistics interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface SellerStatistics {

        double getRatingStars();

        String getRatingValue();

        int getTotal();
    }
}