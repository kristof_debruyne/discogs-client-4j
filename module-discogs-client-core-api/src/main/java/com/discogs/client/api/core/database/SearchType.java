package com.discogs.client.api.core.database;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;

/**
 * Supported search types.
 *
 * @author Sikke303
 * @since 1.0
 */
@RequiredArgsConstructor
public enum SearchType {

    ALL("All", "all"),
    ARTIST("Artists", "artist"),
    LABEL("Labels", "label"),
    MASTER_RELEASE("Masters", "master"),
    RELEASE("Releases", "release");

    @Getter
    private final String label;

    @Getter
    private final String value;

    /**
     * Resolves search type from label.
     *
     * @param label label (optional)
     * @return type or null
     * @throws IllegalArgumentException if type couldn't be resolved
     */
    @Nonnull
    public static SearchType resolveTypeFromLabel(String label) {
        for(SearchType type : values()) {
            if(type.getLabel().equalsIgnoreCase(label)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Search type couldn't be resolved: " + label);
    }

    /**
     * Resolves search type from value.
     *
     * @param value value (optional)
     * @return type or null
     * @throws IllegalArgumentException if type couldn't be resolved
     */
    @Nonnull
    public static SearchType resolveTypeFromValue(String value) {
        for(SearchType type : values()) {
            if(type.getValue().equalsIgnoreCase(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Search type couldn't be resolved: " + value);
    }
}
