package com.discogs.client.api.core.user.list;

import com.discogs.client.api.core.common.stats.Statistics;
import com.discogs.client.api.core.user.User;

/**
 * Discogs list interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface List {

    long getId();

    boolean isPublicList();

    String getDateAdded();

    String getDateChanged();

    String getDescription();

    String getImageUrl();

    ListItem[] getItems();

    String getName();

    String getResourceUrl();

    User getUser();

    String getUri();

    /**
     * Discogs list item interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface ListItem {

        long getId();

        String getComment();

        String getDisplayTitle();

        String getImageUrl();

        String getResourceUrl();

        Statistics getStatistics();

        String getType();

        String getUri();
    }
}