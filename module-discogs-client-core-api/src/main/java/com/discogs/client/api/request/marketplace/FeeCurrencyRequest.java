package com.discogs.client.api.request.marketplace;

import com.discogs.client.api.core.common.Currency;
import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class FeeCurrencyRequest extends AbstractRequest {

    private final double price;
    private final Currency currency;

    @Builder
    private FeeCurrencyRequest(@Nullable AcceptMediaType mediaType,
                               @Nonnull String contentType,
                               @Nullable String callback,
                               double price,
                               @Nonnull Currency currency) {
        super(mediaType, contentType, callback);
        this.price = requireValidPrice(price);
        this.currency = requireNonNull(currency);
    }

    @Nonnull
    @Override
    public String getPath() { return "/marketplace/fee/{price}/{currency}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getPrice(), getCurrency() }; }
}