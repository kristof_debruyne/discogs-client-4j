package com.discogs.client.api.request.database;

import com.discogs.client.api.core.common.Currency;
import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

import static com.discogs.client.api.common.DiscogsConstants.QUERY_PARAM_CURRENCY;
import static java.util.Collections.unmodifiableMap;

@Getter
public class ReleaseRequest extends AbstractRequest {

    private final long releaseId;
    private final Currency currency;

    @Builder
    private ReleaseRequest(@Nullable AcceptMediaType mediaType,
                           @Nullable Authentication authentication,
                           @Nonnull String contentType,
                           @Nullable String callback,
                           long releaseId,
                           @Nullable Currency currency) {
        super(mediaType, contentType, callback, authentication);
        this.releaseId = requirePositiveIdentifier(releaseId);
        this.currency = currency;
    }

    @Nonnull
    @Override
    public String getPath() { return "/releases/{releaseId}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getReleaseId() }; }

    @Nonnull
    @Override
    public Map<String, Object> getQueryParams() {
        final Map<String, Object> params = new HashMap<>(super.getQueryParams());
        params.computeIfAbsent(QUERY_PARAM_CURRENCY, key -> currency);
        return unmodifiableMap(params);
    }
}