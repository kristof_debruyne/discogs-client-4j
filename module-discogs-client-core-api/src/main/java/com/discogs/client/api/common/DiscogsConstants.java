package com.discogs.client.api.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Discogs client constants. <br><br>
 *
 * You can extend from this class, but when creating instance of it <br>
 * an {@link AssertionError} will be thrown. This class should only be <br>
 * invoked in a static manner.
 *
 * @author Sikke303
 * @since 1.0
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DiscogsConstants {

    /**
     * Authenticated request are capped at 60 request per minute.
     */
    public static final int AUTHENTICATED_REQUESTS_PER_MINUTE = 60;

    /**
     * Unauthenticated request are capped at 25 request per minute.
     */
    public static final int UNAUTHENTICATED_REQUESTS_PER_MINUTE = 25;

    /**
     * Requests are track in a (moving) window of 1 minute.
     */
    public static final int REQUEST_TIME_WINDOW_IN_MILLIS = 60 * 1000;

    public static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";
    public static final String CONTENT_TYPE_IMAGE_JPEG = "image/jpeg";
    public static final String CONTENT_TYPE_IMAGE_PNG = "image/png";
    public static final String CONTENT_TYPE_TEXT_CSV = "text/csv";
    public static final String CONTENT_TYPE_TEXT_JAVASCRIPT = "text/javascript";

    /**
     * First page during pagination (attribute 'page').
     */
    public static final int FIRST_PAGE_NUMBER = 1;

    /**
     * Minimum items per page during pagination (attribute 'per_page').
     */
    public static final int MINIMUM_PER_PAGE = 10;

    /**
     * Maximum items per page during pagination (attribute 'per_page').
     */
    public static final int MAXIMUM_PER_PAGE = 100;

    /**
     * Callback query param: callback (JSONP)
     */
    public static final String QUERY_PARAM_CALLBACK = "callback";

    /**
     * Currency query param: curr_abbr
     */
    public static final String QUERY_PARAM_CURRENCY = "curr_abbr";

    /**
     * Pagination query params: page / per_page
     */
    public static final String QUERY_PARAM_PAGE = "page";
    public static final String QUERY_PARAM_PER_PAGE = "per_page";

    /**
     * Sorting query params: sort / sort_order
     */
    public static final String QUERY_PARAM_SORT = "sort";
    public static final String QUERY_PARAM_SORT_ORDER = "sort_order";
}