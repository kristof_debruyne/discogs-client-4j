package com.discogs.client.api.core.common;

/**
 * Discogs company interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Company {

    long getId();

    String getCatalogNumber();

    String getEntityType();

    String getEntityTypeName();

    String getName();

    String getResourceUrl();
}