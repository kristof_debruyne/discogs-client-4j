package com.discogs.client.api.request.database;

import com.discogs.client.api.core.common.rating.RatingValue;
import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.PayloadRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class ReleaseUserRatingUpdateRequest extends AbstractRequest implements PayloadRequest<String> {

    private final long releaseId;
    private final String username;
    private final RatingValue rating;

    @Builder
    private ReleaseUserRatingUpdateRequest(@Nullable AcceptMediaType mediaType,
                                           @Nonnull String contentType,
                                           @Nullable String callback,
                                           @Nonnull Authentication authentication,
                                           long releaseId,
                                           @Nonnull String username,
                                           @Nonnull RatingValue rating) {
        super(mediaType, contentType, callback, requireNonNull(authentication));
        this.releaseId = requirePositiveIdentifier(releaseId);
        this.username = requireNonNull(username);
        this.rating = requireNonNull(rating);
    }

    @Nonnull
    @Override
    public String getPath() { return "/releases/{releaseId}/rating/{username}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getReleaseId(), getUsername() }; }

    @Nonnull
    @Override
    public String getBody() {
        return new JSONObject()
                .put("rating", rating.getValue())
                .toString();
    }
}