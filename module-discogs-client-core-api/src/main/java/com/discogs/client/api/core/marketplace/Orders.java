package com.discogs.client.api.core.marketplace;

import com.discogs.client.api.core.common.paging.PaginationAware;

/**
 * Discogs orders interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Orders extends PaginationAware {

    Order[] getOrders();
}