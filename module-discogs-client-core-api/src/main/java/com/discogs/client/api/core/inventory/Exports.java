package com.discogs.client.api.core.inventory;

import com.discogs.client.api.core.common.paging.PaginationAware;

/**
 * Discogs inventory exports interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Exports extends PaginationAware {

    Export[] getItems();
}