package com.discogs.client.api;

import com.discogs.client.api.annotations.DiscogsClientInterface;
import com.discogs.client.api.annotations.DiscogsEndpoint;
import com.discogs.client.api.core.database.Artist;
import com.discogs.client.api.core.database.ArtistReleases;
import com.discogs.client.api.core.database.Label;
import com.discogs.client.api.core.database.LabelReleases;
import com.discogs.client.api.core.database.MasterRelease;
import com.discogs.client.api.core.database.MasterVersions;
import com.discogs.client.api.core.database.Release;
import com.discogs.client.api.core.database.ReleaseCommunityRating;
import com.discogs.client.api.core.database.ReleaseUserRating;
import com.discogs.client.api.core.database.SearchResult;
import com.discogs.client.api.request.database.ArtistReleasesRequest;
import com.discogs.client.api.request.database.ArtistRequest;
import com.discogs.client.api.request.database.LabelReleasesRequest;
import com.discogs.client.api.request.database.LabelRequest;
import com.discogs.client.api.request.database.MasterReleaseRequest;
import com.discogs.client.api.request.database.MasterVersionsRequest;
import com.discogs.client.api.request.database.ReleaseCommunityRatingRequest;
import com.discogs.client.api.request.database.ReleaseRequest;
import com.discogs.client.api.request.database.ReleaseUserRatingDeleteRequest;
import com.discogs.client.api.request.database.ReleaseUserRatingRequest;
import com.discogs.client.api.request.database.ReleaseUserRatingUpdateRequest;
import com.discogs.client.api.request.database.SearchRequest;
import com.discogs.client.api.response.Response;
import com.discogs.client.auth.api.AuthenticationMode;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Discogs database client interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see DiscogsClient
 */
@DiscogsClientInterface(
        domain = "database",
        description = "Client for querying database.",
        readOnly = false,
        throttling = true,
        links = { "https://www.discogs.com/developers#page:database" }
)
@ParametersAreNonnullByDefault
public interface DiscogsDatabaseClient extends DiscogsClient {

    /**
     * Get artist by request. <br><br>
     *
     * Image URL's will only be visible if you are authenticated !
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.OPTIONAL,
            description = "Get artist by id.",
            link = "https://www.discogs.com/developers#page:database,header:database-artist",
            requiredRequestParams = "artistId",
            cacheable = true
    )
    @Nonnull
    Response<? extends Artist> get(ArtistRequest request);

    /**
     * Get artist releases by request. <br><br>
     *
     * Image URL's will only be visible if you are authenticated !
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.OPTIONAL,
            description = "Get artist releases by id.",
            link = "https://www.discogs.com/developers#page:database,header:database-artist-releases",
            requiredRequestParams = "artistId",
            cacheable = true,
            allowPagination = true,
            allowSorting = true
    )
    @Nonnull
    Response<? extends ArtistReleases> getAll(ArtistReleasesRequest request);

    /**
     * Get label by request. <br><br>
     *
     * Image URL's will only be visible if you are authenticated !
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.OPTIONAL,
            description = "Get label by id.",
            link = "https://www.discogs.com/developers#page:database,header:database-label",
            requiredRequestParams = "labelId",
            cacheable = true
    )
    @Nonnull
    Response<? extends Label> get(LabelRequest request);

    /**
     * Get label releases by request. <br><br>
     *
     * Image URL's will only be visible if you are authenticated !
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.OPTIONAL,
            description = "Get label releases by id.",
            link = "https://www.discogs.com/developers#page:database,header:database-all-label-releases",
            requiredRequestParams = "labelId",
            cacheable = true,
            allowPagination = true
    )
    @Nonnull
    Response<? extends LabelReleases> getAll(LabelReleasesRequest request);

    /**
     * Get master release by request. <br><br>
     *
     * Image URL's will only be visible if you are authenticated !
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.OPTIONAL,
            description = "Get master release by id.",
            link = "https://www.discogs.com/developers#page:database,header:database-master-release",
            requiredRequestParams = "masterId",
            cacheable = true
    )
    @Nonnull
    Response<? extends MasterRelease> get(MasterReleaseRequest request);

    /**
     * Get master versions by request.<br><br>
     *
     * Image URL's will only be visible if you are authenticated !
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.OPTIONAL,
            description = "Get master versions by id.",
            link = "https://www.discogs.com/developers#page:database,header:database-master-release-versions",
            requiredRequestParams = "masterId",
            cacheable = true,
            allowPagination = true,
            allowSorting = true
    )
    @Nonnull
    Response<? extends MasterVersions> getAll(MasterVersionsRequest request);

    /**
     * Get release by request. <br><br>
     *
     * Image URL's will only be visible if you are authenticated !
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.OPTIONAL,
            description = "Get release by id.",
            link = "https://www.discogs.com/developers#page:database,header:database-release",
            requiredRequestParams = "releaseId",
            cacheable = true
    )
    @Nonnull
    Response<? extends Release> get(ReleaseRequest request);

    /**
     * Get release community rating by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.NOT_REQUIRED,
            description = "Get release community rating.",
            link = "https://www.discogs.com/developers#page:database,header:database-community-release-rating",
            requiredRequestParams = "releaseId",
            cacheable = true
    )
    @Nonnull
    Response<? extends ReleaseCommunityRating> get(ReleaseCommunityRatingRequest request);

    /**
     * Get release user rating by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.NOT_REQUIRED,
            description = "Get release user rating.",
            link = "https://www.discogs.com/developers#page:database,header:database-release-rating-by-user-get",
            requiredRequestParams = { "releaseId", "username" },
            cacheable = true
    )
    @Nonnull
    Response<? extends ReleaseUserRating> get(ReleaseUserRatingRequest request);

    /**
     * Update release user rating by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Update release user rating.",
            link = "https://www.discogs.com/developers#page:database,header:database-release-rating-by-user-put",
            requiredRequestParams = { "releaseId", "username" },
            cacheable = false
    )
    @Nonnull
    Response<? extends ReleaseUserRating> modify(ReleaseUserRatingUpdateRequest request);

    /**
     * Delete release user rating by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Delete release user rating.",
            link = "https://www.discogs.com/developers#page:database,header:database-release-rating-by-user-delete",
            requiredRequestParams = { "releaseId", "username" },
            cacheable = false
    )
    @Nonnull
    Response<Void> delete(ReleaseUserRatingDeleteRequest request);

    /**
     * Searches on Discogs database by request.
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.REQUIRED,
            description = "Search on Discogs database.",
            link = "https://www.discogs.com/developers#page:database,header:database-search",
            requiredRequestParams = "query",
            cacheable = false,
            allowPagination = true
    )
    @Nonnull
    Response<? extends SearchResult> search(SearchRequest request);
}