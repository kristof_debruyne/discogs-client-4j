package com.discogs.client.api.core.common;

import com.discogs.client.api.core.common.rating.Rating;

/**
 * Discogs community interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Community {

    Contributor[] getContributors();

    String getDataQuality();

    Integer getHaveCount();

    Rating getRating();

    String getStatus();

    Submitter getSubmitter();

    Integer getWantCount();
}