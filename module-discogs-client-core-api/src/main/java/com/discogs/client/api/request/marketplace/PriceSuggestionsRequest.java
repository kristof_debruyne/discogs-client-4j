package com.discogs.client.api.request.marketplace;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class PriceSuggestionsRequest extends AbstractRequest {

    private final long releaseId;

    @Builder
    private PriceSuggestionsRequest(@Nullable AcceptMediaType mediaType,
                                    @Nonnull String contentType,
                                    @Nullable String callback,
                                    @Nonnull Authentication authentication,
                                    long releaseId) {
        super(mediaType, contentType, callback, requireNonNull(authentication));
        this.releaseId = requirePositiveIdentifier(releaseId);
    }

    @Nonnull
    @Override
    public String getPath() { return "/marketplace/price_suggestions/{releaseId}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getReleaseId() }; }
}