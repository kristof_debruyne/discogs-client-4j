package com.discogs.client.api.core.image;

/**
 * Discogs image interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Image {

    int getHeight();

    int getWidth();

    String getResourceUrl();

    String getType();

    String getUri();

    String getUri150();
}