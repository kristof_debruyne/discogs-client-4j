package com.discogs.client.api.request.database;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Getter
public class LabelRequest extends AbstractRequest {

    private final long labelId;

    @Builder
    private LabelRequest(@Nullable AcceptMediaType mediaType,
                         @Nonnull String contentType,
                         @Nullable String callback,
                         @Nullable Authentication authentication,
                         long labelId) {
        super(mediaType, contentType, callback, authentication);
        this.labelId = requirePositiveIdentifier(labelId);
    }

    @Nonnull
    @Override
    public String getPath() { return "/labels/{labelId}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getLabelId() }; }
}