package com.discogs.client.api.core.database;

import com.discogs.client.api.core.common.Video;
import com.discogs.client.api.core.image.Image;

/**
 * Discogs master release interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface MasterRelease {

    long getId();

    Artist[] getArtists();

    String getDataQuality();

    String[] getGenres();

    Image[] getImages();

    Double getLowestPrice();

    long getMainReleaseId();

    String getMainReleaseUrl();

    int getNumberForSale();

    String getResourceUrl();

    String[] getStyles();

    String getTitle();

    Track[] getTrackList();

    String getUri();

    String getVersionsUrl();

    Video[] getVideos();

    int getYear();
}