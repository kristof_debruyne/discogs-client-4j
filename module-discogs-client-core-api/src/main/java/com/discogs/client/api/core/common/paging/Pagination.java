package com.discogs.client.api.core.common.paging;

/**
 * Discogs pagination interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Pagination {

    int getPage();

    int getPerPage();

    int getItemCount();

    int getPageCount();

    Navigation getNavigation();
}