package com.discogs.client.api.response;

import lombok.Getter;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Error response.
 *
 * @author Sikke303
 * @since 1.0
 * @param <T> body type
 * @see GenericResponse
 */
@Getter
public class ErrorResponse<T> extends GenericResponse<T> {

    private final Throwable error;

    /**
     * Constructor
     *
     * @param error error (required)
     */
    public ErrorResponse(@Nonnull final Throwable error) {
        super();
        this.error = requireNonNull(error);
    }
}