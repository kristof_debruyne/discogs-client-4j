package com.discogs.client.api.response;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import javax.annotation.Nonnegative;
import java.io.Serializable;

/**
 * Rate limit response.
 *
 * @author Sikke303
 * @since 1.0
 * @see RateLimit
 */
@Getter
@Builder
@ToString
public final class RateLimits implements Serializable {

    @Nonnegative private final int rateLimit;
    @Nonnegative private final int rateLimitRemaining;
    @Nonnegative private final int rateLimitUsed;
}
