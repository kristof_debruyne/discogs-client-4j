package com.discogs.client.api.annotations;

import com.discogs.client.auth.api.AuthenticationMode;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Discogs endpoint annotation.
 *
 * @author Sikke303
 * @since 1.0
 */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.METHOD)
public @interface DiscogsEndpoint {

    /**
     * Response cacheable or not.
     *
     * @return boolean cacheable (true) or not (false)
     */
    boolean cacheable();

    /**
     * Allows pagination.
     *
     * @return boolean pagination (true) or not (false)
     */
    boolean allowPagination() default false;

    /**
     * Allows sorting.
     *
     * @return boolean sorting (true) or not (false)
     */
    boolean allowSorting() default false;

    /**
     * Authentication mode.
     *
     * @return mode (required)
     */
    AuthenticationMode authenticationMode() default AuthenticationMode.NOT_APPLICABLE;

    /**
     * Description of the resource.
     *
     * @return description (required)
     */
    String description();

    /**
     * Discogs link of the resource.
     *
     * @return link (required)
     */
    String link();

    /**
     * Required request params.
     *
     * @return required request params, default empty array
     */
    String[] requiredRequestParams() default { };

}