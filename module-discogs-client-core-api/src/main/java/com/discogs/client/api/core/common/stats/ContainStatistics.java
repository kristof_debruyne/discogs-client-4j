package com.discogs.client.api.core.common.stats;

/**
 * Discogs contain statistic interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see CountStatistics
 */
public interface ContainStatistics {

    boolean isInCollection();

    boolean isInWantList();
}