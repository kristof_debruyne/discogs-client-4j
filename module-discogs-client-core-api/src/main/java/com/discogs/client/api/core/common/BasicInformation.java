package com.discogs.client.api.core.common;

import com.discogs.client.api.core.database.Artist;
import com.discogs.client.api.core.database.Label;

/**
 * Discogs basic information interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface BasicInformation  {

    long getId();

    Artist[] getArtists();

    String getCoverImage();

    Format[] getFormats();

    Label[] getLabels();

    String getResourceUrl();

    String getThumbnail();

    String getTitle();

    int getYear();
}