package com.discogs.client.api.core.user.identity;

/**
 * Discogs identity interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Identity {

    long getId();

    String getConsumerName();

    String getResourceUrl();

    String getUserName();
}