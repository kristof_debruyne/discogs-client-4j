package com.discogs.client.api.request.database;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Getter
public class ArtistRequest extends AbstractRequest {

    private final long artistId;

    @Builder
    private ArtistRequest(@Nullable AcceptMediaType mediaType,
                          @Nonnull String contentType,
                          @Nullable String callback,
                          @Nullable Authentication authentication,
                          long artistId) {
        super(mediaType, contentType, callback, authentication);
        this.artistId = requirePositiveIdentifier(artistId);
    }

    @Nonnull
    @Override
    public String getPath() { return "/artists/{artistId}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getArtistId() }; }
}