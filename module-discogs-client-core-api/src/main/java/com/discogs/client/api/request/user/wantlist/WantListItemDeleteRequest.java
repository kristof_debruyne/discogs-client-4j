package com.discogs.client.api.request.user.wantlist;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class WantListItemDeleteRequest extends AbstractRequest {

    private final long releaseId;
    private final String username;

    @Builder
    private WantListItemDeleteRequest(@Nullable AcceptMediaType mediaType,
                                      @Nonnull String contentType,
                                      @Nullable String callback,
                                      @Nonnull Authentication authentication,
                                      long releaseId,
                                      @Nonnull String username) {
        super(mediaType, contentType, callback, requireNonNull(authentication));
        this.releaseId = requirePositiveIdentifier(releaseId);
        this.username = requireNonNull(username);
    }

    @Nonnull
    @Override
    public String getPath() { return "/users/{username}/wants/{releaseId}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getUsername(), getReleaseId() }; }
}