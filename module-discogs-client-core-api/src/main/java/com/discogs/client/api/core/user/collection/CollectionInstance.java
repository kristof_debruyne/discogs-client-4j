package com.discogs.client.api.core.user.collection;

/**
 * Discogs collection instance interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface CollectionInstance {

    long getId();

    String getResourceUrl();
}