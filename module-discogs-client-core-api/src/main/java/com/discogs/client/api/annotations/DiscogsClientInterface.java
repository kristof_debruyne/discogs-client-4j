package com.discogs.client.api.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Discogs client interface.
 *
 * @author Sikke303
 * @since 1.0
 */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
public @interface DiscogsClientInterface {

    boolean readOnly();

    boolean throttling();

    /**
     * Name of the interface.
     *
     * @return name (required)
     */
    String domain();

    /**
     * Description of the interface.
     *
     * @return description (required)
     */
    String description();

    /**
     * Discogs links of the interface.
     *
     * @return links (required)
     */
    String[] links() default {};
}