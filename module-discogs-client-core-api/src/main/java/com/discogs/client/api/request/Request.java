package com.discogs.client.api.request;

import com.discogs.client.api.core.common.sorting.Sort;
import com.discogs.client.api.core.common.sorting.SortOrder;
import com.discogs.client.api.request.common.PaginationRequest;
import com.discogs.client.auth.api.Authentication;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Map;

import static org.apache.commons.lang3.ArrayUtils.EMPTY_OBJECT_ARRAY;

/**
 * Request interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Request {

    /**
     * Checks if request is a JSONP request or not.
     *
     * @return JSONP request (true) or not (false)
     */
    boolean isJsonpRequest();

    /**
     * Gets media type.
     *
     * @return media type (never null)
     */
    @Nonnull
    AcceptMediaType getMediaType();

    /**
     * Gets content type.
     *
     * @return content type (never null)
     */
    @Nonnull
    String getContentType();

    /**
     * Get JSONP callback name.
     *
     * Resource: https://www.discogs.com/developers#header-jsonp
     *
     * @return callback name (JSONP) or null (no JSONP)
     */
    @Nullable
    String getCallback();

    /**
     * Get request path.
     *
     * @return path (never null)
     */
    @Nonnull
    String getPath();

    /**
     * Get path variables.
     *
     * @return path variables or empty array (= default)
     */
    @Nonnull
    default Object[] getPathVariables() {
        return EMPTY_OBJECT_ARRAY;
    }

    /**
     * Gets query parameters map (if any).
     *
     * @return query parameter map (never null)
     */
    @Nonnull
    default Map<String, Object> getQueryParams() {
        return Collections.emptyMap();
    }

    /**
     * Gets authentication token.
     *
     * @return token (never null)
     */
    @Nonnull
    Authentication getAuthentication();

    /**
     * Gets pagination.
     *
     * @return pagination or null
     */
    @Nullable
    PaginationRequest getPagination();

    /**
     * Gets sort mode.
     *
     * @return sort mode or null
     */
    @Nullable
    Sort getSort();

    /**
     * Gets sort order.
     *
     * @return sort order or null
     */
    @Nullable
    SortOrder getSortOrder();
}