package com.discogs.client.api.core.user.list;

import com.discogs.client.api.core.common.paging.PaginationAware;

/**
 * Discogs user lists interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface UserLists extends PaginationAware {

    UserList[] getLists();

    /**
     * Discogs user list interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface UserList {

        long getId();

        boolean isPublicList();

        String getDateAdded();

        String getDateChanged();

        String getDescription();

        String getName();

        String getResourceUrl();

        String getUri();
    }
}