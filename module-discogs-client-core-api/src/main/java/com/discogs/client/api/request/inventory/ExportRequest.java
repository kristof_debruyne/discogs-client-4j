package com.discogs.client.api.request.inventory;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class ExportRequest extends AbstractRequest {

    private final long exportId;

    @Builder
    private ExportRequest(@Nullable AcceptMediaType mediaType,
                          @Nonnull String contentType,
                          @Nullable String callback,
                          @Nonnull Authentication authentication,
                          long exportId) {
        super(mediaType, contentType, callback, requireNonNull(authentication));
        this.exportId = requirePositiveIdentifier(exportId);
    }

    @Nonnull
    @Override
    public String getPath() { return "/inventory/export/{exportId}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getExportId() }; }
}