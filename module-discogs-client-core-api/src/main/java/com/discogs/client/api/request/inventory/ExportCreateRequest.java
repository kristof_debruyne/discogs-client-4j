package com.discogs.client.api.request.inventory;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class ExportCreateRequest extends AbstractRequest {

    @Builder
    private ExportCreateRequest(@Nonnull String contentType,
                                @Nullable String callback,
                                @Nonnull Authentication authentication) {
        super(AcceptMediaType.DEFAULT, contentType, callback, requireNonNull(authentication));
    }

    @Nonnull
    @Override
    public String getPath() { return "/inventory/export"; }
}