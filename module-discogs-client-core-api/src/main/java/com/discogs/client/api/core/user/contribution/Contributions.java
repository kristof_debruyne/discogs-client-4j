package com.discogs.client.api.core.user.contribution;

import com.discogs.client.api.core.common.Community;
import com.discogs.client.api.core.common.Company;
import com.discogs.client.api.core.common.Format;
import com.discogs.client.api.core.common.Video;
import com.discogs.client.api.core.common.paging.PaginationAware;
import com.discogs.client.api.core.database.Artist;
import com.discogs.client.api.core.database.Label;
import com.discogs.client.api.core.image.Image;

/**
 * Discogs contributions interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Contributions extends PaginationAware {

    Contribution[] getResults();

    /**
     * Discogs contribution interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface Contribution {

        long getId();

        Artist[] getArtists();

        Community getCommunity();

        Company[] getCompanies();

        String getCountry();

        String getDataQuality();

        String getDateAdded();

        String getDateChanged();

        int getEstimatedWeight();

        int getFormatQuantity();

        Format[] getFormats();

        String[] getGenres();

        Image[] getImages();

        Label[] getLabels();

        Long getMasterId();

        String getMasterUrl();

        String getNotes();

        String getReleased();

        String getReleasedFormatted();

        String getResourceUrl();

        String[] getSeries();

        String getStatus();

        String[] getStyles();

        String getThumbnail();

        String getTitle();

        String getUri();

        Video[] getVideos();

        Integer getYear();
    }
}