package com.discogs.client.api.core.user.collection;

import com.discogs.client.api.core.common.BasicInformation;

/**
 * Discogs collection item interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface CollectionItem {

    long getId();

    long getFolderId();

    long getInstanceId();

    int getRating();

    String getDateAdded();

    BasicInformation getBasicInformation();

    Note[] getNotes();

    /**
     * Discogs note interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface Note {

        int getFieldId();

        String getValue();
    }
}
