package com.discogs.client.api.request.marketplace;

import com.discogs.client.api.core.marketplace.Condition;
import com.discogs.client.api.core.marketplace.ListingStatus;
import com.discogs.client.api.core.marketplace.SleeveCondition;
import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

@Getter
public class ListingCreateRequest extends AbstractRequest {

    private final long releaseId;
    private final boolean allowOffers;
    private final double price;
    private final Condition condition;
    private final SleeveCondition sleeveCondition;
    private final String comment;
    private final String externalId;
    private final String location;
    private final String formatQuantity;
    private final ListingStatus status;
    private final String weight;

    @Builder
    private ListingCreateRequest(@Nullable AcceptMediaType mediaType,
                                 @Nonnull String contentType,
                                 @Nullable String callback,
                                 @Nonnull Authentication authentication,
                                 long releaseId,
                                 boolean allowOffers,
                                 double price,
                                 @Nonnull Condition condition,
                                 @Nonnull SleeveCondition sleeveCondition,
                                 @Nonnull ListingStatus status,
                                 @Nullable String comment,
                                 @Nullable String externalId,
                                 @Nullable String formatQuantity,
                                 @Nullable String location,
                                 @Nullable String weight) {
        super(mediaType, contentType, callback, requireNonNull(authentication));
        this.releaseId = requirePositiveIdentifier(releaseId);
        this.allowOffers = allowOffers;
        this.price = requireValidPrice(price);
        this.condition = requireNonNull(condition);
        this.sleeveCondition = requireNonNull(sleeveCondition);
        this.status = requireNonNull(status);
        this.comment = comment;
        this.externalId = externalId;
        this.location = location;
        this.formatQuantity = formatQuantity;
        this.weight = weight;
    }

    @Nonnull
    @Override
    public String getPath() { return "/marketplace/listings"; }

    @Nonnull
    @Override
    public Map<String, Object> getQueryParams() {
        final Map<String, Object> params = new HashMap<>(super.getQueryParams());
        params.put("release_id", releaseId);
        params.put("allow_offers", allowOffers);
        params.put("price", price);
        params.put("condition", condition.getValue());
        params.put("sleeve_condition", sleeveCondition.getValue());
        params.put("status", status.getValue());
        params.computeIfAbsent("comment", key -> comment);
        params.computeIfAbsent("external_id", key -> externalId);
        params.computeIfAbsent("format_quantity", key -> isNull(formatQuantity) ? "auto" : formatQuantity);
        params.computeIfAbsent("location", key -> location);
        params.computeIfAbsent("weight", key -> isNull(weight) ? "auto" : weight);
        return unmodifiableMap(params);
    }
}