package com.discogs.client.api.core.database;

import com.discogs.client.api.core.common.rating.Rating;

/**
 * Discogs community rating interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface ReleaseCommunityRating {

    long getReleaseId();

    Rating getRating();
}