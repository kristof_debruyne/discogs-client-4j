package com.discogs.client.api.core.inventory;

import com.discogs.client.api.core.common.paging.PaginationAware;

/**
 * Discogs inventory uploads interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Uploads extends PaginationAware {

    Upload[] getItems();
}