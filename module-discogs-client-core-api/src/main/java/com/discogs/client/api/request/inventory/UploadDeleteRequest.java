package com.discogs.client.api.request.inventory;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.MultipartRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.requireNonNull;

@Getter
public class UploadDeleteRequest extends AbstractRequest implements MultipartRequest {

    private final String filename;
    private final byte[] payload;

    @Builder
    private UploadDeleteRequest(@Nonnull String contentType,
                                @Nullable String callback,
                                @Nonnull Authentication authentication,
                                @Nonnull String filename,
                                @Nonnull byte[] payload) {
        super(AcceptMediaType.DEFAULT, contentType, callback, requireNonNull(authentication));
        this.filename = requireNonNull(filename);
        this.payload = requireNonNull(payload);
    }

    @Nonnull
    @Override
    public String getPath() { return "/inventory/upload/delete"; }

    @Nonnull
    @Override
    public Map<String, Object> getQueryParams() {
        final Map<String, Object> params = new HashMap<>(super.getQueryParams());
        params.put("file", filename);
        return unmodifiableMap(params);
    }
}