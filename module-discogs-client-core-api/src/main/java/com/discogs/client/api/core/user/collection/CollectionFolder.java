package com.discogs.client.api.core.user.collection;

/**
 * Discogs collection folder interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface CollectionFolder {

    long getId();

    long getCount();

    String getName();

    String getResourceUrl();
}
