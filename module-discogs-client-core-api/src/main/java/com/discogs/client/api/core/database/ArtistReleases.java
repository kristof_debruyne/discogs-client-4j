package com.discogs.client.api.core.database;

import com.discogs.client.api.core.common.paging.PaginationAware;

/**
 * Discogs artist releases interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface ArtistReleases extends PaginationAware {

    ArtistRelease[] getReleases();

    /**
     * Discogs artist release interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface ArtistRelease {

        long getId();

        String getArtistName();

        String getLabelName();

        String getFormat();

        long getMainReleaseId();

        String getResourceUrl();

        String getRole();

        String getStatus();

        String getTitle();

        String getThumbnail();

        String getType();

        int getYear();
    }
}