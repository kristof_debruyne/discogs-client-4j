package com.discogs.client.api.request.user.list;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Getter
public class ListRequest extends AbstractRequest {

    private final long listId;

    @Builder
    private ListRequest(@Nullable AcceptMediaType mediaType,
                        @Nonnull String contentType,
                        @Nullable String callback,
                        @Nullable Authentication authentication,
                        long listId) {
        super(mediaType, contentType, callback, authentication);
        this.listId = requirePositiveIdentifier(listId);
    }

    @Nonnull
    @Override
    public String getPath() { return "/lists/{listId}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getListId() }; }
}