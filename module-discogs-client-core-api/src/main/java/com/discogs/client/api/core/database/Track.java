package com.discogs.client.api.core.database;

/**
 * Discogs track interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Track {

    String getDuration();

    Artist[] getExtraArtists();

    String getPosition();

    String getTitle();

    String getType();
}