package com.discogs.client.api.core.inventory;

/**
 * Discogs inventory upload interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Upload {

    long getId();

    String getDateCreated();

    String getDateFinished();

    String getFilename();

    String getResults();

    String getStatus();

    String getType();
}