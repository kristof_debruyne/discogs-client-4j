package com.discogs.client.api.core.user.identity;

import com.discogs.client.api.core.common.Currency;

/**
 * Discogs profile interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Profile {

    long getId();

    String getAvatarUrl();

    String getBannerUrl();

    int getBuyerNumberRating();

    double getBuyerRating();

    int getBuyerRatingStars();

    String getCollectionFieldsUrl();

    String getCollectionFolderUrl();

    Currency getCurrency();

    String getEmail();

    String getHomePage();

    String getInventoryUrl();

    String getLocation();

    String getName();

    int getNumberCollection();

    int getNumberForSale();

    int getNumberLists();

    int getNumberPending();

    int getNumberWantList();

    String getProfile();

    int getRank();

    double getRatingAverage();

    String getRegistered();

    int getReleasesContributed();

    int getReleasesRated();

    String getResourceUrl();

    int getSellerNumberRating();

    double getSellerRating();

    int getSellerRatingStars();

    String getUserName();

    String getUri();

    String getWantListUrl();
}