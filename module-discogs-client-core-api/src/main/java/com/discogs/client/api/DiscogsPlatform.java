package com.discogs.client.api;

import com.discogs.client.api.config.DiscogsContext;
import com.discogs.client.auth.api.AuthenticationContext;
import com.discogs.client.auth.api.AuthenticationService;
import lombok.NonNull;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Discogs platform interface.
 *
 * This class is the main bean to interact with Discogs APU.
 * From here, you can lookup clients and services, get Discogs context, etc ...
 *
 * @author Sikke303
 * @since 1.0
 * @see DiscogsClient
 * @see DiscogsService
 */
@ParametersAreNonnullByDefault
public interface DiscogsPlatform {

    /**
     * Gets Discogs context.
     *
     * @return context (never null)
     */
    @Nonnull
    DiscogsContext<? extends AuthenticationContext> getDiscogsContext();

    /**
     * Gets database client.
     *
     * @return client (never null)
     */
    @Nonnull
    DiscogsDatabaseClient getDatabaseClient();

    /**
     * Gets image client.
     *
     * @return client (never null)
     */
    @Nonnull
    DiscogsImageClient getImageClient();

    /**
     * Gets inventory client.
     *
     * @return client (never null)
     */
    @Nonnull
    DiscogsInventoryClient getInventoryClient();

    /**
     * Gets JSONP client.
     *
     * @return client (never null)
     */
    @Nonnull
    DiscogsJsonpClient getJsonpClient();

    /**
     * Gets marketplace client.
     *
     * @return client (never null)
     */
    @Nonnull
    DiscogsMarketplaceClient getMarketplaceClient();

    /**
     * Gets root client.
     *
     * @return client (never null)
     */
    @Nonnull
    DiscogsRootClient getRootClient();

    /**
     * Gets user client.
     *
     * @return client (never null)
     */
    @Nonnull
    DiscogsUserClient getUserClient();

    /**
     * Gets web client.
     *
     * @return client (never null)
     */
    @Nonnull
    DiscogsWebClient getWebClient();

    /**
     * Get client by class name.
     *
     * @param clientType class name (required)
     * @param <C> client type
     * @return client instance (never null)
     */
    @Nonnull
    <C extends DiscogsClient> C getClient(@NonNull Class<C> clientType);

    /**
     * Gets authentication service.
     *
     * @return authentication service (never null)
     */
    @Nonnull
    <A extends AuthenticationService> A getAuthenticationService();

    /**
     * Get service by class name.
     *
     * @param serviceType class name (required)
     * @param <S> service type
     * @return service instance (never null)
     */
    @Nonnull
    <S extends DiscogsService> S getService(@NonNull Class<S> serviceType);
}