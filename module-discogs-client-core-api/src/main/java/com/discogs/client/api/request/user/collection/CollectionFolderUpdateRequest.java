package com.discogs.client.api.request.user.collection;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.PayloadRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class CollectionFolderUpdateRequest extends AbstractRequest implements PayloadRequest<String> {

    private final long folderId;
    private final String username;
    private final String name;

    @Builder
    private CollectionFolderUpdateRequest(@Nullable AcceptMediaType mediaType,
                                          @Nonnull String contentType,
                                          @Nullable String callback,
                                          @Nonnull Authentication authentication,
                                          long folderId,
                                          @Nonnull String username,
                                          @Nonnull String name) {
        super(mediaType, contentType, callback, requireNonNull(authentication));
        this.folderId = requireValidIdentifier(folderId, true);
        this.username = requireNonNull(username);
        this.name = requireNonNull(name);
    }

    @Nonnull
    @Override
    public String getPath() { return "/users/{username}/collection/folders/{folderId}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getUsername(), getFolderId() }; }

    @Nonnull
    @Override
    public String getBody() {
        return new JSONObject()
                .put("name", name)
                .toString();
    }
}