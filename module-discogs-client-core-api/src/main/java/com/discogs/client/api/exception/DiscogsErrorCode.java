package com.discogs.client.api.exception;

import javax.annotation.Nonnull;

/**
 * Discogs error code interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface DiscogsErrorCode {

    /**
     * Gets error code.
     *
     * @return error code (never null)
     */
    @Nonnull
    String getCode();
}