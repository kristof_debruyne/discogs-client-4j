package com.discogs.client.api.request.common;

import lombok.Builder;
import lombok.Getter;

import static com.discogs.client.api.common.DiscogsConstants.FIRST_PAGE_NUMBER;
import static com.discogs.client.api.common.DiscogsConstants.MAXIMUM_PER_PAGE;
import static com.discogs.client.api.common.DiscogsConstants.MINIMUM_PER_PAGE;

@Getter
public class PaginationRequest {

    private final int page;
    private final int perPage;

    /**
     * Constructor
     *
     * @param page page (optional)
     * @param perPage page size (optional)
     */
    @Builder
    private PaginationRequest(int page, int perPage) {
        this.page = checkPage(page);
        this.perPage = checkPerPage(perPage);
    }

    /**
     * Validates page number.
     *
     * @param page page (required)
     * @return page or default (= 1)
     */
    private int checkPage(int page) {
        return page > 0 ? page: FIRST_PAGE_NUMBER;
    }

    /**
     * Validates per page number.
     *
     * @param perPage page size (required)
     * @return per page or default (= 10)
     */
    private int checkPerPage(int perPage) {
        return (perPage >= MINIMUM_PER_PAGE && perPage <= MAXIMUM_PER_PAGE) ? perPage : MINIMUM_PER_PAGE;
    }
}