package com.discogs.client.api.core.user.collection;

/**
 * Discogs collections interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface CollectionFolders {

    CollectionFolder[] getFolders();
}