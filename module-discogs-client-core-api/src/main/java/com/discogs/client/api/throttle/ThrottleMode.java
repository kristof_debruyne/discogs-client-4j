package com.discogs.client.api.throttle;

/**
 * Supported throttle modes.
 *
 * @author Sikke303
 * @since 1.0
 * @see ThrottleHandler
 */
public enum ThrottleMode {

    /**
     * Throttle mode respecting the Discogs rate limit specification (recommended).
     * No rounding will be applied.
     *
     * E.g: 25 requests per minute = 2400 ms throttling per request
     */
    NORMAL,
    /**
     * Throttle mode respecting the Discogs rate limit specification but with rounding (ceiling).
     *
     * E.g: 25 requests per minute = 3000 ms throttling per request
     */
    NORMAL_CEILING,
    /**
     * Throttle mode respecting the Discogs rate limit specification but with rounding (floor).
     *
     * E.g: 25 requests per minute = 2000 ms throttling per request
     */
    NORMAL_FLOOR,
    /**
     * Trampoline throttling. Requests will go faster until a certain point, then slower like a trampoline.
     */
    TRAMPOLINE
}
