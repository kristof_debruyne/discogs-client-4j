package com.discogs.client.api.core.common.paging;

/**
 * Discogs pagination aware interface (for pageable results).
 *
 * @author Sikke303
 * @since 1.0
 */
public interface PaginationAware {

    Pagination getPagination();
}