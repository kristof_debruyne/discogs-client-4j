package com.discogs.client.api.throttle;

import com.discogs.client.api.common.DiscogsConstants;
import com.discogs.client.api.response.RateLimits;

import javax.annotation.Nullable;
import java.io.Serializable;

/**
 * Discogs throttle context interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface ThrottleContext extends Serializable {

    /**
     * Gets authenticated requests per time window count.
     *
     * @return authenticated request count
     */
    default int getAuthenticatedRequestsPerTimeWindow() {
        return DiscogsConstants.AUTHENTICATED_REQUESTS_PER_MINUTE;
    }

    /**
     * Gets unauthenticated requests per time window count.
     *
     * @return unauthenticated request count
     */
    default int getUnauthenticatedRequestsPerTimeWindow() {
        return DiscogsConstants.UNAUTHENTICATED_REQUESTS_PER_MINUTE;
    }

    /**
     * Gets request time window in millis.
     *
     * @return request time window (in millis)
     */
    default int getRequestTimeWindowInMillis() {
        return DiscogsConstants.REQUEST_TIME_WINDOW_IN_MILLIS;
    }

    /**
     * Gets rate limits (if any).
     *
     * @return rate limits or null
     */
    @Nullable
    RateLimits getRateLimits();

    /**
     * Checks if there are rate limits present or not.
     *
     * @return boolean rate limits present (true) or not (false)
     */
    boolean hasRateLimits();
}
