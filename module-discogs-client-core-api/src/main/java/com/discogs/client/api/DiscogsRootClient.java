package com.discogs.client.api;

import com.discogs.client.api.annotations.DiscogsClientInterface;
import com.discogs.client.api.annotations.DiscogsEndpoint;
import com.discogs.client.api.core.ApiInformation;
import com.discogs.client.api.request.root.ApiInformationRequest;
import com.discogs.client.api.response.Response;
import com.discogs.client.auth.api.AuthenticationMode;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Discogs root client interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see DiscogsClient
 */
@DiscogsClientInterface(
        domain = "root",
        description = "Client for retrieving API related information.",
        readOnly = true,
        throttling = true,
        links = {
            "https://www.discogs.com",
            "https://www.discogs.com/developers",
            "https://www.facebook.com/discogs",
            "https://www.instagram.com/discogs",
            "https://twitter.com/discogs"
        }
)
@ParametersAreNonnullByDefault
public interface DiscogsRootClient extends DiscogsClient {

    /**
     * Get API information. <br><br>
     *
     * - description <br>
     * - documentation url <br>
     * - statistics <br>
     * - version <br>
     *
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.NOT_REQUIRED,
            description = "Get API information and statistics.",
            link = "https://api.discogs.com",
            cacheable = true
    )
    @Nonnull
    Response<? extends ApiInformation> getApiInformation();

    /**
     * Get API information by request. <br><br>
     *
     * - description <br>
     * - documentation url <br>
     * - statistics <br>
     * - version <br>
     *
     * @param request request (required)
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.NOT_REQUIRED,
            description = "Get API information and statistics.",
            link = "https://api.discogs.com",
            cacheable = true
    )
    @Nonnull
    Response<? extends ApiInformation> getApiInformation(ApiInformationRequest request);

    /**
     * Get API version.
     *
     * @return response (never null)
     */
    @DiscogsEndpoint(
            authenticationMode = AuthenticationMode.NOT_REQUIRED,
            description = "Get API version.",
            link = "https://api.discogs.com",
            cacheable = true
    )
    @Nonnull
    Response<String> getApiVersion();

    @Nonnull
    String createArtistApiUrl(@Nonnegative long artistId);

    @Nonnull
    String createLabelApiUrl(@Nonnegative long labelId);

    @Nonnull
    String createMasterApiUrl(@Nonnegative long masterId);

    @Nonnull
    String createReleaseApiUrl(@Nonnegative long releaseId);
}