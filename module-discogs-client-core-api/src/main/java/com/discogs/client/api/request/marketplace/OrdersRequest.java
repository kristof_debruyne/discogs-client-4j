package com.discogs.client.api.request.marketplace;

import com.discogs.client.api.core.common.sorting.Sort;
import com.discogs.client.api.core.common.sorting.SortOrder;
import com.discogs.client.api.core.marketplace.OrderStatus;
import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.common.PaginationRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.requireNonNull;

@Getter
public class OrdersRequest extends AbstractRequest {

    private final boolean archived;
    private final OrderStatus status;

    @Builder
    private OrdersRequest(@Nullable AcceptMediaType mediaType,
                          @Nonnull String contentType,
                          @Nullable String callback,
                          @Nonnull Authentication authentication,
                          @Nullable PaginationRequest pagination,
                          @Nullable SortMode sort,
                          @Nullable SortOrder sortOrder,
                          boolean archived,
                          @Nullable OrderStatus status) {
        super(mediaType, contentType, callback, requireNonNull(authentication), pagination, sort, sortOrder);
        this.archived = archived;
        this.status = status;
    }

    @Nonnull
    @Override
    public String getPath() { return "/marketplace/orders"; }

    @Nonnull
    @Override
    public Map<String, Object> getQueryParams() {
        final Map<String, Object> params = new HashMap<>(super.getQueryParams());
        params.put("archived", archived);
        if(status != null) {
            params.put("status", status.getValue());
        }
        return unmodifiableMap(params);
    }

    /**
     * Supported sort modes.
     *
     * @author Sikke303
     * @since 1.0
     * @see Sort
     * @see SortOrder
     */
    @RequiredArgsConstructor
    public enum SortMode implements Sort {

        BUYER("buyer"),
        CREATED("created"),
        ID("id"),
        LAST_ACTIVITY("last_activity"),
        STATUS("status");

        @Getter
        private final String value;
    }
}