package com.discogs.client.api.response;

import lombok.Getter;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * Tracked response. <br><br>
 *
 * This class extends from {@link GenericResponse}. <br><br>
 *
 * This response will include additionally: <br>
 * - rate limit <br>
 * - rate limit used <br>
 * - rate limit remaining <br>
 *
 * @author Sikke303
 * @since 1.0
 * @param <T> body type
 * @see GenericResponse
 * @see RateLimit
 */
@Getter
public class TrackedResponse<T> extends GenericResponse<T> {

    private final RateLimits rateLimits;

    /**
     * Constructor
     *
     * @param statusCode status code (required)
     * @param headers headers (required)
     * @param body body (optional)
     * @param rateLimits rate limits
     */
    public TrackedResponse(@Nonnegative int statusCode,
                           @Nonnull Map<String, String> headers,
                           @Nullable T body,
                           @Nonnull RateLimits rateLimits) {
        super(statusCode, headers, body);
        this.rateLimits = requireNonNull(rateLimits);
    }
}