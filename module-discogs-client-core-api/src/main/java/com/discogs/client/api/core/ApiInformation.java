package com.discogs.client.api.core;

/**
 * Discogs API information interface. <br><br>
 *
 * Resource: <a href="https://api.discogs.com">API</a>
 *
 * @author Sikke303
 * @since 1.0
 * @see ApiStatistics
 */
public interface ApiInformation {

    String getDescription();

    String getDocumentationUrl();

    ApiStatistics getStatistics();

    String getVersion();

    /**
     * Discogs API statistics interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface ApiStatistics {

        long getArtistCount();

        long getLabelCount();

        long getReleaseCount();
    }
}