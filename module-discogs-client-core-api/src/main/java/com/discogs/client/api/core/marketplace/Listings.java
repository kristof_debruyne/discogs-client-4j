package com.discogs.client.api.core.marketplace;

import com.discogs.client.api.core.common.paging.PaginationAware;

/**
 * Discogs listings interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Listings extends PaginationAware {

    Listing[] getListings();
}