package com.discogs.client.api.config;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Discogs user agent generator interface.
 *
 * For Discogs, you are required to send a header 'User-Agent' on each request.
 *
 * @author Sikke303
 * @since 1.0
 */
@ParametersAreNonnullByDefault
public interface UserAgentGenerator {

    /**
     * Generates user agent header string.
     *
     * @return user agent (never null)
     */
    @Nonnull
    String generate();
}