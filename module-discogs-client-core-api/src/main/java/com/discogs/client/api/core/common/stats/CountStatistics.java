package com.discogs.client.api.core.common.stats;

/**
 * Discogs count statistic interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see ContainStatistics
 */
public interface CountStatistics {

    int getInCollectionCount();

    int getInWantListCount();
}