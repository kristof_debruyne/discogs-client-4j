package com.discogs.client.api;

import com.discogs.client.api.annotations.DiscogsClientInterface;
import com.discogs.client.api.throttle.ThrottleHandler;

import javax.annotation.Nonnull;
import java.net.URI;

/**
 * Discogs client interface.
 *
 * @author Sikke303
 * @since 1.0
 * @see ThrottleHandler
 */
@DiscogsClientInterface(
        domain = "all",
        description = "Generic Discogs client (parent)",
        readOnly = true,
        throttling = true,
        links = { "https://www.discogs.com/developers" }
)
public interface DiscogsClient {

    /**
     * Gets base URI.
     *
     * @return base uri (never null)
     */
    @Nonnull
    URI getBaseUri();
}