package com.discogs.client.api.exception;

import lombok.Getter;
import lombok.NonNull;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Default Discogs runtime exception (system).
 *
 * @author Sikke303
 * @since 1.0
 * @see RuntimeException
 */
@ParametersAreNonnullByDefault
public class DiscogsRuntimeException extends RuntimeException {

    @Getter
    private final DiscogsErrorCode errorCode;

    public DiscogsRuntimeException(@NonNull final String message,
                                   @NonNull final DiscogsErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public DiscogsRuntimeException(@NonNull final String message,
                                   @NonNull final DiscogsErrorCode errorCode,
                                   @NonNull final Throwable exception) {
        super(message, exception);
        this.errorCode = errorCode;
    }
}