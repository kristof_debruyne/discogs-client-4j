package com.discogs.client.api.core.inventory;

/**
 * Discogs inventory export interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Export {

    long getId();

    String getDateCreated();

    String getDateFinished();

    String getDownloadUrl();

    String getFilename();

    String getUrl();

    String getStatus();
}