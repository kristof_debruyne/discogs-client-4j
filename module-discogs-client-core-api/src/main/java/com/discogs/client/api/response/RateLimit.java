package com.discogs.client.api.response;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Supported rate limits. <br><br>
 *
 * Resource: https://www.discogs.com/developers#page:home,header:home-rate-limiting
 *
 * @author Sikke303
 * @since 1.0
 */
@RequiredArgsConstructor
public enum RateLimit {

    RATE_LIMIT("X-Discogs-Ratelimit"),
    RATE_LIMIT_USED("X-Discogs-Ratelimit-Used"),
    RATE_LIMIT_REMAINING("X-Discogs-Ratelimit-Remaining");

    @Getter
    private final String headerName;

    /**
     * Gets all Discogs rate limit header names.
     *
     * @return set of header names (never null)
     */
    @Nonnull
    public static Set<String> getAllRateLimitHeaderNames() {
        return Arrays.stream(values()).map(RateLimit::getHeaderName).collect(Collectors.toSet());
    }

    /**
     * Checks if all rate limit headers (3) are present or not.
     *
     * @param headers headers (required)
     * @return boolean contains rate limit headers (true) or not (false)
     */
    public static boolean containsRateLimitHeaders(@NonNull final Map<String, String> headers) {
        return RateLimit.getAllRateLimitHeaderNames().stream().allMatch(headers::containsKey);
    }
}