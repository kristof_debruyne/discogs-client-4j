package com.discogs.client.api.core.marketplace;

import com.discogs.client.api.core.common.Currency;

/**
 * Discogs fee interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Fee {

    Currency getCurrency();

    double getValue();
}