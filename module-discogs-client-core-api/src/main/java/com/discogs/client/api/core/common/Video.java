package com.discogs.client.api.core.common;

/**
 * Discogs video interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Video {

    boolean isEmbedded();

    int getDuration();

    String getDescription();

    String getTitle();

    String getUri();
}