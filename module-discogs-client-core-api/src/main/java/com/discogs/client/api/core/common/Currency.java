package com.discogs.client.api.core.common;

/**
 * Supported currencies.
 * 
 * @author Sikke303
 * @since 1.0
 */
public enum Currency {

	AUD,
    BRL,
    CAD,
    CHF,
    EUR,
    GBP,
    MXN,
    NZD,
    JPY,
    SEK,
    USD,
    ZAR
}