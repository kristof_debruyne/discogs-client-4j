package com.discogs.client.api.config;

import com.discogs.client.api.DiscogsClient;
import com.discogs.client.api.DiscogsService;
import lombok.NonNull;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Discogs locator interface. <br><br>
 *
 * - can locate beans of type DiscogsClient <br>
 * - can locate beans of type DiscogsService <br>
 *
 * @author Sikke303
 * @since 1.0
 */
public interface DiscogsLocator {

    /**
     * Lookup client by class type.
     *
     * @param clazz class type (required)
     * @param <T> client type
     * @return client instance (never null)
     */
    @Nonnull
    <T extends DiscogsClient> Optional<T> lookupClient(@NonNull final Class<T> clazz);

    /**
     * Lookup service by class type.
     *
     * @param clazz class type (required)
     * @param <T> service type
     * @return service instance (never null)
     */
    @Nonnull
    <T extends DiscogsService> Optional<T> lookupService(@NonNull final Class<T> clazz);
}