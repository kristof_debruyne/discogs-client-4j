package com.discogs.client.api.request.image;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class ImageRequest extends AbstractRequest {

    private final String url;

    @Builder
    private ImageRequest(@Nonnull String contentType,
                         @Nullable String callback,
                         @Nullable Authentication authentication,
                         @Nonnull String url) {
        super(AcceptMediaType.DEFAULT, contentType, callback, authentication);
        this.url = requireNonNull(url);
    }

    @Nonnull
    @Override
    public String getPath() {
        throw new UnsupportedOperationException("Resolving path not supported for image requests !");
    }
}