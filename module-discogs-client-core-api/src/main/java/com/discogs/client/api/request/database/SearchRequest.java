package com.discogs.client.api.request.database;

import com.discogs.client.api.core.database.SearchType;
import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.common.PaginationRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.requireNonNull;

@Getter
public class SearchRequest extends AbstractRequest {

    private final String query;
    private final String anv;
    private final String artist;
    private final String barcode;
    private final String catalogNumber;
    private final String contributor;
    private final String country;
    private final String credit;
    private final String format;
    private final String genre;
    private final String label;
    private final String releaseTitle;
    private final String submitter;
    private final String style;
    private final String title;
    private final String track;
    private final SearchType type;
    private final String year;

    @Builder
    private SearchRequest(@Nullable AcceptMediaType mediaType,
                          @Nonnull String contentType,
                          @Nullable String callback,
                          @Nonnull Authentication authentication,
                          @Nullable PaginationRequest pagination,
                          @Nonnull String query,
                          @Nullable String anv,
                          @Nullable String artist,
                          @Nullable String barcode,
                          @Nullable String catalogNumber,
                          @Nullable String contributor,
                          @Nullable String country,
                          @Nullable String credit,
                          @Nullable String format,
                          @Nullable String genre,
                          @Nullable String label,
                          @Nullable String releaseTitle,
                          @Nullable String submitter,
                          @Nullable String style,
                          @Nullable String title,
                          @Nullable String track,
                          @Nullable SearchType type,
                          @Nullable String year) {
        super(mediaType, contentType, callback, requireNonNull(authentication), pagination, null, null);
        this.query = requireNonNull(query);
        this.anv = anv;
        this.artist = artist;
        this.barcode = barcode;
        this.catalogNumber = catalogNumber;
        this.contributor = contributor;
        this.country = country;
        this.credit = credit;
        this.format = format;
        this.genre = genre;
        this.label = label;
        this.releaseTitle = releaseTitle;
        this.submitter = submitter;
        this.style = style;
        this.title = title;
        this.track = track;
        this.type = type;
        this.year = year;
    }

    @Nonnull
    @Override
    public String getPath() { return "/database/search"; }

    @Nonnull
    @Override
    public Map<String, Object> getQueryParams() {
        Map<String, Object> params = new HashMap<>(super.getQueryParams());
        params.put("q", query);
        params.computeIfAbsent("anv", key -> anv);
        params.computeIfAbsent("artist", key -> artist);
        params.computeIfAbsent("barcode", key -> barcode);
        params.computeIfAbsent("catno", key -> catalogNumber);
        params.computeIfAbsent("contributor", key -> contributor);
        params.computeIfAbsent("country", key -> country);
        params.computeIfAbsent("credit", key -> credit);
        params.computeIfAbsent("format", key -> format);
        params.computeIfAbsent("genre", key -> genre);
        params.computeIfAbsent("label", key -> label);
        params.computeIfAbsent("release_title", key -> releaseTitle);
        params.computeIfAbsent("style", key -> style);
        params.computeIfAbsent("submitter", key -> submitter);
        params.computeIfAbsent("title", key -> title);
        params.computeIfAbsent("track", key -> track);
        if(type != null) {
            params.putIfAbsent("type", type.getValue());
        }
        params.computeIfAbsent("year", key -> year);
        return unmodifiableMap(params);
    }
}