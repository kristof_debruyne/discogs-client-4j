package com.discogs.client.api.request.user.collection;

import com.discogs.client.api.core.common.sorting.Sort;
import com.discogs.client.api.core.common.sorting.SortOrder;
import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.common.PaginationRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class CollectionItemsByFolderRequest extends AbstractRequest {

    private final long folderId;
    private final String username;

    @Builder
    private CollectionItemsByFolderRequest(@Nullable AcceptMediaType mediaType,
                                           @Nonnull String contentType,
                                           @Nullable String callback,
                                           @Nullable Authentication authentication,
                                           @Nullable PaginationRequest pagination,
                                           @Nullable SortMode sort,
                                           @Nullable SortOrder sortOrder,
                                           long folderId,
                                           @Nonnull String username) {
        super(mediaType, contentType, callback, authentication, pagination, sort, sortOrder);
        this.folderId = requireValidIdentifier(folderId, true);
        this.username = requireNonNull(username);
    }

    @Nonnull
    @Override
    public String getPath() { return "/users/{username}/collection/folders/{folderId}/releases"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getUsername(), getFolderId() }; }

    /**
     * Supported sort modes.
     *
     * @author Sikke303
     * @since 1.0
     * @see Sort
     * @see SortOrder
     */
    @RequiredArgsConstructor
    public enum SortMode implements Sort {

        ADDED("added"),
        ARTIST("artist"),
        CATALOG_NUMBER("catno"),
        FORMAT("format"),
        LABEL("label"),
        RATING("rating"),
        TITLE("title"),
        YEAR("year");

        @Getter
        private final String value;
    }
}