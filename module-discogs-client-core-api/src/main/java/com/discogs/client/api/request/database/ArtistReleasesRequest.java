package com.discogs.client.api.request.database;

import com.discogs.client.api.core.common.sorting.Sort;
import com.discogs.client.api.core.common.sorting.SortOrder;
import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.common.PaginationRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Getter
public class ArtistReleasesRequest extends AbstractRequest {

    private final long artistId;

    @Builder
    private ArtistReleasesRequest(@Nullable AcceptMediaType mediaType,
                                  @Nonnull String contentType,
                                  @Nullable String callback,
                                  @Nullable Authentication authentication,
                                  @Nullable PaginationRequest pagination,
                                  @Nullable SortMode sort,
                                  @Nullable SortOrder sortOrder,
                                  long artistId) {
        super(mediaType, contentType, callback, authentication, pagination, sort, sortOrder);
        this.artistId = requirePositiveIdentifier(artistId);
    }

    @Nonnull
    @Override
    public String getPath() { return "/artists/{artistId}/releases"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getArtistId() }; }

    /**
     * Supported sort modes.
     *
     * @author Sikke303
     * @since 1.0
     * @see Sort
     * @see SortOrder
     */
    @RequiredArgsConstructor
    public enum SortMode implements Sort {

        FORMAT("format"),
        TITLE("title"),
        YEAR("year");

        @Getter
        private final String value;
    }
}