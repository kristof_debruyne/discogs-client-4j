package com.discogs.client.api.request.marketplace;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class OrderRequest extends AbstractRequest {

    private final long orderId;

    @Builder
    private OrderRequest(@Nullable AcceptMediaType mediaType,
                         @Nonnull String contentType,
                         @Nullable String callback,
                         @Nonnull Authentication authentication,
                         long orderId) {
        super(mediaType, contentType, callback, requireNonNull(authentication));
        this.orderId = requirePositiveIdentifier(orderId);
    }

    @Nonnull
    @Override
    public String getPath() { return "/marketplace/orders/{orderId}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getOrderId() }; }
}