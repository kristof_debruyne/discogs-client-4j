package com.discogs.client.api.request.root;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import lombok.Builder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class ApiInformationRequest extends AbstractRequest {

    @Builder
    private ApiInformationRequest(@Nullable AcceptMediaType mediaType,
                                  @Nonnull String contentType,
                                  @Nullable String callback) {
        super(mediaType, contentType,  callback);
    }

    @Nonnull
    @Override
    public String getPath() { return "/"; }
}