package com.discogs.client.api.request.database;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Getter
public class ReleaseCommunityRatingRequest extends AbstractRequest {

    private final long releaseId;

    @Builder
    private ReleaseCommunityRatingRequest(@Nullable AcceptMediaType mediaType,
                                          @Nonnull String contentType,
                                          @Nullable String callback,
                                          long releaseId) {
        super(mediaType, contentType, callback);
        this.releaseId = requirePositiveIdentifier(releaseId);
    }

    @Nonnull
    @Override
    public String getPath() { return "/releases/{releaseId}/rating"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getReleaseId() }; }
}