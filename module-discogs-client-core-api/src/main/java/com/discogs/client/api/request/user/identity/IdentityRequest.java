package com.discogs.client.api.request.user.identity;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class IdentityRequest extends AbstractRequest {

    @Builder
    private IdentityRequest(@Nullable AcceptMediaType mediaType,
                            @Nonnull String contentType,
                            @Nullable String callback,
                            @Nonnull Authentication authentication) {
        super(mediaType, contentType, callback, requireNonNull(authentication));
    }

    @Nonnull
    @Override
    public String getPath() { return "/oauth/identity"; }
}