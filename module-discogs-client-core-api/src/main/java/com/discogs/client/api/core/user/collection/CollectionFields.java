package com.discogs.client.api.core.user.collection;

/**
 * Discogs collection custom fields interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface CollectionFields {

    CollectionField[] getFields();

    /**
     * Discogs collection custom field interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface CollectionField {

        long getId();

        boolean isPublicField();

        int getPosition();

        int getLines();

        String getName();

        String[] getOptions();

        String getType();
    }
}