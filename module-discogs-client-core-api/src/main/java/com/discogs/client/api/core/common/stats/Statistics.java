package com.discogs.client.api.core.common.stats;

/**
 * Discogs statistics interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Statistics {

    CountStatistics getCommunityStatistics();

    CountStatistics getUserStatistics();
}