package com.discogs.client.api.core.marketplace;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Supported order statuses.
 *
 * @author Sikke303
 * @since 1.0
 */
@RequiredArgsConstructor
public enum OrderStatus {

    ALL("All"),
    NEW_ORDER("New Order"),
    BUYER_CONTACTED("Buyer Contacted"),
    INVOICE_SENT("Invoice Sent"),
    PAYMENT_PENDING("Payment Pending"),
    PAYMENT_RECEIVED("Payment Received"),
    SHIPPED("Shipped"),
    MERGED("Merged"),
    ORDER_CHANGED("Order Changed"),
    REFUND_SENT("Refund Sent"),
    CANCELLED("Cancelled"),
    CANCELLED_NON_PAYING_BUYER("Cancelled (Non-Paying Buyer)"),
    CANCELLED_ITEM_UNAVAILABLE("Cancelled (Item Unavailable)"),
    CANCELLED_PER_BUYER_REQUESTS("Cancelled (Per Buyer's Request)"),
    CANCELLED_REFUND_RECEIVED("Cancelled (Refund Received)");

    @Getter
    private final String value;
}
