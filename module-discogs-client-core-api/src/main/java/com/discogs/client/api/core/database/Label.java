package com.discogs.client.api.core.database;

import com.discogs.client.api.core.image.Image;

/**
 * Discogs label interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Label {

    long getId();

    String getCatalogNumber();

    String getContactInfo();

    String getDataQuality();

    Image[] getImages();

    String getName();

    String getProfile();

    String getReleasesUrl();

    String getResourceUrl();

    SubLabel[] getSubLabels();

    String getUri();

    String[] getUrls();

    /**
     * Discogs sub label interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface SubLabel {

        long getId();

        String getName();

        String getResourceUrl();
    }
}