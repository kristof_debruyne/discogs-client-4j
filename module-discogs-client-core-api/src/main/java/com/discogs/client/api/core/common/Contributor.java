package com.discogs.client.api.core.common;

/**
 * Discogs contributor interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Contributor {

    String getResourceUrl();

    String getUserName();
}