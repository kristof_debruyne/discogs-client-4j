package com.discogs.client.api.core.marketplace;

import com.discogs.client.api.core.common.paging.PaginationAware;

/**
 * Discogs order messages interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface OrderMessages extends PaginationAware {

    OrderMessage[] getMessages();

    /**
     * Discogs order message interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface OrderMessage {

        Actor getActor();

        String getDateCreated();

        From getFrom();

        int getOriginalCount();

        int getNewCount();

        String getMessage();

        int getStatusId();

        String getSubject();

        String getType();
    }

    /**
     * Discogs actor interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface Actor {

        String getResourceUrl();

        String getUsername();
    }

    /**
     * Discogs from interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface From {

        long getId();

        String getAvatarUrl();

        String getResourceUrl();

        String getUsername();
    }
}