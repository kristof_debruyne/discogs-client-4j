package com.discogs.client.api.core.common.rating;

/**
 * Discogs rating interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Rating {

    double getAverage();

    int getCount();
}