package com.discogs.client.api.request.user.identity;

import com.discogs.client.api.core.common.Currency;
import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.api.request.PayloadRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class ProfileUpdateRequest extends AbstractRequest implements PayloadRequest<String> {

    private final String username;
    private final String name;
    private final String homePage;
    private final String location;
    private final String profile;
    private final Currency currency;

    @Builder
    private ProfileUpdateRequest(@Nullable AcceptMediaType mediaType,
                                 @Nonnull String contentType,
                                 @Nullable String callback,
                                 @Nonnull Authentication authentication,
                                 @Nonnull String username,
                                 @Nullable String name,
                                 @Nullable String homePage,
                                 @Nullable String location,
                                 @Nullable String profile,
                                 @Nullable Currency currency) {
        super(mediaType, contentType, callback, requireNonNull(authentication));
        this.username = requireNonNull(username);
        this.name = name;
        this.homePage = homePage;
        this.location = location;
        this.profile = profile;
        this.currency = currency;
    }

    @Nonnull
    @Override
    public String getPath() { return "/users/{username}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getUsername() }; }

    @Nonnull
    @Override
    public String getBody() {
        return new JSONObject()
                .put("name", name)
                .put("home_page", homePage)
                .put("location", location)
                .put("profile", profile)
                .put("curr_abbr", currency)
                .toString();
    }
}