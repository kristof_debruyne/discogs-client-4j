package com.discogs.client.api.core.marketplace;

/**
 * Discogs buyer interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Buyer {

    long getId();

    String getResourceUrl();

    String getUsername();
}