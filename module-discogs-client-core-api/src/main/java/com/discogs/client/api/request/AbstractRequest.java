package com.discogs.client.api.request;

import com.discogs.client.api.core.common.sorting.Sort;
import com.discogs.client.api.core.common.sorting.SortOrder;
import com.discogs.client.api.request.common.PaginationRequest;
import com.discogs.client.auth.api.Authentication;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.OverridingMethodsMustInvokeSuper;
import java.util.HashMap;
import java.util.Map;

import static com.discogs.client.api.common.DiscogsConstants.QUERY_PARAM_CALLBACK;
import static com.discogs.client.api.common.DiscogsConstants.QUERY_PARAM_PAGE;
import static com.discogs.client.api.common.DiscogsConstants.QUERY_PARAM_PER_PAGE;
import static com.discogs.client.api.common.DiscogsConstants.QUERY_PARAM_SORT;
import static com.discogs.client.api.common.DiscogsConstants.QUERY_PARAM_SORT_ORDER;
import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.nonNull;

/**
 * Abstract request. <br><br>
 *
 * Contains following properties: <br>
 * - media type (optional) <br>
 * - content type (required) <br>
 * - callback name (optional) - JSONP <br>
 * - authentication token (optional) <br>
 * - pagination (optional) <br>
 * - sort (optional) <br>
 * - sort order (optional) <br>
 *
 * @author Sikke303
 * @since 1.0
 * @see Request
 */
@Getter
public abstract class AbstractRequest implements Request {

    private final @Nonnull AcceptMediaType mediaType;
    private final @Nonnull String contentType;
    private final @Nullable String callback;
    private final @Nonnull Authentication authentication;
    private final @Nullable PaginationRequest pagination;
    private final @Nullable Sort sort;
    private final @Nullable SortOrder sortOrder;

    protected AbstractRequest(@Nullable AcceptMediaType mediaType,
                              @Nonnull String contentType,
                              @Nullable String callback) {
        this(mediaType, contentType, callback, new Authentication.Anonymous());
    }

    protected AbstractRequest(@Nullable AcceptMediaType mediaType,
                              @Nonnull String contentType,
                              @Nullable String callback,
                              @Nullable Authentication authentication) {
        this(mediaType, contentType, callback, authentication, null, null, null);
    }

    protected AbstractRequest(@Nullable AcceptMediaType mediaType,
                              @Nonnull String contentType,
                              @Nullable String callback,
                              @Nullable PaginationRequest pagination,
                              @Nullable Sort sort,
                              @Nullable SortOrder sortOrder) {
        this(mediaType, contentType, callback, new Authentication.Anonymous(), pagination, sort, sortOrder);
    }

    /**
     * Main constructor (all other constructor overload to this constructor)
     *
     * @param mediaType media type (optional)
     * @param contentType content type (required)
     * @param callback callback name (optional)
     * @param authentication token (optional)
     * @param pagination pagination (optional)
     * @param sort sort (optional)
     * @param sortOrder sort order (optional)
     */
    protected AbstractRequest(@Nullable AcceptMediaType mediaType,
                              @Nonnull String contentType,
                              @Nullable String callback,
                              @Nullable Authentication authentication,
                              @Nullable PaginationRequest pagination,
                              @Nullable Sort sort,
                              @Nullable SortOrder sortOrder) {
        this.mediaType = verifyMediaType(mediaType);
        this.contentType = contentType;
        this.callback = callback;
        this.authentication = verifyAuthentication(authentication);
        this.pagination = pagination;
        this.sort = sort;
        this.sortOrder = sortOrder;
    }

    public final boolean isJsonpRequest() { return StringUtils.isNotBlank(getCallback()); }

    /**
     * Verifies media type. <br>
     * If media type is not set, then default will be applied.
     *
     * @param mediaType content type (optional)
     * @return content type (never null)
     */
    @Nonnull
    private AcceptMediaType verifyMediaType(@Nullable final AcceptMediaType mediaType) {
        return mediaType != null ? mediaType : AcceptMediaType.DEFAULT;
    }

    /**
     * Verifies authentication. <br>
     * If token is not set, then default will be applied.
     *
     * @param authentication authentication (optional)
     * @return token (never null)
     */
    @Nonnull
    private Authentication verifyAuthentication(final Authentication authentication) {
        return authentication != null ? authentication : new Authentication.Anonymous();
    }

    /**
     * Checks if request has pagination or not.
     *
     * @return pagination (true) or not (false)
     */
    public final boolean hasPagination() { return pagination != null; }

    /**
     * Checks if request has sorting or not.
     *
     * @return sorting (true) or not (false)
     */
    public final boolean hasSorting() { return sort != null; }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @OverridingMethodsMustInvokeSuper
    @Override
    public Map<String, Object> getQueryParams() {
        Map<String, Object> params = new HashMap<>();
        if(StringUtils.isNotBlank(getCallback())) {
            params.put(QUERY_PARAM_CALLBACK, callback);
        }
        if(hasPagination()) {
            params.computeIfAbsent(QUERY_PARAM_PAGE, key -> pagination.getPage());
            params.computeIfAbsent(QUERY_PARAM_PER_PAGE, key -> pagination.getPerPage());
        }
        if(hasSorting()) {
            params.computeIfAbsent(QUERY_PARAM_SORT, key -> sort.getValue());
            if(nonNull(sortOrder)) {
                params.computeIfAbsent(QUERY_PARAM_SORT_ORDER, key -> sortOrder.getValue());
            }
        }
        return unmodifiableMap(params);
    }

    //*** VALIDATION ***//

    /**
     * Validates identifier.
     *
     * @param id identifier (required)
     * @param allowZero allow zero (required)
     * @return identifier
     * @throws IllegalArgumentException if identifier is not valid
     */
    protected final long requireValidIdentifier(final long id, final boolean allowZero) {
        if(allowZero && id < 0) {
            throw new IllegalArgumentException("ID must be greater or equal than zero !");
        } else if(!allowZero && id < 1) {
            throw new IllegalArgumentException("ID must be greater than zero !");
        }
        return id;
    }

    /**
     * Validates identifier.
     *
     * @param id identifier (required)
     * @return identifier
     * @throws IllegalArgumentException if identifier is not valid
     */
    protected final long requirePositiveIdentifier(final long id) {
        return requireValidIdentifier(id, false);
    }

    /**
     * Validates price.
     *
     * @param value value (required)
     * @return value
     * @throws IllegalArgumentException if value is not valid
     */
    protected final double requireValidPrice(final double value) {
        if(value < 0) {
            throw new IllegalArgumentException("Price must be greater or equal than zero !");
        }
        return value;
    }
}