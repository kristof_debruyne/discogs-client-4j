package com.discogs.client.api.request.inventory;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

@Getter
public class UploadRequest extends AbstractRequest {

    private final long uploadId;

    @Builder
    private UploadRequest(@Nullable AcceptMediaType mediaType,
                          @Nonnull String contentType,
                          @Nullable String callback,
                          @Nonnull Authentication authentication,
                          long uploadId) {
        super(mediaType, contentType, callback, requireNonNull(authentication));
        this.uploadId = requirePositiveIdentifier(uploadId);
    }

    @Nonnull
    @Override
    public String getPath() { return "/inventory/upload/{uploadId}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getUploadId() }; }
}