package com.discogs.client.api.request.marketplace;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Getter
public class FeeRequest extends AbstractRequest {

    private final double price;

    @Builder
    private FeeRequest(@Nullable AcceptMediaType mediaType,
                       @Nonnull String contentType,
                       @Nullable String callback,
                       double price) {
        super(mediaType, contentType, callback);
        this.price = requireValidPrice(price);
    }

    @Nonnull
    @Override
    public String getPath() { return "/marketplace/fee/{price}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getPrice() }; }
}