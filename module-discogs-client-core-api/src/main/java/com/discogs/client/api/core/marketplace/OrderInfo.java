package com.discogs.client.api.core.marketplace;

/**
 * Discogs order info interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface OrderInfo {

    String getId();

    String getResourceUrl();
}