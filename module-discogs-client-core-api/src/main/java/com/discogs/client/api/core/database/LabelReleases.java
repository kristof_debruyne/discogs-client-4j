package com.discogs.client.api.core.database;

import com.discogs.client.api.core.common.paging.PaginationAware;

/**
 * Discogs label releases interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface LabelReleases extends PaginationAware {

    LabelRelease[] getReleases();

    /**
     * Discogs label release interface.
     *
     * @author Sikke303
     * @since 1.0
     */
    interface LabelRelease {

        long getId();

        String getArtistName();

        String getCatalogNumber();

        String getFormat();

        String getResourceUrl();

        String getStatus();

        String getTitle();

        String getThumbnail();

        int getYear();
    }
}