package com.discogs.client.api.core.user;


/**
 * Discogs user interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface User {

    long getId();

    String getAvatarUrl();

    String getResourceUrl();

    String getUsername();
}