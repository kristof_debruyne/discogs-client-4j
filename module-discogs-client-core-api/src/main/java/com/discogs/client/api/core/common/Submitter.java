package com.discogs.client.api.core.common;

/**
 * Discogs submitter interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface Submitter {

    String getResourceUrl();

    String getUserName();
}