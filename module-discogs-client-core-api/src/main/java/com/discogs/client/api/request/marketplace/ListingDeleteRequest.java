package com.discogs.client.api.request.marketplace;

import com.discogs.client.api.request.AbstractRequest;
import com.discogs.client.api.request.AcceptMediaType;
import com.discogs.client.auth.api.Authentication;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.requireNonNull;

@Getter
public class ListingDeleteRequest extends AbstractRequest {

    private final long listingId;

    @Builder
    private ListingDeleteRequest(@Nullable AcceptMediaType mediaType,
                                 @Nonnull String contentType,
                                 @Nullable String callback,
                                 @Nonnull Authentication authentication,
                                 long listingId) {
        super(mediaType, contentType, callback, requireNonNull(authentication));
        this.listingId = requirePositiveIdentifier(listingId);
    }

    @Nonnull
    @Override
    public String getPath() { return "/marketplace/listings/{listingId}"; }

    @Nonnull
    @Override
    public Object[] getPathVariables() { return new Object[] { getListingId() }; }

    @Nonnull
    @Override
    public Map<String, Object> getQueryParams() {
        final Map<String, Object> params = new HashMap<>(super.getQueryParams());
        params.put("listing_id", listingId);
        return unmodifiableMap(params);
    }
}