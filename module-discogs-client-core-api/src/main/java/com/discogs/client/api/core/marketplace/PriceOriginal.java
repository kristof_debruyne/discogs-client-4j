package com.discogs.client.api.core.marketplace;

/**
 * Discogs original price interface.
 *
 * @author Sikke303
 * @since 1.0
 */
public interface PriceOriginal extends Price {

    int getCurrencyId();

    String getFormattedValue();
}